#ifndef PROCESS
#define PROCESS

// Header Qt
#include <QCoreApplication>
#include <QMap>
#include <QSocketNotifier>
#include <QtCore>

#define _UNIX_

#include <PvSampleUtils.h>
#include <PvDevice.h>
#include <PvDeviceGEV.h>
#include <PvStream.h>
#include <PvStreamGEV.h>
#include <PvBuffer.h>

// DREX
#include <PvBufferWriter.h>

// Header OpenCV
#include <opencv2/videoio.hpp>

#include <list>
typedef std::list<PvBuffer *> BufferList;

// Classe del processo INP
class INCProcess : public QCoreApplication {
	Q_OBJECT
	
	using super = QCoreApplication;
	using Message = QList<QByteArray>;
	
	int _argc;
	char **_argv;
	
	// Parametri
	QString _camera_ip;
	QDir _trigger_dir;
	
	// Camera
	PvDevice *_device;
	PvStream *_stream;
	BufferList *_buffer_list;
	
	PvGenCommand *_cmd_start;
	PvGenCommand *_cmd_stop;
	
	PvGenFloat *_parameter_frame_rate;
	PvGenFloat *_parameter_bandwidth;
	
	PvBufferWriter *_buffer_writer;
	
	cv::VideoWriter _video_writer;
	
	// Worker
	QString _camera_name;
	bool _video_enable;
	
	bool _acquiring;
	bool _acquire_next;
	QString _acquire_destination;
	QDir _video_dir;
	QString _video_file;
	QDir _image_dir;
	QDir _temp_dir;
	QString _temp_image_file;
	
	int _trigger_index;
	int _image_index;
	int _video_index;
	
	QDateTime _video_start_time;
	uint64_t _max_video_time;
	
	// Costanti
	static constexpr uint32_t BUFFER_COUNT = 16;
	
	// Metodi ausiliari
	
	// Calcolo prossimo nome valido
	QString getName(QDir directory, QString before, QDateTime time, QString after);
	void saveVideo();
	void startVideo();
	bool getNextFreeSlot(QDir &dir, int &index);
	
	// Riduzione del framerate
	int _divider;
	
private:
	//*****************//
	// Sezione privata
	//*****************//
	
	// // Variabili
	
	// Variabili per i segnali di sistema
	QSocketNotifier *_sn_hup;
	QSocketNotifier *_sn_int;
	QSocketNotifier *_sn_term;
	int *_sig_hup_fd;
	int *_sig_int_fd;
	int *_sig_term_fd;
	
	// Metodi
	bool connectToDevice(const PvString &connection_id);
	bool openStream(const PvString &connection_id);
	void configureStream(PvDevice *device, PvStream *stream);
	void createStreamBuffers(PvDevice *device, PvStream *stream, BufferList *buffer_list);
	bool createPipeline(PvDevice *aDevice, PvStream *aStream, PvPipeline *lPipeline);
	void freeStreamBuffers(BufferList *buffer_list);
	
public slots:
	// Gestori dei segnali di sistema di Qt
	void handleSigHup();
	void handleSigInt();
	void handleSigTerm();
	
	// Acquisizione continua
	void acquire();
	
public:
	//******************//
	// Sezione pubblica
	//******************//
	
	// // Metodi
	
	// Costruttore
	explicit INCProcess(int &argc, char **argv);
	
	// Distruttore
	~INCProcess() override;
	
	// Installazione signal handlers
	void setupSignalHandlers(int *p_hup, int *p_int, int *p_term);
};


#endif // PROCESS

