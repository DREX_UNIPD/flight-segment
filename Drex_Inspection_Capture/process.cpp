// Header del processo
#include <process.h>
#include <iostream>

#include <PvPipeline.h>

// Header per i segnali di sistema
#include <sys/socket.h>

#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>

using namespace cv;

// Installazione dei signal handlers
void INCProcess::setupSignalHandlers(int *p_hup, int *p_int, int *p_term) {
	_sig_hup_fd = p_hup;
	_sig_int_fd = p_int;
	_sig_term_fd = p_term;
	
	// // Gestione dei segnali di sistema
	
	// HUP
	::socketpair(AF_UNIX, SOCK_STREAM, 0, _sig_hup_fd);
	_sn_hup = new QSocketNotifier(_sig_hup_fd[1], QSocketNotifier::Read, this);
	connect(_sn_hup, &QSocketNotifier::activated, this, &INCProcess::handleSigHup);
	
	// INT
	::socketpair(AF_UNIX, SOCK_STREAM, 0, _sig_int_fd);
	_sn_int = new QSocketNotifier(_sig_int_fd[1], QSocketNotifier::Read, this);
	connect(_sn_int, &QSocketNotifier::activated, this, &INCProcess::handleSigInt);
	
	// TERM
	::socketpair(AF_UNIX, SOCK_STREAM, 0, _sig_term_fd);
	_sn_term = new QSocketNotifier(_sig_term_fd[1], QSocketNotifier::Read, this);
	connect(_sn_term, &QSocketNotifier::activated, this, &INCProcess::handleSigTerm);
}

// Slot per HUP
void INCProcess::handleSigHup() {
	_sn_hup->setEnabled(false);
	char tmp;
	::read(_sig_hup_fd[1], &tmp, sizeof(tmp));
	
	// Chiudo l'applicazione
	quit();
	
	_sn_hup->setEnabled(true);
}

// Slot per INT
void INCProcess::handleSigInt() {
	_sn_int->setEnabled(false);
	char tmp;
	::read(_sig_int_fd[1], &tmp, sizeof(tmp));
	
	// Chiudo l'applicazione
	quit();
	
	_sn_int->setEnabled(true);
}

// Slot per TERM
void INCProcess::handleSigTerm() {
	_sn_term->setEnabled(false);
	char tmp;
	::read(_sig_term_fd[1], &tmp, sizeof(tmp));
	
	// Chiudo l'applicazione
	quit();
	
	_sn_term->setEnabled(true);
}

// Trovo il prossimo slot libero
bool INCProcess::getNextFreeSlot(QDir &dir, int &index) {
	index = -1;
	
	QStringList filters;
	QStringList files;
	
	do {
		index++;
		filters.clear();
		filters += QString::number(index) + QString("_*");
		files = dir.entryList(filters);
	}
	while (files.count() > 0);
	
	return true;
}

// Costruttore
INCProcess::INCProcess(int &argc, char **argv) : super(argc, argv) {
	// Inizializzo i parametri del processo
	_argc = argc;
	_argv = argv;
	
	_buffer_writer = new PvBufferWriter();
	_buffer_list = new BufferList();
	_cmd_start = nullptr;
	_cmd_stop = nullptr;
	_parameter_frame_rate = nullptr;
	_parameter_bandwidth = nullptr;
	
	// Avvio l'applicazione
	QTimer::singleShot(0, this, &INCProcess::acquire);
}

// Inizializzazione
void INCProcess::acquire() {
	// // // // // // // // // // // // // // // // // //
	// // Verifico che i parametri siano corretti   // //
	// // // // // // // // // // // // // // // // // //
	
	// Parametri
	if (_argc < 11) {
		cout << "Usage: Drex_Inspection_Capture <camera_ip> <video_dir> <images_dir> <trigger_dir> <max_video_time> <video_enable> <trigger_index> <image_index> <video_index> <divider>" << endl;
		quit();
		return;
	}
	
	// Camera IP
	_camera_ip = _argv[1];
	
	// Directory
	_video_dir.setPath(_argv[2]);
	if (!_video_dir.exists()) QDir::root().mkpath(_video_dir.absolutePath());
	_image_dir.setPath(_argv[3]);
	if (!_image_dir.exists()) QDir::root().mkpath(_image_dir.absolutePath());
	_trigger_dir.setPath(_argv[4]);
	if (!_trigger_dir.exists()) QDir::root().mkpath(_trigger_dir.absolutePath());
	
	// Video capturing
	_max_video_time = atoi(_argv[5]);
	_video_enable = (atoi(_argv[6]) != 0);
	_acquire_next = false;
	
	// Slots
	QString str_trigger_index, str_image_index, str_video_index;
	str_trigger_index = _argv[7];
	if (str_trigger_index.toLower() == "auto") {
		getNextFreeSlot(_trigger_dir, _trigger_index);
	}
	else {
		_trigger_index = str_trigger_index.toInt();
	}
	str_image_index = _argv[8];
	if (str_image_index.toLower() == "auto") {
		getNextFreeSlot(_image_dir, _image_index);
	}
	else {
		_image_index = str_image_index.toInt();
	}
	str_video_index = _argv[9];
	if (str_video_index.toLower() == "auto") {
		getNextFreeSlot(_video_dir, _video_index);
	}
	else {
		_video_index = str_video_index.toInt();
	}
	
	// Divisore di framerate
	_divider = atoi(_argv[10]);
	
	// Default video name
	_video_file = _video_dir.absoluteFilePath("video.avi");
	
	QFileInfo video_fileinfo(_video_file);
	if (video_fileinfo.exists()) {
		QFile video_file(video_fileinfo.absoluteFilePath());
		video_file.remove();
	}
	
	// // // // // // // // // // // // // //
	// // Cerco i dispositivi connessi  // //
	// // // // // // // // // // // // // //
	
	// Individuazione delle camere e inizializzazione della classe corrispondente
	PvSystem lSystem;
	vector<const PvDeviceInfo *> lDIVector;
	
	// Trovo tutte le interfacce di sistema
	lSystem.Find();
	
	// Trovo i dispositivi nelle interfacce disponibili
	for (uint32_t i = 0; i < lSystem.GetInterfaceCount(); i++) {
		const PvInterface *lInterface = dynamic_cast<const PvInterface *>(lSystem.GetInterface(i));
		if (lInterface != NULL) {
			for (uint32_t j = 0; j < lInterface->GetDeviceCount(); j++) {
				const PvDeviceInfo *lDI = dynamic_cast<const PvDeviceInfo *>(lInterface->GetDeviceInfo(j));
				if (lDI != NULL) lDIVector.push_back(lDI);
			}
		}
	}
	
	// Verifico di aver trovato almeno un dispositivo
	if (lDIVector.size() == 0) {
		cout << "No devices available" << endl;
		quit();
		return;
	}
	
	// // // // // // // // // // // // // // // // // // // // // // // // // // //
	// // Cerco il dispositivo specificato nei parametri tra quelli connessi   // //
	// // // // // // // // // // // // // // // // // // // // // // // // // // //
	
	// Dispositivo corrente
	const PvDeviceInfoGEV *lDeviceGEV = NULL;
	
	// Itero per tutti i dispositivi connessi fino a individuare quello specificato
	for (size_t i = 0; i < lDIVector.size(); i++) {
		lDeviceGEV = dynamic_cast<const PvDeviceInfoGEV *>(lDIVector[i]);
		if (lDeviceGEV == NULL) continue;
		
		// Ritrovo l'ip del dispositivo corrente
		QString ip;
		ip = lDeviceGEV->GetIPAddress().GetAscii();
		
		// Verifico se l'IP corrisponde a quello specificato
		if (ip == _camera_ip) {
			break;
		}
		
		lDeviceGEV = NULL;
	}
	
	if (lDeviceGEV == NULL) {
		cout << "Dispositivo non trovato" << endl;
		quit();
		return;
	}
	
	// // // // // // // // // // // // // // //
	// Apro la connessione con il dispositivo //
	// // // // // // // // // // // // // // //
	
	PvString connection_id;
	connection_id = lDeviceGEV->GetConnectionID();
	
	// Mi connetto al dispositivo
	if (!connectToDevice(connection_id)) return;
	
	// Creo lo stream
	if (!openStream(connection_id)) return;
	
	// Configuro lo stream
	configureStream(_device, _stream);
	
	// Creo i buffer
	createStreamBuffers(_device, _stream, _buffer_list);
	
	// // // // // // // // // // // // // // // // // //
	// Ritrovo i comandi di inizio e fine acquisizione //
	// // // // // // // // // // // // // // // // // //
	
	// Get device parameters need to control streaming
	PvGenParameterArray *lDeviceParams = _device->GetParameters();
	
	// Imposto il tipo di pixel
//	PvGenParameter *lParameter = lDeviceParams->Get("PixelFormat");
//	PvGenEnum *lPixelFormatParameter = dynamic_cast<PvGenEnum *>(lParameter);
//	cout << lPixelFormatParameter->ToString().GetAscii() << endl;
//	lPixelFormatParameter->SetValue("BayerRG8");
	
	// Map the GenICam AcquisitionStart and AcquisitionStop commands
	_cmd_start = dynamic_cast<PvGenCommand *>(lDeviceParams->Get("AcquisitionStart"));
	_cmd_stop = dynamic_cast<PvGenCommand *>(lDeviceParams->Get("AcquisitionStop"));
	
	// Get stream parameters
//	PvGenParameterArray *lStreamParams = _stream->GetParameters();
	
	// // // // // // // // // // // // // // // //
	// Ritrovo i parametri framerate e bandwith  //
	// // // // // // // // // // // // // // // //
	
	// Map a few GenICam stream stats counters
//	_parameter_frame_rate = dynamic_cast<PvGenFloat *>(lStreamParams->Get("AcquisitionRate"));
//	_parameter_bandwidth = dynamic_cast<PvGenFloat *>(lStreamParams->Get("Bandwidth"));
	
	// // // // // // // // // // // // // //
	// Inizio l'acquisizione se abilitata  //
	// // // // // // // // // // // // // //
	
	// Enable streaming and send the AcquisitionStart command
	_device->StreamEnable();
	_cmd_start->Execute();
	
//	double lFrameRateVal = 0.0;
//	double lBandwidthVal = 0.0;
	
	if (_video_enable) {
		startVideo();
	}
	
	Mat rgb;
	
	cout << "Start acquiring...";
	cout.flush();
	
	int counter = 0;
	
	while (true) {
		PvBuffer *lBuffer = NULL;
		PvResult lOperationResult;
		
		// Verifico se esiste un trigger
		// Trovo i file che corrispondono al pattern prestabilito
		QStringList filters;
		filters.clear();
		filters.append(QString::number(_trigger_index) + "_*");
		QStringList triggers = _trigger_dir.entryList(filters);
		
		// Aggiorno il contatore
		if (triggers.size() > 0) {
			QRegularExpression regex;
			
			_trigger_index++;
			
			// Acquisisco il prossimo trigger
			QString &trigger = triggers.first();
			
			// Ho trovato un comando, lo interpreto
			regex.setPattern("(?i)^\\d+_(?<cmd>[a-z0-9]+)?$"); // (?:_(?<param1>[a-z0-9]+))
			QRegularExpressionMatch match = regex.match(trigger);
			if (match.hasMatch()) {
				QString command = match.captured("cmd").toUpper();
				
				// Capture single image
				if (command == "IMAGE") {
					_acquire_next = true;
				}
				
				// Exit process
				if (command == "EXIT") {
					if (_video_enable) {
						cout << " stop acquiring" << endl;
					}
					break;
				}
				
//					// Reset continuous capturing
//					if (command == "STOP") {
//						continuous = false;
//					}
			}
		}
		
		// Retrieve next buffer
		PvResult lResult = _stream->RetrieveBuffer(&lBuffer, &lOperationResult, 1000);
		if (lResult.IsOK()) {
			
			if (lOperationResult.IsOK() && lBuffer->GetPayloadType() == PvPayloadTypeImage) {
//				_parameter_frame_rate->GetValue(lFrameRateVal);
//				_parameter_bandwidth->GetValue(lBandwidthVal);
				
				counter++;
				if (counter >= _divider || _acquire_next) {
					// Conversione tra Pleora e OpenCV
					PvImage *lImage = lBuffer->GetImage();
					
					// Get image specific buffer interface.
					unsigned int width = lImage->GetWidth();
					unsigned int height = lImage->GetHeight();;
		//			unsigned char* dataBuffer = (unsigned char*)lImage->GetBuffer();
					IplImage iplImage;
					iplImage.imageData = (char*)lImage->GetDataPointer();
					iplImage.imageDataOrigin = iplImage.imageData;
					iplImage.depth = IPL_DEPTH_8U;
					iplImage.nChannels = 1;
					iplImage.width = width;
					iplImage.height = height;
					iplImage.widthStep = lImage->GetBitsPerPixel() / 8 * width + lImage->GetPaddingX();
					iplImage.imageSize = lImage->GetImageSize();
					iplImage.nSize = sizeof(IplImage);
					iplImage.ID = 0;
					iplImage.dataOrder = 0;
					iplImage.origin = 0;
					iplImage.roi = NULL;
					iplImage.maskROI = NULL;
					iplImage.imageId = NULL;
					iplImage.tileInfo = NULL;
					IplImage *im = (IplImage *) &iplImage;
					Mat image = cvarrToMat(im, false);
					rgb = Mat(height, width, CV_8UC3);
					cvtColor(image, rgb, CV_BayerBG2BGR, 0);
					
					// Se sto catturando il video
					if (_video_enable) {
						counter = 0;
						
						_video_writer.write(rgb);
						
						// Se ho superato il limite di dimensione salvo il file e ne apro un altro
						if (_video_start_time.secsTo(QDateTime::currentDateTime()) > _max_video_time) {
							saveVideo();
							startVideo();
						}
					}
					
					if (_acquire_next) {
						_acquire_next = false;
						
						// Salvo l'immagine in JPG
						QString file_name;
						file_name = getName(_image_dir, QString::number(_image_index) + QString("_image_"), QDateTime::currentDateTime(), ".jpg");
						imwrite(file_name.toStdString(), rgb);
						_image_index++;
					}
				}
				
				if (counter >= _divider) counter = 0;
			}
			
			// Re-queue the buffer in the stream object
			if (lBuffer) _stream->QueueBuffer(lBuffer);
		}
	}
	
	// Tell the device to stop sending images.
	_cmd_stop->Execute();
	
	// Disable streaming on the device
	_device->StreamDisable();
	
	// Abort all buffers from the stream and dequeue
	_stream->AbortQueuedBuffers();
	while (_stream->GetQueuedBufferCount() > 0) {
		PvBuffer *lBuffer = NULL;
		PvResult lOperationResult;
		
		_stream->RetrieveBuffer(&lBuffer, &lOperationResult);
	}
	
	// Rilascio i buffer allocati
	freeStreamBuffers(_buffer_list);
	
	// Close the stream
	_stream->Close();
	PvStream::Free(_stream);
	
	// Disconnect the device
	_device->Disconnect();
	PvDevice::Free(_device);
	
	if (_video_enable) {
		saveVideo();
	}
	
	quit();
}

bool INCProcess::connectToDevice(const PvString &connection_id) {
	PvResult lResult;
	_device = NULL;
	_device = PvDevice::CreateAndConnect(connection_id, &lResult);
	return _device != NULL;
}
bool INCProcess::openStream(const PvString &connection_id) {
	PvResult lResult;
	_stream = NULL;
	_stream = PvStream::CreateAndOpen(connection_id, &lResult);
	return _stream != NULL;
}
void INCProcess::configureStream(PvDevice *device, PvStream *stream) {
	PvDeviceGEV* lDeviceGEV = dynamic_cast<PvDeviceGEV *>(device);
	if (lDeviceGEV != NULL) {
		PvStreamGEV *lStreamGEV = static_cast<PvStreamGEV *>(stream);
		lDeviceGEV->NegotiatePacketSize();
		lDeviceGEV->SetStreamDestination(lStreamGEV->GetLocalIPAddress(), lStreamGEV->GetLocalPort());
	}
}
void INCProcess::createStreamBuffers(PvDevice *device, PvStream *stream, BufferList *buffer_list) {
	// Reading payload size from device
	uint32_t lSize = device->GetPayloadSize();
	
	// Use BUFFER_COUNT or the maximum number of buffers, whichever is smaller
	uint32_t lBufferCount = (stream->GetQueuedBufferMaximum() < BUFFER_COUNT) ? stream->GetQueuedBufferMaximum() : BUFFER_COUNT;
	
	// Allocate buffers
	for (uint32_t i = 0; i < lBufferCount; i++) {
		// Create new buffer object
		PvBuffer *lBuffer = new PvBuffer;
	
		// Have the new buffer object allocate payload memory
		lBuffer->Alloc(static_cast<uint32_t>(lSize));
		
		// Add to external list - used to eventually release the buffers
		buffer_list->push_back(lBuffer);
	}
	
	// Queue all buffers in the stream
	BufferList::iterator lIt = buffer_list->begin();
	while (lIt != buffer_list->end()) {
		stream->QueueBuffer(*lIt);
		lIt++;
	}
}
bool INCProcess::createPipeline(PvDevice *aDevice, PvStream *aStream, PvPipeline *lPipeline) {
	// Create the PvPipeline object
	lPipeline = new PvPipeline( aStream );
	
	if (lPipeline != NULL) {
		// Reading payload size from device
		uint32_t lSize = aDevice->GetPayloadSize();
		
		// Set the Buffer count and the Buffer size
		lPipeline->SetBufferCount(BUFFER_COUNT);
		lPipeline->SetBufferSize(lSize);
	}
	
	return lPipeline;
}

void INCProcess::freeStreamBuffers(BufferList *buffer_list) {
	// Go through the buffer list
	BufferList::iterator lIt = buffer_list->begin();
	while (lIt != buffer_list->end()) {
		delete *lIt;
		lIt++;
	}
	
	// Clear the buffer list 
	buffer_list->clear();
}

// Start Video
void INCProcess::startVideo() {
	_video_start_time = QDateTime::currentDateTime();
	_video_writer.open(_video_file.toStdString(), VideoWriter::fourcc('M', 'J', 'P', 'G'), (int) (24 / _divider), Size(1392, 1040));
}

// Stop Video
void INCProcess::saveVideo() {
	QString file_name;
	_video_writer.release();
	file_name = getName(_video_dir, QString::number(_video_index) + QString("_video_"), QDateTime::currentDateTime(), ".avi");
	QFile::rename(_video_file, file_name);
	_video_index++;
}

// Calcolo prossimo nome valido
QString INCProcess::getName(QDir directory, QString before, QDateTime time, QString after) {
	QString time_str;
	QString file_name;
	QFileInfo dest_file;
	
	// Costruisco il nome del file
	time_str = time.toString("yyyy_MM_dd__HH_mm_ss");
	file_name = before + time_str + after;
	dest_file.setFile(directory.absoluteFilePath(file_name));
	
	// Trovo il primo nome disponibile
	int i = 0;
	while (dest_file.exists()) {
		file_name = before + time_str + QString("__") + QString::number(i) + after;
		dest_file.setFile(directory.absoluteFilePath(file_name));
		i++;
	}
	
	return directory.absoluteFilePath(file_name);
}

// Distruttore
INCProcess::~INCProcess() {
	// Chiusura della connessione con la camera
	delete _buffer_writer;
	delete _buffer_list;
}
