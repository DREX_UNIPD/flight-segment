// *****************************************************************************
//
//      Copyright (c) 2013, Pleora Technologies Inc., All rights reserved.
//
// *****************************************************************************

//
// Shows how to use a PvStream object to acquire images from a GigE Vision or
// USB3 Vision device.
//

#include <process.h>
#include <iostream>

// Cattura Ctrl+C o SIGTERM
#include <signal.h>
#include <stdlib.h>

// Variabili per la gestione dei segnali di sistema

int sig_hup_fd[2];
int sig_int_fd[2];
int sig_term_fd[2];

// Gestori dei segnali di sistema

void hupSignalHandler(int unused);
void intSignalHandler(int unused);
void termSignalHandler(int unused);

void signalHandlersInstall() {
	struct sigaction sa_hup, sa_term, sa_int;
	
	// HUP
	
	sa_hup.sa_handler = hupSignalHandler;
	sigemptyset(&sa_hup.sa_mask);
	sa_hup.sa_flags = 0;
	sa_hup.sa_flags |= SA_RESTART;
	
	if (sigaction(SIGHUP, &sa_hup, 0)) {
//		console->error("Cannot install signal handler for HUP");
		exit(EXIT_FAILURE);
	}
	
	// INT
	
	sa_int.sa_handler = intSignalHandler;
	sigemptyset(&sa_int.sa_mask);
	sa_int.sa_flags = 0;
	sa_int.sa_flags |= SA_RESTART;
	
	if (sigaction(SIGINT, &sa_int, 0)) {
//		console->error("Cannot install signal handler for INT");
		exit(EXIT_FAILURE);
	}
	
	// TERM
	
	sa_term.sa_handler = termSignalHandler;
	sigemptyset(&sa_term.sa_mask);
	sa_term.sa_flags |= SA_RESTART;
	
	if (sigaction(SIGTERM, &sa_term, 0)) {
//		console->error("Cannot install signal handler for TERM");
		exit(EXIT_FAILURE);
	}
}

void hupSignalHandler(int) {
	char a = 1;
	::write(sig_hup_fd[0], &a, sizeof(a));
}

void intSignalHandler(int) {
	char a = 1;
	::write(sig_int_fd[0], &a, sizeof(a));
}

void termSignalHandler(int) {
	char a = 1;
	::write(sig_term_fd[0], &a, sizeof(a));
}

// Main
int main(int argc, char *argv[]) {
	// Creo l'oggetto del processo
	INCProcess process(argc, argv);
	
	process.setupSignalHandlers(sig_hup_fd, sig_int_fd, sig_term_fd);
	
	signalHandlersInstall();
	
	// Avvio il processo
	return process.exec();
}
