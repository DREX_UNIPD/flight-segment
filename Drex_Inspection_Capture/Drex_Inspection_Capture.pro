QT += core
QT -= gui

TARGET = Drex_Inspection_Capture
CONFIG += console c++11
CONFIG -= app_bundle

TEMPLATE = app

INCLUDEPATH += /opt/pleora/ebus_sdk/linux-aarch64-arm/include

LIBS += -L/usr/local/lib -lopencv_core -lopencv_highgui -lopencv_imgcodecs -lopencv_videoio -lopencv_imgproc
LIBS += -L "/opt/pleora/ebus_sdk/linux-aarch64-arm/lib/" -lPvAppUtils -lPvBase -lPvBuffer -lPvCameraBridge -lPvDevice
LIBS += -L "/opt/pleora/ebus_sdk/linux-aarch64-arm/lib/" -lPvGenICam -lPvPersistence -lPvSerial -lPvStream -lPvSystem
LIBS += -L "/opt/pleora/ebus_sdk/linux-aarch64-arm/lib/" -lPvTransmitter -lPvVirtualDevice -lSimpleImagingLib

SOURCES += main.cpp \
    process.cpp

HEADERS += \
    process.h

