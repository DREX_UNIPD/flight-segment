//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		06/08/17
//	Descrizione:		Classe per processo INP
//****************************************************************************************************//

#ifndef INP_PROCESS_H
#define INP_PROCESS_H

// Header per logging
#include <spdlog/spdlog.h>

// Header nzmqt
#include <nzmqt/nzmqt.hpp>

// Header Qt
#include <QCoreApplication>
#include <QMap>

// Header YAML
#include <yaml-cpp/yaml.h>

// Header DREX
#include <State/state_object.h>
#include <Socket/socket_utils.h>

//----------------------------------------------------//

namespace drex {
namespace processes {

// Classe del processo INP
class INProcess : public QCoreApplication {
	Q_OBJECT
	
	using super = QCoreApplication;
	using Message = QList<QByteArray>;
	using Logger = spdlog::logger;
	
private:
	//*****************//
	// Sezione privata
	//*****************//
	
	// // Variabili
	
	// Logging
	
	// Logger su console
	std::shared_ptr<Logger> _console;
	
	// Logger su file e console
	std::shared_ptr<Logger> _logger;
	
	// Configurazione
	
	// Configurazione del processo
	YAML::Node _configuration;
	
	// Storage
	
	QDir _storage_home;
	QDir _storage_ssd_dir;
//	QDir _storage_ram_dir;
	
	QDir _storage_video_dir;
	QDir _storage_image_dir;
	QDir _storage_trigger_dir;
	
	QMap<QString, QDir> _storage_cameras_video_dirs;
	QMap<QString, QDir> _storage_cameras_image_dirs;
	QMap<QString, QDir> _storage_cameras_trigger_dirs;
	
	QDir _storage_log_dir;
	
	// Socket
	
	// Contesto per i socket
	nzmqt::ZMQContext *_context;
	
	// Command (ROUTER)
	drex::socket::SocketInfo _command_socket;
	// Logger (PUBLISHER)
	drex::socket::SocketInfo _logger_socket;
	// Signaling (PUBLISHER)
	drex::socket::SocketInfo _signaling_socket;
	
	// Gestione degli stati
	
	// Stato
	std::string _state;
	
	// Timer per la pubblicazione dello stato
	bool _publishing_enabled;
	
	// Pubblicazione dello stato
	bool _publishing_state_enabled;
	int _publishing_state_period;
	QTimer _publishing_state_timer;
	
	// Comandi
	QMap<std::string, drex::state::StateObject *> _state_objects;
	
	// Camere per l'ispezione
	QString _inspection_executable_path;
	QMap<QString, QProcess *> _inspection_processes;
	QTimer _inspection_check_new_timer;
	int _inspection_check_new_period;
	
	QMap<QString, int> _inspection_next_image_index;
	QMap<QString, int> _inspection_next_video_index;
	
	bool inpTrigger(QDir dir, QString command);
	int inpGetNextSlot(QDir dir);
	void inpCheckNewFiles();
	
	// // Metodi
	
	// Caricamento della configurazione
	bool loadConfiguration(QStringList &paths, YAML::Node &destination);
	
	// Variabili per i segnali di sistema
	QSocketNotifier *_sn_hup;
	QSocketNotifier *_sn_int;
	QSocketNotifier *_sn_term;
	int *_sig_hup_fd;
	int *_sig_int_fd;
	int *_sig_term_fd;
	
	//**********//
	// Features
	//**********//
	
	// Metodo per la terminazione del processo
	void killProcess();
	
	// Metodi per la gestione dello stato
	std::string getState();
	bool setState(std::string newstate);
	
	// Metodo per il salvataggio dell'immagine corrente
	bool saveImage(QString name = "");
	
	//******************//
	// Metodi ausiliari
	//******************//
	
	// Verifica della sintassi del messaggio
	bool checkMessageSyntax(const Message &msg, const QStringList &syntax, QList<QRegularExpressionMatch> &matches);
	
	// Verifica della sintassi del comando ed eventuale risposta
	bool verifyCommandSyntax(const Message &msg, const std::vector<const char *> &syntax, QList<QRegularExpressionMatch> &match);
	
	// Inizializzazioni
	
	// Metodo principale
	void init();
	
	// Inizializzazione dello storage
	bool initStorage();
	
	// Inizializzazione del logger su file
	bool initFileLogger();
	
	// Inizializzazione dell'interfaccia di rete
	bool initNetworking();
	
	// Inizializzazione degli stati
	bool initStatesMap();
	
	// Inizializzazione della pubblicazione
	bool initPublishing();
	
	// Inizializzazione dei socket
	bool initSocket(drex::socket::SocketInfo &si, const YAML::Node &node, std::string name, int linger);
	bool startSocket(drex::socket::SocketInfo &si);
	
	// Inizializzazione delle camere
	bool initInspection();
	
	//**********************//
	// Metodi per gli stati
	//**********************//
	
	// IDLE
	bool stateIdleInit();
	bool stateIdleCleanup();
	
	// RUN
	uint64_t _max_video_time;
	bool _cameras_enabled;
	//QMap<QString, camera::CameraWorker *> _camera_workers;
	bool stateRunInit();
	bool findDevice(QString ip);
	bool stateRunCleanup();
	
private slots:
	//**********************//
	// Selettore di comandi
	//**********************//
	
	// Seleziona ed esegue il comando appropriato
	void commandSelector(const Message &msg);
	
	//******************//
	// Comandi standard
	//******************//
	
	// Aiuto
	bool commandHelp(const Message &msg, std::vector<const char *> &parameters);
	
	// Chiusura del processo
	bool commandKill(const Message &msg, std::vector<const char *> &parameters);
	
	// Lettura stato del processo
	bool commandGetState(const Message &msg, std::vector<const char *> &parameters);
	
	// Impostazione stato del processo
	bool commandSetState(const Message &msg, std::vector<const char *> &parameters);
	
	//*******************//
	// Comandi specifici
	//*******************//
	
	// Salvataggio dell'immagine corrente della inspection camera specificata
	bool commandSaveImage(const Message &msg, std::vector<const char *> &parameters);
	
	//***************//
	// Pubblicazione
	//***************//
	
	// Pubblicazione dello stato
	void publishingState();
	
public slots:
	// Gestori dei segnali di sistema di Qt
	void handleSigHup();
	void handleSigInt();
	void handleSigTerm();
	
	// Pubblicazione di un messaggio di Log
	void publishLog(const Message &msg);
	
	// Nuova imamgine disponibile
	void newImage(QString path, double bandwidth, double framerate);
	
	// Nuovo video disponibile
	void newVideo(QString path);
	
signals:
	//*********//
	// Segnali
	//*********//
	
	// Liberazione immagine
	void stopAcquire();
	
	// Acquisizione di un'immagine
	void acquireNext(QString name);
	
public:
	//******************//
	// Sezione pubblica
	//******************//
	
	// // Costanti
	
	// Nome del processo
	static const std::string SHORT_NAME;
	static const std::string FULL_NAME;
	
	// Comandi
	static const std::string CMD_HELP;
	static const std::string CMD_KILL;
	static const std::string CMD_GET_STATE;
	static const std::string CMD_SET_STATE;
	
	static const std::string CMD_SAVE_IMAGE;
	
	// Stati
	static const std::string ST_DEFAULT;
	static const std::string ST_IDLE;
	static const std::string ST_RUN;
	
	// // Metodi
	
	// Costruttore
	explicit INProcess(int &argc, char **argv);
	
	// Distruttore
	~INProcess() override;
	
	// Notifica evento
	bool notify(QObject *obj, QEvent *event) override;
	
	// Installazione signal handlers
	void setupSignalHandlers(int *p_hup, int *p_int, int *p_term);
};

}
}

#endif // INP_PROCESS_H

