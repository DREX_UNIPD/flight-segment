# Qt
QT += core
QT -= gui

# Config
CONFIG -= app_bundle
CONFIG += c++11
CONFIG += console
CONFIG += thread

# Target
TARGET = Drex_Inspection

# Template
TEMPLATE = app

SOURCES += \
	src/main.cpp \
	src/Process/inp_command_selector.cpp \
	src/Process/inp_constants.cpp \
	src/Process/inp_constructor.cpp \
	src/Process/inp_destructor.cpp \
	src/Process/inp_load_configuration.cpp \
	src/Process/inp_notify.cpp \
	src/Process/inp_sys_signals.cpp \
	src/Process/inp_verify_command_syntax.cpp \
	src/Process/inp_socket_management.cpp \
	src/Process/Inits/inp_init.cpp \
	src/Process/Inits/inp_init_states_map.cpp \
	src/Process/Inits/inp_init_networking.cpp \
	src/Process/Inits/inp_init_file_logger.cpp \
	src/Process/Features/inp_kill_process.cpp \
	src/Process/Features/inp_get_state.cpp \
	src/Process/Features/inp_set_state.cpp \
	src/Process/Commands/inp_cmd_kill.cpp \
	src/Process/Commands/inp_cmd_set_state.cpp \
	src/Process/Commands/inp_cmd_get_state.cpp \
	src/Process/States/inp_idle.cpp \
	src/Process/States/inp_run.cpp \
	../Drex/src/State/state_object.cpp \
	../Drex/src/PublisherSink/publisher_sink.cpp \
	src/Process/Commands/inp_cmd_save_image.cpp \
	src/Process/inp_new_image.cpp \
	src/Process/inp_publish_log.cpp \
	src/Process/Inits/inp_init_storage.cpp \
	src/Process/Features/inp_save_image.cpp \
	src/Process/inp_new_video.cpp \
	src/Process/Inits/inp_init_publishing.cpp \
	src/Process/inp_publishing.cpp \
    src/Process/Commands/inp_cmd_help.cpp \
    src/Process/inp_inspection.cpp \
    src/Process/Inits/inp_init_inspection.cpp

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
#DEFINES += QT_NO_DEBUG_OUTPUT PV_NO_GEV1X_PIXEL_TYPES PV_NO_DEPRECATED_PIXEL_TYPES _UNIX_ _LINUX_

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# Include path
INCLUDEPATH += \
	include \
	../ \
	../nzmqt/include \
	../Drex/include
INCLUDEPATH += /opt/pleora/ebus_sdk/linux-aarch64-arm/include

# Altri file
DISTFILES += \
	config/inp.yaml \
    init_scripts/inp.sh \
    ../Drex/config/common.yaml

# Pacchetti
CONFIG += link_pkgconfig
PKGCONFIG += libzmq

# Librerie
LIBS += /usr/local/lib/libyaml-cpp.a
LIBS += -L/usr/local/lib -lopencv_core -lopencv_highgui -lopencv_imgcodecs -lopencv_videoio
LIBS += -L "/opt/pleora/ebus_sdk/linux-aarch64-arm/lib/" -lPvAppUtils -lPvBase -lPvBuffer -lPvCameraBridge -lPvDevice
LIBS += -L "/opt/pleora/ebus_sdk/linux-aarch64-arm/lib/" -lPvGenICam -lPvPersistence -lPvSerial -lPvStream -lPvSystem
LIBS += -L "/opt/pleora/ebus_sdk/linux-aarch64-arm/lib/" -lPvTransmitter -lPvVirtualDevice -lSimpleImagingLib

HEADERS += \
	include/Process/inp.h \
	../Drex/include/State/state_object.h \
	../nzmqt/include/nzmqt/nzmqt.hpp \
	../Drex/include/Socket/socket_utils.h \
	../Drex/include/PublisherSink/publisher_sink.h
