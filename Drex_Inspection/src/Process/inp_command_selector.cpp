//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		06/08/2017
//	Descrizione:		Selettore del comando
//****************************************************************************************************//

// Header del processo
#include <Process/inp.h>

using namespace std;
using namespace drex::processes;
using namespace drex::state;

// Seleziona ed esegue il comando appropriato
void INProcess::commandSelector(const Message &msg) {
	_logger->info("Command selector message frames: " + std::to_string(msg.length()));
	
	// Stampo tutti i campi del messaggio
	QByteArray a;
	a = msg[0];
	_logger->info((QString("Sender address: 0x") + a.toHex()).toStdString());
	for (int i = 1; i < msg.length(); i++) {
		a = msg[i];
		_logger->info("Frame #" + std::to_string(i) + ": " + a.toStdString());
	}
	
	// Verifico se il comando esiste e lo richiamo
	string command = msg[1].toUpper().toStdString();
	
	// Messaggio di risposta
	Message ans;
	ans += msg[0];
	
	// Verifico se il comando è presente tra quelli associati allo stato corrente
	StateObject *so;
	if (!_state.empty()) {
		so = _state_objects[_state];
		if (so->command.find(command) != so->command.end()) {
			vector<const char *> &parameters = so->parameters[command];
			if (!so->command[command](msg, parameters)) {
				_logger->error("Failed to run command");
			}
			return;
		}
		_logger->info("Command not found in current state");
	}
	else {
		_logger->info("State not yet set");
		ans += "ERROR";
		ans += "State not yet set";
		_command_socket.socket->sendMessage(ans);
		return;
	}
	
	// Verifico se il comando è presente tra quelli associati allo stato default
	so = _state_objects[ST_DEFAULT];
	if (so->command.find(command) != so->command.end()) {
		vector<const char *> &parameters = so->parameters[command];
		if (!so->command[command](msg, parameters)) {
			_logger->error("Failed to run command");
		}
		return;
	}
	
	// Se il comando non esiste rispondo negativamente
	string str;
	str += "Command \"";
	str += command.c_str();
	str += "\" not found";
	_logger->warn(str);
	
	// Creo il messaggio di risposta
	ans += "COMMAND NOT FOUND";
	ans += str.c_str();
	_command_socket.socket->sendMessage(ans);
}
