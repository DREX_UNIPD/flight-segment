//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		06/08/2017
//	Descrizione:		Impostazione dello stato
//****************************************************************************************************//

// Header del processo
#include <Process/inp.h>

using namespace std;
using namespace drex::processes;

bool INProcess::setState(string newstate) {
	_logger->info("Set state");
	
	string state;
	
	// Salvo lo stato in caso di errori
	state = getState();
	
	// Richiamo la funzione di clean-up dello stato corrente
	if (!_state.empty()) {
		if (!_state_objects[_state]->cleanup()) {
			_logger->error("Failed to execute previous state cleanup procedure");
			return false;
		}
	}
	
	_state = newstate;
	
	// Richiamo la funzione di inizializzazione del nuovo stato
	if (!_state_objects[_state]->init()) {
		_logger->error("Failed to execute next state init procedure");
		
		// Cerco di ripristinare lo stato precedente
		_state = state;
		_state_objects[_state]->init();
		
		return false;
	}
	
	return true;
}
