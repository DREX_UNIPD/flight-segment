//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		06/08/2017
//	Descrizione:		Richiesta di salvataggio di un'immagine
//****************************************************************************************************//

// Header del processo
#include <Process/inp.h>

using namespace std;
using namespace drex::processes;
using namespace drex::state;

bool INProcess::saveImage(QString name) {
	_logger->info(string("Save image: ") + name.toStdString());
	
	name = name.toLower();
	
	// Verifico che la camera sia configurata e attiva
	if (!_storage_cameras_trigger_dirs.contains(name)) {
		_logger->error("Camera not found");
		return false;
	}
	
	inpTrigger(_storage_cameras_trigger_dirs[name], "image");
	
	return true;
}
