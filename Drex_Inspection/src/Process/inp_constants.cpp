//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		06/08/2017
//	Descrizione:		Costanti
//****************************************************************************************************//

// Header del processo
#include <Process/inp.h>

using namespace std;
using namespace drex::processes;

// // Costanti

// Nome del processo
const string INProcess::SHORT_NAME = "INP";
const string INProcess::FULL_NAME = "Inspection process";

// Comandi
const string INProcess::CMD_HELP = "HELP";
const string INProcess::CMD_KILL = "KILL";
const string INProcess::CMD_GET_STATE = "GET STATE";
const string INProcess::CMD_SET_STATE = "SET STATE";

const string INProcess::CMD_SAVE_IMAGE = "SAVE IMAGE";

// Stati
const string INProcess::ST_DEFAULT = "DEFAULT";
const string INProcess::ST_IDLE = "IDLE";
const string INProcess::ST_RUN = "RUN";
