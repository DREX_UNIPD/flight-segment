//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		10/10/2017
//	Descrizione:		Metodi per la gestione delle camere
//****************************************************************************************************//

// Header del processo
#include <Process/inp.h>

using namespace std;
using namespace drex::processes;

bool INProcess::inpTrigger(QDir dir, QString command) {
	_logger->info(string("Trigger: ") + dir.absolutePath().toStdString() + string("_") + command.toStdString());
	
	// Creo il file di trigger
	int trigger_index = inpGetNextSlot(dir);
	QString filename = dir.absoluteFilePath(QString::number(trigger_index) + QString("_") + command.toUpper());
	QFile file;
	
	// Creo il file per la left camera
	file.setFileName(filename);
	if (!file.open(QIODevice::WriteOnly)) {
		_logger->error("Failed to create trigger file");
		return false;
	}
	file.close();
	
	return true;
}

int INProcess::inpGetNextSlot(QDir dir) {
	int index = -1;
	
	QStringList filters;
	QStringList files;
	
	do {
		index++;
		filters.clear();
		filters += QString::number(index) + QString("_*");
		files = dir.entryList(filters);
	}
	while (files.count() > 0);
	
	return index;
}

void INProcess::inpCheckNewFiles() {
	Message msg;
	
	// Per ogni camera verifico sia le immagini che i video
	
	// Immagini
	for (QString name : _storage_cameras_image_dirs.keys()) {
		QDir image_dir;
		image_dir = _storage_cameras_image_dirs[name];
		
		if (_inspection_next_image_index[name] < inpGetNextSlot(image_dir)) {
			// Aggiorno l'indice
			int &index = _inspection_next_image_index[name];
			
			// Ritrovo il percorso completo dell'immagine
			QFileInfoList image_fil;
			QStringList filters;
			filters += QString::number(index) + QString("_*");
			image_fil = image_dir.entryInfoList(filters);
			if (image_fil.count() == 0) {
				index++;
				continue;
			}
			
			QString filepath = image_fil[0].absoluteFilePath();
			
			_logger->info((QString("New image found (") + name + QString("): ") + filepath).toStdString());
			
			msg.clear();
			msg += "NEW IMAGE";
			msg += (QString("CAMERA = ") + name).toStdString().c_str();
			msg += (string("PATH = ") + filepath.toStdString()).c_str();
			_signaling_socket.socket->sendMessage(msg);
			
			index++;
		}
	}
	
	for (QString name : _storage_cameras_video_dirs.keys()) {
		QDir video_dir;
		video_dir = _storage_cameras_video_dirs[name];
		
		if (_inspection_next_video_index[name] < inpGetNextSlot(video_dir)) {
			// Aggiorno l'indice
			int &index = _inspection_next_video_index[name];
			
			// Ritrovo il percorso completo dell'immagine
			QFileInfoList video_fil;
			QStringList filters;
			filters += QString::number(index) + QString("_*");
			video_fil = video_dir.entryInfoList(filters);
			if (video_fil.count() == 0) {
				index++;
				continue;
			}
			
			QString filepath = video_fil[0].absoluteFilePath();
			
			_logger->info((QString("New video found (") + name + QString("): ") + filepath).toStdString());
			
			msg.clear();
			msg += "NEW VIDEO";
			msg += (QString("CAMERA = ") + name).toStdString().c_str();
			msg += (string("PATH = ") + filepath.toStdString()).c_str();
			_signaling_socket.socket->sendMessage(msg);
			
			index++;
		}
	}
}
