//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		06/08/2017
//	Descrizione:		Stato Run: il processo acquisisce le immagini continuativamente e le salva
//						in formato video con durata predeterminata.
//****************************************************************************************************//

// Header del processo
#include <Process/inp.h>

using namespace std;
using namespace drex::processes;

// Inizializzazione dello stato
bool INProcess::stateRunInit() {
	_logger->info("RUN: Init");
	
	// Verifico se almeno una camera è abilitata
	if (!_cameras_enabled) {
		_logger->warn("No camera configured");
		return true;
	}
	
	// Configurazione delle camere
	const YAML::Node &cameras = _configuration["Inspection"]["Cameras"];
	
	for (size_t j = 0; j < cameras.size(); j++) {
		const YAML::Node &current_camera = cameras[j];
		
		bool enabled = current_camera["Enabled"].as<bool>();
		
		// Se non è abilitata la ignoro
		if (!enabled) {
			continue;
		}
		
		// Catturo le impostazioni
		QString video = QString(current_camera["Video"].as<bool>() ? "1" : "0");
		QString name = QString::fromStdString(current_camera["Name"].as<string>()).toLower();
		QString ip = QString::fromStdString(current_camera["IP"].as<string>());
		
		// Inizializzo gli indici per le immagini e i video
		_inspection_next_image_index[name] = inpGetNextSlot(_storage_cameras_image_dirs[name]);
		_inspection_next_video_index[name] = inpGetNextSlot(_storage_cameras_video_dirs[name]);
		
		// <camera_ip> <video_dir> <images_dir> <trigger_dir> <max_video_time> <video_enable> <trigger_index> <image_index> <video_index>
		// Avvio il processo
		QProcess *process;
		QStringList arguments;
		process = new QProcess(this);
		arguments += ip;
		arguments += _storage_cameras_video_dirs[name].absolutePath();
		arguments += _storage_cameras_image_dirs[name].absolutePath();
		arguments += _storage_cameras_trigger_dirs[name].absolutePath();
		arguments += QString::number(_max_video_time);
		arguments += video;
		arguments += "auto";
		arguments += "auto";
		arguments += "auto";
		
		_logger->info(string("Starting ") + _inspection_executable_path.toStdString());
		
		process->start(_inspection_executable_path, arguments);
		_inspection_processes.insert(name, process);
	}
	
	// Abilito la verifica per segnalazione di nuovi file
	QObject::connect(&_inspection_check_new_timer, &QTimer::timeout, this, &INProcess::inpCheckNewFiles, Qt::QueuedConnection);
	_inspection_check_new_timer.start(_inspection_check_new_period);
	
	return true;
}

// Pulizia dello stato
bool INProcess::stateRunCleanup() {
	_logger->info("RUN: Cleanup");
	
	// Fermo il controllo per i nuovi file
	QObject::disconnect(&_inspection_check_new_timer, &QTimer::timeout, this, &INProcess::inpCheckNewFiles);
	_inspection_check_new_timer.stop();
	
	// Fermo l'acquisizione
	for (int i = 0; i < _storage_cameras_trigger_dirs.count(); i++) {
		const QString name = _storage_cameras_trigger_dirs.keys().at(i);
		QDir trigger_dir = _storage_cameras_trigger_dirs[name];
		inpTrigger(trigger_dir, "exit");
		QProcess *process;
		process = _inspection_processes[name];
		process->waitForFinished(5000);
		QString output(process->readAllStandardOutput());
		_logger->info((QString("Process ") + name + QString(" output:\n") + output).toStdString());
	}
	
	_inspection_processes.clear();
	
	return true;
}
