//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		06/08/2017
//	Descrizione:		Stato Idle: il processo è inattivo
//****************************************************************************************************//

// Header del processo
#include <Process/inp.h>

using namespace std;
using namespace drex::processes;

// Inizializzazione dello stato
bool INProcess::stateIdleInit() {
	_logger->info("IDLE: Init");
	
	return true;
}

// Pulizia dello stato
bool INProcess::stateIdleCleanup() {
	_logger->info("IDLE: Cleanup");
	
	return true;
}
