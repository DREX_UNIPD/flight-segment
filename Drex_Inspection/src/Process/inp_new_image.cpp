//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		13/09/17
//	Descrizione:		Notifica salvataggio di una nuova immagine
//****************************************************************************************************//

// Header del processo
#include <Process/inp.h>

using namespace std;
using namespace nzmqt;
using namespace drex::processes;

// Acquisizione di una nuova immagine
void INProcess::newImage(QString path, double bandwidth, double framerate) {
	_logger->info(string("Bandwidth: ") + to_string(bandwidth) + string(", Framerate: ") + to_string(framerate));
	
	// Segnalo la presenza della nuova immagine
	Message msg;
	msg += "NEW IMAGE";
	msg += (string("FILEPATH = ") + path.toStdString()).c_str();
	_signaling_socket.socket->sendMessage(msg);
}
