//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		06/08/2017
//	Descrizione:		Inizializzazione degli stati
//****************************************************************************************************//

// Header del processo
#include <Process/inp.h>

using namespace std;
using namespace drex::processes;
using namespace drex::state;

bool INProcess::initStatesMap() {
	_logger->info("Init states map");
	
	// Popolo la mappa stato -> StateObject
	StateObject *so;
	
	// Elimino l'eventuale contenuto della mappa
	_state_objects.clear();
	
	// DEFAULT
	so = new StateObject();
	so->init = [this] () { return true; };
	
	// Help
	so->command.insert(CMD_HELP, [this] (const Message &msg, vector<const char *> &parameters) { return this->commandHelp(msg, parameters); });
	so->parameters.insert(CMD_HELP, vector<const char *>());
	
	// Kill
	so->command.insert(CMD_KILL, [this] (const Message &msg, vector<const char *> &parameters) { return this->commandKill(msg, parameters); });
	so->parameters.insert(CMD_KILL, vector<const char *>());
	
	// Get state
	so->command.insert(CMD_GET_STATE, [this] (const Message &msg, vector<const char *> &parameters) { return this->commandGetState(msg, parameters); });
	so->parameters.insert(CMD_GET_STATE, vector<const char *>());
	
	// Set state
	so->command.insert(CMD_SET_STATE, [this] (const Message &msg, vector<const char *> &parameters) { return this->commandSetState(msg, parameters); });
	so->parameters.insert(CMD_SET_STATE, vector<const char *>({
		"(?i)^ *(?<newstate>IDLE|RUN) *$"
	}));
	
	// Save image
	so->command.insert(CMD_SAVE_IMAGE, [this] (const Message &msg, vector<const char *> &parameters) { return this->commandSaveImage(msg, parameters); });
	so->parameters.insert(CMD_SAVE_IMAGE, vector<const char *>({
		"(?i)^ *CAMERA *= *(?<camera>.+) *$"
	}));
	
	so->cleanup = [this] () { return true; };
	_state_objects.insert(INProcess::ST_DEFAULT, so);
	
	// IDLE
	so = new StateObject();
	so->init = [this] () { return this->stateIdleInit(); };
	so->cleanup = [this] () { return this->stateIdleCleanup(); };
	_state_objects.insert(INProcess::ST_IDLE, so);
	
	// RUN
	so = new StateObject();
	so->init = [this] () { return this->stateRunInit(); };
	so->cleanup = [this] () { return this->stateRunCleanup(); };
	_state_objects.insert(INProcess::ST_RUN, so);
	
	return true;
}



