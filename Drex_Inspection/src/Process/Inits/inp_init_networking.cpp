//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		06/08/2017
//	Descrizione:		Inizializzazione dell'interfaccia di rete
//****************************************************************************************************//

// Header del processo
#include <Process/inp.h>

using namespace std;
using namespace nzmqt;
using namespace drex::processes;

bool INProcess::initNetworking() {
	// Inizializzazione dell'interfaccia di rete
	
	// Leggo i parametri dei socket
	const YAML::Node &config_sockets = _configuration["Sockets"];
	
	int default_linger = config_sockets["DefaultLinger"].as<int>();
	
	// Creo i socket
	_context = createDefaultContext();
	_context->start();
	
	// Command socket
	if (!initSocket(_command_socket, config_sockets["Command"], "Command", default_linger)) return false;
	QObject::connect(_command_socket.socket, &ZMQSocket::messageReceived, this, &INProcess::commandSelector, Qt::QueuedConnection);
	if (!startSocket(_command_socket)) return false;
	
	// Logger socket
	if (!initSocket(_logger_socket, config_sockets["Logger"], "Logger", default_linger)) return false;
	if (!startSocket(_logger_socket)) return false;
	
	// Signaling socket
	if (!initSocket(_signaling_socket, config_sockets["Signaling"], "Signaling", default_linger)) return false;
	if (!startSocket(_signaling_socket)) return false;
	
	// Faccio partire il timer per la pubblicazione dello stato
	
	
	return true;
}
