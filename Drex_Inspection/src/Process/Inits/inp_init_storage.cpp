//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		15/09/2017
//	Descrizione:		Inizializzazione dello storage
//****************************************************************************************************//

// Header del processo
#include <Process/inp.h>

using namespace std;
using namespace drex::processes;

// Inizializzo lo storage
bool INProcess::initStorage() {
	// Leggo i parametri dello storage
	const YAML::Node &config_storage = _configuration["Storage"];
	
	// Carico le impostazioni di limite massimo dei video
	_max_video_time = config_storage["MaxVideoTime"].as<uint64_t>();
	
	// Nomi delle cartelle
	_storage_video_dir = QString::fromStdString(config_storage["Video"].as<string>());
	_storage_image_dir = QString::fromStdString(config_storage["Image"].as<string>());
	_storage_trigger_dir = QString::fromStdString(config_storage["Trigger"].as<string>());
	
	// Carico le cartelle dello storage e le creo se non esistono
	auto create_dir = [this] (QDir &base, string path, string errmsg) {
		QString p = QString::fromStdString(path);
		if (!base.cd(p)) {
			if (!base.mkpath(p)) {
				_console->error(errmsg);
				return false;
			}
			base.cd(p);
		}
		return true;
	};
	
	_storage_home = QDir(config_storage["Common"]["Home"].as<string>().c_str());
	_storage_ssd_dir = _storage_home;
	
	// Verifico che esista la cartella SSD
	if (!_storage_ssd_dir.cd(QString::fromStdString(config_storage["Common"]["SSD"].as<string>()))) {
		_console->error("SSD folder doesn't exists");
		return false;
	}
	
	if (!create_dir(_storage_ssd_dir, config_storage["Process"].as<string>(), "Failed to create SSD process folder")) return false;
	
	_storage_video_dir = _storage_ssd_dir;
	_storage_image_dir = _storage_ssd_dir;
	_storage_trigger_dir = _storage_ssd_dir;
	_storage_log_dir = _storage_ssd_dir;
	
	if (!create_dir(_storage_video_dir, config_storage["Video"].as<string>(), "Failed to create Video folder")) return false;
	if (!create_dir(_storage_image_dir, config_storage["Image"].as<string>(), "Failed to create Image folder")) return false;
	if (!create_dir(_storage_trigger_dir, config_storage["Trigger"].as<string>(), "Failed to create Trigger folder")) return false;
	if (!create_dir(_storage_log_dir, config_storage["Log"].as<string>(), "Failed to create Log folder")) return false;
	
	// Non sarebbe necessario creare le cartelle per video, immagini e trigger perché lo fa da solo Drex_Inspection_Capture
	const YAML::Node &cameras = _configuration["Inspection"]["Cameras"];
	for (size_t j = 0; j < cameras.size(); j++) {
		const YAML::Node &current_camera = cameras[j];
		
		bool enabled = current_camera["Enabled"].as<bool>();
		
		if (!enabled) {
			continue;
		}
		
		QString name = QString::fromStdString(current_camera["Name"].as<string>()).toLower();
		
		// Images
		QDir image_dir;
		image_dir = _storage_image_dir;
		if (!create_dir(image_dir, name.toStdString(), string("Failed to create ") + name.toStdString() + string(" camera images folder"))) return false;
		_storage_cameras_image_dirs.insert(name, image_dir);
		
		// Videos
		QDir video_dir;
		video_dir = _storage_video_dir;
		if (!create_dir(video_dir, name.toStdString(), string("Failed to create ") + name.toStdString() + string(" camera videos folder"))) return false;
		_storage_cameras_video_dirs.insert(name, video_dir);
		
		// Triggers
		QDir trigger_dir;
		trigger_dir = _storage_trigger_dir;
		if (!create_dir(trigger_dir, name.toStdString(), string("Failed to create ") + name.toStdString() + string(" camera triggers folder"))) return false;
		_storage_cameras_trigger_dirs.insert(name, trigger_dir);
	}
	
	return true;
}
