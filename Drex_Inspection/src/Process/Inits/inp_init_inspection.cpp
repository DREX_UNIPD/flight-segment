//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		09/08/2017
//	Descrizione:		Inizializzazione delle camere
//****************************************************************************************************//

// Header del processo
#include <Process/inp.h>

using namespace std;
using namespace drex::processes;

bool INProcess::initInspection() {
	_logger->info("Init inspection");
	
	// Sezione inspection
	const YAML::Node &config_inspection = _configuration["Inspection"];
	
	// Percorso dell'eseguibile
	_inspection_executable_path = _storage_home.absoluteFilePath(config_inspection["Executable"].as<string>().c_str());
	_inspection_check_new_period = config_inspection["NewPeriod"].as<int>();
	
	// Verifico se almeno una camera è configurata e abilitata
	const YAML::Node &cameras = config_inspection["Cameras"];
	
	_cameras_enabled = false;
	
	for (size_t i = 0; i < cameras.size(); i++) {
		const YAML::Node &current_camera = cameras[i];
		if (current_camera["Enabled"].as<bool>()) {
			_cameras_enabled = true;
		}
	}
	
	return true;
}
