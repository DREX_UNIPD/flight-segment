//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		14/09/2017
//	Descrizione:		Pubblicazione dei messaggi di log
//****************************************************************************************************//

// Header del processo
#include <Process/inp.h>

using namespace drex::processes;

void INProcess::publishLog(const Message &msg) {
	_logger_socket.socket->sendMessage(msg);
}
