//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		09/08/2017
//	Descrizione:		Salvataggio dell'immagine di una camera
//****************************************************************************************************//

// Header del processo
#include <Process/inp.h>

using namespace std;
using namespace drex::processes;

// Salvataggio dell'ultima immagine acquisita della inspection camera specificata
bool INProcess::commandSaveImage(const Message &msg, vector<const char *> &parameters) {
	_logger->info("Command: Save Image");
	
	// Verifica della sintassi
	QList<QRegularExpressionMatch> matches;
	if (!verifyCommandSyntax(msg, parameters, matches)) {
		return false;
	}
	
	// Estraggo il percorso dove salvare l'immagine
	QString camera = matches[0].captured("camera");
	
	Message ans;
	ans += msg[0];
	
	// Salvo l'immagine
	if (!saveImage(camera)) {
		_logger->error("Failed to save image");
		ans += "ERROR";
		ans += "Failed to save image";
		_command_socket.socket->sendMessage(ans);
		return false;
	}
	
	// Invio la risposta
	ans += "OK";
	_command_socket.socket->sendMessage(ans);
	
	return true;
}
