//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		08/10/2017
//	Descrizione:		Comando per ricevere la lista dei comandi
//****************************************************************************************************//

// Header del processo
#include <Process/inp.h>

using namespace std;
using namespace drex::state;
using namespace drex::processes;

// Chiusura del processo
bool INProcess::commandHelp(const Message &msg, vector<const char *> &) {
	_logger->info("Command: Help");
	
	// Invio la risposta
	Message ans;
	ans += msg[0];
	ans += "OK";
	ans += "Available commands:";
	
	// Esamino tutti gli stati
	for (int i = 0; i < _state_objects.count(); i++) {
		const string state = _state_objects.keys().at(i);
		StateObject *so = _state_objects[state];
		
		ans += (string("State ") + state).c_str();
		
		if (so->command.count() == 0) {
			ans += "    No commands";
			continue;
		}
		
		// Esamino tutti i comandi per lo stato corrente
		for (int j = 0; j < so->command.count(); j++) {
			const string command = so->command.keys().at(j);
			vector<const char *> &parameters = so->parameters[command];
			
			ans += (string("    Command ") + command).c_str();
			
			if (parameters.size() == 0) {
				ans += "        No parameters";
				continue;
			}
			
			// Esamino tutti i parametri per il comando corrente
			for (unsigned int p = 0; p < parameters.size(); p++) {
				const char *param = parameters[p];
				ans += (string("        Parameter \"") + string(param) + string("\"")).c_str();
			}
		}
	}
	
	_command_socket.socket->sendMessage(ans);
	
	return true;
}
