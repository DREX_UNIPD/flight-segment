.:
total 32
drwxrwxrwx 4 root root 32768 Oct  3 23:40 SensorAcquisition

./SensorAcquisition:
total 64
drwxrwxrwx 2 root root 32768 Oct  4 07:12 log
drwxrwxrwx 2 root root 32768 Oct  4 06:57 sensor

./SensorAcquisition/log:
total 431680
-rw-rw-rw- 1 root root  1638400 Oct  4 07:14 sap.log
-rw-rw-rw- 1 root root 10485740 Oct  4 07:12 sap.log.1
-rw-rw-rw- 1 root root 10485719 Oct  4 05:35 sap.log.10
-rw-rw-rw- 1 root root 10485685 Oct  4 05:24 sap.log.11
-rw-rw-rw- 1 root root 10485739 Oct  4 05:14 sap.log.12
-rw-rw-rw- 1 root root 10485685 Oct  4 05:03 sap.log.13
-rw-rw-rw- 1 root root 10485685 Oct  4 04:52 sap.log.14
-rw-rw-rw- 1 root root 10485688 Oct  4 04:41 sap.log.15
-rw-rw-rw- 1 root root 10485759 Oct  4 04:31 sap.log.16
-rw-rw-rw- 1 root root 10485719 Oct  4 04:20 sap.log.17
-rw-rw-rw- 1 root root 10485756 Oct  4 04:09 sap.log.18
-rw-rw-rw- 1 root root 10485709 Oct  4 03:58 sap.log.19
-rw-rw-rw- 1 root root 10485733 Oct  4 07:01 sap.log.2
-rw-rw-rw- 1 root root 10485747 Oct  4 03:47 sap.log.20
-rw-rw-rw- 1 root root 10485750 Oct  4 03:37 sap.log.21
-rw-rw-rw- 1 root root 10485724 Oct  4 03:26 sap.log.22
-rw-rw-rw- 1 root root 10485715 Oct  4 03:15 sap.log.23
-rw-rw-rw- 1 root root 10485716 Oct  4 03:04 sap.log.24
-rw-rw-rw- 1 root root 10485742 Oct  4 02:54 sap.log.25
-rw-rw-rw- 1 root root 10485696 Oct  4 02:43 sap.log.26
-rw-rw-rw- 1 root root 10485684 Oct  4 02:32 sap.log.27
-rw-rw-rw- 1 root root 10485744 Oct  4 02:21 sap.log.28
-rw-rw-rw- 1 root root 10485715 Oct  4 02:11 sap.log.29
-rw-rw-rw- 1 root root 10485738 Oct  4 06:50 sap.log.3
-rw-rw-rw- 1 root root 10485719 Oct  4 02:00 sap.log.30
-rw-rw-rw- 1 root root 10485689 Oct  4 01:49 sap.log.31
-rw-rw-rw- 1 root root 10485700 Oct  4 01:38 sap.log.32
-rw-rw-rw- 1 root root 10485748 Oct  4 01:28 sap.log.33
-rw-rw-rw- 1 root root 10485694 Oct  4 01:17 sap.log.34
-rw-rw-rw- 1 root root 10485760 Oct  4 01:06 sap.log.35
-rw-rw-rw- 1 root root 10485718 Oct  4 00:55 sap.log.36
-rw-rw-rw- 1 root root 10485709 Oct  4 00:45 sap.log.37
-rw-rw-rw- 1 root root 10485696 Oct  4 00:34 sap.log.38
-rw-rw-rw- 1 root root 10485727 Oct  4 00:23 sap.log.39
-rw-rw-rw- 1 root root 10485734 Oct  4 06:40 sap.log.4
-rw-rw-rw- 1 root root 10485686 Oct  4 00:12 sap.log.40
-rw-rw-rw- 1 root root 10485713 Oct  4 00:02 sap.log.41
-rw-rw-rw- 1 root root 10485738 Oct  3 23:51 sap.log.42
-rw-rw-rw- 1 root root 10485699 Oct  4 06:29 sap.log.5
-rw-rw-rw- 1 root root 10485699 Oct  4 06:18 sap.log.6
-rw-rw-rw- 1 root root 10485704 Oct  4 06:07 sap.log.7
-rw-rw-rw- 1 root root 10485758 Oct  4 05:57 sap.log.8
-rw-rw-rw- 1 root root 10485703 Oct  4 05:46 sap.log.9

./SensorAcquisition/sensor:
total 36832
-rw-rw-rw- 1 root root  949132 Oct  4 07:14 barometer.altitude.0
-rw-rw-rw- 1 root root  921776 Oct  4 07:14 barometer.pressure.0
-rw-rw-rw- 1 root root  920995 Oct  4 07:14 barometer.temperature.0
-rw-rw-rw- 1 root root  761824 Oct  4 07:14 gpioexpander.microswitch.0
-rw-rw-rw- 1 root root  761824 Oct  4 07:14 gpioexpander.power_good_5v.0
-rw-rw-rw- 1 root root  761824 Oct  4 07:14 gpioexpander.power_good_inspection_1.0
-rw-rw-rw- 1 root root  761824 Oct  4 07:14 gpioexpander.power_good_inspection_2.0
-rw-rw-rw- 1 root root  761824 Oct  4 07:14 gpioexpander.restore.0
-rw-rw-rw- 1 root root 1048578 Oct  4 00:11 imu.acceleration.0
-rw-rw-rw- 1 root root 1048623 Oct  4 00:42 imu.acceleration.1
-rw-rw-rw- 1 root root 1048605 Oct  4 05:19 imu.acceleration.10
-rw-rw-rw- 1 root root 1048609 Oct  4 05:50 imu.acceleration.11
-rw-rw-rw- 1 root root 1048585 Oct  4 06:21 imu.acceleration.12
-rw-rw-rw- 1 root root 1048599 Oct  4 06:52 imu.acceleration.13
-rw-rw-rw- 1 root root  739160 Oct  4 07:14 imu.acceleration.14
-rw-rw-rw- 1 root root 1048617 Oct  4 01:13 imu.acceleration.2
-rw-rw-rw- 1 root root 1048598 Oct  4 01:43 imu.acceleration.3
-rw-rw-rw- 1 root root 1048622 Oct  4 02:14 imu.acceleration.4
-rw-rw-rw- 1 root root 1048606 Oct  4 02:45 imu.acceleration.5
-rw-rw-rw- 1 root root 1048594 Oct  4 03:16 imu.acceleration.6
-rw-rw-rw- 1 root root 1048597 Oct  4 03:47 imu.acceleration.7
-rw-rw-rw- 1 root root 1048625 Oct  4 04:18 imu.acceleration.8
-rw-rw-rw- 1 root root 1048590 Oct  4 04:48 imu.acceleration.9
-rw-rw-rw- 1 root root 1048610 Oct  4 04:59 imu.angularrate.0
-rw-rw-rw- 1 root root  441467 Oct  4 07:14 imu.angularrate.1
-rw-rw-rw- 1 root root 1048600 Oct  4 00:43 imu.freefall.0
-rw-rw-rw- 1 root root 1048600 Oct  4 01:45 imu.freefall.1
-rw-rw-rw- 1 root root 1048600 Oct  4 02:47 imu.freefall.2
-rw-rw-rw- 1 root root 1048600 Oct  4 03:50 imu.freefall.3
-rw-rw-rw- 1 root root 1048600 Oct  4 04:52 imu.freefall.4
-rw-rw-rw- 1 root root 1048600 Oct  4 05:55 imu.freefall.5
-rw-rw-rw- 1 root root 1048600 Oct  4 06:57 imu.freefall.6
-rw-rw-rw- 1 root root  277592 Oct  4 07:14 imu.freefall.7
-rw-rw-rw- 1 root root  899056 Oct  4 07:14 imu.temperature.0
-rw-rw-rw- 1 root root  168671 Oct  4 07:14 thermometer.temperature(11).0
-rw-rw-rw- 1 root root  168671 Oct  4 07:14 thermometer.temperature(13).0
-rw-rw-rw- 1 root root  168671 Oct  4 07:14 thermometer.temperature(15).0
-rw-rw-rw- 1 root root  168671 Oct  4 07:14 thermometer.temperature(17).0
-rw-rw-rw- 1 root root  168671 Oct  4 07:14 thermometer.temperature(3).0
-rw-rw-rw- 1 root root  168671 Oct  4 07:14 thermometer.temperature(5).0
-rw-rw-rw- 1 root root  168671 Oct  4 07:14 thermometer.temperature(7).0
-rw-rw-rw- 1 root root  168671 Oct  4 07:14 thermometer.temperature(9).0
-rw-rw-rw- 1 root root  276947 Oct  4 07:14 thermometer.voltage(19).0
-rw-rw-rw- 1 root root  949835 Oct  4 07:14 vacuometer.altitude.0
-rw-rw-rw- 1 root root  925072 Oct  4 07:14 vacuometer.pressure.0
-rw-rw-rw- 1 root root  924822 Oct  4 07:14 vacuometer.temperature.0
