Drex_Master
bash
%{sourceDir}/../Drex/scripts/post_build.sh %{buildDir} %{sourceDir} /home/ubuntu/DREX %{CurrentProject:Name} mp

Drex_Inspection
bash
%{sourceDir}/../Drex/scripts/post_build.sh %{buildDir} %{sourceDir} /home/ubuntu/DREX %{CurrentProject:Name} inp

Drex_SensorAcquisition
bash
%{sourceDir}/../Drex/scripts/post_build.sh %{buildDir} %{sourceDir} /home/ubuntu/DREX %{CurrentProject:Name} sap

Drex_Stereovision
bash
%{sourceDir}/../Drex/scripts/post_build.sh %{buildDir} %{sourceDir} /home/ubuntu/DREX %{CurrentProject:Name} svp

Drex_Stereovision_Capture
cp
%{buildDir}/%{CurrentProject:Name} /home/ubuntu/DREX/%{CurrentProject:Name}
