############################################################################################################
#	Team:				Drex
#	Autore:				Loris Bogo
#	Data creazione:		25/04/2017
#	Processo:			(SAP) Sensor Acquisition Process
#	Descrizione:		File di configurazione
############################################################################################################

##################################################
#####	Storage
##################################################
Storage:

	# Impostazioni comuni
	Common: *COMMON_STORAGE

	# Cartella del processo
	Process: "SensorAcquisition"

	# Directory per il salvataggio delle misure
	Sensor: "sensor"

	# Directory per il salvataggio dei log
	Log: "log"

	# Limite di dimensione per i file di storage
#	FilesizeLimit: 1024 # 1 KB
	FilesizeLimit: 1048576 # 1 MB

##################################################
#####	File sink
##################################################
FileSink:

#	Percorso del file [string]
	Name: "sap.log"

#	Dimensione massima del file in byte [int]
	MaxSize: 10485760

#	Massimo numero di file di log [int]
	MaxNumber: 1000

##################################################
#####	Sockets
##################################################
Sockets:

#	DefaultLinger [int]
#	E' il tempo, in millisecondi, che viene concesso al socket
#	dopo un'istruzione di close per svuotare il buffer di messaggi
#	-1 => Infinito

#	Endpoint [string = [@,>]<protocol>://<address>:<port>]
#	@ = bind, > = connect
#	<protocol> = tcp, inproc, ipc
#	<address> = * => tutti gli indirizzi

	DefaultLinger: -1

# Command è il socket che riceve i comandi
	Command:
		Type: *ZMQ_ROUTER
		Connect: false
		IP: "*"
		Port: *SAP_COMMAND_PORT

# Logger è il socket che pubblica i messaggi di log
	Logger:
		Type: *ZMQ_PUB
		Connect: false
		IP: "*"
		Port: *SAP_LOGGER_PORT

# Data è il socket che pubblica i risultati dei sensori
	Data:
		Type: *ZMQ_PUB
		Connect: false
		IP: "*"
		Port: *SAP_DATA_PORT

# Reply è il socket che riceve dati da processi esterni e li replica come propri
	Reply:
		Type: *ZMQ_SUB
		Connect: false
		IP: "*"
		Port: *SAP_REPLY_PORT

##################################################
#####	Publishing settings
##################################################
Publishing:

	# Abilitazione della pubblicazione
	Enabled: true

	# Stato
	State:

		# Abilitazione
		Enabled: true

		# Intervallo di pubblicazione [s]
		Interval: 1

###################################################
######	Sensors
###################################################
Sensors:

	Barometer:

#		Abilitazioni
		Enabled: true
		PressureEnabled: true
		AltitudeEnabled: true
		TemperatureEnabled: true

#		Bus I2C
		Device: "/dev/i2c-0"

#		Indirizzo del dispositivo [int]
#		Indirizzi disponibili: 0x5c, 0x5d
		Address: 0x5d

#		Frequenza di lettura [int]
#		In Hz
#		0 = disattivato
#		Fmax = 1 kHz
		ThermometerFrequency: 1.0
		BarometerFrequency: 1.0

		TemperatureFilename: "barometer.temperature"
		PressureFilename: "barometer.pressure"
		AltitudeFilename: "barometer.altitude"

	Imu:

#		Abilitazioni
		Enabled: true
		AccelerationEnabled: true
		FreeFallEnabled: false
		AngularRateEnabled: true
		TemperatureEnabled: true

#		Bus I2C
		Device: "/dev/i2c-0"

#		Indirizzo del dispositivo [int]
#		Indirizzi disponibili: 0x6a, 0x6b
		Address: 0x6b

#		Frequenza di lettura [int]
#		In Hz
#		0.0 = disattivato
#		Fmax = 1 kHz
		ThermometerFrequency: 1.0
		GyroscopeFrequency: 1.0
		AccelerometerFrequency: 10.0

#		Funzione di autoscale dei sensori [bool]
#		Se l'autoscale e' abilitato viene ignorata la scala fissa
		GyroscopeAutoScale: false
		AcceletometerAutoScale: false

#		Scale fisse dei sensori [int]

#		Giroscopio [dps]: 125, 245, 500, 1000, 2000
		GyroscopeFixedScale: 125
#		Accelerometro [g]: 2, 4, 8, 16
		AcceletometerFixedScale: 2

# TODO: implementare l'antirimbalzo e il segnale di freefall booleano
# TODO: verificare se il suo freefall è meglio
#		Accelerazione sotto la quale viene considerato freefall [g = 9.81 m/s^2]
		FreefallAccelerationLimit: 0.2
#		Numero di conteggi per l'antirimbalzo
		FreefallCounterLimit: 10

		TemperatureFilename: "imu.temperature"
		AccelerationFilename: "imu.acceleration"
		FreefallFilename: "imu.freefall"
		AngularRateFilename: "imu.angularrate"

	Magnetometer:

#		Abilitazioni
		Enabled: false
		MagneticFieldEnable: true
		TemperatureEnabled: false

#		Bus I2C
		Device: "/dev/i2c-0"

#		Indirizzo del dispositivo [int]
#		Indirizzi disponibili: 0x1c, 0x1e
		Address: 0x1e

#		Frequenza di lettura [int]
#		In Hz
#		0 = disattivato
#		Fmax = 1 kHz
		ThermometerFrequency: 0.0
		MagnetometerFrequency: 5.0

#		Funzione di autoscale dei sensori [bool]
#		Se l'autoscale e' abilitato viene ignorata la scala fissa
		AutoScale: false

#		Scale fisse dei sensori [int]

#		Magnetometro [G]: 4, 8, 12, 16
		FixedScale: 4

		TemperatureFilename: "magnetometer.temperature"
		MagneticFieldFilename: "magnetometer.magneticfield"

	Vacuometer:

#		Abilitazione
		Enabled: true
		PressureEnabled: true
		AltitudeEnabled: false
		TemperatureEnabled: true

#		Bus I2C
		Device: "/dev/i2c-0"

#		Indirizzo del dispositivo [int]
#		Indirizzi disponibili: 0x28, 0x36, 0x46
		Address: 0x28

#		Frequenza di lettura [int]
#		In Hz
#		0 = disattivato
#		Fmax = 1 kHz
		Frequency: 1.0

#		Parametri del dispositivo
		PsiMin: 0.0
		PsiMax: 30.0
		PsiRawMin: 0x0666
		PsiRawMax: 0x399a
		TemperatureMin: -50
		TemperatureMax: 150
		TemperatureRawMin: 0
		TemperatureRawMax: 0x07ff

#		Offset sulla pressione da sommare alla lettura
		PressureOffset: 0.0

		PressureFilename: "vacuometer.pressure"
		AltitudeFilename: "vacuometer.altitude"
		TemperatureFilename: "vacuometer.temperature"

	Thermometer:

#		Abilitazione
		Enabled: true

#		Canali abilitati: ID, Temperature (positivo = temperatura, negativo = tensione)
		Channels:
			- 3
			- 5
			- 7
			- 9
			- 11
			- 13
			- 15
			- 17
			- -19

#		Canale Rsense
		RSenseChannel: 2
		RSenseValue: 10000.0

#		Log voltaggio batteria
		BatteryVoltage:
			Enabled: true
			Channel: 19
			IdealRatio: 14.245 # partitore 7.55k e 100k
			CorrectionFactor: 1.0059

#		Bus SPI
		Device: "/dev/spidev0.0"

#		Frequenza di lettura [int]
#		In Hz
#		0 = disattivato
#		Fmax = 1 kHz
		Frequency: 0.2
		
		TemperatureFilename: "thermometer.temperature"
		VoltageFilename: "thermometer.voltage"

###################################################
######	Actuators
###################################################
Actuators:

	DACs:

	#	DAC: Thermalcutter
		DAC_Thermalcutter:

	#		Abilitazione
			Enabled: true

	#		Bus I2C
			Device: "/dev/i2c-0"

	#		Indirizzo del dispositivo [int]
			Address: 0x60

	#	DAC: Heater
		DAC_Heater:

	#		Abilitazione
			Enabled: true

	#		Bus I2C
			Device: "/dev/i2c-0"

	#		Indirizzo del dispositivo [int]
			Address: 0x61

	#	DAC: LED 1
		DAC_Led_1:
	
	#		Abilitazione
			Enabled: true
	
	#		Bus I2C
			Device: "/dev/i2c-0"
	
	#		Indirizzo del dispositivo [int]
			Address: 0x62
	
	#	DAC: LED 2
		DAC_Led_2:
	
	#		Abilitazione
			Enabled: true
	
	#		Bus I2C
			Device: "/dev/i2c-0"
	
	#		Indirizzo del dispositivo [int]
			Address: 0x63

	# Current to voltage conversion for Thermalcutter
	PAM:
		Current:
			min: 0.408
			max: 2.066
		Voltage:
			min: 0.3
			max: 2.4

###################################################
######	Bidirectionals
###################################################
# Modalità dei pin
MODE_DISABLED: &MODE_DISABLED 0
MODE_INPUT: &MODE_INPUT 1
MODE_OUTPUT: &MODE_OUTPUT 2
MODE_PWM: &MODE_PWM 3
MODE_OPEN_DRAIN: &MODE_OPEN_DRAIN 4

Bidirectionals:

	GPIOExpander:

#		Abilitazione
		Enabled: true

#		Bus I2C
		Device: "/dev/i2c-0"

#		Indirizzo del dispositivo [int]
		Address: 0x3e

#		Frequenza di acquisizione
		Frequency: 1.0

#		Configurazione dei pin
		Pins:

#			[Number] Numero del GPIO Expander corrispondente al pin
#			[Name] Nome del pin da usare nei comandi di settaggio
#			[Mode] Modalità del pin
#			[Value] Valore (0 / 1) corrispondente all'uscita digitale
#			[PWM] Percentuale del duty cycle (0.0 .. 100.0)

#			Restore signal
			-   Number: 0
				Name: "RESTORE"
				Mode: *MODE_INPUT
				Value: 0
				PWM: 0.0
				Filename: "gpioexpander.restore"

#			Microswitch
			-   Number: 1
				Name: "MICROSWITCH_"
				Mode: *MODE_INPUT
				Value: 0
				PWM: 0.0
				Filename: "gpioexpander.microswitch"

#			Stereocamera trigger
			-   Number: 2
				Name: "STEREOCAMERA TRIGGER"
				Mode: *MODE_OUTPUT
				Value: 0
				PWM: 0.0
				Filename: "gpioexpander.stereocamera_trigger"

#			Power good DC DC 5 V
			-   Number: 3
				Name: "POWER GOOD 5V"
				Mode: *MODE_INPUT
				Value: 0
				PWM: 0.0
				Filename: "gpioexpander.power_good_5v"

#			Power enable inspection 2
			-   Number: 4
				Name: "POWER ENABLE INSPECTION 2"
				Mode: *MODE_OUTPUT
				Value: 1
				PWM: 0.0
				Filename: "gpioexpander.power_enable_inspection_2"

#			Power good inspection 2
			-   Number: 5
				Name: "POWER GOOD INSPECTION 2"
				Mode: *MODE_INPUT
				Value: 0
				PWM: 0.0
				Filename: "gpioexpander.power_good_inspection_2"

##			Power enable inspection 1
			#-   Number: 6
				#Name: "POWER ENABLE INSPECTION 1"
				#Mode: *MODE_OPEN_DRAIN
				#Value: 0
				#PWM: 0.0
				#Filename: "gpioexpander.power_enable_inspection_1"

#			Power good inspection 1
			-   Number: 7
				Name: "POWER GOOD INSPECTION 1"
				Mode: *MODE_INPUT
				Value: 0
				PWM: 0.0
				Filename: "gpioexpander.power_good_inspection_1"

#			Led 2
			-   Number: 8
				Name: "LED 2"
				Mode: *MODE_PWM
				Value: 0
				PWM: 0.0
				Filename: "gpioexpander.led_2"

#			Led 1
			-   Number: 9
				Name: "LED 1"
				Mode: *MODE_PWM
				Value: 0
				PWM: 0.0
				Filename: "gpioexpander.led_1"

#			Autostart disable
			-   Number: 12
				Name: "AUTOSTART DISABLE"
				Mode: *MODE_OUTPUT
				Value: 0
				PWM: 0.0
				Filename: "gpioexpander.autostart_disable"

#			Power disable jetson
			-   Number: 15
				Name: "POWER DISABLE JETSON"
				Mode: *MODE_OUTPUT
				Value: 0
				PWM: 0.0
				Filename: "gpioexpander.power_disable_jetson"
