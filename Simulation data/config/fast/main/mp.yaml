############################################################################################################
#	Team:				Drex
#	Autore:				Loris Bogo
#	Data creazione:		17/05/2017
#	Processo:			(MP) Master Process
#	Descrizione:		File di configurazione
############################################################################################################

##################################################
#####	Storage
##################################################
Storage:

	# Impostazioni comuni
	Common: *COMMON_STORAGE

	# Cartella del processo
	Process: "Master"

	# Directory per il salvataggio dei log
	Log: "log"

##################################################
#####	File sink
##################################################
FileSink:

#	Percorso del file [string]
	Name: "mp.log"

#	Dimensione massima del file in byte [int]
	MaxSize: 10485760

#	Massimo numero di file di log [int]
	MaxNumber: 1000

##################################################
#####	Board identifier
##################################################
# Identificatore della board [bool]
# true => Main board
# false => Backup board
MainBoard: true
# Modalità manuale [bool]
ManualMode: false
# Modalità manuale per la stereo [bool]
StereovisionManualMode: true

##################################################
#####	Watchdog settings
##################################################
Watchdog:

	# Abilitazione [bool]
	Enabled: false

	# Periodo di polling per il recupero dello stato globale [s]
	PollingPeriod: 10

	# Periodo di tempo che intercorre tra due richieste di stato globale [s]
	KeepalivePeriod: 60

	# Timeout alla richiesta di stato remoto [s]
	AnswerTimeout: 10

	# Periodo di tempo in cui la board remota rimane senza alimentazione [s]
	PowerDownPeriod: 10

	# Perdiodo di tempo in cui la board si riavvia
	BootDelayPeriod: 20

	# Timeout per lo scambio di ruoli dopo il riavvio [s]
	SwitchRolesTimeout: 60

##################################################
#####	Date update
##################################################
DateUpdate:

	Enabled: true
	Period: 2

##################################################
#####	Sockets
##################################################
Sockets:

#	DefaultLinger [int]
#	E' il tempo, in millisecondi, che viene concesso al socket
#	dopo un'istruzione di close per svuotare il buffer di messaggi
#	-1 => Infinito

#	Endpoint [string = [@,>]<protocol>://<address>:<port>]
#	@ = bind, > = connect
#	<protocol> = tcp, inproc, ipc
#	<address> = * => tutti gli indirizzi

	DefaultLinger: -1

# Command è il socket che riceve i comandi
	Command:
		Type: *ZMQ_ROUTER
		Connect: false
		IP: "*"
		Port: *MP_COMMAND_PORT

# Logger è il socket che pubblica i messaggi di log
	Logger:
		Type: *ZMQ_PUB
		Connect: false
		IP: "*"
		Port: *MP_LOGGER_PORT

# Data è il socket che pubblica i dati del processo
	Data:
		Type: *ZMQ_PUB
		Connect: false
		IP: "*"
		Port: *MP_DATA_PORT

# SAP Command è il socket che invia i comandi al sensors locale
	SAPCommand:
		Type: *ZMQ_DEALER
		Connect: true
		IP: *LOCAL_IP
		Port: *SAP_COMMAND_PORT

# SAP Data è il socket che riceve le letture dei sensori in locale
	SAPData:
		Type: *ZMQ_SUB
		Connect: true
		IP: *LOCAL_IP
		Port: *SAP_DATA_PORT

# INP Command è il socket che invia i comandi all'inspection locale
	INPCommand:
		Type: *ZMQ_DEALER
		Connect: true
		IP: *LOCAL_IP
		Port: *INP_COMMAND_PORT

# SVP Command è il socket che invia i comandi alla stereovisione locale
	SVPCommand:
		Type: *ZMQ_DEALER
		Connect: true
		IP: *LOCAL_IP
		Port: *SVP_COMMAND_PORT

# MP Command è il socket che comunica con il Master Process remoto
	MPCommand:
		Type: *ZMQ_DEALER
		Connect: true
		IP: *REMOTE_IP
		Port: *MP_COMMAND_PORT

# MP Command Self è il socket che comunica con il Master Process locale
	MPCommandSelf:
		Type: *ZMQ_DEALER
		Connect: true
		IP: *LOCAL_IP
		Port: *MP_COMMAND_PORT

# Socket per il watchdog
	WDMP:
		Type: *ZMQ_DEALER
		Connect: true
		IP: *REMOTE_IP
		Port: *MP_COMMAND_PORT
	WDSAP:
		Type: *ZMQ_SUB
		Connect: true
		IP: *LOCAL_IP
		Port: *SAP_DATA_PORT
	WDINP:
		Type: *ZMQ_SUB
		Connect: true
		IP: *LOCAL_IP
		Port: *INP_SIGNALING_PORT
	WDSVP:
		Type: *ZMQ_SUB
		Connect: true
		IP: *LOCAL_IP
		Port: *SVP_DATA_PORT

# Socket per l'aggiornamento della data
	GSS:
		Type: *ZMQ_DEALER
		Connect: true
		IP: *GROUND_IP
		Port: *GSS_COMMAND_PORT

##################################################
#####	Publishing settings
##################################################
Publishing:

	# Abilitazione della pubblicazione
	Enabled: true

	# Stato
	State:

		# Abilitazione
		Enabled: true

		# Intervallo di pubblicazione [s]
		Interval: 1

##################################################
#####	Process flow
##################################################
GROUND_ALTITUDE: &GROUND_ALTITUDE 1000.0
STATUS_CHECK_ALTITUDE: &STATUS_CHECK_ALTITUDE 12000.0
DESCENDING_ALTITUDE: &DESCENDING_ALTITUDE 5000.0

# Process flow
ProcessFlow:

	# ------------ #
	# Impostazioni
	# ------------ #

	# Fonte altitudine per soglie di cambio stato
	BarometerAltitude: false
	VacuometerAltitude: true

	# ----- #
	# Stati
	# ----- #

	# ----- Boot (BOOT) ----- #
	# Ritardo per l'avvio del processo Inspection [s]
	BootInspectionStartDelay: 30

	# ----- First (FST) ----- #
	# Altezza oltre la quale non deve andare la prima acquisizione in condizioni nominali [m]
	FirstAltitudeLimit: *GROUND_ALTITUDE

	# ----- Ground (GND) ----- #
	# Altezza oltre la quale viene considerato terminato il ground [m]
	GroundAltitudeLimit: *GROUND_ALTITUDE

	# ----- Pre-deployment (PRE) ----- #
	# Altezza alla quale inizia la procedura di status check [m]
	PredeploymentAltitudeLimit: *STATUS_CHECK_ALTITUDE
	# Timeout per il safety timer del pre-deployment [s]
	PredeploymentTimeout: 2300 # 2200 è il tempo effettivo

	# ----- Status check (SCH) ----- #
	# Timeout per lo status check [s]
	StatusCheckPeriod: 10 # per arrivare a 20 k da 12 k
	# Intervallo di segnalazione [ms]
	StatusCheckSignalingInterval: 1000

	# ----- Deployment (DEP) ----- #
	OnlyMainThermalcutter: true
	# Periodo di durata del thermal cutter [ms]
	MainThermalCutterPeriod: 30000
	BackupThermalCutterPeriod: 30000
	# Timeout per la verifica del deployment tramite microswitch [s]
	MainThermalCutterTimeout: 40
	BackupThermalCutterTimeout: 40
	# Parametri DAC per i thermal cutter
	# Primario
	MainThermalCutterDAC: "THERMALCUTTER"
	MainThermalCutterCurrent: 1.8

	# ----- Post-deployment (POS) ----- #
	# Safety timer per il cutoff [s]
	PostdeploymentSafetyTimeout: 500
	# Intervallo di segnalazione [ms]
	PostdeploymentSignalingInterval: 1000

	# ----- Cut-off (COF) ----- #
	# Durata del cutoff [s]
	CutoffPeriod: 10
	# Intervallo di segnalazione [ms]
	CutoffSignalingInterval: 1000

	# ----- Descending (DES) ----- #
	# Limite di altezza oltre il quale parte il timeout nominale di spegnimento [m]
	DescendingAltitudeLimit: *DESCENDING_ALTITUDE

	# Timeout per lo spegnimento definitivo [s]
	DescendingSafetyTimeout: 2400 # 1625 + 400
	DescendingNominalTimeout: 10
	# Intervallo di segnalazione [ms]
	DescendingSignalingInterval: 1000

	# ----- Shutdown (SHD) ----- #
	# Timeout per il lancio dello shutdown [s]
	ShutdownTimeout: 10
	ShutdownInspectionDirs:
		- "Inspection/trigger/left"
		- "Inspection/trigger/right"
		- "Inspection/trigger/feed"




















