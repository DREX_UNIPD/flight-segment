############################################################################################################
#	Team:				Drex
#	Autore:				Loris Bogo
#	Data creazione:		06/08/2017
#	Processo:			(SVP) Stereovision Process
#	Descrizione:		File di configurazione
############################################################################################################

##################################################
#####	Storage
##################################################
Storage:

	# Impostazioni comuni
	Common: *COMMON_STORAGE

	# Cartella del processo
	Process: "Stereovision"

	# Directory per il salvataggio della depth map acquisita
	DepthMap: "depth_map"

	# Directory per il salvataggio dei log
	Log: "log"

	# Limite di dimensione per i file di storage
#	FilesizeLimit: 1024 # 1 KB
	FilesizeLimit: 1048576 # 1 MB

##################################################
#####	File sink
##################################################
FileSink:

#	Percorso del file [string]
	Name: "svp.log"

#	Dimensione massima del file in byte [int]
	MaxSize: 10485760

#	Massimo numero di file di log [int]
	MaxNumber: 1000

##################################################
#####	Sockets
##################################################
Sockets:

#	DefaultLinger [int]
#	E' il tempo, in millisecondi, che viene concesso al socket
#	dopo un'istruzione di close per svuotare il buffer di messaggi
#	-1 => Infinito

#	Endpoint [string = [@,>]<protocol>://<address>:<port>]
#	@ = bind, > = connect
#	<protocol> = tcp, inproc, ipc
#	<address> = * => tutti gli indirizzi

	DefaultLinger: -1

# Command è il socket che riceve i comandi
	Command:
		Type: *ZMQ_ROUTER
		Connect: false
		IP: "*"
		Port: *SVP_COMMAND_PORT

# Logger è il socket che pubblica i messaggi di log
	Logger:
		Type: *ZMQ_PUB
		Connect: false
		IP: "*"
		Port: *SVP_LOGGER_PORT

# Data è il socket che invia i risultati della stereovisione
	Data:
		Type: *ZMQ_PUB
		Connect: false
		IP: "*"
		Port: *SVP_DATA_PORT

##################################################
#####	Publishing settings
##################################################
Publishing:

	# Abilitazione della pubblicazione
	Enabled: true

	# Stato
	State:

		# Abilitazione
		Enabled: true

		# Intervallo di pubblicazione [s]
		Interval: 1

##################################################
#####	Stereovision
##################################################
Stereovision:

	# Abilitazione della stereovisione
	Enabled: true

	# Divider
	Divider: 4

	# Nome del file per lo storage della depth map
	DepthMapFilename: "depth_map"

	# Percorso del programma di cattura immagini
	CaptureProcess: "DREX/Drex_Stereovision_Capture"

	# Periodo per la verifica della presenza delle immagini [ms]
	ImageCheckPeriod: 25

	# Parametri ottici della telecamera
	CameraMatrix:
		fx: 4.7542965295768745e+03
		fy: 4.7822689461863984e+03
		cx: 1.5682253342923068e+03
		cy: 1.4446192507661333e+03

	DistortionCoefficients:
		- -1.1072164198330427e-01
		- -1.9169101637647601e-01
		- 5.3038270098660096e-03
		- -5.0415850443485963e-03
		- 2.2524458690489454e-01

	# ArUco dictionary
	ArucoDictionary: "ARUCO_MIP_36h12"

	# Marker size in m
	MarkerSize: 0.030

	# Dimensioni dell'immagine
	Width: 3856
	Height: 2764

	# Dimensioni della scalebar in mm
	Scalebar:
		Width: 145.0
		Height: 14.0

	# Impostazioni delle camere
	Left:
		Name: "left"
		Serial: "15710720"
		Mode: 2 # 0 = 640x480, 1 = 1920x1080, 2 = 3856x2764 (10M)
		Triggers: "left/triggers"
		Images: "left/images"
		Extension: "jpg"
		ScalebarROI: # ROI Scalebar (misure in percentuale rispetto le dimensioni dell'immagine)
			x: 15.0
			y: 60.0
			width: 50.0
			height: 30.0

	Right:
		Name: "right"
		Serial: "16710458"
		Mode: 2 # 0 = 640x480, 1 = 1920x1080, 2 = 3856x2764 (10M)
		Triggers: "right/triggers"
		Images: "right/images"
		Extension: "jpg"
		ScalebarROI: # ROI Scalebar (misure in percentuale rispetto le dimensioni dell'immagine)
			x: 33.71
			y: 65.12
			width: 46.68
			height: 25.33

	Both:
		Triggers: "both/triggers"
		Images: "both/images"

	# Delay prima dell'acquisizione [ms]
	Delay: 5000

	## Periodo a livello logico alto del trigger [ms]
	#UpTime: 10

	## Attesa prima dell'acquisizione [ms]
	#WaitBeforeAcquisition: 100

	# LED
	LED:

		# Abilitazione
		Enabled: true

		# Modalità PWM (se false è comandato da DAC)
		PWM: true

		# Limiti
		MaxVoltage: 1.5
		MaxPWM: 50

		# ROI per il calcolo della luminosità
		ROI:
			x: 0
			y: 0
			width: 100
			height: 100
