#!/bin/bash

# # Verifico il numero di parametri
if [ "$#" -ne 5 ]; then
	echo "Illegal number of parameters"
	exit -1
fi

# # Assegno i parametri alle variabili

buildDir=$1
sourceDir=$2
installDir=$3
projectName=$4
processName=$5

# # Creo le cartelle necessarie

# Cartelle per i file di configurazione
mkdir -p $buildDir/config
mkdir -p $installDir/config

# Cartelle per gli script di inizializzazione
mkdir -p $buildDir/init_scripts
mkdir -p $installDir/init_scripts

# # Copio i file

# Eseguibile
cp $buildDir/$projectName $installDir/$projectName

# File di configurazione comune
cp $sourceDir/../Drex/config/common.yaml $buildDir/config/common.yaml
#cp $sourceDir/../Drex/config/common.yaml $installDir/config/common.yaml

# File di configurazione specifico del processo
cp $sourceDir/config/$processName.yaml $buildDir/config/$processName.yaml
#cp $sourceDir/config/$processName.yaml $installDir/config/$processName.yaml

# Script di inizializzazione
cp $sourceDir/init_scripts/$processName.sh $buildDir/init_scripts/$processName.sh
cp $sourceDir/init_scripts/$processName.sh $installDir/init_scripts/$processName.sh

# Fine
exit 0

