//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		05/08/17
//	Descrizione:		Sink personalizzato per l'invio di log tramite publisher socket
//****************************************************************************************************//

// Header della classe
#include <PublisherSink/publisher_sink.h>

// Flush
void PublisherSink::flush() {}

// Log
void PublisherSink::log(const spdlog::details::log_msg &msg) {
	// Your code here. 
	// details::log_msg is a struct containing the log entry info like level, timestamp, thread id etc.
	// msg.formatted contains the formatted log.
	// msg.raw contains pre formatted log
	Message message;
	message += "LOG";
	message += msg.raw.c_str();
	emit requestSendLogMessage(message);
}
