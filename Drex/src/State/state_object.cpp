//****************************************************************************************************//
// Team:			Drex
// Autore:			Loris Bogo
// Data creazione:	30/07/17
// Descrizione:		Struttura che racchiude le informazioni di uno stato
//****************************************************************************************************//

// Header DREX
#include <State/state_object.h>

using namespace std;
using namespace drex::state;

// Serializzazione dello stato globale
QString GlobalStateObject::serialize() {
	return
		masterProcessState +
		"." +
		sensorAcquisitionProcessState +
		"." +
		inspectionProcessState +
		"." +
		stereoVisionProcessState
	;
}

// Deserializzazione dello stato globale
bool GlobalStateObject::deserialize(QString serialized) {
	const char *syntax = "(?i)^ *(?<mp>\\w+) *\\. *(?<sap>\\w+) *\\. *(?<inp>\\w+) *\\. *(?<svp>\\w+) *$";
	
	// Interpreto i parametri del comando
	QRegularExpression rx(syntax);
	QRegularExpressionMatch match = rx.match(serialized);
	
	// Verifico la sintassi del comando
	if (!match.hasMatch()) {
		return false;
	}
	
	// Creo l'oggetto GSO
	masterProcessState = match.captured("mp");
	sensorAcquisitionProcessState = match.captured("sap");
	stereoVisionProcessState = match.captured("svp");
	inspectionProcessState = match.captured("inp");
	
	return true;
}

// Verifica della presenza dello stato globale
bool GlobalStateObject::isEmpty() {
	return
		masterProcessState.isEmpty() ||
		sensorAcquisitionProcessState.isEmpty() ||
		stereoVisionProcessState.isEmpty() ||
		inspectionProcessState.isEmpty()
	;
}
