//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		23/07/17
//	Descrizione:		Utilità per l'utilizzo dei socket
//****************************************************************************************************//

#ifndef SOCKET_UTILS_H
#define SOCKET_UTILS_H

#include <nzmqt/nzmqt.hpp>

namespace drex {
namespace socket {

// Struttura per l'aggregazione dei dati del socket
struct SocketInfo {
	nzmqt::ZMQSocket *socket;
	bool connect;
	std::string ip;
	int port;
	int linger;
	std::string name;
};

}
}

#endif // SOCKET_UTILS_H

