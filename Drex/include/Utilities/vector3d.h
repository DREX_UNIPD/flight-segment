//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		30/04/17
//	Descrizione:		Struttura che rappresenta un vettore tridimensionale
//****************************************************************************************************//

#ifndef VECTOR3D_H
#define VECTOR3D_H

// Header STL
#include <ostream>
#include <cmath>

// Vettore a 3 dimensioni
template <typename T>
struct Vector3D {
private:
	T _component[3];
public:
	T &x = _component[0];
	T &y = _component[1];
	T &z = _component[2];
	
	T &operator [](int index) { return _component[index]; }
	
	Vector3D() {}
	
	Vector3D(Vector3D &other) {
		this->_component[0] = other._component[0];
		this->_component[1] = other._component[1];
		this->_component[2] = other._component[2];
	}
	
	T &absoluteMax() {
		int index = 0;
		T *current = &_component[index];
		for (; index < 3; index++) {
			if (std::abs(_component[index]) > std::abs(*current)) {
				current = &_component[index];
			}
		}
		return *current;
	}
	
	T magnitude() {
		return sqrt(x * x + y * y + z * z);
	}
	
	friend std::ostream &operator<<(std::ostream &str, const Vector3D<T> &v) { 
		str << "(" << v.x << ", " << v.y << ", " << v.z << ")";
		return str;
	}
};

#endif // VECTOR3D_H
