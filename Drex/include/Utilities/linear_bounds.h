//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		02/05/17
//	Descrizione:		Struttura che contiene i limiti di una scala lineare
//****************************************************************************************************//

#ifndef LINEAR_BOUNDS_H
#define LINEAR_BOUNDS_H

// Struttura contenente i valori
struct LinearBounds {
private:
	double _component[2];
public:
	double &lower = _component[0];
	double &upper = _component[1];
	
	LinearBounds() {}
	
	LinearBounds(double lower, double upper) {
		_component[0] = lower;
		_component[1] = upper;
	}
	
	LinearBounds(const LinearBounds &other) {
		_component[0] = other._component[0];
		_component[1] = other._component[1];
	}
	
	LinearBounds &operator =(const LinearBounds &other) {
		_component[0] = other._component[0];
		_component[1] = other._component[1];
		return *this;
	}
	
//	bool operator ==(const LinearBounds &other) const {
//		return (other.lower == this->lower) && (other.upper == this->upper);
//	}
	
//	bool operator !=(const LinearBounds &other) const {
//		return !(*this == other);
//	}
	
	double &operator [](int index) { return _component[index]; }
};

#endif // LINEAR_BOUNDS_H
