//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		30/04/17
//	Descrizione:		Struttura che contiene i parametri per una conversione lineare
//****************************************************************************************************//

#ifndef LINEAR_CONVERSION_H
#define LINEAR_CONVERSION_H

// Struttura per la conversione lineare
struct LinearConversion {
private:
	double _component[2];
public:
	double &offset = _component[0];
	double &scale = _component[1];
	
	LinearConversion() {}
	
	LinearConversion(double offset, double scale) {
		_component[0] = offset;
		_component[1] = scale;
	}
	
	LinearConversion(const LinearConversion &other) {
		_component[0] = other._component[0];
		_component[1] = other._component[1];
	}
	
	LinearConversion &operator =(const LinearConversion &other) {
		_component[0] = other._component[0];
		_component[1] = other._component[1];
		return *this;
	}
	
	double &operator [](int index) { return _component[index]; }
};

#endif // LINEAR_CONVERSION_H
