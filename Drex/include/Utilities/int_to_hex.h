//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		21/09/17
//	Descrizione:		Metodo per la conversione di un intero in una stringa esadecimale
//****************************************************************************************************//

#ifndef INT_TO_HEX_H
#define INT_TO_HEX_H

#include <string>
#include <sstream>

// Metodo ausiliario:
// conversione di un intero in una string in formato esadecimale
template<typename T>
std::string int_to_hex(T number, int digits = -1, std::string pre = "") {
	std::stringstream stream;
	if (digits < 0) {
		digits = sizeof(T) * 2;
	}
	stream << pre;
	for (int i = digits - 1; i >= 0; i--) {
		int64_t byte, shift;
		shift = i * 4;
		byte = number & (0x0f << shift);
		byte >>= shift;
		stream << std::hex << byte;
	}
	
	return stream.str();
}

#endif // INT_TO_HEX_H
