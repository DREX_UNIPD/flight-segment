//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		05/08/17
//	Descrizione:		Sink personalizzato per l'invio di log tramite publisher socket
//****************************************************************************************************//

#ifndef PUBLISHER_SINK_H
#define PUBLISHER_SINK_H

// Header Qt
#include <QtCore>

// Header per logging
#include <spdlog/spdlog.h>
#include <spdlog/sinks/sink.h>

// Publisher Sink
class PublisherSink : public QObject, public spdlog::sinks::sink {
	Q_OBJECT
	
	using Message = QList<QByteArray>;
	
public:
	// Funzione di log
	void log(const spdlog::details::log_msg& msg) override;
	
	// Funzione di flush
	void flush();
	
signals:
	// Richiesta di invio messaggio di log
	void requestSendLogMessage(const Message &log_msg);
};

#endif // PUBLISHER_SINK_H
