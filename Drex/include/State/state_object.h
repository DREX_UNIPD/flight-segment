//****************************************************************************************************//
// Team:			Drex
// Autore:			Loris Bogo
// Data creazione:	25/04/17
// Descrizione:		Struttura che racchiude le informazioni di uno stato
//****************************************************************************************************//

#ifndef STATE_OBJECT_H
#define STATE_OBJECT_H

// Header Qt
#include <QtCore>

// Header STL
#include <string>
#include <functional>

namespace drex {
namespace state {

// Struttura StateObject
// Contiene le funzioni di inizializzazione, chiusura e i comandi dello stato
struct StateObject {
	using Message = QList<QByteArray>;
	
public:
	// Funzioni dello stato
	std::function<bool()> init;
	std::function<bool()> cleanup;
	QMap<std::string, std::function<bool(const Message &, std::vector<const char *> &)>> command;
	QMap<std::string, std::vector<const char *>> parameters;
};

// Struttura GlobalStateObject
// Contiene tutte le informazioni necessarie al ripristino dello stato della board
struct GlobalStateObject {
public:
	QString masterProcessState;
	QString sensorAcquisitionProcessState;
	QString stereoVisionProcessState;
	QString inspectionProcessState;
	
	QString serialize();
	bool deserialize(QString serialized);
	bool isEmpty();
};

}
}

#endif // STATE_OBJECT_H
