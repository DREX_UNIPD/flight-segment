//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		22/07/17
//	Descrizione:		Classe che rappresenta il vacuometro 86BSD
//****************************************************************************************************//

// Header della classe
#include <Sensor/sap_vacuometer.h>

// Header STL
#include <cmath>

using namespace std;
using namespace drex::sensor;

Vacuometer::Vacuometer(const string device, const unsigned char address, bool pressure_enabled, bool altitude_enabled, bool temperature_enabled, QDir storage_dir, qint64 filesize_limit, string pressure_filename, string altitude_filename, string temperature_filename, double psi_min, double psi_max, uint16_t psi_raw_min, uint16_t psi_raw_max, double temperature_min, double temperature_max, uint16_t temperature_raw_min, uint16_t temperature_raw_max, double pressure_offset, const double frequency, QObject *parent) :
	QObject(parent),
	GenericSensor(),
	I2CDevice(device, address),
	_pressure_enabled(pressure_enabled),
	_altitude_enabled(altitude_enabled),
	_temperature_enabled(temperature_enabled),
	_storage_dir(storage_dir),
	_filesize_limit(filesize_limit),
	_pressure_filename(QString::fromStdString(pressure_filename)),
	_altitude_filename(QString::fromStdString(altitude_filename)),
	_temperature_filename(QString::fromStdString(temperature_filename)),
	_publishing(false),
	_frequency(frequency),
	_psi_min(psi_min),
	_psi_max(psi_max),
	_psi_raw_min(psi_raw_min),
	_psi_raw_max(psi_raw_max),
	_temperature_min(temperature_min),
	_temperature_max(temperature_max),
	_temperature_raw_min(temperature_raw_min),
	_temperature_raw_max(temperature_raw_max),
	_pressure_offset(pressure_offset) {
	_logger = spdlog::get("logger");
	
	if (_pressure_enabled || _altitude_enabled || _temperature_enabled) QObject::connect(&_timer, &QTimer::timeout, this, &Vacuometer::publishPressureAndTemperature, Qt::QueuedConnection);
}

Vacuometer::~Vacuometer() {
	if (_pressure_enabled || _altitude_enabled || _temperature_enabled) QObject::disconnect(&_timer, &QTimer::timeout, this, &Vacuometer::publishPressureAndTemperature);
}

// Inizializzazione
bool Vacuometer::initDevice() {
	_logger->info("[Vacuometer] init device");
	
	return true;
}

// Inizio lettura
bool Vacuometer::startReading() {
	_logger->info("[Vacuometer] start reading");
	
	// Verifico se l'acquisizione e' gia' in corso
	if (_publishing) {
		_logger->warn("[Vacuometer] already publishing");
		return true;
	}
	_publishing = true;
	
	// Avvio i timer se la frequenza specificata è maggiore di zero
	auto start_timer = [this] (const char *name, double f, QTimer &t) {
		if (f > 0.0) {
			int ms;
			try {
				ms = (int) (1000 / f);
				if (ms <= 0) ms = 1;
				t.start(ms);
				return true;
			}
			catch (exception e) {
				string err;
				err += "[Vacuometer] cannot start ";
				err += name;
				err += " timer: ";
				err += e.what();
				_logger->warn(err);
				return false;
			}
		}
		return true;
	};
	
	if (_pressure_enabled || _altitude_enabled || _temperature_enabled) {
		if (!start_timer("vacuometer", _frequency, _timer)) {
			return false;
		}
	}
	
	// Creo se non esistono, altrimenti apro i file di storage
	auto init_storage = [this] (QFile &file, QDir &storage_dir, QString &filename, qint64 &filesize, int &index) {
		bool exists;
		
		if (!_getNextFreeIndex(storage_dir, filename, filesize, index, exists)) {
			_logger->error("Failed to get next free file index");
			return false;
		}
		
		// Imposto il nome del file
		file.setFileName(storage_dir.absoluteFilePath(filename + QString(".") + QString::number(index)));
		
		if (exists) {
			// Se esiste lo apro in modalità Append
			file.open(QIODevice::Append);
		}
		else {
			file.open(QIODevice::WriteOnly);
		}
		
		return true;
	};
	
	if (_temperature_enabled) {
		if (!init_storage(_temperature_file, _storage_dir, _temperature_filename, _filesize_limit, _temperature_file_index)) {
			return false;
		}
	}
	
	if (_pressure_enabled) {
		if (!init_storage(_pressure_file, _storage_dir, _pressure_filename, _filesize_limit, _pressure_file_index)) {
			return false;
		}
	}
	
	if (_altitude_enabled) {
		if (!init_storage(_altitude_file, _storage_dir, _altitude_filename, _filesize_limit, _altitude_file_index)) {
			return false;
		}
	}
	
	return true;
}

// Fine lettura
bool Vacuometer::stopReading() {
	_logger->info("[Vacuometer] stop reading");
	
	// Verifico se l'acquisizione e' in corso
	if (!_publishing) {
		_logger->warn("[Vacuometer] not publishing yet");
		return true;
	}
	_publishing = false;
	
	// Fermo il timer attivo
	if (_timer.isActive()) {
		_timer.stop();
	}
	
	// Chiudo i file
	if (_pressure_enabled) _pressure_file.close();
	if (_altitude_enabled) _altitude_file.close();
	if (_temperature_enabled) _temperature_file.close();
	
	return true;
}

// Lettura della pressione
bool Vacuometer::getRawPressureAndTemperature(uint16_t &pressure, uint16_t &temperature) {
	_logger->info("[Vacuometer] get raw pressure and temperature");
	
	// Leggo il dato di pressione e temperatura in formato numerico
	// e lo restituisco senza elaborazioni
	
	// Apro il dispositivo
	if (!i2cOpen()) {
		return false;
	}
	
	// Leggo il dato grezzo
	__u8 buffer[4];
	if (!i2cGetRaw(buffer, ARRAY_SIZE(buffer))) {
		i2cClose();
		return false;
	}
	
	// Estraggo la pressione
	pressure = buffer[1] + (buffer[0] << 8);
	
	// Estraggo la temperatura
	temperature = (buffer[3] >> 5) + (buffer[2] << 3);
	
	// Chiudo il dispositivo
	if (!i2cClose()) {
		return false;
	}
	
	return true;
}
bool Vacuometer::getPressureAndTemperature(double &pressure, double &temperature) {
	uint16_t raw_pressure, raw_temperature;
	
	// Acquisisco il valore grezzo di pressione e temperatura
	if (!getRawPressureAndTemperature(raw_pressure, raw_temperature)) {
		return false;
	}
	
	// Calcolo la pressione in PSI
	double psi;
	psi = _psi_min + ((_psi_max - _psi_min) / (_psi_raw_max - _psi_raw_min)) * (raw_pressure - _psi_raw_min);
	
	// La converto da PSI a mBar
	pressure = psi * PSI_TO_MBAR_RATIO;
	
	// Calcolo la temperatura in °C
	temperature = _temperature_min + ((_temperature_max - _temperature_min) / (_temperature_raw_max - _temperature_raw_min)) * (raw_temperature - _temperature_raw_min);
	
	return true;
}
bool Vacuometer::getAltitudeFromPressure(double &pressure, double &altitude, double offset) {
	// Formula di conversione mBar -> m
	altitude = (1 - std::pow((pressure + offset) / SEA_LEVEL_PRESSURE, PRESSURE_EXP)) * PRESSURE_FACTOR * FT_TO_M_RATIO;
	return true;
}

// Pubblicazione dei dati
void Vacuometer::publishPressureAndTemperature() {
	_logger->info("[Vacuometer] Publish pressure and temperature");
	
	double pressure, temperature, altitude;
	
	if (!getPressureAndTemperature(pressure, temperature)) {
		_logger->warn("[Vacuometer] Failed to read pressure and temperature");
		return;
	}
	
	// Creo il messaggio e lo invio
	Message msg;
	
	if (_pressure_enabled) {
		msg.clear();
		msg += "VACUOMETER/PRESSURE";
		msg += (string("PRESSURE = ") + to_string(pressure)).c_str();
		emit requestPublishMessage(msg);
		
		// Salvo il dato su file
		QString record;
		record += QString("[") + QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss.zzz") + QString("] ");
		record += QString::number(pressure) + QString("\n");
		_pressure_file.write(record.toStdString().c_str());
		
		// Se il file supera la dimensione massima consentita lo chiudo e apro un nuovo file
		if (_pressure_file.size() >= _filesize_limit) {
			_pressure_file.close();
			_pressure_file_index++;
			_pressure_file.setFileName(_storage_dir.absoluteFilePath(_pressure_filename + QString(".") + QString::number(_pressure_file_index)));
			_pressure_file.open(QIODevice::WriteOnly);
		}
	}
	
	if (_altitude_enabled) {
		if (!getAltitudeFromPressure(pressure, altitude, _pressure_offset)) {
			_logger->warn("[Vacuometer] Failed to convert pressure in altitude");
			return;
		}
		
		msg.clear();
		msg += "VACUOMETER/ALTITUDE";
		msg += (string("ALTITUDE = ") + to_string(altitude)).c_str();
		emit requestPublishMessage(msg);
		
		// Salvo il dato su file
		QString record;
		record += QString("[") + QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss.zzz") + QString("] ");
		record += QString::number(altitude) + QString("\n");
		_altitude_file.write(record.toStdString().c_str());
		
		// Se il file supera la dimensione massima consentita lo chiudo e apro un nuovo file
		if (_altitude_file.size() >= _filesize_limit) {
			_altitude_file.close();
			_altitude_file_index++;
			_altitude_file.setFileName(_storage_dir.absoluteFilePath(_altitude_filename + QString(".") + QString::number(_altitude_file_index)));
			_altitude_file.open(QIODevice::WriteOnly);
		}
		
	}
	
	if (_temperature_enabled) {
		msg.clear();
		msg += "VACUOMETER/TEMPERATURE";
		msg += (string("TEMPERATURE = ") + to_string(temperature)).c_str();
		emit requestPublishMessage(msg);
		
		// Salvo il dato su file
		QString record;
		record += QString("[") + QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss.zzz") + QString("] ");
		record += QString::number(temperature) + QString("\n");
		_temperature_file.write(record.toStdString().c_str());
		
		// Se il file supera la dimensione massima consentita lo chiudo e apro un nuovo file
		if (_temperature_file.size() >= _filesize_limit) {
			_temperature_file.close();
			_temperature_file_index++;
			_temperature_file.setFileName(_storage_dir.absoluteFilePath(_temperature_filename + QString(".") + QString::number(_temperature_file_index)));
			_temperature_file.open(QIODevice::WriteOnly);
		}
	}
}

// Metodi per il salvataggio dei dati
bool Vacuometer::_getNextFreeIndex(QDir &folder, QString &filename, qint64 &size_limit, int &index, bool &file_exists) {
	QStringList filters;
	filters += filename + QString(".*");
	
	QFileInfoList files;
	files = folder.entryInfoList(filters);
	
	// Non esistono file a cui collegarsi
	if (files.count() == 0) {
		index = 0;
		file_exists = false;
		return true;
	}
	
	// Trovo l'indice massimo
	QRegularExpression regex;
	QRegularExpressionMatch match;
	int max_index = 0;
	QFileInfo max_fi;
	regex.setPattern(QString("^") + filename + QString("\\.(?<index>\\d+)$"));
	for (int i = 0; i < files.count(); i++) {
		match = regex.match(files[i].fileName());
		if (!match.hasMatch()) continue;
		int temp;
		temp = match.captured("index").toInt();
		if (max_index < temp) {
			max_fi = files[i];
			max_index = temp;
		}
	}
	
	if (max_fi.size() >= size_limit) {
		index = max_index + 1;
		file_exists = false;
	}
	else {
		index = max_index;
		file_exists = true;
	}
	
	return true;
}
