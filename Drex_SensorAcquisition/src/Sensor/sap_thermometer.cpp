//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		16/07/17
//	Descrizione:		Classe che rappresenta il termometro Linear LTC2983
//****************************************************************************************************//

// Header della classe
#include <Sensor/sap_thermometer.h>

// Header per la SPI
#include <sys/ioctl.h>

// Header DREX
#include <Utilities/int_to_hex.h>

using namespace std;
using namespace drex::sensor;

#define FOURTH_BYTE(a) ((a & 0xff000000) >> 24)
#define THIRD_BYTE(a) ((a & 0xff0000) >> 16)
#define SECOND_BYTE(a) ((a & 0xff00) >> 8)
#define FIRST_BYTE(a) (a & 0xff)

Thermometer::Thermometer(const string device, const RSenseChannelAssignment sense_resistor_channel, const double sense_resistor_value, const QList<ChannelConfiguration> registered_channels, QDir storage_dir, qint64 filesize_limit, string temperature_filename, string voltage_filename, const bool battery_voltage_enabled, const __u8 battery_voltage_channel, const double battery_voltage_ideal_ratio, const double battery_voltage_correction_factor, const double frequency, QObject *parent) :
	QObject(parent),
	GenericSensor(),
	SPIDevice(device),
	_sense_resistor_channel(sense_resistor_channel),
	_sense_resistor_value(sense_resistor_value),
	_storage_dir(storage_dir),
	_filesize_limit(filesize_limit),
	_temperature_filename(QString::fromStdString(temperature_filename)),
	_voltage_filename(QString::fromStdString(voltage_filename)),
	_converting_registered_channels(registered_channels),
	_battery_voltage_enabled(battery_voltage_enabled),
	_battery_voltage_channel(battery_voltage_channel),
	_battery_voltage_ideal_ratio(battery_voltage_ideal_ratio),
	_battery_voltage_correction_factor(battery_voltage_correction_factor),
	_publishing(false),
	_frequency(frequency) {
	_logger = spdlog::get("logger");
	
	_logger->info(string("Constructor: Registered channels = ") + to_string(_converting_registered_channels.count()));
	
	_convertReset();
	QObject::connect(&_converting_timer, &QTimer::timeout, this, &Thermometer::_tryPublish, Qt::QueuedConnection);
	
	QObject::connect(&_timer, &QTimer::timeout, this, &Thermometer::publishMultiple, Qt::QueuedConnection);
}

// Distruttore
Thermometer::~Thermometer() {
	QObject::disconnect(&_converting_timer, &QTimer::timeout, this, &Thermometer::_tryPublish);
	QObject::disconnect(&_timer, &QTimer::timeout, this, &Thermometer::publishMultiple);
}

// Inizializzazione
bool Thermometer::initDevice() {
	_logger->info("[Thermometer] init device");
	
	// Apro il dispositivo
	if (!spiOpen()) {
		return false;
	}
	
	// Inizializzo i parametri di connessione
	if (!spiInit()) {
		return false;
	}
	
	// Configuro gradi Celsius e la reiezione alla tensione di rete
	if (!_write(GLOBAL_CONFIGURATION_REGISTER, (__u8) (TEMP_UNIT_CELSIUS | REJECTION_50_60_HZ))) {
		return false;
	}
	
	// Imposto il ritardo tra le conversioni pari a 0 ms
	if (!_write(MUX_CONFIGURATION_DELAY_REGISTER, (__u8) 0)) {
		return false;
	}
	
	// Configuro i canali
	bool configured[20];
	for (__u8 i = 0; i < 20; i++) configured[i] = false;
	
	// Configuro il sense resistor
	if (!_setChannelSenseResistor(_sense_resistor_channel, _sense_resistor_value)) {
		return false;
	}
	configured[((int) _sense_resistor_channel) - 1] = true;
	
	// Configuro tutti i canali registrati
	__u32 mask;
	mask = 0;
	_logger->info(string("Init Device: Registered channels = ") + to_string(_converting_registered_channels.count()));
	for (int i = 0; i < _converting_registered_channels.count(); i++) {
		const ChannelConfiguration &cc = _converting_registered_channels.at(i);
		configured[((int) cc.channel) - 1] = true;
		if (cc.temperature) {
			// Temperatura
			if (!_setChannelRTD(cc.channel, RTDType::PT1000, _sense_resistor_channel, Wires::TWO, ExcitationMode::RSENSE_SHARED, ExcitationCurrent::I_100_uA, Curve::EUROPEAN)) {
				return false;
			}
			
			_temperature_files.insert(cc.channel, new QFile());
			_temperature_file_index.insert(cc.channel, 0);
		}
		else {
			// Tensione
			if (!_setChannelDirectADC(cc.channel, EndingMode::SINGLE)) {
				return false;
			}
			
			_voltage_files.insert(cc.channel, new QFile());
			_voltage_file_index.insert(cc.channel, 0);
		}
		
		// Aggiorno la maschera
		mask |= (1 << (cc.channel - 1));
	}
	
	// Imposto la maschera per la conversione multipla
	if (!_write(MULTICONVERT_MASK_REGISTER, mask)) {
		return false;
	}
	
	// Configuro come non assegnati i canali rimanenti
	for (__u8 i = 0; i < 20; i++) {
		if (!configured[i]) {
			if (!_setChannelUnassigned(i + 1)) {
				return false;
			}
		}
	}
	
	// Chiudo il dispositivo
	if (!spiClose()) {
		return false;
	}
	
	return true;
}

// Inizio lettura
bool Thermometer::startReading() {
	_logger->info("[Thermometer] start reading");
	
	// Verifico se l'acquisizione e' gia' in corso
	if (_publishing) {
		_logger->warn("[Thermometer] already publishing");
		return true;
	}
	_publishing = true;
	
	// Avvio i timer se la frequenza specificata è maggiore di zero
	auto start_timer = [this] (const char *name, double f, QTimer &t) {
		if (f > 0.0) {
			int ms;
			try {
				ms = (int) (1000 / f);
				if (ms <= 0) ms = 1;
				t.start(ms);
				return true;
			}
			catch (exception e) {
				string err;
				err += "[Thermometer] cannot start ";
				err += name;
				err += " timer: ";
				err += e.what();
				_logger->warn(err);
				return false;
			}
		}
		return true;
	};
	
	if (!start_timer("thermometer", _frequency, _timer)) {
		return false;
	}
	
	// Creo se non esistono, altrimenti apro i file di storage
	auto init_storage = [this] (QFile &file, QDir &storage_dir, QString &filename, int channel, qint64 &filesize, int &index) {
		bool exists;
		
		QString channel_filename = filename + QString("(") + QString::number(channel) + QString(")");
		
		if (!_getNextFreeIndex(storage_dir, channel_filename, filesize, index, exists)) {
			_logger->error("Failed to get next free file index");
			return false;
		}
		
		// Imposto il nome del file
		file.setFileName(storage_dir.absoluteFilePath(channel_filename + QString(".") + QString::number(index)));
		
		if (exists) {
			// Se esiste lo apro in modalità Append
			file.open(QIODevice::Append);
		}
		else {
			file.open(QIODevice::WriteOnly);
		}
		
		return true;
	};
	
	for (int i = 0; i < _converting_registered_channels.count(); i++) {
		const ChannelConfiguration &cc = _converting_registered_channels.at(i);
		if (cc.temperature) {
			if (!init_storage(*_temperature_files[cc.channel], _storage_dir, _temperature_filename, cc.channel, _filesize_limit, _temperature_file_index[cc.channel])) {
				return false;
			}
		}
		else {
			if (!init_storage(*_voltage_files[cc.channel], _storage_dir, _voltage_filename, cc.channel, _filesize_limit, _voltage_file_index[cc.channel])) {
				return false;
			}
		}
	}
	
	return true;
}

// Fine lettura
bool Thermometer::stopReading() {
	_logger->info("[Thermometer] stop reading");
	
	// Verifico se l'acquisizione e' in corso
	if (!_publishing) {
		_logger->warn("[Thermometer] not publishing yet");
		return true;
	}
	_publishing = false;
	
	// Fermo i timer attivi
	if (_timer.isActive()) {
		_timer.stop();
	}
	
	// Chiudo i file
	for(int channel : _temperature_files.keys()) {
		QFile *file = _temperature_files[channel];
		file->close();
	}
	for(int channel : _voltage_files.keys()) {
		QFile *file = _voltage_files[channel];
		file->close();
	}
	
	return true;
}

// Pubblicazione del dato
void Thermometer::publishMultiple() {
	_logger->info("[Thermometer] publish temperature");
	
	// Verifico che non sia ancora in corso la conversione
	if (_converting_flag != Converting::FREE) {
		_logger->warn("[Thermometer] attempt to convert failed, conversion still running");
		return;
	}
	
	// Verifico che ci sia almeno un canale registrato
	if (_converting_registered_channels.count() == 0) {
		return;
	}
	
	// Apro la SPI
	if (!spiOpen()) {
		return;
	}
	
	// Avvio la lettura
	if (!_convertMultipleChannels()) {
		return;
	}
	
	// Chiudo la SPI
	spiClose();
}

// Configurazione ingressi
bool Thermometer::_setChannelUnassigned(__u8 channel) {
	uint32_t chdata;
	chdata = (uint32_t) (SENSOR_TYPE_UNASSIGNED << SENSOR_TYPE_OFFSET);
	return _configChannel(channel, chdata);
}

//bool _setChannelThermocouple(__u8 channel, ColdJunctionChannelAssignment cjca, EndingMode ending_node, OCCheck oc_check, OCCurrent oc_current, CustomAddress custom_address, CustomLength custom_length);
bool Thermometer::_setChannelRTD(__u8 channel, RTDType type, RSenseChannelAssignment rsca, Wires wires, ExcitationMode excitation_mode, ExcitationCurrent excitation_current, Curve curve) {
	uint32_t chdata;
	chdata =
		(uint32_t) (((int)type) << SENSOR_TYPE_OFFSET) |
		(uint32_t) (((int)rsca) << RTD_RSENSE_CHANNEL_ASSINGMENT_OFFSET) |
		(uint32_t) (((int)wires) << RTD_WIRES_OFFSET) |
		(uint32_t) (((int)excitation_mode) << RTD_EXCITATION_MODE_OFFSET) |
		(uint32_t) (((int)excitation_current) << RTD_EXCITATION_CURRENT_OFFSET) |
		(uint32_t) (((int)curve) << RTD_CURVE_OFFSET)
	;
	return _configChannel(channel, chdata);
}
//bool _setChannelThermistor(__u8 channel);
//bool _setChannelDiode(__u8 channel);
bool Thermometer::_setChannelSenseResistor(RSenseChannelAssignment rsca, double sense_resistor_value) {
	uint32_t chdata;
	chdata =
		(uint32_t) (SENSOR_TYPE_SENSE_RESISTOR << SENSOR_TYPE_OFFSET) |
		(uint32_t) (sense_resistor_value * 1024.0)
	;
	return _configChannel((__u8) rsca, chdata);
}
bool Thermometer::_setChannelDirectADC(__u8 channel, EndingMode ending_mode) {
	uint32_t chdata;
	chdata =
		(uint32_t) (SENSOR_TYPE_DIRECT_ADC << SENSOR_TYPE_OFFSET) |
		(uint32_t) (((int)ending_mode) << DIRECT_ADC_ENDING_MODE_OFFSET)
	;
	return _configChannel(channel, chdata);
}


bool Thermometer::_configChannel(__u8 channel, __u32 configuration) {
	uint16_t address = CONFIG_BASE_ADDR + 4 * (channel - 1);
	return _write(address, configuration);
}
bool Thermometer::_convertChannel(__u8 channel, bool temperature) {
	// Verifico se c'è già una conversione in corso
	if (_converting_flag != Converting::FREE) {
		_logger->warn("[Temperature] Failed to start conversion, device busy");
		return false;
	}
	
	// Segnalo l'inizio della conversione
	ChannelConfiguration cc;
	_convertReset();
	_converting_flag = Converting::SINGLE;
	cc.channel = channel;
	cc.temperature = temperature;
	_converting_channels.append(cc);
	
	// Faccio partire la conversione
	_write(COMMAND_REGISTER, (__u8)((1 << START_BIT) | channel));
	_converting_timer.start(CONVERTING_PERIOD);
	
	return true;
}
bool Thermometer::_addChannel(__u8 channel, bool temperature) {
	// Verifico che il canale non sia già presente
	for (int i = 0; i < _converting_registered_channels.count(); i++) {
		const ChannelConfiguration &cc = _converting_registered_channels.at(i);
		if (cc.channel == channel) {
			_logger->error("Failed to add channel, channel already configured");
			return false;
		}
	}
	
	// Aggiungo il nuovo canale a quelli registrati
	ChannelConfiguration cc_new;
	cc_new.channel = channel;
	cc_new.temperature = temperature;
	_converting_registered_channels.append(cc_new);
	
	// Aggiorno la maschera per la conversione multipla
	__u32 mask;
	if (!_read(MULTICONVERT_MASK_REGISTER, mask)) {
		_logger->error("Failed to read multiconvert mask register");
		return false;
	}
	if (!_write(MULTICONVERT_MASK_REGISTER, mask | (1 << (channel - 1)))) {
		_logger->error("Failed to write multiconvert mask register");
		return false;
	}
	
	// A seconda del tipo di canale imposto la sua configurazione
	if (cc_new.temperature) {
		// Temperatura
		if (!_setChannelRTD(cc_new.channel, RTDType::PT1000, _sense_resistor_channel, Wires::TWO, ExcitationMode::RSENSE_SHARED, ExcitationCurrent::I_100_uA, Curve::EUROPEAN)) {
			return false;
		}
	}
	else {
		// Tensione
		if (!_setChannelDirectADC(cc_new.channel, EndingMode::SINGLE)) {
			return false;
		}
	}
	
	return true;
}
bool Thermometer::_removeChannel(__u8 channel) {
	// Verifico che il canale sia presente
	for (int i = 0; i < _converting_registered_channels.count(); i++) {
		const ChannelConfiguration &cc = _converting_registered_channels.at(i);
		if (cc.channel == channel) {
			// Aggiorno la maschera per la conversione multipla
			__u32 mask;
			if (!_read(MULTICONVERT_MASK_REGISTER, mask)) {
				_logger->error("Failed to read multiconvert mask register");
				return false;
			}
			if (!_write(MULTICONVERT_MASK_REGISTER, mask & ~(1 << (channel - 1)))) {
				_logger->error("Failed to write multiconvert mask register");
				return false;
			}
			
			// Rimuovo il canale
			_converting_registered_channels.removeAt(i);
			
			return true;
		}
	}
	
	_logger->error("Failed to remove channel, channel not configured");
	return false;
}
void Thermometer::_convertReset() {
	_converting_attempts_counter = 0;
	_converting_flag = Converting::FREE;
	_converting_timer.stop();
}
bool Thermometer::_convertMultipleChannels() {
	// Verifico se c'è già una conversione in corso
	if (_converting_flag != Converting::FREE) {
		_logger->warn("[Temperature] Failed to start conversion, device busy");
		return false;
	}
	
	// Segnalo l'inizio della conversione
	_convertReset();
	_converting_flag = Converting::MULTIPLE;
	_converting_channels.clear();
	_converting_channels.append(_converting_registered_channels);
	
	// Faccio partire la conversione
	_write(COMMAND_REGISTER, (__u8)(1 << START_BIT));
	_converting_timer.start(CONVERTING_PERIOD);
	
	return true;
}
bool Thermometer::_convertDone(bool &result) {
	uint8_t value = 0;
	if (!_read(COMMAND_REGISTER, value)) return false;
	result = (value & (1 << DONE_BIT)) != 0;
	return true;
}
void Thermometer::_tryPublish() {
	// Apro la SPI
	if (!spiOpen()) {
		return;
	}
	
	// Avvio la lettura
	_checkConversionDone();
	
	// Chiudo la SPI
	spiClose();
}
void Thermometer::_checkConversionDone() {
	// Verifico il tipo di conversione in corso
	if (_converting_flag == Converting::FREE) {
		_convertReset();
		return;
	}
	
	// Verifico se la conversione è riuscita
	bool result;
	if (!_convertDone(result)) {
		// Impossibile leggere il registro
		_convertReset();
		return;
	}
	if (!result) {
		// Conteggio il tentativo di lettura
		_converting_attempts_counter++;
		if (_converting_attempts_counter >= CONVERTING_ATTEMPTS) {
			// Rinuncio alla conversione
			_convertReset();
			return;
		}
		
		// Aspetto ancora
		return;
	}
	
	// Pubblico tutti i canali
	for (int i = 0; i < _converting_channels.count(); i++) {
		// Canale corrente
		const ChannelConfiguration &cc = _converting_channels.at(i);
		
		// Verifico se è una misura di temperatura o di tensione
		if (cc.temperature) {
			double temperature;
			if (!_getTemperature(cc.channel, temperature)) {
				_convertReset();
				return;
			}
			
			Message msg;
			msg += "THERMOMETER/TEMPERATURE";
			msg += (string("TEMPERATURE = ") + to_string(temperature)).c_str();
			msg += (string("CHANNEL = ") + to_string(cc.channel)).c_str();
			emit requestPublishMessage(msg);
			
			// Salvo il dato su file
			QString record;
			QFile *file;
			int &index = _temperature_file_index[cc.channel];
			file = _temperature_files[cc.channel];
			if (file->isOpen()) {
				record += QString("[") + QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss.zzz") + QString("] ");
				record += QString::number(temperature) + QString("\n");
				file->write(record.toStdString().c_str());
				
				// Se il file supera la dimensione massima consentita lo chiudo e apro un nuovo file
				if (file->size() >= _filesize_limit) {
					file->close();
					index++;
					QString channel_filename = _temperature_filename + QString("(") + QString::number(cc.channel) + QString(")");
					file->setFileName(_storage_dir.absoluteFilePath(channel_filename + QString(".") + QString::number(index)));
					file->open(QIODevice::WriteOnly);
				}
			}
		}
		else {
			// Tensione (divido per 2^21)
			double voltage;
			bool sensor_hard_fault;
			bool range_hard_fault;
			bool soft_above;
			bool soft_below;
			bool soft_range;
			
			if (!_getVoltage(cc.channel, voltage, sensor_hard_fault, range_hard_fault, soft_above, soft_below, soft_range)) {
				_convertReset();
				return;
			}
			
			Message msg;
			
			// Verifico se è la tensione della batteria
			if (_battery_voltage_channel == cc.channel) {
				voltage *= _battery_voltage_ideal_ratio * _battery_voltage_correction_factor;
				msg += "THERMOMETER/BATTERY VOLTAGE";
			}
			else {
				msg += "THERMOMETER/VOLTAGE";
			}
			msg += (string("VOLTAGE = ") + to_string(voltage)).c_str();
			msg += (string("CHANNEL = ") + to_string(cc.channel)).c_str();
			msg += (string("SENSOR HARD FAULT = ") + string(sensor_hard_fault ? "true" : "false")).c_str();
			msg += (string("RANGE HARD FAULT = ") + string(range_hard_fault ? "true" : "false")).c_str();
			msg += (string("SOFT ABOVE = ") + string(soft_above ? "true" : "false")).c_str();
			msg += (string("SOFT BELOW = ") + string(soft_below ? "true" : "false")).c_str();
			msg += (string("SOFT RANGE = ") + string(soft_range ? "true" : "false")).c_str();
			emit requestPublishMessage(msg);
			
			// Salvo il dato su file
			QString record;
			QFile *file;
			int &index = _voltage_file_index[cc.channel];
			file = _voltage_files[cc.channel];
			if (file->isOpen()) {
				record += QString("[") + QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss.zzz") + QString("] ");
				record +=
					QString::number(voltage) + QString(", ") +
					QString(sensor_hard_fault ? "1" : "0") + QString(", ") +
					QString(range_hard_fault ? "1" : "0") + QString(", ") +
					QString(soft_above ? "1" : "0") + QString(", ") +
					QString(soft_below ? "1" : "0") + QString(", ") +
					QString(soft_range ? "1" : "0") + QString(", ") +
				QString("\n");
				file->write(record.toStdString().c_str());
				
				// Se il file supera la dimensione massima consentita lo chiudo e apro un nuovo file
				if (file->size() >= _filesize_limit) {
					file->close();\
					index++;
					QString channel_filename = _temperature_filename + QString("(") + QString::number(cc.channel) + QString(")");
					file->setFileName(_storage_dir.absoluteFilePath(channel_filename + QString(".") + QString::number(index)));
					file->open(QIODevice::WriteOnly);
				}
			}
		}
	}
	
	_convertReset();
}

bool Thermometer::_read(uint16_t address, __u8 &value) {
	struct spi_ioc_transfer tr[2];
	int ret;
	uint8_t tx[3];
	uint8_t rx;
	
	tx[0] = READ_INSTR;
	tx[1] = (address & 0xFF00) >> 8;		// MSB
	tx[2] = (address & 0x00FF);				// LSB
	
	tr[0].tx_buf = (unsigned long) tx;
	tr[0].rx_buf = (unsigned long) NULL;
	tr[0].len = ARRAY_SIZE(tx);
	tr[0].delay_usecs = _delay_usec;
	tr[0].speed_hz = _speed_hz;
	tr[0].bits_per_word = _bits_per_word;
	tr[0].cs_change = 0;
	
	tr[1].tx_buf = (unsigned long) NULL;
	tr[1].rx_buf = (unsigned long) &rx;
	tr[1].len = 1;
	tr[1].delay_usecs = _delay_usec;
	tr[1].speed_hz = _speed_hz;
	tr[1].bits_per_word = _bits_per_word;
	tr[1].cs_change = 1;
	
	ret = ioctl(_file, SPI_IOC_MESSAGE(2), tr);
	if (ret < 1) {
		_logger->error("[Thermometer] Failed to read byte");
		_logger->error(strerror(errno));
		return false;
	}
	
	value = rx;
	
	_logger->info(string("\033[37m[") + int_to_hex<uint16_t>(address) + string("] \033[32m=>\033[37m ") + int_to_hex<__u8>(value));
	
	return true;
}
bool Thermometer::_read(uint16_t address, __u32 &value) {
	struct spi_ioc_transfer tr[2];
	int ret;
	uint8_t tx[3];
	uint8_t rx[4];
	
	tx[0] = READ_INSTR;
	tx[1] = (address & 0xFF00) >> 8;		// MSB
	tx[2] = (address & 0x00FF);				// LSB
	
	tr[0].tx_buf = (unsigned long) tx;
	tr[0].rx_buf = (unsigned long) NULL;
	tr[0].len = ARRAY_SIZE(tx);
	tr[0].delay_usecs = _delay_usec;
	tr[0].speed_hz = _speed_hz;
	tr[0].bits_per_word = _bits_per_word;
	tr[0].cs_change = 0;
	
	tr[1].tx_buf = (unsigned long) NULL;
	tr[1].rx_buf = (unsigned long) &rx;
	tr[1].len = ARRAY_SIZE(rx);
	tr[1].delay_usecs = _delay_usec;
	tr[1].speed_hz = _speed_hz;
	tr[1].bits_per_word = _bits_per_word;
	tr[1].cs_change = 1;
	
	ret = ioctl(_file, SPI_IOC_MESSAGE(2), tr);
	if (ret < 1) {
		_logger->error("[Thermometer] Failed to read double word");
		_logger->error(strerror(errno));
		return false;
	}
	
	value = rx[3];
	value += (rx[2] << 8);
	value += (rx[1] << 16);
	value += (rx[0] << 24);
	
	_logger->info(string("\033[37m[") + int_to_hex<uint16_t>(address) + string("] \033[32m=>\033[37m ") + int_to_hex<__u32>(value));
	
	return true;
}
bool Thermometer::_write(uint16_t address, __u8 value) {
	struct spi_ioc_transfer tr;
	int ret;
	uint8_t tx[4];
	
	tx[0] = WRITE_INSTR;
	tx[1] = (address & 0xFF00) >> 8;		// MSB
	tx[2] = (address & 0x00FF);				// LSB
	tx[3] = value;
	
	tr.tx_buf = (unsigned long) tx;
	tr.rx_buf = (unsigned long) NULL;
	tr.len = ARRAY_SIZE(tx);
	tr.delay_usecs = _delay_usec;
	tr.speed_hz = _speed_hz;
	tr.bits_per_word = _bits_per_word;
	
	ret = ioctl(_file, SPI_IOC_MESSAGE(1), &tr);
	if (ret < 1) {
		_logger->error("[Thermometer] Failed to write byte");
		_logger->error(strerror(errno));
		return false;
	}
	
	_logger->info(string("\033[37m[") + int_to_hex<uint16_t>(address) + string("] \033[31m<=\033[37m ") + int_to_hex<__u8>(value));
	
	return true;
}
bool Thermometer::_write(uint16_t address, __u32 value) {
	struct spi_ioc_transfer tr;
	int ret;
	uint8_t tx[7];
	
	tx[0] = WRITE_INSTR;
	tx[1] = (address & 0xFF00) >> 8;		// MSB
	tx[2] = (address & 0x00FF);				// LSB
	tx[3] = (value & 0xFF000000) >> 24;
	tx[4] = (value & 0x00FF0000) >> 16;
	tx[5] = (value & 0x0000FF00) >> 8;
	tx[6] = value & 0x000000FF;
	
	tr.tx_buf = (unsigned long) tx;
	tr.rx_buf = (unsigned long) NULL;
	tr.len = ARRAY_SIZE(tx);
	tr.delay_usecs = _delay_usec;
	tr.speed_hz = _speed_hz;
	tr.bits_per_word = _bits_per_word;
	
	ret = ioctl(_file, SPI_IOC_MESSAGE(1), &tr);
	if (ret < 1) {
		_logger->error("[Thermometer] Failed to write double word");
		_logger->error(strerror(errno));
		return false;
	}
	
	_logger->info(string("\033[37m[") + int_to_hex<uint16_t>(address) + string("] \033[31m<=\033[37m ") + int_to_hex<__u32>(value));
	
	return true;
}
bool Thermometer::_getRawResult(__u8 channel, __u32 &value) {
	uint16_t address = RESULT_BASE_ADDR + 4 * (channel - 1);
	return _read(address, value);
}
bool Thermometer::_getSignedFromRaw(__u32 raw, int32_t &value) {
	// Converto un valore da 24 bit in complemento a due a 32 bit in complemento a due
	
	// Estraggo il valore a 24 bit
	value = raw & 0x00FFFFFF;
	
	// Verifico se aveva segno
	if(raw & 0x00800000) {
		value |= 0xFF000000;
	}
	
	return true;
}
bool Thermometer::_getTemperature(__u8 channel, double &value) {
	__u32 raw_value;
	int32_t signed_value;
	if (!_getRawResult(channel, raw_value)) {
		return false;
	}
	if (!_getSignedFromRaw(raw_value, signed_value)) {
		return false;
	}
	// Divido per 2^10
	value = signed_value / 1024.0;
	return true;
}
bool Thermometer::_getVoltage(__u8 channel, double &value, bool &sensor_hard_fault, bool &range_hard_fault, bool &soft_above, bool &soft_below, bool &soft_range) {
	__u32 raw_value;
	int32_t signed_value;
	if (!_getRawResult(channel, raw_value)) {
		return false;
	}
	sensor_hard_fault = (((1 << SENSOR_HARD_FAILURE_BIT) & raw_value) != 0);
	range_hard_fault = (((1 << ADC_HARD_FAILURE_BIT) & raw_value) != 0);
	soft_above = (((1 << SENSOR_ABOVE_BIT) & raw_value) != 0);
	soft_below = (((1 << SENSOR_BELOW_BIT) & raw_value) != 0);
	soft_range = (((1 << ADC_RANGE_ERROR_BIT) & raw_value) != 0);
	if (!_getSignedFromRaw(raw_value, signed_value)) {
		return false;
	}
	// Divido per 2^21
	value = signed_value / 2097152.0;
	return true;
}

// Metodi per il salvataggio dei dati
bool Thermometer::_getNextFreeIndex(QDir &folder, QString &filename, qint64 &size_limit, int &index, bool &file_exists) {
	QStringList filters;
	filters += filename + QString(".*");
	
	QFileInfoList files;
	files = folder.entryInfoList(filters);
	
	// Non esistono file a cui collegarsi
	if (files.count() == 0) {
		index = 0;
		file_exists = false;
		return true;
	}
	
	// Trovo l'indice massimo
	QRegularExpression regex;
	QRegularExpressionMatch match;
	int max_index = 0;
	QFileInfo max_fi;
	QString escaped = filename;
	escaped.replace(QRegularExpression("\\("), "\\(").replace(QRegularExpression("\\)"), "\\)");
	
	regex.setPattern(QString("^") + escaped + QString("\\.(?<index>\\d+)$"));
	for (int i = 0; i < files.count(); i++) {
		match = regex.match(files[i].fileName());
		if (!match.hasMatch()) continue;
		int temp;
		temp = match.captured("index").toInt();
		if (max_index < temp) {
			max_fi = files[i];
			max_index = temp;
		}
	}
	
	if (max_fi.size() >= size_limit) {
		index = max_index + 1;
		file_exists = false;
	}
	else {
		index = max_index;
		file_exists = true;
	}
	
	return true;
}
