//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		29/04/17
//	Descrizione:		Classe che rappresenta l'IMU (accelerometro e giroscopio) LSM6DS33
//****************************************************************************************************//

// Header della classe
#include <Sensor/sap_imu.h>

// Header STL
#include <string>
#include <sstream>
#include <cmath>

using namespace std;
using namespace drex::sensor;

// Costruttori
Imu::Imu(const string device, const unsigned char address, bool acceleration_enabled, bool freefall_enabled, bool angular_rate_enabled, bool temperature_enabled, QDir storage_dir, qint64 filesize_limit, string temperature_filename, string acceleration_filename, string freefall_filename, string angularrate_filename, const double freefall_acceleration_limit, const int freefall_counter_limit, const double thermometer_frequency, const double gyroscope_frequency, const double accelerometer_frequency, QObject *parent) : 
	QObject(parent),
	GenericSensor(), 
	I2CDevice(device, address),
	_acceleration_enabled(acceleration_enabled),
	_freefall_enabled(freefall_enabled),
	_angular_rate_enabled(angular_rate_enabled),
	_temperature_enabled(temperature_enabled),
	_storage_dir(storage_dir),
	_filesize_limit(filesize_limit),
	_temperature_filename(QString::fromStdString(temperature_filename)),
	_acceleration_filename(QString::fromStdString(acceleration_filename)),
	_freefall_filename(QString::fromStdString(freefall_filename)),
	_angular_rate_filename(QString::fromStdString(angularrate_filename)),
	_publishing(false),
	_thermometer_frequency(thermometer_frequency),
	_gyroscope_frequency(gyroscope_frequency),
	_accelerometer_frequency(accelerometer_frequency),
	_freefall_acceleration_limit(freefall_acceleration_limit),
	_freefall_counter_limit(freefall_counter_limit),
	_freefall_counter(0) {
	_logger = spdlog::get("logger");
	
	_gyroscope_scale_bounds.insert(GyroscopeScale::DPS_125, LinearBounds(0.0, 100.0));
	_gyroscope_scale_bounds.insert(GyroscopeScale::DPS_245, LinearBounds(100.0, 200.0));
	_gyroscope_scale_bounds.insert(GyroscopeScale::DPS_500, LinearBounds(200.0, 450.0));
	_gyroscope_scale_bounds.insert(GyroscopeScale::DPS_1000, LinearBounds(450.0, 900.0));
	_gyroscope_scale_bounds.insert(GyroscopeScale::DPS_2000, LinearBounds(900.0, 3000.0));
	
	_gyroscope_scale_previous.insert(GyroscopeScale::DPS_245, GyroscopeScale::DPS_125);
	_gyroscope_scale_previous.insert(GyroscopeScale::DPS_500, GyroscopeScale::DPS_245);
	_gyroscope_scale_previous.insert(GyroscopeScale::DPS_1000, GyroscopeScale::DPS_500);
	_gyroscope_scale_previous.insert(GyroscopeScale::DPS_2000, GyroscopeScale::DPS_1000);
	
	_gyroscope_scale_next.insert(GyroscopeScale::DPS_125, GyroscopeScale::DPS_245);
	_gyroscope_scale_next.insert(GyroscopeScale::DPS_245, GyroscopeScale::DPS_500);
	_gyroscope_scale_next.insert(GyroscopeScale::DPS_500, GyroscopeScale::DPS_1000);
	_gyroscope_scale_next.insert(GyroscopeScale::DPS_1000, GyroscopeScale::DPS_2000);
	
	_accelerometer_scale_bounds.insert(AccelerometerScale::G_2, LinearBounds(0.0, 1.5));
	_accelerometer_scale_bounds.insert(AccelerometerScale::G_4, LinearBounds(1.5, 3.5));
	_accelerometer_scale_bounds.insert(AccelerometerScale::G_8, LinearBounds(3.5, 7.5));
	_accelerometer_scale_bounds.insert(AccelerometerScale::G_16, LinearBounds(7.5, 20.0));
	
	_accelerometer_scale_previous.insert(AccelerometerScale::G_4, AccelerometerScale::G_2);
	_accelerometer_scale_previous.insert(AccelerometerScale::G_8, AccelerometerScale::G_4);
	_accelerometer_scale_previous.insert(AccelerometerScale::G_16, AccelerometerScale::G_8);
	
	_accelerometer_scale_next.insert(AccelerometerScale::G_2, AccelerometerScale::G_4);
	_accelerometer_scale_next.insert(AccelerometerScale::G_4, AccelerometerScale::G_8);
	_accelerometer_scale_next.insert(AccelerometerScale::G_8, AccelerometerScale::G_16);
	
	// Inizializzo le conversioni
	_gyroscope_conversion.insert(GyroscopeScale::DPS_125, LinearConversion(0.0, 4.375 * 0.001));
	_gyroscope_conversion.insert(GyroscopeScale::DPS_245, LinearConversion(0.0, 8.75 * 0.001));
	_gyroscope_conversion.insert(GyroscopeScale::DPS_500, LinearConversion(0.0, 17.50 * 0.001));
	_gyroscope_conversion.insert(GyroscopeScale::DPS_1000, LinearConversion(0.0, 35.0 * 0.001));
	_gyroscope_conversion.insert(GyroscopeScale::DPS_2000, LinearConversion(0.0, 70.0 * 0.001));
	
	_accelerometer_conversion.insert(AccelerometerScale::G_2, LinearConversion(0.0, 0.061 * 0.001));
	_accelerometer_conversion.insert(AccelerometerScale::G_4, LinearConversion(0.0, 0.122 * 0.001));
	_accelerometer_conversion.insert(AccelerometerScale::G_8, LinearConversion(0.0, 0.244 * 0.001));
	_accelerometer_conversion.insert(AccelerometerScale::G_16, LinearConversion(0.0, 0.488 * 0.001));
	
	if (_temperature_enabled) QObject::connect(&_thermometer_timer, &QTimer::timeout, this, &Imu::publishTemperature, Qt::QueuedConnection);
	if (_angular_rate_enabled) QObject::connect(&_gyroscope_timer, &QTimer::timeout, this, &Imu::publishAngularRate, Qt::QueuedConnection);
	if (_acceleration_enabled || _freefall_enabled) QObject::connect(&_accelerometer_timer, &QTimer::timeout, this, &Imu::publishAcceleration, Qt::QueuedConnection);
}

// Distruttore
Imu::~Imu() {
	if (_temperature_enabled) QObject::disconnect(&_thermometer_timer, &QTimer::timeout, this, &Imu::publishTemperature);
	if (_angular_rate_enabled) QObject::disconnect(&_gyroscope_timer, &QTimer::timeout, this, &Imu::publishAngularRate);
	if (_acceleration_enabled || _freefall_enabled) QObject::disconnect(&_accelerometer_timer, &QTimer::timeout, this, &Imu::publishAcceleration);
}

// Inizializzazione
bool Imu::initDevice() {
	_logger->info("[Imu] init device");
	
	// Inizializzo il dispositivo
	
	// Apro il dispositivo
	if (!i2cOpen()) {
		return false;
	}
	
	// Configuro i registri
	unsigned char init_values[][2] = {
		{FUNC_CFG_ACCESS, FUNC_CFG_ACCESS__DEFAULT},
		
		{FIFO_CTRL1, FIFO_CTRL1__DEFAULT},
		{FIFO_CTRL2, FIFO_CTRL2__DEFAULT},
		{FIFO_CTRL3, FIFO_CTRL3__DEFAULT},
		{FIFO_CTRL4, FIFO_CTRL4__DEFAULT},
		{FIFO_CTRL5, FIFO_CTRL5__DEFAULT},
		
		{ORIENT_CFG_G, ORIENT_CFG_G__DEFAULT},
		
		{INT1_CTRL, INT1_CTRL__DEFAULT},
		{INT2_CTRL, INT2_CTRL__DEFAULT},
		
		{CTRL1_XL, CTRL1_XL__DEFAULT},
		{CTRL2_G, CTRL2_G__DEFAULT},
		{CTRL3_C, CTRL3_C__DEFAULT},
		{CTRL4_C, CTRL4_C__DEFAULT},
		{CTRL5_C, CTRL5_C__DEFAULT},
		{CTRL6_C, CTRL6_C__DEFAULT},
		{CTRL7_G, CTRL7_G__DEFAULT},
		{CTRL8_XL, CTRL8_XL__DEFAULT},
		{CTRL9_XL, CTRL9_XL__DEFAULT},
		{CTRL10_C, CTRL10_C__DEFAULT},
		
		{TAP_CFG, TAP_CFG__DEFAULT},
		{TAP_THS_6D, TAP_THS_6D__DEFAULT},
		{INT_DUR2, INT_DUR2__DEFAULT},
		{WAKE_UP_THS, WAKE_UP_THS__DEFAULT},
		{WAKE_UP_DUR, WAKE_UP_DUR__DEFAULT},
		{FREE_FALL, FREE_FALL__DEFAULT},
		{MD1_CFG, MD1_CFG__DEFAULT},
		{MD2_CFG, MD2_CFG__DEFAULT}
	};
	
	// Imposto la configurazione
	for (unsigned int r = 0; r < sizeof(init_values) / 2; r++) {
		unsigned char *config = init_values[r];
		
		if (!i2cSetRegister(config[0], config[1])) {
			i2cClose();
			return false;
		}
	}
	
	// Salvo i valori di scala del giroscopio e dell'accelerometro
	if (!_getGyroscopeScaleAndSpeed(_gyroscope_scale, _gyroscope_speed)) {
		i2cClose();
		return false;
	}
	
	if (!_getAccelerometerScaleAndSpeed(_accelerometer_scale, _accelerometer_speed)) {
		i2cClose();
		return false;
	}
	
	// Chiudo il dispositivo
	if (!i2cClose()) {
		return false;
	}
	
	return true;
}

// Inizio lettura
bool Imu::startReading() {
	_logger->info("[Imu] start reading");
	
	// Verifico se l'acquisizione e' gia' in corso
	if (_publishing) {
		_logger->warn("[Imu] already publishing");
		return true;
	}
	_publishing = true;
	
	// Avvio i timer se la frequenza specificata è maggiore di zero
	auto start_timer = [this] (const char *name, double f, QTimer &t) {
		if (f > 0.0) {
			int ms;
			try {
				ms = (int) (1000 / f);
				if (ms <= 0) ms = 1;
				t.start(ms);
				return true;
			}
			catch (exception e) {
				string err;
				err += "[Imu] cannot start ";
				err += name;
				err += " timer: ";
				err += e.what();
				_logger->warn(err);
				return false;
			}
		}
		return true;
	};
	
	if (_temperature_enabled) {
		if (!start_timer("thermometer", _thermometer_frequency, _thermometer_timer)) {
			return false;
		}
	}
	
	if (_angular_rate_enabled) {
		if (!start_timer("gyroscope", _gyroscope_frequency, _gyroscope_timer)) {
			return false;
		}
	}
	
	if (_acceleration_enabled || _freefall_enabled) {
		if (!start_timer("accelerometer", _accelerometer_frequency, _accelerometer_timer)) {
			return false;
		}
	}
	
	// Creo se non esistono, altrimenti apro i file di storage
	auto init_storage = [this] (QFile &file, QDir &storage_dir, QString &filename, qint64 &filesize, int &index) {
		bool exists;
		
		if (!_getNextFreeIndex(storage_dir, filename, filesize, index, exists)) {
			_logger->error("Failed to get next free file index");
			return false;
		}
		
		// Imposto il nome del file
		file.setFileName(storage_dir.absoluteFilePath(filename + QString(".") + QString::number(index)));
		
		if (exists) {
			// Se esiste lo apro in modalità Append
			file.open(QIODevice::Append);
		}
		else {
			file.open(QIODevice::WriteOnly);
		}
		
		return true;
	};
	
	if (_temperature_enabled) {
		if (!init_storage(_temperature_file, _storage_dir, _temperature_filename, _filesize_limit, _temperature_file_index)) {
			return false;
		}
	}
	
	if (_acceleration_enabled) {
		if (!init_storage(_acceleration_file, _storage_dir, _acceleration_filename, _filesize_limit, _acceleration_file_index)) {
			return false;
		}
	}
	
	if (_freefall_enabled) {
		if (!init_storage(_freefall_file, _storage_dir, _freefall_filename, _filesize_limit, _freefall_file_index)) {
			return false;
		}
	}
	
	if (_angular_rate_enabled) {
		if (!init_storage(_angular_rate_file, _storage_dir, _angular_rate_filename, _filesize_limit, _angular_rate_file_index)) {
			return false;
		}
	}
	
	return true;
}

// Fine lettura
bool Imu::stopReading() {
	_logger->info("[Imu] stop reading");
	
	// Verifico se l'acquisizione e' in corso
	if (!_publishing) {
		_logger->warn("[Imu] not publishing yet");
		return true;
	}
	_publishing = false;
	
	// Fermo i timer attivi
	if (_thermometer_timer.isActive()) {
		_thermometer_timer.stop();
	}
	
	if (_gyroscope_timer.isActive()) {
		_gyroscope_timer.stop();
	}
	
	if (_accelerometer_timer.isActive()) {
		_accelerometer_timer.stop();
	}
	
	// Chiudo i file
	if (_temperature_enabled) _temperature_file.close();
	if (_acceleration_enabled) _acceleration_file.close();
	if (_freefall_enabled) _freefall_file.close();
	if (_angular_rate_enabled) _angular_rate_file.close();
	
	return true;
}

// Configurazione dei parametri del giroscopio
bool Imu::getGyroscopeScale(GyroscopeScale &scale) {
	_logger->info("[Imu] get gyroscope scale");
	
	// Restituisco la scala del giroscopio
	
	// Apro il dispositivo
	if (!i2cOpen()) {
		return false;
	}
	
	if (!_getGyroscopeScale(scale)) {
		i2cClose();
		return false;
	}
	
	// Chiudo il dispositivo
	if (!i2cClose()) {
		return false;
	}
	
	return true;
}
bool Imu::setGyroscopeScale(GyroscopeScale scale) {
	_logger->info("[Imu] set gyroscope scale");
	
	// Imposto la scala del giroscopio
	
	// Apro il dispositivo
	if (!i2cOpen()) {
		return false;
	}
	
	if (!_setGyroscopeScale(scale)) {
		i2cClose();
		return false;
	}
	
	// Chiudo il dispositivo
	if (!i2cClose()) {
		return false;
	}
	
	return true;
}

// Configurazione dei parametri dell'accelerometro
bool Imu::getAccelerometerScale(AccelerometerScale &scale) {
	_logger->info("[Imu] get accelerometer scale");
	
	// Restituisco la scala dell'accelerometro
	
	// Apro il dispositivo
	if (!i2cOpen()) {
		return false;
	}
	
	if (!_getAccelerometerScale(scale)) {
		i2cClose();
		return false;
	}
	
	// Chiudo il dispositivo
	if (!i2cClose()) {
		return false;
	}
	
	return true;
}
bool Imu::setAccelerometerScale(AccelerometerScale scale) {
	_logger->info("[Imu] set accelerometer scale");
	
	// Imposto la scala dell'accelerometro
	
	// Apro il dispositivo
	if (!i2cOpen()) {
		return false;
	}
	
	if (!_setAccelerometerScale(scale)) {
		i2cClose();
		return false;
	}
	
	// Chiudo il dispositivo
	if (!i2cClose()) {
		return false;
	}
	
	return true;
}

// Lettura della temperatura
bool Imu::getTemperature(double &value) {
	_logger->info("[Imu] get temperature");
	
	// Apro il dispositivo
	if (!i2cOpen()) {
		return false;
	}
	
	if (!_getTemperature(value)) {
		i2cClose();
		return false;
	}
	
	// Chiudo il dispositivo
	if (!i2cClose()) {
		return false;
	}
	
	return true;
}
bool Imu::getRawTemperature(int16_t &value) {
	_logger->info("[Imu] get raw temperature");
	
	// Leggo il dato di temperatura in formato numerico
	// e lo restituisco senza elaborazioni
	
	// Apro il dispositivo
	if (!i2cOpen()) {
		return false;
	}
	
	if (!_getRawTemperature(value)) {
		i2cClose();
		return false;
	}
	
	// Chiudo il dispositivo
	if (!i2cClose()) {
		return false;
	}
	
	return true;
}

// Lettura del giroscopio
bool Imu::getAngularRate(Vector3D<double> &value) {
	_logger->info("[Imu] get angular rate");
	
	// Apro il dispositivo
	if (!i2cOpen()) {
		return false;
	}
	
	if (!_getAngularRate(value)) {
		i2cClose();
		return false;
	}
	
	// Chiudo il dispositivo
	if (!i2cClose()) {
		return false;
	}
	
	return true;
}
bool Imu::getRawAngularRate(Vector3D<int16_t> &value) {
	_logger->info("[Imu] get raw angular rate");
	
	// Leggo il dato di accelerazione angolare in formato numerico
	// e lo restituisco senza elaborazioni
	
	// Apro il dispositivo
	if (!i2cOpen()) {
		return false;
	}
	
	if (!_getRawAngularRate(value)) {
		i2cClose();
		return false;
	}
	
	// Chiudo il dispositivo
	if (!i2cClose()) {
		return false;
	}
	
	return true;
}

// Lettura dell'accelerometro
bool Imu::getAcceleration(Vector3D<double> &value) {
	_logger->info("[Imu] get acceleration");
	
	// Apro il dispositivo
	if (!i2cOpen()) {
		return false;
	}
	
	if (!_getAcceleration(value)) {
		i2cClose();
		return false;
	}
	
	// Chiudo il dispositivo
	if (!i2cClose()) {
		return false;
	}
	
	return true;
}
bool Imu::getRawAcceleration(Vector3D<int16_t> &value) {
	_logger->info("[Imu] get raw acceleration");
	
	// Leggo il dato di accelerazione lineare in formato numerico
	// e lo restituisco senza elaborazioni
	
	// Apro il dispositivo
	if (!i2cOpen()) {
		return false;
	}
	
	if (!_getRawAcceleration(value)) {
		i2cClose();
		return false;
	}
	
	// Chiudo il dispositivo
	if (!i2cClose()) {
		return false;
	}
	
	return true;
}

// Pubblicazione del dato
void Imu::publishTemperature() {
	_logger->info("[Imu] Publish temperature");
	
	double temperature;
	
	if (!getTemperature(temperature)) {
		_logger->warn("[Imu] Failed to read temeprature");
		return;
	}
	
	// Creo il messaggio e lo invio
	Message msg;
	msg += "IMU/TEMPERATURE";
	msg += (string("TEMPERATURE = ") + to_string(temperature)).c_str();
	emit requestPublishMessage(msg);
	
	// Salvo il dato su file
	QString record;
	record += QString("[") + QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss.zzz") + QString("] ");
	record += QString::number(temperature) + QString("\n");
	_temperature_file.write(record.toStdString().c_str());
	
	// Se il file supera la dimensione massima consentita lo chiudo e apro un nuovo file
	if (_temperature_file.size() >= _filesize_limit) {
		_temperature_file.close();
		_temperature_file_index++;
		_temperature_file.setFileName(_storage_dir.absoluteFilePath(_temperature_filename + QString(".") + QString::number(_temperature_file_index)));
		_temperature_file.open(QIODevice::WriteOnly);
	}
}
// Pubblicazione del dato
void Imu::publishAngularRate() {
	_logger->info("[Imu] Publish angular rate");
	
	// Apro il dispositivo
	if (!i2cOpen()) {
		return;
	}
	
	// Fermo l'acquisizione
	GyroscopeSpeed speed = _gyroscope_speed;
	if (!_setGyroscopeSpeed(GyroscopeSpeed::POWER_DOWN)) {
		i2cClose();
		return;
	}
	
	Vector3D<double> angular_rate;
	
	if (!_getAngularRate(angular_rate)) {
		_logger->warn("[Imu] Failed to read angular rate");
		_setGyroscopeSpeed(speed);
		i2cClose();
		return;
	}
	
	if (!_setGyroscopeSpeed(speed)) {
		i2cClose();
		return;
	}
	
	// Se l'autoscale e' attivo
	if (_gyroscope_autoscale) {
		double max = abs(angular_rate.absoluteMax());
		LinearBounds current_bounds = _gyroscope_scale_bounds[_gyroscope_scale];
		GyroscopeScale next_scale = _gyroscope_scale;
		while (max < current_bounds.lower) {
			next_scale = _gyroscope_scale_previous[next_scale];
			current_bounds = _gyroscope_scale_bounds[next_scale];
		}
		while (max > current_bounds.upper) {
			next_scale = _gyroscope_scale_next[next_scale];
			current_bounds = _gyroscope_scale_bounds[next_scale];
		}
		if (_gyroscope_scale != next_scale) {
			if (!_setGyroscopeScale(next_scale)) {
				_logger->warn("[Imu] Failed to autoscale gyroscope");
			}
		}
	}
	
	// Chiudo il dispositivo
	if (!i2cClose()) {
		return;
	}
	
	// Formatto la stringa
	stringstream str_angular_rate;
	str_angular_rate << angular_rate;
	
	// Creo il messaggio e lo invio
	Message msg;
	msg += "IMU/ANGULAR RATE";
	msg += (string("ANGULAR RATE") + str_angular_rate.str()).c_str();
	emit requestPublishMessage(msg);
	
	// Salvo il dato su file
	QString record;
	record += QString("[") + QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss.zzz") + QString("] ");
	record += QString::fromStdString(str_angular_rate.str()) + QString("\n");
	_angular_rate_file.write(record.toStdString().c_str());
	
	// Se il file supera la dimensione massima consentita lo chiudo e apro un nuovo file
	if (_angular_rate_file.size() >= _filesize_limit) {
		_angular_rate_file.close();
		_angular_rate_file_index++;
		_angular_rate_file.setFileName(_storage_dir.absoluteFilePath(_angular_rate_filename + QString(".") + QString::number(_angular_rate_file_index)));
		_angular_rate_file.open(QIODevice::WriteOnly);
	}
}
// Pubblicazione del dato
void Imu::publishAcceleration() {
	_logger->info("[Imu] publish acceleration");
	
	// Apro il dispositivo
	if (!i2cOpen()) {
		return;
	}
	
	// Fermo l'acquisizione
	AccelerometerSpeed speed = _accelerometer_speed;
	if (!_setAccelerometerSpeed(AccelerometerSpeed::POWER_DOWN)) {
		i2cClose();
		return;
	}
	
	Vector3D<double> acceleration;
	
	if (!_getAcceleration(acceleration)) {
		_logger->warn("[Imu] failed to read acceleration");
		_setAccelerometerSpeed(speed);
		i2cClose();
		return;
	}
	
	if (!_setAccelerometerSpeed(speed)) {
		i2cClose();
		return;
	}
	
	// Se l'autoscale e' attivo
	if (_accelerometer_autoscale) {
		double max = abs(acceleration.absoluteMax());
		LinearBounds current_bounds = _accelerometer_scale_bounds[_accelerometer_scale];
		AccelerometerScale next_scale = _accelerometer_scale;
		while (max < current_bounds.lower) {
			next_scale = _accelerometer_scale_previous[next_scale];
			current_bounds = _accelerometer_scale_bounds[next_scale];
		}
		while (max > current_bounds.upper) {
			next_scale = _accelerometer_scale_next[next_scale];
			current_bounds = _accelerometer_scale_bounds[next_scale];
		}
		if (_accelerometer_scale != next_scale) {
			if (!_setAccelerometerScale(_accelerometer_scale)) {
				_logger->warn("[Imu] failed to autoscale accelerometer");
			}
		}
	}
	
	// Chiudo il dispositivo
	if (!i2cClose()) {
		return;
	}
	
	Message msg;
	
	if (_acceleration_enabled) {
		// Formatto la stringa
		stringstream str_acceleration;
		str_acceleration << acceleration;
		
		// Creo il messaggio e lo invio
		msg.clear();
		msg += "IMU/ACCELERATION";
		msg += (string("ACCELERATION = ") + str_acceleration.str()).c_str();
		emit requestPublishMessage(msg);
		
		// Salvo il dato su file
		QString record;
		record += QString("[") + QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss.zzz") + QString("] ");
		record += QString::fromStdString(str_acceleration.str()) + QString("\n");
		_acceleration_file.write(record.toStdString().c_str());
		
		// Se il file supera la dimensione massima consentita lo chiudo e apro un nuovo file
		if (_acceleration_file.size() >= _filesize_limit) {
			_acceleration_file.close();
			_acceleration_file_index++;
			_acceleration_file.setFileName(_storage_dir.absoluteFilePath(_acceleration_filename + QString(".") + QString::number(_acceleration_file_index)));
			_acceleration_file.open(QIODevice::WriteOnly);
		}
	}
	
	if (_freefall_enabled) {
		bool freefall;
		
		// Verifico la condizione di freefalling
		freefall = false;
		if (acceleration.magnitude() <= _freefall_acceleration_limit) {
			_freefall_counter++;
			if (_freefall_counter >= _freefall_counter_limit) {
				_freefall_counter = _freefall_counter_limit;
				freefall = true;
			}
		}
		
		string str_freefall;
		str_freefall = string(freefall ? "1" : "0");
		
		// Creo il messaggio e lo invio
		msg.clear();
		msg += "IMU/FREEFALL";
		msg += (string("FREEFALL = ") + str_freefall).c_str();
		emit requestPublishMessage(msg);
		
		// Salvo il dato su file
		QString record;
		record += QString("[") + QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss.zzz") + QString("] ");
		record += QString::fromStdString(str_freefall) + QString("\n");
		_freefall_file.write(record.toStdString().c_str());
		
		// Se il file supera la dimensione massima consentita lo chiudo e apro un nuovo file
		if (_freefall_file.size() >= _filesize_limit) {
			_freefall_file.close();
			_freefall_file_index++;
			_freefall_file.setFileName(_storage_dir.absoluteFilePath(_freefall_filename + QString(".") + QString::number(_freefall_file_index)));
			_freefall_file.open(QIODevice::WriteOnly);
		}
	}
}

bool Imu::_getGyroscopeScale(GyroscopeScale &scale) {
	_logger->info("[Imu] _ get gyroscope scale");
	
	// Leggo il valore attuale del registro di configurazione
	__u8 ctrl2_g;
	if (!i2cGetRegister(CTRL2_G, ctrl2_g)) {
		return false;
	}
	
	_gyroscope_scale = (GyroscopeScale) (ctrl2_g & GYROSCOPE_SCALE_MASK);
	scale = _gyroscope_scale;
	
	return true;
}
bool Imu::_setGyroscopeScale(GyroscopeScale scale) {
	_logger->info("[Imu] _ set gyroscope scale");
	
	// Leggo il valore attuale del registro di configurazione
	// perche' non devo modificare gli altri bit
	__u8 ctrl2_g;
	if (!i2cGetRegister(CTRL2_G, ctrl2_g)) {
		return false;
	}
	
	// Azzero i bit
	ctrl2_g &= ~GYROSCOPE_SCALE_MASK;
	
	// Imposto i nuovi bit
	ctrl2_g |= (__u8) scale;
	
	// Scrivo il valore nel registro del dispositivo
	if (!i2cSetRegister(CTRL2_G, ctrl2_g)) {
		return false;
	}
	
	_gyroscope_scale = scale;
	
	return true;
}
bool Imu::_getGyroscopeSpeed(GyroscopeSpeed &speed) {
	_logger->info("[Imu] _ get gyroscope speed");
	
	// Leggo il valore attuale del registro di configurazione
	__u8 ctrl2_g;
	if (!i2cGetRegister(CTRL2_G, ctrl2_g)) {
		return false;
	}
	
	_gyroscope_speed = (GyroscopeSpeed) (ctrl2_g & GYROSCOPE_SPEED_MASK);
	speed = _gyroscope_speed;
	
	return true;
}
bool Imu::_setGyroscopeSpeed(GyroscopeSpeed speed) {
	_logger->info("[Imu] _ set gyroscope speed");
	
	// Leggo il valore attuale del registro di configurazione
	// perche' non devo modificare gli altri bit
	__u8 ctrl2_g;
	if (!i2cGetRegister(CTRL2_G, ctrl2_g)) {
		return false;
	}
	
	// Azzero i bit
	ctrl2_g &= ~GYROSCOPE_SPEED_MASK;
	
	// Imposto i nuovi bit
	ctrl2_g |= (__u8) speed;
	
	// Scrivo il valore nel registro del dispositivo
	if (!i2cSetRegister(CTRL2_G, ctrl2_g)) {
		return false;
	}
	
	_gyroscope_speed = speed;
	
	return true;
}
bool Imu::_getGyroscopeScaleAndSpeed(GyroscopeScale &scale, GyroscopeSpeed &speed) {
	_logger->info("[Imu] _ get gyroscope scale and speed");
	
	// Leggo il valore attuale del registro di configurazione
	__u8 ctrl2_g;
	if (!i2cGetRegister(CTRL2_G, ctrl2_g)) {
		return false;
	}
	
	_gyroscope_scale = (GyroscopeScale) (ctrl2_g & GYROSCOPE_SCALE_MASK);
	scale = _gyroscope_scale;
	_gyroscope_speed = (GyroscopeSpeed) (ctrl2_g & GYROSCOPE_SPEED_MASK);
	speed = _gyroscope_speed;
	
	return true;
}
bool Imu::_setGyroscopeScaleAndSpeed(GyroscopeScale scale, GyroscopeSpeed speed) {
	_logger->info("[Imu] _ set gyroscope scale and speed");
	
	// Leggo il valore attuale del registro di configurazione
	// perche' non devo modificare gli altri bit
	__u8 ctrl2_g;
	if (!i2cGetRegister(CTRL2_G, ctrl2_g)) {
		return false;
	}
	
	// Azzero i bit della scala
	ctrl2_g &= ~(GYROSCOPE_SCALE_MASK | GYROSCOPE_SPEED_MASK);
	
	// Imposto i nuovi bit
	ctrl2_g |= ((__u8) scale) | ((__u8) speed);
	
	// Scrivo il valore nel registro del dispositivo
	if (!i2cSetRegister(CTRL2_G, ctrl2_g)) {
		return false;
	}
	
	_gyroscope_scale = scale;
	_gyroscope_speed = speed;
	
	return true;
}

bool Imu::_getAccelerometerScale(AccelerometerScale &scale) {
	_logger->info("[Imu] _ get accelerometer scale");
	
	// Leggo il valore attuale del registro di configurazione
	__u8 ctrl1_xl;
	if (!i2cGetRegister(CTRL1_XL, ctrl1_xl)) {
		return false;
	}
	
	_accelerometer_scale = (AccelerometerScale) (ctrl1_xl & ACCELEROMETER_SCALE_MASK);
	scale = _accelerometer_scale;
	
	return true;
}
bool Imu::_setAccelerometerScale(AccelerometerScale scale) {
	_logger->info("[Imu] _ set accelerometer scale");
	
	// Leggo il valore attuale del registro di configurazione
	// perche' non devo modificare gli altri bit
	__u8 ctrl1_xl;
	if (!i2cGetRegister(CTRL1_XL, ctrl1_xl)) {
		return false;
	}
	
	// Azzero i bit
	ctrl1_xl &= ~ACCELEROMETER_SCALE_MASK;
	
	// Imposto i nuovi bit
	ctrl1_xl |= (__u8) scale;
	
	// Scrivo il valore nel registro del dispositivo
	if (!i2cSetRegister(CTRL1_XL, ctrl1_xl)) {
		return false;
	}
	
	_accelerometer_scale = scale;
	
	return true;
}
bool Imu::_getAccelerometerSpeed(AccelerometerSpeed &speed) {
	_logger->info("[Imu] _ get accelerometer speed");
	
	// Leggo il valore attuale del registro di configurazione
	__u8 ctrl1_xl;
	if (!i2cGetRegister(CTRL1_XL, ctrl1_xl)) {
		return false;
	}
	
	_accelerometer_speed = (AccelerometerSpeed) (ctrl1_xl & ACCELEROMETER_SPEED_MASK);
	speed = _accelerometer_speed;
	
	return true;
}
bool Imu::_setAccelerometerSpeed(AccelerometerSpeed speed) {
	_logger->info("[Imu] _ set accelerometer speed");
	
	// Leggo il valore attuale del registro di configurazione
	// perche' non devo modificare gli altri bit
	__u8 ctrl1_xl;
	if (!i2cGetRegister(CTRL1_XL, ctrl1_xl)) {
		return false;
	}
	
	// Azzero i bit
	ctrl1_xl &= ~ACCELEROMETER_SPEED_MASK;
	
	// Imposto i nuovi bit
	ctrl1_xl |= (__u8) speed;
	
	// Scrivo il valore nel registro del dispositivo
	if (!i2cSetRegister(CTRL1_XL, ctrl1_xl)) {
		return false;
	}
	
	_accelerometer_speed = speed;
	
	return true;
}
bool Imu::_getAccelerometerScaleAndSpeed(AccelerometerScale &scale, AccelerometerSpeed &speed) {
	_logger->info("[Imu] _ get accelerometer scale and speed");
	
	// Leggo il valore attuale del registro di configurazione
	__u8 ctrl1_xl;
	if (!i2cGetRegister(CTRL1_XL, ctrl1_xl)) {
		return false;
	}
	
	_accelerometer_scale = (AccelerometerScale) (ctrl1_xl & ACCELEROMETER_SCALE_MASK);
	scale = _accelerometer_scale;
	_accelerometer_speed = (AccelerometerSpeed) (ctrl1_xl & ACCELEROMETER_SPEED_MASK);
	speed = _accelerometer_speed;
	
	return true;
}
bool Imu::_setAccelerometerScaleAndSpeed(AccelerometerScale scale, AccelerometerSpeed speed) {
	_logger->info("[Imu] _ set accelerometer scale and speed");
	
	// Leggo il valore attuale del registro di configurazione
	// perche' non devo modificare gli altri bit
	__u8 ctrl1_xl;
	if (!i2cGetRegister(CTRL1_XL, ctrl1_xl)) {
		return false;
	}
	
	// Azzero i bit
	ctrl1_xl &= ~(ACCELEROMETER_SCALE_MASK | ACCELEROMETER_SPEED_MASK);
	
	// Imposto i nuovi bit
	ctrl1_xl |= ((__u8) scale) | ((__u8) speed);
	
	// Scrivo il valore nel registro del dispositivo
	if (!i2cSetRegister(CTRL1_XL, ctrl1_xl)) {
		return false;
	}
	
	_accelerometer_scale = scale;
	_accelerometer_speed = speed;
	
	return true;
}

// Lettura della temperatura
bool Imu::_getTemperature(double &value) {
	_logger->info("[Imu] _ get temperature");
	
	// Leggo il dato grezzo
	int16_t raw;
	if (!_getRawTemperature(raw)) {
		return false;
	}
	
	// Lo converto in gradi centigradi
	value = TEMPERATURE_OFFSET + raw * TEMPERATURE_SCALE;
	
	return true;
}
bool Imu::_getRawTemperature(int16_t &value) {
	_logger->info("[Imu] _ get raw temperature");
	
	// Leggo una word intera
	if (!i2cGetWord(OUT_TEMP_L, value)) {
		return false;
	}
	
	return true;
}

// Lettura del giroscopio
bool Imu::_getAngularRate(Vector3D<double> &value) {
	_logger->info("[Imu] _ get angular rate");
	
	// Leggo il dato grezzo
	Vector3D<int16_t> raw;
	if (!_getRawAngularRate(raw)) {
		return false;
	}
	
	// Lo converto in gradi al secondo
	LinearConversion conversion = _gyroscope_conversion[_gyroscope_scale];
	value.x = conversion.offset + raw.x * conversion.scale;
	value.y = conversion.offset + raw.y * conversion.scale;
	value.z = conversion.offset + raw.z * conversion.scale;
	
	return true;
}
bool Imu::_getRawAngularRate(Vector3D<int16_t> &value) {
	_logger->info("[Imu] _ get raw angular rate");
	
	// Registri da leggere
	__u8 registers[] = {
		OUTX_L_G,
		OUTY_L_G,
		OUTZ_L_G
	};
	
	// Leggo i valori di tutte le direzioni
	for (uint direction = 0; direction < sizeof(registers); direction++) {
		// Leggo il valore
		if (!i2cGetWord(registers[direction], value[direction])) {
			return false;
		}
	}
	
	return true;
}

// Lettura dell'accelerometro
bool Imu::_getAcceleration(Vector3D<double> &value) {
	_logger->info("[Imu] _ get acceleration");
	
	// Leggo il dato grezzo
	Vector3D<int16_t> raw;
	if (!_getRawAcceleration(raw)) {
		return false;
	}
	
	// Lo converto in gradi al secondo
	LinearConversion conversion = _accelerometer_conversion[_accelerometer_scale];
	value.x = conversion.offset + raw.x * conversion.scale;
	value.y = conversion.offset + raw.y * conversion.scale;
	value.z = conversion.offset + raw.z * conversion.scale;
	
	return true;
}
bool Imu::_getRawAcceleration(Vector3D<int16_t> &value) {
	_logger->info("[Imu] _ get raw acceleration");
	
	// Registri da leggere
	__u8 registers[] = {
		OUTX_L_XL,
		OUTY_L_XL,
		OUTZ_L_XL
	};
	
	// Leggo i valori di tutte le direzioni
	for (uint direction = 0; direction < sizeof(registers); direction++) {
		// Leggo il valore
		if (!i2cGetWord(registers[direction], value[direction])) {
			return false;
		}
	}
	
	return true;
}

// Metodi per il salvataggio dei dati
bool Imu::_getNextFreeIndex(QDir &folder, QString &filename, qint64 &size_limit, int &index, bool &file_exists) {
	QStringList filters;
	filters += filename + QString(".*");
	
	QFileInfoList files;
	files = folder.entryInfoList(filters);
	
	// Non esistono file a cui collegarsi
	if (files.count() == 0) {
		index = 0;
		file_exists = false;
		return true;
	}
	
	// Trovo l'indice massimo
	QRegularExpression regex;
	QRegularExpressionMatch match;
	int max_index = 0;
	QFileInfo max_fi;
	regex.setPattern(QString("^") + filename + QString("\\.(?<index>\\d+)$"));
	for (int i = 0; i < files.count(); i++) {
		match = regex.match(files[i].fileName());
		if (!match.hasMatch()) continue;
		int temp;
		temp = match.captured("index").toInt();
		if (max_index < temp) {
			max_fi = files[i];
			max_index = temp;
		}
	}
	
	if (max_fi.size() >= size_limit) {
		index = max_index + 1;
		file_exists = false;
	}
	else {
		index = max_index;
		file_exists = true;
	}
	
	return true;
}
