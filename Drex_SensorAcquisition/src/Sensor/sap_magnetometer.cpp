//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		30/04/17
//	Descrizione:		Classe che rappresenta il magnetometro LIS3MDL
//****************************************************************************************************//

// Header della classe
#include <Sensor/sap_magnetometer.h>

// Header STL
#include <string>
#include <sstream>

using namespace std;
using namespace drex::sensor;

// Costruttori
//Magnetometer::Magnetometer(Magnetometer *other) :
//	QObject(other->parent()),
//	GenericSensor(),
//	I2CSensor(other->_device, other->_address) {
//	_logger = spdlog::get("logger");
//	this->_publishing = other->_publishing;
	
//	this->_thermometer_frequency = other->_thermometer_frequency;
	
//	this->_magnetometer_frequency = other->_magnetometer_frequency;
//	this->_magnetometer_autoscale = other->_magnetometer_autoscale;
//	this->_magnetometer_scale = other->_magnetometer_scale;
//	this->_magnetometer_conversion = other->_magnetometer_conversion;
//	this->_magnetometer_scale_bounds = other->_magnetometer_scale_bounds;
//	this->_magnetometer_scale_previous = other->_magnetometer_scale_previous;
//	this->_magnetometer_scale_next = other->_magnetometer_scale_next;
//}
Magnetometer::Magnetometer(const std::string device, const unsigned char address, bool magnetic_field_enabled, bool temperature_enabled, const double thermometer_frequency, const double magnetometer_frequency, QObject *parent) :
	QObject(parent),
	GenericSensor(),
	I2CDevice(device, address),
	_magnetic_field_enabled(magnetic_field_enabled),
	_temperature_enabled(temperature_enabled),
	_publishing(false),
	_thermometer_frequency(thermometer_frequency),
	_magnetometer_frequency(magnetometer_frequency) {
	_logger = spdlog::get("logger");
	
	_magnetometer_scale_bounds.insert(MagnetometerScale::GAUSS_4, LinearBounds(0.0, 3.5));
	_magnetometer_scale_bounds.insert(MagnetometerScale::GAUSS_8, LinearBounds(3.5, 7.5));
	_magnetometer_scale_bounds.insert(MagnetometerScale::GAUSS_12, LinearBounds(7.5, 11.5));
	_magnetometer_scale_bounds.insert(MagnetometerScale::GAUSS_16, LinearBounds(11.5, 20.0));
	
	_magnetometer_scale_previous.insert(MagnetometerScale::GAUSS_8, MagnetometerScale::GAUSS_4);
	_magnetometer_scale_previous.insert(MagnetometerScale::GAUSS_12, MagnetometerScale::GAUSS_8);
	_magnetometer_scale_previous.insert(MagnetometerScale::GAUSS_16, MagnetometerScale::GAUSS_12);
	
	_magnetometer_scale_next.insert(MagnetometerScale::GAUSS_4, MagnetometerScale::GAUSS_8);
	_magnetometer_scale_next.insert(MagnetometerScale::GAUSS_8, MagnetometerScale::GAUSS_12);
	_magnetometer_scale_next.insert(MagnetometerScale::GAUSS_12, MagnetometerScale::GAUSS_16);
	
	// Inizializzo le conversioni
	_magnetometer_conversion.insert(MagnetometerScale::GAUSS_4, LinearConversion(0.0, 1.0 / 6842));
	_magnetometer_conversion.insert(MagnetometerScale::GAUSS_8, LinearConversion(0.0, 1.0 / 3421));
	_magnetometer_conversion.insert(MagnetometerScale::GAUSS_12, LinearConversion(0.0, 1.0 / 2281));
	_magnetometer_conversion.insert(MagnetometerScale::GAUSS_16, LinearConversion(0.0, 1.0 / 1711));
	
	if (_temperature_enabled) QObject::connect(&_thermometer_timer, &QTimer::timeout, this, &Magnetometer::publishTemperature, Qt::QueuedConnection);
	if (_magnetic_field_enabled) QObject::connect(&_magnetometer_timer, &QTimer::timeout, this, &Magnetometer::publishMagneticField, Qt::QueuedConnection);
}

Magnetometer::~Magnetometer() {
	if (_temperature_enabled) QObject::disconnect(&_thermometer_timer, &QTimer::timeout, this, &Magnetometer::publishTemperature);
	if (_magnetic_field_enabled) QObject::disconnect(&_magnetometer_timer, &QTimer::timeout, this, &Magnetometer::publishMagneticField);
}

// Inizializzazione
bool Magnetometer::initDevice() {
	_logger->info("[Magnetometer] init device");
	
	// Apro il dispositivo
	if (!i2cOpen()) {
		return false;
	}
	
	// Configuro i registri
	unsigned char init_values[][2] = {
		{CTRL_REG1, CTRL_REG1__DEFAULT},
		{CTRL_REG2, CTRL_REG2__DEFAULT},
		{CTRL_REG3, CTRL_REG3__DEFAULT},
		{CTRL_REG4, CTRL_REG4__DEFAULT},
		{CTRL_REG5, CTRL_REG5__DEFAULT},
		
		{INT_CFG, INT_CFG__DEFAULT},
		{INT_SRC, INT_SRC__DEFAULT},
		{INT_THS_L, INT_THS_L__DEFAULT},
		{INT_THS_H, INT_THS_H__DEFAULT}
	};
	
	// Imposto la configurazione
	for (unsigned int r = 0; r < sizeof(init_values); r++) {
		unsigned char *config = init_values[r];
		
		if (!i2cSetRegister(config[0], config[1])) {
			i2cClose();
			return false;
		}
	}
	
	// Salvo i valori di scala del magnetometro
	__u8 ctrl_reg2;
	if (!i2cGetRegister(CTRL_REG2, ctrl_reg2)) {
		i2cClose();
		return false;
	}
	
	_magnetometer_scale = (MagnetometerScale) (ctrl_reg2 & MAGNETOMETER_SCALE_MASK);
	
	// Chiudo il dispositivo
	if (!i2cClose()) {
		return false;
	}
	
	return true;
}

// Inizio lettura
bool Magnetometer::startReading() {
	_logger->info("[Magnetometer] start reading");
	
	// Verifico se l'acquisizione e' gia' in corso
	if (_publishing) {
		_logger->warn("[Magnetometer] already publishing");
		return true;
	}
	_publishing = true;
	
	// Avvio i timer se la frequenza specificata è maggiore di zero
	auto start_timer = [this] (const char *name, double f, QTimer &t) {
		if (f > 0.0) {
			int ms;
			try {
				ms = (int) (1000 / f);
				if (ms <= 0) ms = 1;
				t.start(ms);
				return true;
			}
			catch (exception e) {
				string err;
				err += "[Magnetometer] cannot start ";
				err += name;
				err += " timer: ";
				err += e.what();
				_logger->warn(err);
				return false;
			}
		}
		return true;
	};
	
	if (_temperature_enabled) {
		if (!start_timer("thermometer", _thermometer_frequency, _thermometer_timer)) {
			return false;
		}
	}
	
	if (_magnetic_field_enabled) {
		if (!start_timer("magnetometer", _magnetometer_frequency, _magnetometer_timer)) {
			return false;
		}
	}
	
	return true;
}

// Fine lettura
bool Magnetometer::stopReading() {
	_logger->info("[Magnetometer] stop reading");
	
	// Verifico se l'acquisizione e' in corso
	if (!_publishing) {
		_logger->warn("[Magnetometer] not publishing yet");
		return true;
	}
	_publishing = false;
	
	// Fermo i timer attivi
	if (_thermometer_timer.isActive()) {
		_thermometer_timer.stop();
	}
	
	if (_magnetometer_timer.isActive()) {
		_magnetometer_timer.stop();
	}
	
	return true;
}

// Configurazione dei parametri del magnetometro
bool Magnetometer::getMagnetometerScale(MagnetometerScale &scale) {
	_logger->info("[Magnetometer] get magnetometer scale");
	
	// Restituisco la scala del magnetometro
	
	// Apro il dispositivo
	if (!i2cOpen()) {
		return false;
	}
	
	__u8 ctrl_reg2;
	if (!i2cGetRegister(CTRL_REG2, ctrl_reg2)) {
		i2cClose();
		return false;
	}
	
	_magnetometer_scale = (MagnetometerScale) (ctrl_reg2 & MAGNETOMETER_SCALE_MASK);
	scale = _magnetometer_scale;
	
	// Chiudo il dispositivo
	if (!i2cClose()) {
		return false;
	}
	
	return true;
}
bool Magnetometer::setMagnetometerScale(MagnetometerScale scale) {
	_logger->info("[Magnetometer] set magnetometer scale");
	
	// Imposto la scala del magnetometro
	
	// Apro il dispositivo
	if (!i2cOpen()) {
		return false;
	}
	
	// Leggo il valore attuale del registro di configurazione
	// perche' non devo modificare gli altri bit
	__u8 ctrl_reg2;
	if (!i2cGetRegister(CTRL_REG2, ctrl_reg2)) {
		i2cClose();
		return false;
	}
	
	// Azzero i bit della scala
	ctrl_reg2 &= ~MAGNETOMETER_SCALE_MASK;
	
	// Imposto i nuovi bit
	ctrl_reg2 |= (__u8) scale;
	
	// Scrivo il valore nel registro del dispositivo
	if (!i2cSetRegister(CTRL_REG2, ctrl_reg2)) {
		i2cClose();
		return false;
	}
	
	_magnetometer_scale = scale;
	
	// Chiudo il dispositivo
	if (!i2cClose()) {
		return false;
	}
	
	return true;
}

// Lettura della temperatura
bool Magnetometer::getTemperature(double &value) {
	_logger->info("[Magnetometer] get temperature");
	
	// Leggo il dato grezzo
	int16_t raw_temperature;
	if (!getRawTemperature(raw_temperature)) {
		return false;
	}
	
	// Lo converto in gradi centigradi
	value = TEMPERATURE_OFFSET + raw_temperature * TEMPERATURE_SCALE;
	
	return true;
}
bool Magnetometer::getRawTemperature(int16_t &value) {
	_logger->info("[Magnetometer] get raw temperature");
	
	// Leggo il dato di temperatura in formato numerico
	// e lo restituisco senza elaborazioni
	
	// Apro il dispositivo
	if (!i2cOpen()) {
		return false;
	}
	
	// Parte bassa
	__u8 temp;
	if (!i2cGetRegister(TEMP_OUT_L, temp)) {
		i2cClose();
		return false;
	}
	value = temp;
	
	// Parte alta
	if (!i2cGetRegister(TEMP_OUT_H, temp)) {
		i2cClose();
		return false;
	}
	value |= temp << 8;
	
	// Chiudo il dispositivo
	if (!i2cClose()) {
		return false;
	}
	
	return true;
}

// Lettura del magnetometro
bool Magnetometer::getMagneticField(Vector3D<double> &value) {
	_logger->info("[Magnetometer] get magnetic field");
	
	// Leggo il dato grezzo
	Vector3D<int16_t> raw_magnetic_field;
	if (!getRawMagneticField(raw_magnetic_field)) {
		return false;
	}
	
	// Lo converto in gauss
	LinearConversion conversion = _magnetometer_conversion[_magnetometer_scale];
	value.x = conversion.offset + raw_magnetic_field.x * conversion.scale;
	value.y = conversion.offset + raw_magnetic_field.y * conversion.scale;
	value.z = conversion.offset + raw_magnetic_field.z * conversion.scale;
	
	return true;
}
bool Magnetometer::getRawMagneticField(Vector3D<int16_t> &value) {
	_logger->info("[Magnetometer] get raw magnetic field");
	
	// Leggo il dato di campo magnetico in formato numerico
	// e lo restituisco senza elaborazioni
	
	// Apro il dispositivo
	if (!i2cOpen()) {
		return false;
	}
	
	// Registri da leggere
	__u8 registers[][2] = {
		{OUT_X_L, OUT_X_H},
		{OUT_Y_L, OUT_Y_H},
		{OUT_Z_L, OUT_Z_H}
	};
	
	// Leggo i valori di tutte le direzioni
	for (uint direction = 0; direction < sizeof(registers); direction++) {
		// Direzione corrente
		__u8 *current = registers[direction];
		
		// Leggo il valore
		__u8 temp;
		if (!i2cGetRegister(current[0], temp)) {
			i2cClose();
			return false;
		}
		value[direction] = temp;
		
		if (!i2cGetRegister(current[1], temp)) {
			i2cClose();
			return false;
		}
		value[direction] |= temp << 8;
	}
	
	// Chiudo il dispositivo
	if (!i2cClose()) {
		return false;
	}
	
	return true;
}

// Pubblicazione del dato
void Magnetometer::publishTemperature() {
	_logger->info("[Magnetometer] Publish temperature");
	
	double temperature;
	
	if (!getTemperature(temperature)) {
		_logger->warn("[Magnetometer] Failed to read temeprature");
		return;
	}
	
	// Creo il messaggio e lo invio
	Message msg;
	msg += "MAGNETOMETER/TEMPERATURE";
	msg += (string("TEMPERATURE") + to_string(temperature)).c_str();
	emit requestPublishMessage(msg);
}
// Pubblicazione del dato
void Magnetometer::publishMagneticField() {
	_logger->info("[Magnetometer] Publish magnetic field");
	
	Vector3D<double> magnetic_field;
	
	if (!getMagneticField(magnetic_field)) {
		_logger->warn("[Magnetometer] Failed to read magnetic field");
		return;
	}
	
	// Se l'autoscale e' attivo
	if (_magnetometer_autoscale) {
		double max = abs(magnetic_field.absoluteMax());
		LinearBounds current_bounds = _magnetometer_scale_bounds[_magnetometer_scale];
		MagnetometerScale next_scale = _magnetometer_scale;
		while (max < current_bounds.lower) {
			next_scale = _magnetometer_scale_previous[next_scale];
			current_bounds = _magnetometer_scale_bounds[next_scale];
		}
		while (max > current_bounds.upper) {
			next_scale = _magnetometer_scale_next[next_scale];
			current_bounds = _magnetometer_scale_bounds[next_scale];
		}
		if (_magnetometer_scale != next_scale) {
			if (!setMagnetometerScale(next_scale)) {
				_logger->warn("[Magnetometer] Failed to autoscale magnetometer");
			}
		}
	}
	
	// Formatto la stringa
	stringstream str_magnetic_field;
	str_magnetic_field << magnetic_field;
	
	// Creo il messaggio e lo invio
	Message msg;
	msg += "IMU/MAGNETIC_FIELD";
	msg += (string("MAGNETIC_FIELD") + str_magnetic_field.str()).c_str();
	emit requestPublishMessage(msg);
}
