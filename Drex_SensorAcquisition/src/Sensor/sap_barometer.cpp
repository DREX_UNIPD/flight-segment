//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		26/04/17
//	Descrizione:		Classe che rappresenta il barometro LPS25H
//****************************************************************************************************//

// Header della classe
#include <Sensor/sap_barometer.h>

// Header STL
#include <string>
#include <sstream>
#include <iomanip>

using namespace std;
using namespace drex::sensor;

// Costruttori
//Barometer::Barometer(Barometer *other) :
//	QObject(other->parent()),
//	GenericSensor(),
//	I2CSensor(other->_device, other->_address) {
//	_logger = spdlog::get("logger");
//	this->_thermometer_frequency = other->_thermometer_frequency;
//	this->_barometer_frequency = other->_barometer_frequency;
//	this->_publishing = other->_publishing;
	
//	// Collego i timer
//	QObject::connect(&_thermometer_timer, &QTimer::timeout, other, &Barometer::publishTemperature, Qt::QueuedConnection);
//	QObject::connect(&_barometer_timer, &QTimer::timeout, other, &Barometer::publishPressure, Qt::QueuedConnection);
//}
Barometer::Barometer(const string device, const unsigned char address, bool pressure_enabled, bool altitude_enabled, bool temperature_enabled, QDir storage_dir, qint64 filesize_limit, string pressure_filename, string altitude_filename, string temperature_filename, const double thermometer_frequency, const double barometer_frequency, QObject *parent) :
	QObject(parent),
	GenericSensor(), 
	I2CDevice(device, address),
	_pressure_enabled(pressure_enabled),
	_altitude_enabled(altitude_enabled),
	_temperature_enabled(temperature_enabled),
	_storage_dir(storage_dir),
	_filesize_limit(filesize_limit),
	_pressure_filename(QString::fromStdString(pressure_filename)),
	_altitude_filename(QString::fromStdString(altitude_filename)),
	_temperature_filename(QString::fromStdString(temperature_filename)),
	_thermometer_frequency(thermometer_frequency),
	_barometer_frequency(barometer_frequency),
	_publishing(false) {
	_logger = spdlog::get("logger");
	
	// Collego i timer
	if (_temperature_enabled) QObject::connect(&_thermometer_timer, &QTimer::timeout, this, &Barometer::publishTemperature, Qt::QueuedConnection);
	if (_pressure_enabled || _altitude_enabled) QObject::connect(&_barometer_timer, &QTimer::timeout, this, &Barometer::publishPressureAndAltitude, Qt::QueuedConnection);
}

// Distruttore
Barometer::~Barometer() {
	if (_temperature_enabled) QObject::disconnect(&_thermometer_timer, &QTimer::timeout, this, &Barometer::publishTemperature);
	if (_pressure_enabled || _altitude_enabled) QObject::disconnect(&_barometer_timer, &QTimer::timeout, this, &Barometer::publishPressureAndAltitude);
}

// Inizializzazione
bool Barometer::initDevice() {
	_logger->info("[Barometer] init device");
	
	// Apro il dispositivo
	if (!i2cOpen()) {
		return false;
	}
	
	// Configuro i registri
	unsigned char init_values[][2] = {
		{REF_P_XL, REF_P_XL__DEFAULT},
		{REF_P_L, REF_P_L__DEFAULT},
		{REF_P_H, REF_P_H__DEFAULT},
		
		{RES_CONF, RES_CONF__DEFAULT},
		
		{CTRL_REG1, CTRL_REG1__DEFAULT},
		{CTRL_REG2, CTRL_REG2__DEFAULT},
		{CTRL_REG3, CTRL_REG3__DEFAULT},
		{CTRL_REG4, CTRL_REG4__DEFAULT},
		
		{INTERRUPT_CFG, INTERRUPT_CFG__DEFAULT},
		
		{STATUS_REG, STATUS_REG__DEFAULT},
		
		{FIFO_CTRL, FIFO_CTRL__DEFAULT},
		
		{THS_P_L, THS_P_L__DEFAULT},
		{THS_P_H, THS_P_H__DEFAULT},
		
		{RPDS_L, RPDS_L__DEFAULT},
		{RPDS_H, RPDS_H__DEFAULT}
	};
	
	// Imposto la configurazione
	for (unsigned int r = 0; r < sizeof(init_values) / 2; r++) {
		unsigned char *config = init_values[r];
		
		if (!i2cSetRegister(config[0], config[1])) {
			i2cClose();
			return false;
		}
	}
	
	// Chiudo il dispositio
	if (!i2cClose()) {
		return false;
	}
	
	return true;
}

// Inizio lettura
bool Barometer::startReading() {
	_logger->info("[Barometer] start reading");
	
	// Verifico se l'acquisizione e' gia' in corso
	if (_publishing) {
		_logger->warn("[Barometer] already publishing");
		return true;
	}
	_publishing = true;
	
	// Avvio i timer se la frequenza specificata è maggiore di zero
	auto start_timer = [this] (const char *name, double f, QTimer &t) {
		if (f > 0.0) {
			int ms;
			try {
				ms = (int) (1000 / f);
				if (ms <= 0) ms = 1;
				t.start(ms);
				return true;
			}
			catch (exception e) {
				string err;
				err += "[Barometer] cannot start ";
				err += name;
				err += " timer: ";
				err += e.what();
				_logger->warn(err);
				return false;
			}
		}
		return true;
	};
	
	if (_temperature_enabled) {
		if (!start_timer("thermometer", _thermometer_frequency, _thermometer_timer)) {
			return false;
		}
	}
	
	if (_pressure_enabled || _altitude_enabled) {
		if (!start_timer("barometer", _barometer_frequency, _barometer_timer)) {
			return false;
		}
	}
	
	// Creo se non esistono, altrimenti apro i file di storage
	auto init_storage = [this] (QFile &file, QDir &storage_dir, QString &filename, qint64 &filesize, int &index) {
		bool exists;
		
		if (!_getNextFreeIndex(storage_dir, filename, filesize, index, exists)) {
			_logger->error("Failed to get next free file index");
			return false;
		}
		
		// Imposto il nome del file
		file.setFileName(storage_dir.absoluteFilePath(filename + QString(".") + QString::number(index)));
		
		if (exists) {
			// Se esiste lo apro in modalità Append
			file.open(QIODevice::Append);
		}
		else {
			file.open(QIODevice::WriteOnly);
		}
		
		return true;
	};
	
	if (_temperature_enabled) {
		if (!init_storage(_temperature_file, _storage_dir, _temperature_filename, _filesize_limit, _temperature_file_index)) {
			return false;
		}
	}
	
	if (_pressure_enabled) {
		if (!init_storage(_pressure_file, _storage_dir, _pressure_filename, _filesize_limit, _pressure_file_index)) {
			return false;
		}
	}
	
	if (_altitude_enabled) {
		if (!init_storage(_altitude_file, _storage_dir, _altitude_filename, _filesize_limit, _altitude_file_index)) {
			return false;
		}
	}
	
	return true;
}

// Fine lettura
bool Barometer::stopReading() {
	_logger->info("[Barometer] stop reading");
	
	// Verifico se l'acquisizione e' in corso
	if (!_publishing) {
		_logger->warn("[Barometer] not publishing yet");
		return true;
	}
	_publishing = false;
	
	// Fermo i timer attivi
	if (_thermometer_timer.isActive()) {
		_thermometer_timer.stop();
	}
	
	if (_barometer_timer.isActive()) {
		_barometer_timer.stop();
	}
	
	// Chiudo i file
	if (_pressure_enabled) _pressure_file.close();
	if (_altitude_enabled) _altitude_file.close();
	if (_temperature_enabled) _temperature_file.close();
	
	return true;
}

// Lettura della temperatura
bool Barometer::getTemperature(double &value) {
	_logger->info("[Barometer] get temperature");
	
	// Apro il dispositivo
	if (!i2cOpen()) {
		return false;
	}
	
	bool is_ready = false;
	
	if (!_isTemperatureReady(is_ready)) {
		i2cClose();
		return false;
	}
	
	if (!is_ready) {
		if (!_doMeasure()) {
			i2cClose();
			return false;
		}
	
		if (!_waitForTemperatureReady()) {
			i2cClose();
			return false;
		}
	}
	
	// Leggo il dato
	if (!_getTemperature(value)) {
		i2cClose();
		return false;
	}
	
	// Chiudo il dispositivo
	if (!i2cClose()) {
		return false;
	}
	
	return true;
}
bool Barometer::getRawTemperature(int16_t &value) {
	_logger->info("[Barometer] get raw temperature");
	
	// Leggo il dato di temperatura in formato numerico
	// e lo restituisco senza elaborazioni
	
	// Apro il dispositivo
	if (!i2cOpen()) {
		return false;
	}
	
	bool is_ready = false;
	
	if (!_isTemperatureReady(is_ready)) {
		i2cClose();
		return false;
	}
	
	if (!is_ready) {
		if (!_doMeasure()) {
			i2cClose();
			return false;
		}
		
		if (!_waitForTemperatureReady()) {
			i2cClose();
			return false;
		}
	}
	
	// Leggo il dato grezzo
	if (!_getRawTemperature(value)) {
		i2cClose();
		return false;
	}
	
	// Chiudo il dispositivo
	if (!i2cClose()) {
		return false;
	}
	
	return true;
}

// Lettura della pressione
bool Barometer::getPressure(double &value) {
	_logger->info("[Barometer] get pressure");
	
	// Apro il dispositivo
	if (!i2cOpen()) {
		return false;
	}
	
	bool is_ready = false;
	
	if (!_isPressureReady(is_ready)) {
		i2cClose();
		return false;
	}
	
	if (!is_ready) {
		if (!_doMeasure()) {
			i2cClose();
			return false;
		}
		
		if (!_waitForPressureReady()) {
			i2cClose();
			return false;
		}
	}
	
	// Leggo il dato
	if (!_getPressure(value)) {
		i2cClose();
		return false;
	}
	
	// Chiudo il dispositivo
	if (!i2cClose()) {
		return false;
	}
	
	return true;
}
bool Barometer::getRawPressure(int32_t &value) {
	_logger->info("[Barometer] get raw pressure");
	
	// Leggo il dato di pressione in formato numerico
	// e lo restituisco senza elaborazioni
	
	// Apro il dispositivo
	if (!i2cOpen()) {
		return false;
	}
	
	bool is_ready = false;
	
	if (!_isPressureReady(is_ready)) {
		i2cClose();
		return false;
	}
	
	if (!is_ready) {
		if (!_doMeasure()) {
			i2cClose();
			return false;
		}
		
		if (!_waitForPressureReady()) {
			i2cClose();
			return false;
		}
	}
	
	// Leggo il dato grezzo
	if (!_getRawPressure(value)) {
		i2cClose();
		return false;
	}
	
	// Chiudo il dispositivo
	if (!i2cClose()) {
		return false;
	}
	
	return true;
}
bool Barometer::getAltitudeFromPressure(double &pressure, double &altitude) {
	// Formula di conversione mBar -> m
	altitude = (1 - std::pow(pressure / SEA_LEVEL_PRESSURE, PRESSURE_EXP)) * PRESSURE_FACTOR * FT_TO_M_RATIO;
	return true;
}

// Pubblicazione del dato
void Barometer::publishTemperature() {
	_logger->info("[Barometer] publish temperature");
	
	double temperature;
	
	if (!getTemperature(temperature)) {
		_logger->warn("[Barometer] failed to read temperature");
		return;
	}
	
	// Formatto la stringa
	stringstream str_temperature;
	str_temperature << temperature; // std::setprecision(3) << 
	
	// Creo il messaggio e lo invio
	Message msg;
	msg += "BAROMETER/TEMPERATURE";
	msg += (string("TEMPERATURE = ") + to_string(temperature)).c_str();
	//msg += _measure_timestamp.toString("dd/MM/yyyy HH:mm:ss.zzz").toStdString().c_str();
	emit requestPublishMessage(msg);
	
	// Salvo il dato su file
	QString record;
	record += QString("[") + QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss.zzz") + QString("] ");
	record += QString::number(temperature) + QString("\n");
	_temperature_file.write(record.toStdString().c_str());
	
	// Se il file supera la dimensione massima consentita lo chiudo e apro un nuovo file
	if (_temperature_file.size() >= _filesize_limit) {
		_temperature_file.close();
		_temperature_file_index++;
		_temperature_file.setFileName(_storage_dir.absoluteFilePath(_temperature_filename + QString(".") + QString::number(_temperature_file_index)));
		_temperature_file.open(QIODevice::WriteOnly);
	}
}
void Barometer::publishPressureAndAltitude() {
	_logger->info("[Barometer] publish pressure and altitude");
	
	double pressure, altitude;
	
	if (!getPressure(pressure)) {
		_logger->warn("[Barometer] failed to read pressure");
		return;
	}
	
	// Creo il messaggio e lo invio
	Message msg;
	
	if (_pressure_enabled) {
		msg.clear();
		msg += "BAROMETER/PRESSURE";
		msg += (string("PRESSURE = ") + to_string(pressure)).c_str();
		//msg += _measure_timestamp.toString("dd/MM/yyyy HH:mm:ss.zzz").toStdString().c_str();
		emit requestPublishMessage(msg);
		
		// Salvo il dato su file
		QString record;
		record += QString("[") + QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss.zzz") + QString("] ");
		record += QString::number(pressure) + QString("\n");
		_pressure_file.write(record.toStdString().c_str());
		
		// Se il file supera la dimensione massima consentita lo chiudo e apro un nuovo file
		if (_pressure_file.size() >= _filesize_limit) {
			_pressure_file.close();
			_pressure_file_index++;
			_pressure_file.setFileName(_storage_dir.absoluteFilePath(_pressure_filename + QString(".") + QString::number(_pressure_file_index)));
			_pressure_file.open(QIODevice::WriteOnly);
		}
	}
	
	if (_altitude_enabled) {
		// Calcolo l'altitudine in funzione della pressione
		if (getAltitudeFromPressure(pressure, altitude)) {
			msg.clear();
			msg += "BAROMETER/ALTITUDE";
			msg += (string("ALTITUDE = ") + to_string(altitude)).c_str();
			//msg += _measure_timestamp.toString("dd/MM/yyyy HH:mm:ss.zzz").toStdString().c_str();
			emit requestPublishMessage(msg);
		}
		else {
			_logger->warn("[Barometer] Failed to convert pressure in altitude");
			return;
		}
		
		// Salvo il dato su file
		QString record;
		record += QString("[") + QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss.zzz") + QString("] ");
		record += QString::number(altitude) + QString("\n");
		_altitude_file.write(record.toStdString().c_str());
		
		// Se il file supera la dimensione massima consentita lo chiudo e apro un nuovo file
		if (_altitude_file.size() >= _filesize_limit) {
			_altitude_file.close();
			_altitude_file_index++;
			_altitude_file.setFileName(_storage_dir.absoluteFilePath(_altitude_filename + QString(".") + QString::number(_altitude_file_index)));
			_altitude_file.open(QIODevice::WriteOnly);
		}
	}
}

bool Barometer::_doMeasure() {
	_logger->info("[Barometer] _ do measure");
	
	__u8 ctrl_reg2;
	
	// Leggo il registro
	if (!i2cGetRegister(CTRL_REG2, ctrl_reg2)) {
		return false;
	}
	
	// Invio la richiesta di misura
	if (!i2cSetRegister(CTRL_REG2, ctrl_reg2 | (1 << CTRL_REG2_ONE_SHOT))) {
		return false;
	}
	
	_measure_timestamp = QDateTime::currentDateTime();
	
	return true;
}
bool Barometer::_isTemperatureReady(bool &value) {
	_logger->info("[Barometer] _ is temperature ready");
	
	// Verifico se il dato di temperatura e' disponibile
	__u8 status_reg;
	if (!i2cGetRegister(STATUS_REG, status_reg)) {
		return false;
	}
	
	value = (status_reg & (1 << STATUS_REG_T_DA)) != 0x00;
	
	return true;
}
bool Barometer::_isPressureReady(bool &value) {
	_logger->info("[Barometer] _ is pressure ready");
	
	// Verifico se il dato di pressione e' disponibile
	__u8 status_reg;
	if (!i2cGetRegister(STATUS_REG, status_reg)) {
		return false;
	}
	
	value = (status_reg & (1 << STATUS_REG_P_DA)) != 0x00;
	
	return true;
}
bool Barometer::_waitForTemperatureReady() {
	_logger->info("[Barometer] _ wait for temperature ready");
	
	// TODO implementare un timeout
	// TODO mettere la lettura in un thread a parte
	
	// Aspetto che la misura sia effettuata
	bool is_ready = false;
	do {
		if (!_isTemperatureReady(is_ready)) {
			return false;
		}
		usleep(1000);
	}
	while (!is_ready);
	
	return true;
}
bool Barometer::_waitForPressureReady() {
	_logger->info("[Barometer] _ wait for pressure ready");
	
	// TODO implementare un timeout
	// TODO mettere la lettura in un thread a parte
	
	// Aspetto che la misura sia effettuata
	bool is_ready = false;
	do {
		if (!_isPressureReady(is_ready)) {
			return false;
		}
		usleep(1000);
	}
	while (!is_ready);
	
	return true;
}
bool Barometer::_getRawTemperature(int16_t &value) {
	_logger->info("[Barometer] _ get raw temperature");
	
	// Parte bassa
	__u8 temp;
	if (!i2cGetRegister(TEMP_OUT_L, temp)) {
		return false;
	}
	value = temp;
	
	// Parte alta
	if (!i2cGetRegister(TEMP_OUT_H, temp)) {
		return false;
	}
	value |= temp << 8;
	
	return true;
}
bool Barometer::_getRawPressure(int32_t &value) {
	_logger->info("[Barometer] _ get raw pressure");
	
	// Parte bassa
	__u8 temp;
	if (!i2cGetRegister(PRESS_OUT_XL, temp)) {
		return false;
	}
	value = temp;
	
	// Parte centrale
	if (!i2cGetRegister(PRESS_OUT_L, temp)) {
		return false;
	}
	value |= temp << 8;
	
	// Parte alta
	if (!i2cGetRegister(PRESS_OUT_H, temp)) {
		return false;
	}
	value |= temp << 16;
	
	return true;
}
bool Barometer::_getTemperature(double &value) {
	_logger->info("[Barometer] _ get temperature");
	
	int16_t raw;
	
	// Leggo il dato grezzo
	if (!_getRawTemperature(raw)) {
		return false;
	}
	
	// Lo converto in gradi centigradi
	value = TEMPERATURE_OFFSET + raw * TEMPERATURE_SCALE;
	
	return true;
}
bool Barometer::_getPressure(double &value) {
	_logger->info("[Barometer] _ get pressure");
	
	int32_t raw;
	
	// Leggo il dato grezzo
	if (!_getRawPressure(raw)) {
		return false;
	}
	
	// Lo converto in hPa
	value = PRESSURE_OFFSET + raw * PRESSURE_SCALE;
	
	return true;
}

// Metodi per il salvataggio dei dati
bool Barometer::_getNextFreeIndex(QDir &folder, QString &filename, qint64 &size_limit, int &index, bool &file_exists) {
	QStringList filters;
	filters += filename + QString(".*");
	
	QFileInfoList files;
	files = folder.entryInfoList(filters);
	
	// Non esistono file a cui collegarsi
	if (files.count() == 0) {
		index = 0;
		file_exists = false;
		return true;
	}
	
	// Trovo l'indice massimo
	QRegularExpression regex;
	QRegularExpressionMatch match;
	int max_index = 0;
	QFileInfo max_fi;
	regex.setPattern(QString("^") + filename + QString("\\.(?<index>\\d+)$"));
	for (int i = 0; i < files.count(); i++) {
		match = regex.match(files[i].fileName());
		if (!match.hasMatch()) continue;
		int temp;
		temp = match.captured("index").toInt();
		if (max_index < temp) {
			max_fi = files[i];
			max_index = temp;
		}
	}
	
	if (max_fi.size() >= size_limit) {
		index = max_index + 1;
		file_exists = false;
	}
	else {
		index = max_index;
		file_exists = true;
	}
	
	return true;
}



































