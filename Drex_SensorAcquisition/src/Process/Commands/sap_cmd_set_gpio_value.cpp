//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		18/09/2017
//	Descrizione:		Comando per il settaggio del valore di un gpio
//****************************************************************************************************//

// Header del processo
#include <Process/sap.h>

using namespace std;
using namespace drex::bidirectional;
using namespace drex::processes;

// Impostazione valore del GPIO
bool SAProcess::commandSetGPIOValue(const Message &msg, vector<const char *> &parameters) {
	// Cambio il valore del pin in uscita
	_logger->info("Command: Set GPIO Value");
	
	// Verifica della sintassi
	QList<QRegularExpressionMatch> matches;
	if (!verifyCommandSyntax(msg, parameters, matches)) {
		return false;
	}
	
	// Messaggio di risposta
	Message ans;
	ans += msg[0];
	
	if (!_gpio_expander_enabled) {
		ans += "ERROR";
		ans += "GPIO Expander disabled";
		_command_socket.socket->sendMessage(ans);
		return false;
	}
	
	// Estraggo il nome del GPIO
	QString name = matches[0].captured("name").toUpper();
	bool value = (matches[1].captured("value") == "1");
	
	// Verifico se è un GPIO Output
	GPIOPinMode mode;
	unsigned int number;
	bool found = false;
	for (number = 0; number < GPIOExpander::GPIO_NUMBER; number++) {
		QString gpio_name;
		
		if (!_gpio_expander->getPinName(number, gpio_name)) {
			ans += "ERROR";
			ans += "Failed to retrive GPIO Pin name";
			_command_socket.socket->sendMessage(ans);
			return false;
		}
		
		if (gpio_name == name) {
			found = true;
			break;
		}
	}
	
	// Pin non trovato
	if (!found) {
		ans += "ERROR";
		ans += "GPIO Pin name unknown";
		_command_socket.socket->sendMessage(ans);
		return false;
	}
	
	if (!_gpio_expander->getPinMode(number, mode)) {
		ans += "ERROR";
		ans += "Failed to retrive GPIO Pin mode";
		_command_socket.socket->sendMessage(ans);
		return false;
	}
	
	if (!_gpio_expander->setPinValue(number, value)) {
		ans += "ERROR";
		ans += "Failed to set pin value";
		_command_socket.socket->sendMessage(ans);
		return false;
	}
	
	// Invio la risposta
	ans += "OK";
	_command_socket.socket->sendMessage(ans);
	
	return true;
}
