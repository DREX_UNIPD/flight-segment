//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		08/10/2017
//	Descrizione:		Comando per il settaggio del valore di uscita del PAM
//****************************************************************************************************//

// Header del processo
#include <Process/sap.h>

using namespace std;
using namespace drex::processes;
using namespace drex::actuator;

// Lettura stato del processo
bool SAProcess::commandSetPAM(const Message &msg, vector<const char *> &parameters) {
	_logger->info("Command: Set PAM");
	
	// Verifica della sintassi
	QList<QRegularExpressionMatch> matches;
	if (!verifyCommandSyntax(msg, parameters, matches)) {
		return false;
	}
	
	// Estraggo i dati
	string dac = matches[0].captured("dac").toUpper().toStdString();
	double current = matches[1].captured("current").toDouble();
	bool persistent = matches[2].captured("persistent").toLower() == "true";
	
	auto dac_set = [this, msg] (bool &enabled, DAC *&dac_device, double &current, bool &persistent, const char *strerr_disabled, const char *strerr_failed) {
		Message ans;
		ans += msg[0];
		
		// Verifico che il dispositivo sia abilitato
		if (!enabled) {
			ans += "ERROR";
			ans += strerr_disabled;
			_command_socket.socket->sendMessage(ans);
			return false;
		}
		
		// Imposto l'uscita desiderata
		if (!setPAM(dac_device, current, persistent)) {
			ans += "ERROR";
			ans += strerr_failed;
			_command_socket.socket->sendMessage(ans);
			return false;
		}
		
		ans += "OK";
		_command_socket.socket->sendMessage(ans);
		
		return true;
	};
	
	if (dac == "LED1") {
		if (!dac_set(_dac_led_1_enabled, _dac_led_1, current, persistent, "DAC LED 1 disabled", "Failed to set DAC LED 1 output")) {
			return false;
		}
	}
	
	if (dac == "LED2") {
		if (!dac_set(_dac_led_2_enabled, _dac_led_2, current, persistent, "DAC LED 2 disabled", "Failed to set DAC LED 2 output")) {
			return false;
		}
	}
	
	if (dac == "HEATER") {
		if (!dac_set(_dac_heater_enabled, _dac_heater, current, persistent, "DAC Heater disabled", "Failed to set DAC Heater output")) {
			return false;
		}
	}
	
	if (dac == "THERMALCUTTER") {
		if (!dac_set(_dac_thermalcutter_enabled, _dac_thermalcutter, current, persistent, "DAC Thermalcutter disabled", "Failed to set DAC Thermalcutter output")) {
			return false;
		}
	}
	
	return true;
}
