//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		18/09/2017
//	Descrizione:		Comando per il settaggio del pwm di un gpio
//****************************************************************************************************//

// Header del processo
#include <Process/sap.h>

using namespace std;
using namespace drex::bidirectional;
using namespace drex::processes;

// Impostazione del valore PWM del GPIO
bool SAProcess::commandSetGPIOPWM(const Message &msg, vector<const char *> &parameters) {
	// Cambio il pwm del pin in uscita
	_logger->info("Command: Set GPIO PWM");
	
	// Verifica della sintassi
	QList<QRegularExpressionMatch> matches;
	if (!verifyCommandSyntax(msg, parameters, matches)) {
		return false;
	}
	
	// Messaggio di risposta
	Message ans;
	ans += msg[0];
	
	if (!_gpio_expander_enabled) {
		ans += "ERROR";
		ans += "GPIO Expander disabled";
		_command_socket.socket->sendMessage(ans);
		return false;
	}
	
	// Estraggo il nome del GPIO
	QString name = matches[0].captured("name").toUpper();
	double pwm = matches[1].captured("pwm").toDouble();
	
	// Verifico che il valore di PWM sia nel range 0-100
	if (pwm < 0.0 || pwm > 100.0) {
		ans += "ERROR";
		ans += "PWM value out of range (0 - 100)";
		_command_socket.socket->sendMessage(ans);
		return false;
	}
	
	// Verifico se è un GPIO PWM
	GPIOPinMode mode;
	unsigned int number;
	bool found = false;
	for (number = 0; number < GPIOExpander::GPIO_NUMBER; number++) {
		QString gpio_name;
		
		if (!_gpio_expander->getPinName(number, gpio_name)) {
			ans += "ERROR";
			ans += "Failed to retrive GPIO Pin name";
			_command_socket.socket->sendMessage(ans);
			return false;
		}
		
		if (gpio_name == name) {
			found = true;
			break;
		}
	}
	
	// Pin non trovato
	if (!found) {
		ans += "ERROR";
		ans += "GPIO Pin name unknown";
		_command_socket.socket->sendMessage(ans);
		return false;
	}
	
	if (!_gpio_expander->getPinMode(number, mode)) {
		ans += "ERROR";
		ans += "Failed to retrive GPIO Pin mode";
		_command_socket.socket->sendMessage(ans);
		return false;
	}
	
	if (mode != GPIOPinMode::PWM) {
		ans += "ERROR";
		ans += (string("Wrong GPIO Pin mode: needed PWM, found ") + GPIOPinConfiguration::mode_to_string(mode)).c_str();
		_command_socket.socket->sendMessage(ans);
		return false;
	}
	
	if (!_gpio_expander->setPinPWM(number, pwm / 100.0)) {
		ans += "ERROR";
		ans += "Failed to set pin PWM";
		_command_socket.socket->sendMessage(ans);
		return false;
	}
	
	// Invio la risposta
	ans += "OK";
	_command_socket.socket->sendMessage(ans);
	
	return true;
}
