//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		19/09/2017
//	Descrizione:		Comando per il settaggio della modalità di un gpio
//****************************************************************************************************//

// Header del processo
#include <Process/sap.h>

using namespace std;
using namespace drex::bidirectional;
using namespace drex::processes;

// Impostazione modalità del GPIO
bool SAProcess::commandSetGPIOMode(const Message &msg, vector<const char *> &parameters) {
	// Cambio il valore del pin in uscita
	_logger->info("Command: Set GPIO Mode");
	
	// Verifica della sintassi
	QList<QRegularExpressionMatch> matches;
	if (!verifyCommandSyntax(msg, parameters, matches)) {
		return false;
	}
	
	// Messaggio di risposta
	Message ans;
	ans += msg[0];
	
	if (!_gpio_expander_enabled) {
		ans += "ERROR";
		ans += "GPIO Expander disabled";
		_command_socket.socket->sendMessage(ans);
		return false;
	}
	
	// Estraggo il nome del GPIO
	QString name = matches[0].captured("name").toUpper();
	QString mode = matches[1].captured("mode").toUpper();
	
	// Cerco il GPIO associato al nome indicato
	unsigned int number;
	bool found = false;
	for (number = 0; number < GPIOExpander::GPIO_NUMBER; number++) {
		QString gpio_name;
		
		if (!_gpio_expander->getPinName(number, gpio_name)) {
			ans += "ERROR";
			ans += "Failed to retrive GPIO Pin name";
			_command_socket.socket->sendMessage(ans);
			return false;
		}
		
		if (gpio_name == name) {
			found = true;
			break;
		}
	}
	
	// Pin non trovato
	if (!found) {
		ans += "ERROR";
		ans += "GPIO Pin name unknown";
		_command_socket.socket->sendMessage(ans);
		return false;
	}
	
	// Imposto la modalità indicata
	bool done = false;
	if (mode == "DISABLED") done = _gpio_expander->setPinMode(number, GPIOPinMode::Disabled);
	if (mode == "INPUT") done = _gpio_expander->setPinMode(number, GPIOPinMode::Input);
	if (mode == "OUTPUT") done = _gpio_expander->setPinMode(number, GPIOPinMode::Output);
	if (mode == "OPEN DRAIN") done = _gpio_expander->setPinMode(number, GPIOPinMode::OpenDrain);
	if (mode == "PWM") done = _gpio_expander->setPinMode(number, GPIOPinMode::PWM);
	
	// Verifico se l'operazione è andata a buon fine
	if (!done) {
		ans += "ERROR";
		ans += "Failed to set GPIO Pin mode";
		_command_socket.socket->sendMessage(ans);
		return false;
	}
	
	// Invio la risposta
	ans += "OK";
	_command_socket.socket->sendMessage(ans);
	
	return true;
}
