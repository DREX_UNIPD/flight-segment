//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		29/07/2017
//	Descrizione:		Comando per l'interrogazione dello stato del processo
//****************************************************************************************************//

// Header del processo
#include <Process/sap.h>

using namespace std;
using namespace drex::processes;

// Lettura stato del processo
bool SAProcess::commandGetState(const Message &msg, vector<const char *> &) {
	_logger->info("Command: Get State");
	
	string state;
	state = getState();
	
	// Restituisco lo stato
	Message ans;
	ans += msg[0];
	ans += "OK";
	ans += state.c_str();
	_command_socket.socket->sendMessage(ans);
	
	return true;
}
