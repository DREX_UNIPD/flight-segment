//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		29/07/2017
//	Descrizione:		Richiesta dello stato attuale
//****************************************************************************************************//

// Header del processo
#include <Process/sap.h>

using namespace std;
using namespace drex::processes;
using namespace drex::state;

string SAProcess::getState() {
	_logger->info("Get state");
	
	return _state;
}
