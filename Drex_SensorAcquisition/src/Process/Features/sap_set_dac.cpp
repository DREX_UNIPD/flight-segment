//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		08/08/2017
//	Descrizione:		Impostazione del valore di uscita del DAC
//****************************************************************************************************//

// Header del processo
#include <Process/sap.h>

using namespace std;
using namespace drex::processes;

bool SAProcess::setDAC(drex::actuator::DAC *dac, double voltage, bool persistent) {
	_logger->info("Set DAC");
	
	// Imposto l'uscita del DAC
	return dac->setOutput(voltage, persistent);
}
