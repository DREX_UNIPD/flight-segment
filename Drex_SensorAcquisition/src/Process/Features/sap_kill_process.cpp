//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		29/07/2017
//	Descrizione:		Terminazione del processo
//****************************************************************************************************//

// Header del processo
#include <Process/sap.h>

using namespace drex::processes;

// Metodo per la terminazione del processo
void SAProcess::killProcess() {
	_logger->info("Kill process");
	
	// Mando il segnale di chiusura del processo
	QTimer::singleShot(0, this, &SAProcess::quit);
}
