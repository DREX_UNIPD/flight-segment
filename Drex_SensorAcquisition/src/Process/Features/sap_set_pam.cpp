//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		08/10/2017
//	Descrizione:		Impostazione del valore di uscita del PAM
//****************************************************************************************************//

// Header del processo
#include <Process/sap.h>

using namespace std;
using namespace drex::processes;

bool SAProcess::setPAM(drex::actuator::DAC *dac, double current, bool persistent) {
	_logger->info("Set PAM");
	
	double voltage;
	
	// Verifico se il PAM dev'essere spento
	if (current == 0.0) {
		voltage = 0.0;
		return dac->setOutput(voltage, persistent);
	}
	
	// Verifico che i parametri siano corretti
	if (current < _pam_current_min) {
		_logger->error(string("Current (") + to_string(current) + string(") is lower than minimum current (") + to_string(_pam_current_min) + string(")"));
		return false;
	}
	if (current > _pam_current_max) {
		_logger->error(string("Current (") + to_string(current) + string(") is higher than maximum current (") + to_string(_pam_current_max) + string(")"));
		return false;
	}
	
	// Converto la corrente in tensione
	double m, q;
	m = (_pam_voltage_max - _pam_voltage_min) / (_pam_current_max - _pam_current_min);
	q = _pam_voltage_min - _pam_current_min * m;
	
	voltage = current * m + q;
	
	// Imposto l'uscita del PAM
	return dac->setOutput(voltage, persistent);
}
