//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		29/07/2017
//	Descrizione:		Inizializzazione del logger su file
//****************************************************************************************************//

// Header del processo
#include <Process/sap.h>

// Header per logging
#include <PublisherSink/publisher_sink.h>

using namespace std;
using namespace nzmqt;
using namespace drex::processes;

bool SAProcess::initFileLogger() {
	// Inizializzazione del logger su file
	
	// Leggo la configurazione del logger su file
	const YAML::Node &file_sink = _configuration["FileSink"];
	string name = file_sink["Name"].as<string>();
	int max_size = file_sink["MaxSize"].as<int>();
	int max_number = file_sink["MaxNumber"].as<int>();
	
	// Creo il logger con tre sink, console, file e publisher
	string filepath = _storage_log_dir.absoluteFilePath(name.c_str()).toStdString();
	PublisherSink *ps = new PublisherSink();
	std::shared_ptr<PublisherSink> shared_ps(ps);
	QObject::connect(ps, &PublisherSink::requestSendLogMessage, this, &SAProcess::publishLog, Qt::QueuedConnection);
	std::vector<spdlog::sink_ptr> sinks;
	sinks.push_back(std::make_shared<spdlog::sinks::ansicolor_sink>(spdlog::sinks::stdout_sink_mt::instance()));
	sinks.push_back(std::make_shared<spdlog::sinks::rotating_file_sink_mt>(filepath, max_size, max_number));
	sinks.push_back(shared_ps);
	_logger = std::make_shared<spdlog::logger>("logger", begin(sinks), end(sinks));
	spdlog::register_logger(_logger);
	
	return true;
}
