//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		29/07/2017
//	Descrizione:		Inizializzazione dell'interfaccia di rete
//****************************************************************************************************//

// Header del processo
#include <Process/sap.h>

using namespace std;
using namespace nzmqt;
using namespace drex::processes;

bool SAProcess::initNetworking() {
	// Inizializzazione dell'interfaccia di rete
	
	// Leggo i parametri dei socket
	const YAML::Node &config_sockets = _configuration["Sockets"];
	
	int default_linger = config_sockets["DefaultLinger"].as<int>();
	
	// Creo i socket
	_context = createDefaultContext();
	_context->start();
	
	// Command socket
	if (!initSocket(_command_socket, config_sockets["Command"], "Command", default_linger)) return false;
	QObject::connect(_command_socket.socket, &ZMQSocket::messageReceived, this, &SAProcess::commandSelector, Qt::QueuedConnection);
	if (!startSocket(_command_socket)) return false;
	
	// Logger socket
	if (!initSocket(_logger_socket, config_sockets["Logger"], "Logger", default_linger)) return false;
	if (!startSocket(_logger_socket)) return false;
	
	// Data socket
	if (!initSocket(_data_socket, config_sockets["Data"], "Data", default_linger)) return false;
	if (!startSocket(_data_socket)) return false;
	
	// Reply socket
	if (!initSocket(_reply_socket, config_sockets["Reply"], "Reply", default_linger)) return false;
	QObject::connect(_reply_socket.socket, &ZMQSocket::messageReceived, this, &SAProcess::publishMessage, Qt::QueuedConnection);
	if (!startSocket(_reply_socket)) return false;
	
	
	
	return true;
}
