//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		02/05/2017
//	Descrizione:		Inizializzazione dei sensori
//****************************************************************************************************//

// Header del processo
#include <Process/sap.h>

using namespace std;
using namespace drex::processes;
using namespace drex::sensor;

bool SAProcess::initSensors() {
	// Creazione degli oggetti sensore e assegnazione valori da configurazione
	const YAML::Node &sensors = _configuration["Sensors"];
	
	_imu = nullptr;
	_barometer = nullptr;
	_magnetometer = nullptr;
	_vacuometer = nullptr;
	_thermometer = nullptr;
//	_microswitch = nullptr;
	
	_imu_enabled = sensors["Imu"]["Enabled"].as<bool>();
	_barometer_enabled = sensors["Barometer"]["Enabled"].as<bool>();
	_magnetometer_enabled = sensors["Magnetometer"]["Enabled"].as<bool>();
	_vacuometer_enabled = sensors["Vacuometer"]["Enabled"].as<bool>();
	_thermometer_enabled = sensors["Thermometer"]["Enabled"].as<bool>();
//	_microswitch_enabled = sensors["Microswitch"]["Enabled"].as<bool>();
	
//	// Inizializzo i sensori
//	auto initSensor = [sensors, this] (const char *name, QObject *object, function<bool(const YAML::Node &)> init, function<void(const Message &)> signal) {
//		if (sensors[name]["Enabled"].as<bool>()){
//			if (init(sensors[name])) {
//				QObject::connect(object, &signal, this, &SAProcess::publishMessage, Qt::QueuedConnection);
//			}
//		}
//	};
	
//	initSensor("Imu", _imu, initIMU, Imu::requestPublishMessage);
	
	if (_imu_enabled){
		if (initIMU(sensors["Imu"])) {
			QObject::connect(_imu, &Imu::requestPublishMessage, this, &SAProcess::publishMessage, Qt::QueuedConnection);
		}
	}
	if (_barometer_enabled) {
		if (initBarometer(sensors["Barometer"])) {
			QObject::connect(_barometer, &Barometer::requestPublishMessage, this, &SAProcess::publishMessage, Qt::QueuedConnection);
		}
	}
	if (_magnetometer_enabled) {
		if (initMagnetometer(sensors["Magnetometer"])) {
			QObject::connect(_magnetometer, &Magnetometer::requestPublishMessage, this, &SAProcess::publishMessage, Qt::QueuedConnection);
		}
	}
	if (_vacuometer_enabled) {
		if (initVacuometer(sensors["Vacuometer"])) {
			QObject::connect(_vacuometer, &Vacuometer::requestPublishMessage, this, &SAProcess::publishMessage, Qt::QueuedConnection);
		}
	}
	if (_thermometer_enabled) {
		if (initThermometer(sensors["Thermometer"])) {
			QObject::connect(_thermometer, &Thermometer::requestPublishMessage, this, &SAProcess::publishMessage, Qt::QueuedConnection);
		}
	}
//	if (_microswitch_enabled) {
//		if (initMicroswitch(sensors["Microswitch"])) {
//			QObject::connect(_microswitch, &Microswitch::requestPublishMessage, this, &SAProcess::publishMessage, Qt::QueuedConnection);
//		}
//	}
	
	return true;
}

bool SAProcess::initIMU(const YAML::Node &imu) {
	_imu = new Imu(
		imu["Device"].as<string>(),
		(unsigned char) imu["Address"].as<unsigned int>(),
		imu["AccelerationEnabled"].as<bool>(),
		imu["FreeFallEnabled"].as<bool>(),
		imu["AngularRateEnabled"].as<bool>(),
		imu["TemperatureEnabled"].as<bool>(),
		_storage_sensor_dir,
		_storage_filesize_limit,
		imu["TemperatureFilename"].as<string>(),
		imu["AccelerationFilename"].as<string>(),
		imu["FreefallFilename"].as<string>(),
		imu["AngularRateFilename"].as<string>(),
		imu["FreefallAccelerationLimit"].as<double>(),
		imu["FreefallCounterLimit"].as<int>(),
		imu["ThermometerFrequency"].as<double>(),
		imu["GyroscopeFrequency"].as<double>(),
		imu["AccelerometerFrequency"].as<double>(),
		this
	);
	
	if (!_imu->initDevice()) {
		_logger->error("Imu init failed");
		_imu = nullptr;
		return false;
	}
	
	return true;
}
bool SAProcess::initBarometer(const YAML::Node &barometer) {
	_barometer = new Barometer(
		barometer["Device"].as<string>(),
		(unsigned char) barometer["Address"].as<unsigned int>(),
		barometer["PressureEnabled"].as<bool>(),
		barometer["AltitudeEnabled"].as<bool>(),
		barometer["TemperatureEnabled"].as<bool>(),
		_storage_sensor_dir,
		_storage_filesize_limit,
		barometer["PressureFilename"].as<string>(),
		barometer["AltitudeFilename"].as<string>(),
		barometer["TemperatureFilename"].as<string>(),
		barometer["ThermometerFrequency"].as<double>(),
		barometer["BarometerFrequency"].as<double>(),
		this
	);
	
	if (!_barometer->initDevice()) {
		_logger->error("Barometer init failed");
		_barometer = nullptr;
		return false;
	}
	
	return true;
}
bool SAProcess::initMagnetometer(const YAML::Node &magnetometer) {
	_magnetometer = new Magnetometer(
		magnetometer["Device"].as<string>(),
		(unsigned char) magnetometer["Address"].as<unsigned int>(),
		magnetometer["MagneticFieldEnable"].as<bool>(),
		magnetometer["TemperatureEnabled"].as<bool>(),
		magnetometer["ThermometerFrequency"].as<double>(),
		magnetometer["MagnetometerFrequency"].as<double>(),
		this
	);
	
	if (!_magnetometer->initDevice()) {
		_logger->error("Magnetometer init failed");
		_magnetometer = nullptr;
		return false;
	}
	
	return true;
}
bool SAProcess::initVacuometer(const YAML::Node &vacuometer) {
	_vacuometer = new Vacuometer(
		vacuometer["Device"].as<string>(),
		(unsigned char) vacuometer["Address"].as<unsigned int>(),
		vacuometer["PressureEnabled"].as<bool>(),
		vacuometer["AltitudeEnabled"].as<bool>(),
		vacuometer["TemperatureEnabled"].as<bool>(),
		_storage_sensor_dir,
		_storage_filesize_limit,
		vacuometer["PressureFilename"].as<string>(),
		vacuometer["AltitudeFilename"].as<string>(),
		vacuometer["TemperatureFilename"].as<string>(),
		vacuometer["PsiMin"].as<double>(),
		vacuometer["PsiMax"].as<double>(),
		(uint16_t) vacuometer["PsiRawMin"].as<int>(),
		(uint16_t) vacuometer["PsiRawMax"].as<int>(),
		vacuometer["TemperatureMin"].as<double>(),
		vacuometer["TemperatureMax"].as<double>(),
		(uint16_t) vacuometer["TemperatureRawMin"].as<int>(),
		(uint16_t) vacuometer["TemperatureRawMax"].as<int>(),
		vacuometer["PressureOffset"].as<double>(),
		vacuometer["Frequency"].as<double>(),
		this
	);
	
	if (!_vacuometer->initDevice()) {
		_logger->error("Vacuometer init failed");
		_vacuometer = nullptr;
		return false;
	}
	
	return true;
}
bool SAProcess::initThermometer(const YAML::Node &thermometer) {
	vector<int> config_channels = thermometer["Channels"].as<vector<int>>();
	QList<ChannelConfiguration> channels;
	
	// Per ogni canale considero PT1000 se positivo, tensione single ended se negativo
	for (unsigned int i = 0; i < config_channels.size(); i++) {
		ChannelConfiguration cc;
		cc.channel = config_channels.at(i);
		if (!(cc.temperature = (cc.channel < 128))) cc.channel = 256 - cc.channel;
		channels.append(cc);
	}
	
	_thermometer = new Thermometer(
		thermometer["Device"].as<string>(),
		(RSenseChannelAssignment) thermometer["RSenseChannel"].as<int>(),
		thermometer["RSenseValue"].as<double>(),
		channels,
		_storage_sensor_dir,
		_storage_filesize_limit,
		thermometer["TemperatureFilename"].as<string>(),
		thermometer["VoltageFilename"].as<string>(),
		thermometer["BatteryVoltage"]["Enabled"].as<bool>(),
		(__u8) thermometer["BatteryVoltage"]["Channel"].as<unsigned int>(),
		thermometer["BatteryVoltage"]["IdealRatio"].as<double>(),
		thermometer["BatteryVoltage"]["CorrectionFactor"].as<double>(),
		thermometer["Frequency"].as<double>(),
		this
	);
	
	if (!_thermometer->initDevice()) {
		_logger->error("Thermometer init failed");
		_thermometer = nullptr;
		return false;
	}
	
	return true;
}
//bool SAProcess::initMicroswitch(const YAML::Node &microswitch) {
//	_microswitch = new Microswitch(
//		microswitch["Frequency"].as<double>(),
//		this
//	);
	
//	if (!_microswitch->initDevice()) {
//		_logger->error("Microswitch init failed");
//		_microswitch = nullptr;
//		return false;
//	}
	
//	return true;
//}
