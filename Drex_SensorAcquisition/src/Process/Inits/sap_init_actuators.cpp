//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		08/08/2017
//	Descrizione:		Inizializzazione degli attuatori
//****************************************************************************************************//

// Header del processo
#include <Process/sap.h>

using namespace std;
using namespace drex::processes;
using namespace drex::actuator;

bool SAProcess::initActuators() {
	// Creazione degli oggetti attuatore e assegnazione valori da configurazione
	const YAML::Node &actuators = _configuration["Actuators"];
	
	// Inizializzazione DAC
	const YAML::Node &dacs = actuators["DACs"];
	
	_dac_led_1 = nullptr;
	_dac_led_2 = nullptr;
	_dac_heater = nullptr;
	_dac_thermalcutter = nullptr;
	
	_dac_led_1_enabled = dacs["DAC_Led_1"]["Enabled"].as<bool>();
	_dac_led_2_enabled = dacs["DAC_Led_2"]["Enabled"].as<bool>();
	_dac_heater_enabled = dacs["DAC_Heater"]["Enabled"].as<bool>();
	_dac_thermalcutter_enabled = dacs["DAC_Thermalcutter"]["Enabled"].as<bool>();
	
	if (_dac_led_1_enabled && !initDAC(dacs["DAC_Led_1"], _dac_led_1)) _logger->error("Failed to init DAC LED 1");
	if (_dac_led_2_enabled && !initDAC(dacs["DAC_Led_2"], _dac_led_2)) _logger->error("Failed to init DAC LED 2");
	if (_dac_heater_enabled && !initDAC(dacs["DAC_Heater"], _dac_heater)) _logger->error("Failed to init DAC Heater");
	if (_dac_thermalcutter_enabled && !initDAC(dacs["DAC_Thermalcutter"], _dac_thermalcutter)) _logger->error("Failed to init DAC Thermalcutter");
	
	// Conversione corrente tensione del PAM
	const YAML::Node &pam = actuators["PAM"];
	
	_pam_voltage_min = pam["Voltage"]["min"].as<double>();
	_pam_voltage_max = pam["Voltage"]["max"].as<double>();
	_pam_current_min = pam["Current"]["min"].as<double>();
	_pam_current_max = pam["Current"]["max"].as<double>();
	
	return true;
}

bool SAProcess::initDAC(const YAML::Node &dac_config, DAC *&dac) {
	dac = new DAC(
		dac_config["Device"].as<string>(),
		(unsigned char) dac_config["Address"].as<int>(),
		this
	);
	
	return true;
}
