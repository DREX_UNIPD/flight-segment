//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		09/07/2017
//	Descrizione:		Inizializzazione degli stati
//****************************************************************************************************//

// Header del processo
#include <Process/sap.h>

using namespace std;
using namespace drex::processes;
using namespace drex::state;

bool SAProcess::initStatesMap() {
	_logger->info("Init states map");
	
	// Elimino l'eventuale contenuto della mappa
	_state_objects.clear();
	
	// Popolo la mappa stato -> StateObject
	StateObject *so;
	
	// DEFAULT
	so = new StateObject();
	so->init = [this] () { return true; };
	
	// Help
	so->command.insert(CMD_HELP, [this] (const Message &msg, vector<const char *> &parameters) { return this->commandHelp(msg, parameters); });
	so->parameters.insert(CMD_HELP, vector<const char *>());
	
	// Kill
	so->command.insert(CMD_KILL, [this] (const Message &msg, vector<const char *> &parameters) { return this->commandKill(msg, parameters); });
	so->parameters.insert(CMD_KILL, vector<const char *>());
	
	// Get state
	so->command.insert(CMD_GET_STATE, [this] (const Message &msg, vector<const char *> &parameters) { return this->commandGetState(msg, parameters); });
	so->parameters.insert(CMD_GET_STATE, vector<const char *>());
	
	// Set state
	so->command.insert(CMD_SET_STATE, [this] (const Message &msg, vector<const char *> &parameters) { return this->commandSetState(msg, parameters); });
	so->parameters.insert(CMD_SET_STATE, vector<const char *>({
		"(?i)^ *(?<newstate>IDLE|RUN) *$"
	}));
	
	// Set GPIO value
	so->command.insert(CMD_SET_GPIO_VALUE, [this] (const Message &msg, vector<const char *> &parameters) { return this->commandSetGPIOValue(msg, parameters); });
	so->parameters.insert(CMD_SET_GPIO_VALUE, vector<const char *>({
		"(?i)^ *NAME *= *(?<name>\\w(?:\\w|\\d| |_)*)$",
		"(?i)^ *VALUE *= *(?<value>1|0) *$"
	}));
	
	// Set GPIO PWM
	so->command.insert(CMD_SET_GPIO_PWM, [this] (const Message &msg, vector<const char *> &parameters) { return this->commandSetGPIOPWM(msg, parameters); });
	so->parameters.insert(CMD_SET_GPIO_PWM, vector<const char *>({
		"(?i)^ *NAME *= *(?<name>\\w(?:\\w|\\d| |_)*)$",
		"(?i)^ *PWM *= *(?<pwm>\\d+\\.?\\d*) *$"
	}));
	
	// Set GPIO mode
	so->command.insert(CMD_SET_GPIO_MODE, [this] (const Message &msg, vector<const char *> &parameters) { return this->commandSetGPIOMode(msg, parameters); });
	so->parameters.insert(CMD_SET_GPIO_MODE, vector<const char *>({
		"(?i)^ *NAME *= *(?<name>\\w(?:\\w|\\d| |_)*)$",
		"(?i)^ *MODE *= *(?<mode>DISABLED|INPUT|OUTPUT|OPEN DRAIN|PWM) *$"
	}));
	
	// Set DAC
	so->command.insert(CMD_SET_DAC, [this] (const Message &msg, vector<const char *> &parameters) { return this->commandSetDAC(msg, parameters); });
	so->parameters.insert(CMD_SET_DAC, vector<const char *>({
		"(?i)^ *DAC *= *(?<dac>LED1|LED2|HEATER|THERMALCUTTER) *$",
		"(?i)^ *VOLTAGE *= *(?<voltage>\\d+\\.?\\d*) *$",
		"(?i)^ *PERSISTENT *= *(?<persistent>TRUE|FALSE) *$"
	}));
	
	// Set PAM
	so->command.insert(CMD_SET_PAM, [this] (const Message &msg, vector<const char *> &parameters) { return this->commandSetPAM(msg, parameters); });
	so->parameters.insert(CMD_SET_PAM, vector<const char *>({
		"(?i)^ *DAC *= *(?<dac>LED1|LED2|HEATER|THERMALCUTTER) *$",
		"(?i)^ *CURRENT *= *(?<current>\\d+\\.?\\d*) *$",
		"(?i)^ *PERSISTENT *= *(?<persistent>TRUE|FALSE) *$"
	}));
	
	so->cleanup = [this] () { return true; };
	_state_objects.insert(ST_DEFAULT, so);
	
	// IDLE
	so = new StateObject();
	so->init = [this] () { return this->stateIdleInit(); };
	so->cleanup = [this] () { return this->stateIdleCleanup(); };
	_state_objects.insert(ST_IDLE, so);
	
	// RUN
	so = new StateObject();
	so->init = [this] () { return this->stateRunInit(); };
	so->cleanup = [this] () { return this->stateRunCleanup(); };
	_state_objects.insert(ST_RUN, so);
	
	return true;
}



