//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		15/09/2017
//	Descrizione:		Inizializzazione dello storage
//****************************************************************************************************//

// Header del processo
#include <Process/sap.h>

using namespace std;
using namespace drex::processes;

// Inizializzo lo storage
bool SAProcess::initStorage() {
	// Leggo i parametri dello storage
	const YAML::Node &config_storage = _configuration["Storage"];
	
	// Carico le cartelle dello storage e le creo se non esistono
	auto create_dir = [this] (QDir &base, string path, string errmsg) {
		QString p = QString::fromStdString(path);
		if (!base.cd(p)) {
			if (!base.mkpath(p)) {
				_console->error(errmsg);
				return false;
			}
			base.cd(p);
		}
		return true;
	};
	
	// Dimensione massima dei file di storage dei sensori
	_storage_filesize_limit = config_storage["FilesizeLimit"].as<qint64>();
	
	_storage_home = QDir(config_storage["Common"]["Home"].as<string>().c_str());
	_storage_ssd_dir = _storage_home;
	
	// Verifico che esista la cartella SSD
	if (!_storage_ssd_dir.cd(QString::fromStdString(config_storage["Common"]["SSD"].as<string>()))) {
		_console->error("SSD folder doesn't exists");
		return false;
	}
	
	// Creo, se non esiste, la cartella del processo
	if (!create_dir(
		_storage_ssd_dir,
		config_storage["Process"].as<string>(),
		"Failed to create SSD process folder"
	)) return false;
	
	_storage_sensor_dir = _storage_ssd_dir;
	_storage_log_dir = _storage_ssd_dir;
	
	if (!create_dir(_storage_sensor_dir, config_storage["Sensor"].as<string>(), "Failed to create Sensor folder")) return false;
	if (!create_dir(_storage_log_dir, config_storage["Log"].as<string>(), "Failed to create Log folder")) return false;
	
	return true;
}

