//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		16/09/2017
//	Descrizione:		Inizializzazione dei dispositivi bidirezionali
//****************************************************************************************************//

// Header del processo
#include <Process/sap.h>

using namespace std;
using namespace drex::bidirectional;
using namespace drex::processes;

// Inizializzazione dei dispositivi bidirezionali
bool SAProcess::initBidirectionals() {
	// Inizializzazione GPIO Expander
	const YAML::Node &bidirectionals = _configuration["Bidirectionals"];
	
	_gpio_expander_enabled = bidirectionals["GPIOExpander"]["Enabled"].as<bool>();
	
	if (_gpio_expander_enabled) {
		if (initGPIOExpander(bidirectionals["GPIOExpander"])) {
			QObject::connect(_gpio_expander, &GPIOExpander::requestPublishMessage, this, &SAProcess::publishMessage, Qt::QueuedConnection);
		}
		else {
			_logger->error("Failed to init GPIO Expander");
		}
	}
	
	return true;
}

bool SAProcess::initGPIOExpander(const YAML::Node &gpio_expander) {
	_gpio_expander = new GPIOExpander(
		gpio_expander["Device"].as<string>(),
		(unsigned char) gpio_expander["Address"].as<unsigned int>(),
		_storage_sensor_dir,
		_storage_filesize_limit,
		gpio_expander["Frequency"].as<double>(),
		this
	);
	
	// Inizializzo il dispositivo
	if (!_gpio_expander->initDevice()) {
		_logger->error("GPIOExpander init failed");
		_gpio_expander = nullptr;
		return false;
	}
	
	// Imposto la configurazione dei pin
	for (unsigned int i = 0; i < gpio_expander["Pins"].size(); i++) {
		const YAML::Node &current_gpio = gpio_expander["Pins"][i];
		
		unsigned int number;
		QString name;
		GPIOPinMode mode;
		bool value;
		double pwm;
		string filename;
		
		number = current_gpio["Number"].as<unsigned int>();
		name = QString::fromStdString(current_gpio["Name"].as<string>());
		mode = (GPIOPinMode) current_gpio["Mode"].as<int>();
		value = (current_gpio["Value"].as<int>() != 0);
		pwm = current_gpio["PWM"].as<double>() / 100.0;
		filename = current_gpio["Filename"].as<string>();
		
		// Imposto la modalità
		if (!_gpio_expander->setPinMode(number, mode)) {
			_logger->error("Failed to set pin mode");
			continue;
		}
		
		// Imposto il nome
		if (!_gpio_expander->setPinName(number, name)) {
			_logger->error("Failed to set pin name");
			continue;
		}
		
		// Imposto il nome del file per la registrazione
		if (!_gpio_expander->setFileName(number, filename)) {
			_logger->error("Failed to set pin filename");
			continue;
		}
		
		// Imposto il valore a seconda del pin
		switch (mode) {
			case GPIOPinMode::Disabled:
				break;
			case GPIOPinMode::Input:
				break;
			case GPIOPinMode::Output:
				if (!_gpio_expander->setPinValue(number, value)) {
					_logger->error("Failed to set pin value");
					continue;
				}
				break;
			case GPIOPinMode::PWM:
				if (!_gpio_expander->setPinPWM(number, pwm)) {
					_logger->error("Failed to set pin pwm");
					continue;
				}
				break;
			case GPIOPinMode::OpenDrain:
				if (!_gpio_expander->setPinValue(number, value)) {
					_logger->error("Failed to set pin value");
					continue;
				}
				break;
			default:
				_logger->error("Pin mode not supported");
				continue;
		}
	}
	
	return true;
}
