//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		06/08/2017
//	Descrizione:		Stato Idle: il processo è inattivo
//****************************************************************************************************//

// Header del processo
#include <Process/sap.h>

using namespace std;
using namespace drex::processes;

// Inizializzazione dello stato
bool SAProcess::stateIdleInit() {
	_logger->info("IDLE: Init");
	
	return true;
}

// Pulizia dello stato
bool SAProcess::stateIdleCleanup() {
	_logger->info("IDLE: Cleanup");
	
	return true;
}
