//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		26/04/2017
//	Descrizione:		Stato Run: il processo acquisisce i sensori e pubblica i risultati, elabora
//						inoltre segnali aggiuntivi e effettua l'antirimbalzo
//****************************************************************************************************//

// Header del processo
#include <Process/sap.h>

using namespace std;
using namespace drex::processes;
using namespace drex::sensor;

// Inizializzazione dello stato
bool SAProcess::stateRunInit() {
	_logger->info("RUN: Init");
	
	// Avvio la lettura dei sensori e pubblicazione dei risultati
	if (_imu_enabled) {
		if (!_imu->startReading()){
			_imu->stopReading();
		}
	}
	if (_barometer_enabled) {
		if (!_barometer->startReading()) {
			_barometer->stopReading();
		}
	}
	if (_magnetometer_enabled) {
		if (!_magnetometer->startReading()) {
			_magnetometer->stopReading();
		}
	}
	if (_vacuometer_enabled) {
		if (!_vacuometer->startReading()) {
			_vacuometer->stopReading();
		}
	}
	if (_thermometer_enabled) {
		if (!_thermometer->startReading()) {
			_thermometer->stopReading();
		}
	}
	if (_gpio_expander_enabled) {
		if (!_gpio_expander->startReading()) {
			_gpio_expander->stopReading();
		}
	}
//	if (_microswitch_enabled) {
//		if (!_microswitch->startReading()) {
//			_microswitch->stopReading();
//		}
//	}
	
	return true;
}

// Pulizia prima del passaggio di stato
bool SAProcess::stateRunCleanup() {
	_logger->info("RUN: Cleanup");
	
	// Fermo tutti i sensori
	if (_imu_enabled) {
		_imu->stopReading();
	}
	if (_barometer_enabled) {
		_barometer->stopReading();
	}
	if (_magnetometer_enabled) {
		_magnetometer->stopReading();
	}
	if (_vacuometer_enabled) {
		_vacuometer->stopReading();
	}
	if (_thermometer_enabled) {
		_thermometer->stopReading();
	}
	if (_gpio_expander_enabled) {
		_gpio_expander->stopReading();
	}
//	if (_microswitch_enabled) {
//		_microswitch->stopReading();
//	}
	
	return true;
}
