//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		25/04/2017
//	Descrizione:		Funzione di notifica di QObject
//****************************************************************************************************//

// Header del processo
#include <Process/sap.h>

using namespace drex::processes;

// Notifica evento
bool SAProcess::notify(QObject *obj, QEvent *event) {
	try {
		return super::notify(obj, event);
	}
	catch (std::exception &ex) {
		_logger->error(ex.what());
		return false;
	}
}
