//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		25/04/2017
//	Descrizione:		Costruttore
//****************************************************************************************************//

// Header del processo
#include <Process/sap.h>

using namespace drex::processes;
using namespace std;

// Costruttore
SAProcess::SAProcess(int &argc, char **argv) : super(argc, argv) {
	// Inizializzo il logger su console
	_console = spdlog::get("console");
	if (!_console) {
		_console = spdlog::stdout_color_mt("console");
	}
	
	// Messaggio informativo
	_console->info("Starting (" + SHORT_NAME + ") " + FULL_NAME);
	
	// Avvio l'applicazione
	QTimer::singleShot(0, this, &SAProcess::init);
}
