//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		03/05/2017
//	Descrizione:		Pubblicazione della misura
//****************************************************************************************************//

// Header del processo
#include <Process/sap.h>

using namespace std;
using namespace nzmqt;
using namespace drex::processes;

void SAProcess::publishMessage(const Message &msg) {
	_data_socket.socket->sendMessage(msg);
}
