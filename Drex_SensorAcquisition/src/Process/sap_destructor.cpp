//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		25/04/2017
//	Descrizione:		Distruttore
//****************************************************************************************************//

// Header del processo
#include <Process/sap.h>

using namespace drex::processes;

// Distruttore
SAProcess::~SAProcess() {
	// Messaggio informativo
	if (_logger != nullptr) _logger->info("Stopping (" + SHORT_NAME + ") " + FULL_NAME);
	else _console->info("Stopping (" + SHORT_NAME + ") " + FULL_NAME);
}
