//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		25/04/2017
//	Descrizione:		Gestione dei segnali di sistema
//****************************************************************************************************//

// Header del processo
#include <Process/sap.h>

// Header per i segnali di sistema
#include <sys/socket.h>

using namespace drex::processes;

// Installazione dei signal handlers
void SAProcess::setupSignalHandlers(int *p_hup, int *p_int, int *p_term) {
	_sig_hup_fd = p_hup;
	_sig_int_fd = p_int;
	_sig_term_fd = p_term;
	
	// // Gestione dei segnali di sistema
	
	// HUP
	if (::socketpair(AF_UNIX, SOCK_STREAM, 0, _sig_hup_fd)) {
		_console->error("Couldn't create HUP socketpair");
	}
	_sn_hup = new QSocketNotifier(_sig_hup_fd[1], QSocketNotifier::Read, this);
	connect(_sn_hup, &QSocketNotifier::activated, this, &SAProcess::handleSigHup);
	
	// INT
	if (::socketpair(AF_UNIX, SOCK_STREAM, 0, _sig_int_fd)) {
		_logger->error("Couldn't create INT socketpair");
	}
	_sn_int = new QSocketNotifier(_sig_int_fd[1], QSocketNotifier::Read, this);
	connect(_sn_int, &QSocketNotifier::activated, this, &SAProcess::handleSigInt);
	
	// TERM
	if (::socketpair(AF_UNIX, SOCK_STREAM, 0, _sig_term_fd)) {
		_logger->error("Couldn't create TERM socketpair");
	}
	_sn_term = new QSocketNotifier(_sig_term_fd[1], QSocketNotifier::Read, this);
	connect(_sn_term, &QSocketNotifier::activated, this, &SAProcess::handleSigTerm);
}

// Slot per HUP
void SAProcess::handleSigHup() {
	_sn_hup->setEnabled(false);
	char tmp;
	::read(_sig_hup_fd[1], &tmp, sizeof(tmp));
	
	// Messaggio informativo
	_logger->info("System signal HUP received, shutting down the process..");
	
	// Flusho il logger
	_logger->flush();
	
	// Chiudo l'applicazione
	quit();
	
	_sn_hup->setEnabled(true);
}

// Slot per INT
void SAProcess::handleSigInt() {
	_sn_int->setEnabled(false);
	char tmp;
	::read(_sig_int_fd[1], &tmp, sizeof(tmp));
	
	// Messaggio informativo
	_logger->info("System signal INT received, shutting down the process..");
	
	// Flusho il logger
	_logger->flush();
	
	// Chiudo l'applicazione
	quit();
	
	_sn_int->setEnabled(true);
}

// Slot per TERM
void SAProcess::handleSigTerm() {
	_sn_term->setEnabled(false);
	char tmp;
	::read(_sig_term_fd[1], &tmp, sizeof(tmp));
	
	// Messaggio informativo
	_logger->info("System signal TERM received, shutting down the process..");
	
	// Flusho il logger
	_logger->flush();
	
	// Chiudo l'applicazione
	quit();
	
	_sn_term->setEnabled(true);
}
