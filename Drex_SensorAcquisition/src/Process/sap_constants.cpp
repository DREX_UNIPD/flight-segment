//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		25/04/2017
//	Descrizione:		Costanti
//****************************************************************************************************//

// Header del processo
#include <Process/sap.h>

using namespace std;
using namespace drex::processes;

// // Costanti

// Nome del processo
const string SAProcess::SHORT_NAME = "SAP";
const string SAProcess::FULL_NAME = "Sensor acquisition process";

// Comandi
const string SAProcess::CMD_HELP = "HELP";
const string SAProcess::CMD_KILL = "KILL";
const string SAProcess::CMD_GET_STATE = "GET STATE";
const string SAProcess::CMD_SET_STATE = "SET STATE";

const string SAProcess::CMD_SET_GPIO_VALUE = "SET GPIO VALUE";
const string SAProcess::CMD_SET_GPIO_PWM = "SET GPIO PWM";
const string SAProcess::CMD_SET_GPIO_MODE = "SET GPIO MODE";
const string SAProcess::CMD_SET_DAC = "SET DAC";
const string SAProcess::CMD_SET_PAM = "SET PAM";

// Stati
const string SAProcess::ST_DEFAULT = "DEFAULT";
const string SAProcess::ST_IDLE = "IDLE";
const string SAProcess::ST_RUN = "RUN";
