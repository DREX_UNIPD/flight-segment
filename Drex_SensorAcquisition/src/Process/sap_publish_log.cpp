//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		14/09/2017
//	Descrizione:		Pubblicazione dei messaggi di log
//****************************************************************************************************//

// Header del processo
#include <Process/sap.h>

using namespace drex::processes;

void SAProcess::publishLog(const Message &msg) {
	_logger_socket.socket->sendMessage(msg);
}

