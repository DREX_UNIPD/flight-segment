//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		29/04/17
//	Descrizione:		Classe che rappresenta un generico dispositivo I2C
//****************************************************************************************************//

// Header della classe
#include <Device/sap_i2c_device.h>

// Header STL
#include <iomanip>

// Header Linux
#include <sys/ioctl.h>

// Header DREX
#include <Utilities/int_to_hex.h>

using namespace std;
using namespace drex::device;

// Metodi

// Apertura del bus I2C
bool I2CDevice::i2cOpen() {
	// Apro il file
	_logger->info("Device opening: " + _device);
	
	// Verifico se il bus è aperto
	if (_file >= 0) {
		_logger->warn("Device already opened");
		return true;
	}
	
	if ((_file = open(_device.c_str(), O_RDWR)) < 0) {
		// Errore
		string err = strerror(errno);
		_logger->error("Failed to open the I2C bus: " + err);
		return false;
	}
	
	// Imposto l'indirizzo del dispositivo
	if (ioctl(_file, I2C_SLAVE, _address) < 0) {
		// Errore
		string err = strerror(errno);
		_logger->error("Failed to acquire bus access and/or talk to slave: " + err);
		return false;
	}
	
	return true;
}

// Chiusura del bus I2C
bool I2CDevice::i2cClose() {
	_logger->info("Device closing: " + _device);
	
	// Verifico se il bus è aperto
	if (_file < 0) {
		_logger->warn("Device already closed");
		return true;
	}
	
	// Chiudo il file aperto
	if (close(_file) < 0) {
		string err = strerror(errno);
		_logger->error("Failed to close the I2C bus: " + err);
		return false;
	}
	
	_file = -1;
	
	return true;
}

// Leggo un byte
bool I2CDevice::i2cGetRegister(__u8 address, __u8 &value) {
	__s32 result;
	
	// Verifico se il bus è aperto
	if (_file < 0) {
		_logger->error("Device closed, register reading not possible");
		return false;
	}
	
	// Leggo il registro
	if ((result = i2c_smbus_read_byte_data(_file, address)) < 0) {
		// Errore
		string err = strerror(errno);
		_logger->error("Failed to read byte " + int_to_hex<__u8>(address) + ": " + err);
		return false;
	}
	
	// Restituisco il valore
	value = (__u8) result;
	_logger->info("\033[36m{" + int_to_hex<unsigned char>(_address) + "}\033[37m[" + int_to_hex<__u8>(address) + "] \033[32m=>\033[37m " + int_to_hex<__u8>(value));
	return true;
}

// Scrivo un byte
bool I2CDevice::i2cSetRegister(__u8 address, __u8 value) {
	// Verifico se il bus è aperto
	if (_file < 0) {
		_logger->error("Device closed, register writing not possible");
		return false;
	}
	
	if (i2c_smbus_write_byte_data(_file, address, value) < 0) {
		// Errore
		string err = strerror(errno);
		_logger->error("Failed to write byte " + int_to_hex<__u8>(address) + ": " + err);
		return false;
	}
	
	_logger->info("\033[36m{" + int_to_hex<unsigned char>(_address) + "}\033[37m[" + int_to_hex<__u8>(address) + "] \033[31m<=\033[37m " + int_to_hex<__u8>(value));
	return true;
}

bool I2CDevice::i2cGetWord(__u8 address, int16_t &value) {
	__s32 result;
	
	// Verifico se il bus è aperto
	if (_file < 0) {
		_logger->error("Device closed, word register writing not possible");
		return false;
	}
	
	if ((result = i2c_smbus_read_word_data(_file, address)) < 0) {
		// Errore
		string err = strerror(errno);
		_logger->error("Failed to read word " + int_to_hex<__u8>(address) + ": " + err);
		return false;
	}
	
	value = (int16_t) result;
	_logger->info("\033[36m{" + int_to_hex<unsigned char>(_address) + "}\033[37m[" + int_to_hex<__u8>(address) + ", " + int_to_hex<__u8>(address + 1) + "] \033[32m=>\033[37m " + int_to_hex<int16_t>(value, 4));
	return true;
}

bool I2CDevice::i2cGetRaw(__u8 *value, size_t size) {
	// Verifico se il bus è aperto
	if (_file < 0) {
		_logger->error("Device closed, byte reading not possible");
		return false;
	}
	
	if (read(_file, value, size) <= 0) {
		_logger->error("Reading error");
		return false;
	}
	
	return true;
}

bool I2CDevice::i2cSetRaw(__u8 *value, size_t size) {
	// Verifico se il bus è aperto
	if (_file < 0) {
		_logger->error("Device closed, byte reading not possible");
		return false;
	}
	
	if (write(_file, value, size) <= 0) {
		_logger->error("Writing error");
		return false;
	}
	
	return true;
}

// Costruttore
I2CDevice::I2CDevice(const string device, const unsigned char address) :
	_device(device),
	_address(address) {
	_logger = spdlog::get("logger");
	_file = -1;
}
