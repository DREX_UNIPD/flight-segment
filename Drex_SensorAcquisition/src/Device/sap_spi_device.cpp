//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		16/07/17
//	Descrizione:		Classe che rappresenta un generico dispositivo SPI
//****************************************************************************************************//

// Header della classe
#include <Device/sap_spi_device.h>

// Header Linux
#include <sys/ioctl.h>

using namespace std;
using namespace drex::device;

// Metodi

// Apertura del bus SPI
bool SPIDevice::spiOpen() {
	// Apro il file
	_logger->info("Device opening: " + _device);
	
	// Verifico se il bus è aperto
	if (_file >= 0) {
		_logger->warn("Device already opened");
		return true;
	}
	
	if ((_file = open(_device.c_str(), O_RDWR)) < 0) {
		// Errore
		string err = strerror(errno);
		_logger->error("Failed to open the SPI bus: " + err);
		return false;
	}
	
	return true;
}

bool SPIDevice::spiInit() {
	_logger->info("[Thermometer] SPI Init");
	
	int ret;
	__u8 dummy8;
	__u32 dummy32;
	
	// Verifico che il file sia aperto
	if (_file < 0) {
		_logger->error("SPI device closed, failed to perform init");
		return false;
	}
	
	// SPI mode
	ret = ioctl(_file, SPI_IOC_WR_MODE, &_mode);
	if (ret == -1) {
		_logger->error("SPI failed to set mode");
		return false;
	}
	
//	_logger->info("SPI mode setted");
	
	// SPI bits per word
	ret = ioctl(_file, SPI_IOC_WR_BITS_PER_WORD, &_bits_per_word);
	if (ret == -1) {
		_logger->error("SPI failed to set bits per word");
		return false;
	}
	
//	_logger->info("SPI bits per word setted");
	
	ret = ioctl(_file, SPI_IOC_RD_BITS_PER_WORD, &dummy8);
	if (ret == -1) {
		_logger->error("SPI failed to get bits per word");
		return false;
	}
	
	if (dummy8 != _bits_per_word) {
		_logger->error("SPI failed to compare bits per word");
		return false;
	}
	
	// SPI SCK speed
	ret = ioctl(_file, SPI_IOC_WR_MAX_SPEED_HZ, &_speed_hz);
	if (ret == -1) {
		_logger->error("SPI failed to set speed");
		return false;
	}
	
	ret = ioctl(_file, SPI_IOC_RD_MAX_SPEED_HZ, &dummy32);
	if (ret == -1) {
		_logger->error("SPI failed to get speed");
		return false;
	}
	
	if (dummy32 != _speed_hz) {
		_logger->error("SPI failed to compare speed");
		return false;
	}
	
	return true;
}

// Chiusura del bus SPI
bool SPIDevice::spiClose() {
	// Chiudo il file
	_logger->info("Device closing: " + _device);
	
	// Verifico se il bus è aperto
	if (_file < 0) {
		_logger->warn("Device already closed");
		return true;
	}
	
	// Chiudo il file aperto
	if (close(_file) < 0) {
		string err = strerror(errno);
		_logger->error("Failed to close the SPI bus: " + err);
		return false;
	}
	
	_file = -1;
	
	return true;
}

// Trasferimento bytes
bool SPIDevice::spiTransfer(__u32 len, __u8 *tx_buf, __u8 *rx_buf) {
	// Verifico che il file sia aperto
	if (_file < 0) {
		_logger->error("SPI device closed, failed to perform transfer");
		return false;
	}
	
	// Scrittura e lettura contemporanea di una sequenza di byte
	spi_ioc_transfer tr;
	tr.bits_per_word = _bits_per_word;
	tr.cs_change = _cs_change;
	tr.delay_usecs = _delay_usec;
	tr.len = len;
	tr.pad = _pad;
	tr.rx_buf = (__u64) rx_buf;
//	tr.rx_nbits = _rx_nbits;
	tr.speed_hz = _speed_hz;
	tr.tx_buf = (__u64) tx_buf;
//	tr.tx_nbits = _tx_nbits;
	
	int ret;
	ret = ioctl(_file, SPI_IOC_MESSAGE(1), &tr);
	
	if (ret <= 0) {
		return false;
	}
	
	return true;
}

// Costruttore
SPIDevice::SPIDevice(
		const string device,
		const __u32 mode,
		const __u32 speed_hz,
		const __u8 bits_per_word,
		const __u8 cs_change,
		const __u16 delay_usec,
		const __u16 pad
	) :
	_device(device),
	_mode(mode),
	_bits_per_word(bits_per_word),
	_cs_change(cs_change),
	_delay_usec(delay_usec),
	_pad(pad),
	_speed_hz(speed_hz)
{
	_logger = spdlog::get("logger");
	_file = -1;
}
