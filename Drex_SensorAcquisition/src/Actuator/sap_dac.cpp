//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		07/08/17
//	Descrizione:		Classe che rappresenta il DAC MCP4725
//****************************************************************************************************//

// Header della classe
#include <Actuator/sap_dac.h>

using namespace std;
using namespace drex::actuator;

// Costruttore
DAC::DAC(const string device, const unsigned char address, QObject *parent) :
	QObject(parent),
	I2CDevice(device, address) {
	_logger = spdlog::get("logger");
}

// Distruttore
DAC::~DAC() {
}

// Settaggio dell'output
bool DAC::setOutput(double voltage, bool persistent) {
	_logger->info(string("[DAC] set output(voltage = ") + to_string(voltage) + string(", persistent = ") + string(persistent ? "true)" : "false)"));
	
	// Apro il dispositivo
	if (!i2cOpen()) {
		return false;
	}
	
	// Calcolo il numero da scrivere
	__u16 value;
	value = (__u16) ((MAX_VALUE - MIN_VALUE) * (voltage - MIN_VOLTAGE) / (MAX_VOLTAGE - MIN_VOLTAGE));
	
	// Correggo eventuali valori fuori scala
	if (value > MAX_VALUE) value = MAX_VALUE;
	//if (value < MIN_VALUE) value = MIN_VALUE;
	
	// Imposto l'uscita
	if (persistent) {
		// Persistent mode
		__u8 buffer[3];
		buffer[0] = 0x60; // C2 = 0, C1 = 1, C0 = 1
		buffer[1] = (value & 0x0ff0) >> 4;
		buffer[2] = (value & 0x000f) << 4;
		if (!i2cSetRaw(buffer, 3)) {
			return false;
		}
	}
	else {
		// Fast mode
		__u8 buffer[2];
		buffer[0] = (value & 0x0f00) >> 8;
		buffer[1] = value & 0xff;
		if (!i2cSetRaw(buffer, 2)) {
			return false;
		}
	}
	
	// Chiudo il dispositivo
	if (!i2cClose()) {
		return false;
	}
	
	return true;
}
