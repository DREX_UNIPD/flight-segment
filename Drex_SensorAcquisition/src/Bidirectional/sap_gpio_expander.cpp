//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		12/09/17
//	Descrizione:		Classe che rappresenta il barometro LPS25H
//****************************************************************************************************//

// Header della classe
#include <Bidirectional/sap_gpio_expander.h>

using namespace std;
using namespace drex::bidirectional;

// Costruttore
GPIOExpander::GPIOExpander(const string device, const unsigned char address, QDir storage_dir, qint64 filesize_limit, const double frequency, QObject *parent) :
	QObject(parent),
	I2CDevice(device, address),
	_storage_dir(storage_dir),
	_filesize_limit(filesize_limit),
	_frequency(frequency),
	_publishing(false),
	_out_data(0xffff) {
	_logger = spdlog::get("logger");
	
	// Inizializzo le strutture della configurazione dei pin
	for (unsigned int i = 0; i < GPIO_NUMBER; i++) {
		GPIOPinConfiguration current;
		current.name = "";
		current.mode = GPIOPinMode::Disabled;
		current.value = false;
		current.pwm_value = 0.0;
		_pin_configuration.push_back(current);
	}
	
	QObject::connect(&_timer, &QTimer::timeout, this, &GPIOExpander::publishGPIO, Qt::QueuedConnection);
}

// Distruttore
GPIOExpander::~GPIOExpander() {
}

// Inizializzazione
bool GPIOExpander::initDevice() {
	_logger->info("[GPIOExpander] init device");
	
	auto init_gpio = [this] () {
		// Imposto a livello logico basso le uscite
		if (!_setData(0x0000)) return false;
		
		// Imposto il clock interno
		if (!_setOscillatorSource(CLOCK_INTERNAL)) return false;
		
		// Imposto il divisore del clock a 1 (0 = off, n=1..7 divisore 2^n)
		if (!_setLEDDriverDivider(1)) return false;
		
		return true;
	};
	
	// Apro il dispositivo
	if (!i2cOpen()) {
		return false;
	}
	
	// Resetto il dispositivo
	if (!_softwareReset()) {
		_logger->error("Failed to reset device");
		i2cClose();
		return false;
	}
	
	// Imposto tutti i pin disabilitati
	if (!init_gpio()) {
		_logger->error("Failed to init device");
		i2cClose();
		return false;
	}
	
	// Chiudo il dispositivo
	if (!i2cClose()) {
		return false;
	}
	
	return true;
}

// Inizio lettura
bool GPIOExpander::startReading() {
	_logger->info("[GPIOExpander] start reading");
	
	// Verifico se l'acquisizione e' gia' in corso
	if (_publishing) {
		_logger->warn("[GPIOExpander] already publishing");
		return true;
	}
	_publishing = true;
	
	// Avvio i timer se la frequenza specificata è maggiore di zero
	auto start_timer = [this] (const char *name, double f, QTimer &t) {
		if (f > 0.0) {
			int ms;
			try {
				ms = (int) (1000 / f);
				if (ms <= 0) ms = 1;
				t.start(ms);
				return true;
			}
			catch (exception e) {
				string err;
				err += "[GPIOExpander] cannot start ";
				err += name;
				err += " timer: ";
				err += e.what();
				_logger->warn(err);
				return false;
			}
		}
		return true;
	};
	
	if (!start_timer("gpio", _frequency, _timer)) {
		return false;
	}
	
	return true;
}

// Fine lettura
bool GPIOExpander::stopReading() {
	_logger->info("[GPIOExpander] stop reading");
	
	// Verifico se l'acquisizione e' in corso
	if (!_publishing) {
		_logger->warn("[GPIOExpander] not publishing yet");
		return true;
	}
	_publishing = false;
	
	// Fermo i timer attivi
	if (_timer.isActive()) {
		_timer.stop();
	}
	
	// Chiudo tutti i file
	for (int key : _gpio_files.keys()) {
		QFile *file = _gpio_files[key];
		file->close();
		delete file;
	}
	
	_gpio_files.clear();
	
	return true;
}

// Metodi ausiliari
bool GPIOExpander::_get(uint8_t address_lower, uint8_t address_higher, uint16_t &data) {
	uint8_t lower, higher;
	
	// Ritrovo il valore del registro basso
	if (!i2cGetRegister(address_lower, lower)) {
		return false;
	}
	
	// Ritrovo il valore del registro basso
	if (!i2cGetRegister(address_higher, higher)) {
		return false;
	}
	
	// Costruisco il registro risultato
	data = (higher << 8) | lower;
	
	return true;
}
bool GPIOExpander::_set(uint8_t address_lower, uint8_t address_higher, uint16_t data) {
	uint8_t lower, higher;
	
	// Estraggo i due byte
	lower = (uint8_t) data & 0xff;
	higher = (uint8_t) (data >> 8) & 0xff;
	
	// Imposto il valore del registro basso
	if (!i2cSetRegister(address_lower, lower)) {
		return false;
	}
	
	// Imposto il valore del registro alto
	if (!i2cSetRegister(address_higher, higher)) {
		return false;
	}
	
	return true;
}
bool GPIOExpander::_update(uint8_t address_lower, uint8_t address_higher, uint16_t data, uint16_t mask) {
	uint16_t old_data;
	
	// Ritrovo il valore
	if (!_get(address_lower, address_higher, old_data)) {
		return false;
	}
	
	// Aggiorno il valore attuale con il nuovo usando la maschera
	if (!_set(address_lower, address_higher, (old_data & ~mask) | (data & mask))) {
		return false;
	}
	
	return true;
}

// Input Buffer
bool GPIOExpander::_getInputBufferDisable(uint16_t &data) {
	_logger->info("[GPIOExpander] get input buffer disable");
	
	if (!_get(RegInputDisableA, RegInputDisableB, data)) return false;
	return true;
}
bool GPIOExpander::_setInputBufferDisable(uint16_t data) {
	_logger->info("[GPIOExpander] set input buffer disable");
	
	if (!_set(RegInputDisableA, RegInputDisableB, data)) return false;
	return true;
}
bool GPIOExpander::_updateInputBufferDisable(uint16_t data, uint16_t mask) {
	_logger->info("[GPIOExpander] update input buffer disable");
	
	if (!_update(RegInputDisableA, RegInputDisableB, data, mask)) return false;
	return true;
}

// Long Slew
bool GPIOExpander::_getLongSlew(uint16_t &data) {
	_logger->info("[GPIOExpander] get long slew");
	
	if (!_get(RegLongSlewA, RegLongSlewB, data)) return false;
	return true;
}
bool GPIOExpander::_setLongSlew(uint16_t data) {
	_logger->info("[GPIOExpander] set long slew");
	
	if (!_set(RegLongSlewA, RegLongSlewB, data)) return false;
	return true;
}
bool GPIOExpander::_updateLongSlew(uint16_t data, uint16_t mask) {
	_logger->info("[GPIOExpander] update long slew");
	
	if (!_update(RegLongSlewA, RegLongSlewB, data, mask)) return false;
	return true;
}

// Low Drive
bool GPIOExpander::_getLowDrive(uint16_t &data) {
	_logger->info("[GPIOExpander] get low drive");
	
	if (!_get(RegLowDriveA, RegLowDriveB, data)) return false;
	return true;
}
bool GPIOExpander::_setLowDrive(uint16_t data) {
	_logger->info("[GPIOExpander] set low drive");
	
	if (!_set(RegLowDriveA, RegLowDriveB, data)) return false;
	return true;
}
bool GPIOExpander::_updateLowDrive(uint16_t data, uint16_t mask) {
	_logger->info("[GPIOExpander] update low drive");
	
	if (!_update(RegLowDriveA, RegLowDriveB, data, mask)) return false;
	return true;
}

bool GPIOExpander::_getPullUp(uint16_t &data) {
	_logger->info("[GPIOExpander] get pullup");
	
	if (!_get(RegPullUpA, RegPullUpB, data)) return false;
	return true;
}
bool GPIOExpander::_setPullUp(uint16_t data) {
	_logger->info("[GPIOExpander] set pullup");
	
	if (!_set(RegPullUpA, RegPullUpB, data)) return false;
	return true;
}
bool GPIOExpander::_updatePullUp(uint16_t data, uint16_t mask) {
	_logger->info("[GPIOExpander] update pullup");
	
	if (!_update(RegPullUpA, RegPullUpB, data, mask)) return false;
	return true;
}

bool GPIOExpander::_getPullDown(uint16_t &data) {
	_logger->info("[GPIOExpander] get pulldown");
	
	if (!_get(RegPullDownA, RegPullDownB, data)) return false;
	return true;
}
bool GPIOExpander::_setPullDown(uint16_t data) {
	_logger->info("[GPIOExpander] set pulldown");
	
	if (!_set(RegPullDownA, RegPullDownB, data)) return false;
	return true;
}
bool GPIOExpander::_updatePullDown(uint16_t data, uint16_t mask) {
	_logger->info("[GPIOExpander] update pulldown");
	
	if (!_update(RegPullDownA, RegPullDownB, data, mask)) return false;
	return true;
}

bool GPIOExpander::_getOpenDrain(uint16_t &data) {
	_logger->info("[GPIOExpander] get open drain");
	
	if (!_get(RegOpenDrainA, RegOpenDrainB, data)) return false;
	return true;
}
bool GPIOExpander::_setOpenDrain(uint16_t data) {
	_logger->info("[GPIOExpander] set open drain");
	
	if (!_set(RegOpenDrainA, RegOpenDrainB, data)) return false;
	return true;
}
bool GPIOExpander::_updateOpenDrain(uint16_t data, uint16_t mask) {
	_logger->info("[GPIOExpander] update open drain");
	
	if (!_update(RegOpenDrainA, RegOpenDrainB, data, mask)) return false;
	return true;
}

bool GPIOExpander::_getPolarityInversion(uint16_t &data) {
	_logger->info("[GPIOExpander] get polarity inversion");
	
	if (!_get(RegPolarityA, RegPolarityB, data)) return false;
	return true;
}
bool GPIOExpander::_setPolarityInversion(uint16_t data) {
	_logger->info("[GPIOExpander] set polarity inversion");
	
	if (!_set(RegPolarityA, RegPolarityB, data)) return false;
	return true;
}
bool GPIOExpander::_updatePolarityInversion(uint16_t data, uint16_t mask) {
	_logger->info("[GPIOExpander] update polarity inversion");
	
	if (!_update(RegPolarityA, RegPolarityB, data, mask)) return false;
	return true;
}

bool GPIOExpander::_getDirection(uint16_t &data) {
	_logger->info("[GPIOExpander] get direction");
	
	if (!_get(RegDirA, RegDirB, data)) return false;
	return true;
}
bool GPIOExpander::_setDirection(uint16_t data) {
	_logger->info("[GPIOExpander] set direction");
	
	if (!_set(RegDirA, RegDirB, data)) return false;
	return true;
}
bool GPIOExpander::_updateDirection(uint16_t data, uint16_t mask) {
	_logger->info("[GPIOExpander] update direction");
	
	if (!_update(RegDirA, RegDirB, data, mask)) return false;
	return true;
}

bool GPIOExpander::_getOutData(uint16_t &data, uint16_t &mask) {
	_logger->info("[GPIOExpander] get out data");
	
	if (!_getDirection(mask)) return false;
	mask = ~mask;
	data = _out_data;
	
	return true;
}
bool GPIOExpander::_getInData(uint16_t &data, uint16_t &mask) {
	_logger->info("[GPIOExpander] get in data");
	
	if (!_getDirection(mask)) return false;
	if (!_get(RegDataA, RegDataB, data)) return false;
	
	return true;
}

bool GPIOExpander::_setData(uint16_t data) {
	_logger->info("[GPIOExpander] set data");
	
	if (!_set(RegDataA, RegDataB, data)) return false;
	_out_data = data;
	
	return true;
}
bool GPIOExpander::_updateData(uint16_t data, uint16_t mask) {
	_logger->info("[GPIOExpander] update data");
	
	uint16_t old_data, dir_mask;
	if (!_getOutData(old_data, dir_mask)) return false;
	data &= mask;
	data |= old_data & ~mask;
	if (!_setData(data)) return false;
	
	return true;
}

bool GPIOExpander::_getInterruptMask(uint16_t &data) {
	_logger->info("[GPIOExpander] get interrupt mask");
	
	if (!_get(RegInterruptMaskA, RegInterruptMaskB, data)) return false;
	return true;
}
bool GPIOExpander::_setInterruptMask(uint16_t data) {
	_logger->info("[GPIOExpander] set interrupt mask");
	
	if (!_set(RegInterruptMaskA, RegInterruptMaskB, data)) return false;
	return true;
}
bool GPIOExpander::_updateInterruptMask(uint16_t data, uint16_t mask) {
	_logger->info("[GPIOExpander] update interrupt mask");
	
	if (!_update(RegInterruptMaskA, RegInterruptMaskB, data, mask)) return false;
	return true;
}

bool GPIOExpander::_getEdgeSensitivity(uint16_t &rising, uint16_t &falling) {
	_logger->info("[GPIOExpander] get edge sensitivity");
	
	uint16_t a, b;
	uint32_t c;
	
	// Ritrovo il valore
	if (!_get(RegSenseLowB, RegSenseHighB, b)) {
		return false;
	}
	if (!_get(RegSenseLowA, RegSenseHighA, a)) {
		return false;
	}
	
	// Separo rising e falling
	c = a | (b << 16);
	
	rising = 0;
	falling = 0;
	for (int i = 0; i < 16; i++) {
		// Pari, rising bit
		rising |= (c & (1 << 2 * i)) >> i;
		// Dispari, falling bit
		falling |= (falling & (1 << (2 * i + 1))) >> (i + 1);
	}
	
	return true;
}
bool GPIOExpander::_setEdgeSensitivity(uint16_t rising, uint16_t falling) {
	_logger->info("[GPIOExpander] set edge sensitivity");
	
	uint16_t a, b;
	uint32_t c;
	
	// Unisco rising e falling
	c = 0;
	for (int i = 0; i < 16; i++) {
		// Pari, rising bit
		c |= (rising & (1 << i)) << i;
		// Dispari, falling bit
		c |= (falling & (1 << i)) << (i + 1);
	}
	
	a = (uint16_t) c;
	b = (uint16_t) (c >> 16);
	
	// Ritrovo il valore
	if (!_set(RegSenseLowB, RegSenseHighB, b)) {
		return false;
	}
	if (!_set(RegSenseLowA, RegSenseHighA, a)) {
		return false;
	}
	
	return true;
}
bool GPIOExpander::_updateEdgeSensitivity(uint16_t rising, uint16_t falling, uint16_t mask) {
	_logger->info("[GPIOExpander] update edge sensitivity");
	
	uint16_t old_rising, old_falling;
	
	if (!_getEdgeSensitivity(old_rising, old_falling)) return false;
	if (!_setEdgeSensitivity(
		(rising & mask) | (old_rising & ~mask),
		(falling & mask) | (old_falling & ~mask))
	) return false;
	
	return true;
}

bool GPIOExpander::_getInterruptSource(uint16_t &data) {
	_logger->info("[GPIOExpander] get interrupt source");
	
	if (!_get(RegInterruptSourceA, RegInterruptSourceB, data)) return false;
	return true;
}
bool GPIOExpander::_clearInterruptSource(uint16_t mask) {
	_logger->info("[GPIOExpander] clear interrupt source");
	
	if (!_set(RegInterruptSourceA, RegInterruptSourceB, mask)) return false;
	return true;
}

bool GPIOExpander::_getEventStatus(uint16_t data) {
	_logger->info("[GPIOExpander] get event status");
	
	if (!_set(RegEventStatusA, RegEventStatusB, data)) return false;
	return true;
}
bool GPIOExpander::_clearEventStatus(uint16_t mask) {
	_logger->info("[GPIOExpander] clear event status");
	
	if (!_set(RegEventStatusA, RegEventStatusB, mask)) return false;
	return true;
}

bool GPIOExpander::_getLevelShifter(uint8_t &a_to_b, uint8_t &b_to_a) {
	_logger->info("[GPIOExpander] get level shifter");
	
	uint16_t c;
	
	// Ritrovo il valore
	if (!_get(RegLevelShifter2, RegLevelShifter1, c)) {
		return false;
	}
	
	// Separo a_to_b e b_to_a
	a_to_b = 0;
	b_to_a = 0;
	for (int i = 0; i < 8; i++) {
		// Pari, rising bit
		a_to_b |= (c & (1 << 2 * i)) >> i;
		// Dispari, falling bit
		b_to_a |= (c & (1 << (2 * i + 1))) >> (i + 1);
	}
	
	return true;
}
bool GPIOExpander::_setLevelShifter(uint8_t a_to_b, uint8_t b_to_a) {
	_logger->info("[GPIOExpander] set level shifter");
	
	uint16_t c;
	
	// Verifico che non ci sia conflitto nei bit
	if (a_to_b & b_to_a) {
		return false;
	}
	
	// Unisco a_to_b e b_to_a
	c = 0;
	for (int i = 0; i < 8; i++) {
		// Pari, a_to_b bit
		c |= (a_to_b & (1 << i)) << i;
		// Dispari, falling bit
		c |= (b_to_a & (1 << i)) << (i + 1);
	}
	
	// Ritrovo il valore
	if (!_set(RegLevelShifter2, RegLevelShifter1, c)) {
		return false;
	}
	
	return true;
}
bool GPIOExpander::_updateLevelShifter(uint8_t a_to_b, uint8_t b_to_a, uint8_t mask) {
	_logger->info("[GPIOExpander] update level shifter");
	
	uint8_t old_a_to_b, old_b_to_a;
	if (!_getLevelShifter(old_a_to_b, old_b_to_a)) return false;
	if (!_setLevelShifter(
		(a_to_b & mask) | (old_a_to_b & ~mask),
		(b_to_a & mask) | (old_b_to_a & ~mask))
	) return false;
	return true;
}
// TODO: implementare i metodi rimanenti
bool GPIOExpander::_getOscillatorSource(uint8_t &source) {
	_logger->info("[GPIOExpander] get oscillator source");
	_logger->warn("Method not implemented");
	return true;
}
bool GPIOExpander::_setOscillatorSource(uint8_t source) {
	_logger->info("[GPIOExpander] set oscillator source");
	
	// Leggo il valore del registro e aggiorno i bit relativi al clock
	uint8_t reg;
	if (!i2cGetRegister(RegClock, reg)) return false;
	reg &= ~MASK_CLOCK;
	reg |= source & MASK_CLOCK;
	if (!i2cSetRegister(RegClock, reg)) return false;
	
	return true;
}
bool GPIOExpander::_getOSCIODirection(bool output) {
	_logger->info("[GPIOExpander] get OSCIO direction");
	_logger->warn("Method not implemented");
	return true;
}
bool GPIOExpander::_setOSCIODirection(bool output) {
	_logger->info("[GPIOExpander] set OSCIO direction");
	_logger->warn("Method not implemented");
	return true;
}
bool GPIOExpander::_getOSCOUTFrequency(bool &always_off, bool &always_on, uint8_t &frequency) {
	_logger->info("[GPIOExpander] get OSCOUT frequency");
	_logger->warn("Method not implemented");
	return true;
}
bool GPIOExpander::_setOSCOUTFrequency(bool always_off, bool always_on, uint8_t frequency) {
	_logger->info("[GPIOExpander] set OSCOUT frequency");
	_logger->warn("Method not implemented");
	return true;
}
bool GPIOExpander::_getLEDDriverMode(bool bank_a, bool &logarithmic) {
	_logger->info("[GPIOExpander] get LED driver mode");
	_logger->warn("Method not implemented");
	return true;
}
bool GPIOExpander::_setLEDDriverMode(bool bank_a, bool logarithmic) {
	_logger->info("[GPIOExpander] set LED driver mode");
	_logger->warn("Method not implemented");
	return true;
}
bool GPIOExpander::_getLEDDriverDivider(uint8_t &divider) {
	_logger->info("[GPIOExpander] get LED driver divider");
	_logger->warn("Method not implemented");
	return true;
}
bool GPIOExpander::_setLEDDriverDivider(uint8_t divider) {
	_logger->info("[GPIOExpander] set LED driver divider");
	
	// Verifico il valore del divider
	if (divider > 7) {
		_logger->error("Divider value exceedes limit");
		return false;
	}
	
	// Leggo il valore del registro e aggiorno i bit relativi al divider
	uint8_t reg;
	if (!i2cGetRegister(RegMisc, reg)) return false;
	reg &= ~MASK_DIVIDER;
	reg |= (divider << OFFSET_DIVIDER) & MASK_DIVIDER;
	if (!i2cSetRegister(RegMisc, reg)) return false;
	
	return true;
}
bool GPIOExpander::_getNRESETFunction(bool &POR) {
	_logger->info("[GPIOExpander] get NRESET function");
	_logger->warn("Method not implemented");
	return true;
}
bool GPIOExpander::_setNRESETFunction(bool POR) {
	_logger->info("[GPIOExpander] set NRESET function");
	_logger->warn("Method not implemented");
	return true;
}
bool GPIOExpander::_getAddressRegisterAutoincrement(bool &autoincrement) {
	_logger->info("[GPIOExpander] get address register autoincrement");
	_logger->warn("Method not implemented");
	return true;
}
bool GPIOExpander::_setAddressRegisterAutoincrement(bool autoincrement) {
	_logger->info("[GPIOExpander] set address register autoincrement");
	_logger->warn("Method not implemented");
	return true;
}
bool GPIOExpander::_getAutoclearNINT(bool &autoclear) {
	_logger->info("[GPIOExpander] get autoclear NINT");
	_logger->warn("Method not implemented");
	return true;
}
bool GPIOExpander::_setAutoclearNINT(bool autoclear) {
	_logger->info("[GPIOExpander] set autoclear NINT");
	_logger->warn("Method not implemented");
	return true;
}

bool GPIOExpander::_getLEDDriverEnable(uint16_t &data) {
	_logger->info("[GPIOExpander] get LED driver enable");
	
	if (!_get(RegLEDDriverEnableA, RegLEDDriverEnableB, data)) return false;
	return true;
}
bool GPIOExpander::_setLEDDriverEnable(uint16_t data) {
	_logger->info("[GPIOExpander] set LED driver enable");
	
	if (!_set(RegLEDDriverEnableA, RegLEDDriverEnableB, data)) return false;
	return true;
}
bool GPIOExpander::_updateLEDDriverEnable(uint16_t data, uint16_t mask) {
	_logger->info("[GPIOExpander] update LED driver enable");
	
	// Leggo il valore del registro e aggiorno i bit relativi
	uint16_t old_data;
	if (!_getLEDDriverEnable(old_data)) return false;
	data &= mask;
	data |= old_data & ~mask;
	if (!_setLEDDriverEnable(data)) return false;
	
	return true;
}

bool GPIOExpander::_getDebounceTime(uint8_t &data) {
	_logger->info("[GPIOExpander] get debounce time");
	_logger->warn("Method not implemented");
	return true;
}
bool GPIOExpander::_setDebounceTime(uint8_t data) {
	_logger->info("[GPIOExpander] set debounce time");
	_logger->warn("Method not implemented");
	return true;
}
bool GPIOExpander::_updateDebounceTime(uint8_t data, uint8_t mask) {
	_logger->info("[GPIOExpander] update debounce time");
	_logger->warn("Method not implemented");
	return true;
}

bool GPIOExpander::_getDebounceEnable(uint16_t &data) {
	_logger->info("[GPIOExpander] get debounce enable");
	_logger->warn("Method not implemented");
	return true;
}
bool GPIOExpander::_setDebounceEnable(uint16_t data) {
	_logger->info("[GPIOExpander] set debounce enable");
	_logger->warn("Method not implemented");
	return true;
}
bool GPIOExpander::_updateDebounceEnable(uint16_t data, uint16_t mask) {
	_logger->info("[GPIOExpander] update debounce enable");
	_logger->warn("Method not implemented");
	return true;
}

bool GPIOExpander::_getAutoSleepTime(uint8_t &data) {
	_logger->info("[GPIOExpander] get auto sleep time");
	_logger->warn("Method not implemented");
	return true;
}
bool GPIOExpander::_setAutoSleepTime(uint8_t data) {
	_logger->info("[GPIOExpander] set auto sleep time");
	_logger->warn("Method not implemented");
	return true;
}

bool GPIOExpander::_getScanTime(uint8_t &data) {
	_logger->info("[GPIOExpander] get scan time");
	_logger->warn("Method not implemented");
	return true;
}
bool GPIOExpander::_setScanTime(uint8_t data) {
	_logger->info("[GPIOExpander] set scan time");
	_logger->warn("Method not implemented");
	return true;
}

bool GPIOExpander::_getRowNumber(uint8_t &data) {
	_logger->info("[GPIOExpander] get row number");
	_logger->warn("Method not implemented");
	return true;
}
bool GPIOExpander::_setRowNumber(uint8_t data) {
	_logger->info("[GPIOExpander] set row number");
	_logger->warn("Method not implemented");
	return true;
}

bool GPIOExpander::_getColNumber(uint8_t &data) {
	_logger->info("[GPIOExpander] get col number");
	_logger->warn("Method not implemented");
	return true;
}
bool GPIOExpander::_setColNumber(uint8_t data) {
	_logger->info("[GPIOExpander] set col number");
	_logger->warn("Method not implemented");
	return true;
}

bool GPIOExpander::_getNINTColumn(uint8_t &column) {
	_logger->info("[GPIOExpander] get NINT column");
	_logger->warn("Method not implemented");
	return true;
}
bool GPIOExpander::_getNINTRow(uint8_t &row) {
	_logger->info("[GPIOExpander] get NINT row");
	_logger->warn("Method not implemented");
	return true;
}

bool GPIOExpander::_getTOn(unsigned int number, uint8_t &data) {
	_logger->info(string("[GPIOExpander] get TOn") + to_string(number));
	_logger->warn("Method not implemented");
	return true;
}
bool GPIOExpander::_setTOn(unsigned int number, uint8_t data) {
	_logger->info(string("[GPIOExpander] set TOn") + to_string(number));
	_logger->warn("Method not implemented");
	return true;
}

bool GPIOExpander::_getIOn(unsigned int number, uint8_t &data) {
	_logger->info(string("[GPIOExpander] get IOn") + to_string(number));
	
	if (number >= GPIO_NUMBER) {
		_logger->warn("Pin number out of range");
		return false;
	}
	
	if (!i2cGetRegister(_REG_ION[number], data)) return false;
	
	return true;
}
bool GPIOExpander::_setIOn(unsigned int number, uint8_t data) {
	_logger->info(string("[GPIOExpander] set IOn") + to_string(number));
	
	if (number >= GPIO_NUMBER) {
		_logger->warn("Pin number out of range");
		return false;
	}
	
	if (!i2cSetRegister(_REG_ION[number], data)) return false;
	
	return true;
}

bool GPIOExpander::_getOff(unsigned int number, uint8_t &data) {
	_logger->info(string("[GPIOExpander] get Off") + to_string(number));
	_logger->warn("Method not implemented");
	return true;
}
bool GPIOExpander::_setOff(unsigned int number, uint8_t data) {
	_logger->info(string("[GPIOExpander] set Off") + to_string(number));
	_logger->warn("Method not implemented");
	return true;
}

bool GPIOExpander::_getTRise(unsigned int number, uint8_t &data) {
	_logger->info(string("[GPIOExpander] get TRise") + to_string(number));
	_logger->warn("Method not implemented");
	return true;
}
bool GPIOExpander::_setTRise(unsigned int number, uint8_t data) {
	_logger->info(string("[GPIOExpander] set TRise") + to_string(number));
	_logger->warn("Method not implemented");
	return true;
}

bool GPIOExpander::_getTFall(unsigned int number, uint8_t &data) {
	_logger->info(string("[GPIOExpander] get TFall") + to_string(number));
	_logger->warn("Method not implemented");
	return true;
}
bool GPIOExpander::_setTFall(unsigned int number, uint8_t data) {
	_logger->info(string("[GPIOExpander] set TFall") + to_string(number));
	_logger->warn("Method not implemented");
	return true;
}

bool GPIOExpander::_getHighInput(uint16_t &data) {
	_logger->info("[GPIOExpander] get High Input");
	_logger->warn("Method not implemented");
	return true;
}
bool GPIOExpander::_setHighInput(uint16_t data) {
	_logger->info("[GPIOExpander] set High Input");
	_logger->warn("Method not implemented");
	return true;
}
bool GPIOExpander::_updateHighInput(uint16_t data, uint16_t mask) {
	_logger->info("[GPIOExpander] update High Input");
	_logger->warn("Method not implemented");
	return true;
}

bool GPIOExpander::_softwareReset() {
	_logger->info("[GPIOExpander] software reset");
	
	if (!i2cSetRegister(RegReset, RESET_MAGIC1)) return false;
	if (!i2cSetRegister(RegReset, RESET_MAGIC2)) return false;
	_out_data = 0xffff;
	
	return true;
}

bool GPIOExpander::_readPinValues() {
	_logger->info("[GPIOExpander] read pin values");
	
	// Ritrovo il valore degli input dal dispositivo
	uint16_t values, mask;
	if (!_getInData(values, mask)) {
		return false;
	}
	
	// Aggiorno i pin di input
	for (unsigned int i = 0; i < GPIO_NUMBER; i++) {
		GPIOPinConfiguration &pc = _pin_configuration[i];
		if (pc.mode != GPIOPinMode::Input) continue;
		
		pc.value = ((values & (1 << i)) != 0);
	}
	
	return true;
}

// Impostazione parametri del pin
bool GPIOExpander::setPinName(unsigned int number, QString name) {
	_logger->info((string("[GPIOExpander] set pin name: ") +
		string("number = ") + to_string(number) +
		string(", name = ") + name.toStdString()
		).c_str()
	);
	
	if (number >= GPIO_NUMBER) {
		_logger->warn("Pin number out of range");
		return false;
	}
	
	GPIOPinConfiguration &pc = _pin_configuration[number];
	pc.name = name;
	
	return true;
}
bool GPIOExpander::setPinMode(unsigned int number, GPIOPinMode mode) {
	_logger->info((string("[GPIOExpander] set pin mode: ") +
		string("number = ") + to_string(number) +
		string(", mode = ") + GPIOPinConfiguration::mode_to_string(mode)
		).c_str()
	);
	
	if (number >= GPIO_NUMBER) {
		_logger->warn("Pin number out of range");
		return false;
	}
	
	GPIOPinConfiguration &pc = _pin_configuration[number];
	
	// Verifico che la modalità non sia già impostata
	if (mode == pc.mode) {
		return true;
	}
	
	auto change_mode = [this] (unsigned int number, GPIOPinMode mode) {
		switch(mode) {
			case GPIOPinMode::Disabled:
				// Imposto il pin in ingresso
				if (!_updateDirection((1 << number), (1 << number))) return false;
				// Disabilito l'open drain
				if (!_updateOpenDrain(0, (1 << number))) return false;
				// Disabilito il buffer in ingresso
				if (!_updateInputBufferDisable((1 << number), (1 << number))) return false;
				// Disabilito il pulldown
				if (!_updatePullDown(0, (1 << number))) return false;
				// Disabilito il PWM
				if (!_updateLEDDriverEnable(0, (1 << number))) return false;
				
				return true;
			case GPIOPinMode::Input:
				// Imposto il pin in ingresso
				if (!_updateDirection((1 << number), (1 << number))) return false;
				// Disabilito l'open drain
				if (!_updateOpenDrain(0, (1 << number))) return false;
				// Abilito il buffer in ingresso
				if (!_updateInputBufferDisable(0, (1 << number))) return false;
				// Abilito il pulldown
				if (!_updatePullDown((1 << number), (1 << number))) return false;
				// Disabilito il PWM
				if (!_updateLEDDriverEnable(0, (1 << number))) return false;
				
				return true;
			case GPIOPinMode::Output:
				// Disabilito l'open drain
				if (!_updateOpenDrain(0, (1 << number))) return false;
				// Abilito il buffer in ingresso
				if (!_updateInputBufferDisable(0, (1 << number))) return false;
				// Disabilito il pulldown
				if (!_updatePullDown(0, (1 << number))) return false;
				// Disabilito il PWM
				if (!_updateLEDDriverEnable(0, (1 << number))) return false;
				// Imposto il pin in uscita
				if (!_updateDirection(0, (1 << number))) return false;
				
				return true;
			case GPIOPinMode::PWM:
				// Disabilito l'open drain
				if (!_updateOpenDrain(0, (1 << number))) return false;
				// Disabilito il buffer in ingresso
				if (!_updateInputBufferDisable((1 << number), (1 << number))) return false;
				// Disabilito il pulldown
				if (!_updatePullDown(0, (1 << number))) return false;
				// Imposto il pin in uscita
				if (!_updateDirection(0, (1 << number))) return false;
				// Abilito il PWM
				if (!_updateLEDDriverEnable((1 << number), (1 << number))) return false;
				// Abilito il pin
				if (!_updateData(0, (1 << number))) return false;
				
				return true;
			case GPIOPinMode::OpenDrain:
				// Disabilito il PWM
				if (!_updateLEDDriverEnable(0, (1 << number))) return false;
				// Disabilito il buffer in ingresso
				if (!_updateInputBufferDisable((1 << number), (1 << number))) return false;
				// Disabilito il pulldown
				if (!_updatePullDown(0, (1 << number))) return false;
				// Abilito l'open drain
				if (!_updateOpenDrain((1 << number), (1 << number))) return false;
				// Imposto il pin in uscita
				if (!_updateDirection(0, (1 << number))) return false;
				
				return true;
			default:
				return false;
		}
	};
	
	// Apro il dispositivo
	if (!i2cOpen()) {
		return false;
	}
	
	// Cambio la modalità del pin
	if (!change_mode(number, mode)) {
		_logger->error("Failed to set pin mode");
		i2cClose();
		return false;
	}
	
	// Chiudo il dispositivo
	if (!i2cClose()) {
		return false;
	}
	
	// Aggiorno la modalità
	pc.mode = mode;
	
	return true;
}
bool GPIOExpander::setPinValue(unsigned int number, bool value) {
	_logger->info((string("[GPIOExpander] set pin value: ") +
		string("number = ") + to_string(number) +
		string(", value = ") + string(value ? "high" : "low")
		).c_str()
	);
	
	if (number >= GPIO_NUMBER) {
		_logger->warn("Pin number out of range");
		return false;
	}

	// Apro il dispositivo
	if (!i2cOpen()) {
		return false;
	}
	
	// Cambio il valore del registro
	if (!_updateData(value ? (1 << number) : 0, (1 << number))) {
		_logger->error("Failed to set pin value");
		i2cClose();
		return false;
	}
	
	// Chiudo il dispositivo
	if (!i2cClose()) {
		return false;
	}
	
	return true;
}
bool GPIOExpander::setPinPWM(unsigned int number, double pwm) {
	_logger->info((string("[GPIOExpander] set pin pwm: ") +
		string("number = ") + to_string(number) +
		string(", pwm = ") + to_string((int) (pwm * 100)) + string("%")
		).c_str()
	);
	
	if (number >= GPIO_NUMBER) {
		_logger->warn("Pin number out of range");
		return false;
	}
	
	if (pwm < 0.0 || pwm > 1.0) {
		_logger->warn("PWM value out of range, value setted limited");
	}
	
	// Cambio il valore del PWM
	uint8_t ion_value;
	
	if (pwm <= 0.0) {
		ion_value = 255;
	}
	else {
		if (pwm >= 1.0) ion_value = 0;
		else {
			ion_value = 255 - ((uint8_t) (255 * pwm));
		}
	}
	
	// Apro il dispositivo
	if (!i2cOpen()) {
		return false;
	}
	
	if (!_setIOn(number, ion_value)) {
		_logger->error("Failed to set pin value");
		i2cClose();
		return false;
	}
	
	// Chiudo il dispositivo
	if (!i2cClose()) {
		return false;
	}
	
	return true;
}

bool GPIOExpander::setFileName(unsigned int number, std::string filename) {
	_gpio_filename[number] = QString::fromStdString(filename);
	return true;
}

bool GPIOExpander::getPinName(unsigned int number, QString &name) {
//	_logger->info((string("[GPIOExpander] get pin name: ") +
//		string("number = ") + to_string(number)
//		).c_str()
//	);
	
	if (number >= GPIO_NUMBER) {
		_logger->warn("Pin number out of range");
		return false;
	}
	
	GPIOPinConfiguration &pc = _pin_configuration[number];
	
	name = pc.name;
	
	return true;
}
bool GPIOExpander::getPinMode(unsigned int number, GPIOPinMode &mode) {
	_logger->info((string("[GPIOExpander] get pin mode: ") +
		string("number = ") + to_string(number)
		).c_str()
	);
	
	if (number >= GPIO_NUMBER) {
		_logger->warn("Pin number out of range");
		return false;
	}
	
	GPIOPinConfiguration &pc = _pin_configuration[number];
	
	mode = pc.mode;
	
	return true;
}
bool GPIOExpander::getPinValue(unsigned int number, bool &value) {
	_logger->info((string("[GPIOExpander] get pin value: ") +
		string("number = ") + to_string(number)
		).c_str()
	);
	
	if (number >= GPIO_NUMBER) {
		_logger->warn("Pin number out of range");
		return false;
	}
	
	// Apro il dispositivo
	if (!i2cOpen()) {
		return false;
	}
	
	// Leggo il valore
	uint16_t data, mask;
	if (!_getInData(data, mask)) {
		_logger->error("Failed to get pin value");
		i2cClose();
		return false;
	}
	
	value = ((data & (1 << number)) != 0);
	
	// Chiudo il dispositivo
	if (!i2cClose()) {
		return false;
	}
	
	return true;
}
bool GPIOExpander::getPinPWM(unsigned int number, double &pwm) {
	_logger->info((string("[GPIOExpander] get pin pwm: ") +
		string("number = ") + to_string(number)
		).c_str()
	);
	
	if (number >= GPIO_NUMBER) {
		_logger->warn("Pin number out of range");
		return false;
	}
	
	// Apro il dispositivo
	if (!i2cOpen()) {
		return false;
	}
	
	// Leggo il valore
	uint8_t data;
	if (!_getIOn(number, data)) {
		_logger->error("Failed to get pin pwm");
		i2cClose();
		return false;
	}
	
	pwm = (data / 255.0);
	
	// Chiudo il dispositivo
	if (!i2cClose()) {
		return false;
	}
	
	return true;
}

bool GPIOExpander::softwareReset() {
	// Apro il dispositivo
	if (!i2cOpen()) {
		return false;
	}
	
	// Resetto il dispositivo
	if (!_softwareReset()) {
		i2cClose();
		return false;
	}
	
	// Chiudo il dispositivo
	if (!i2cClose()) {
		return false;
	}
	
	return true;
}

// Pubblicazione del dato
void GPIOExpander::publishGPIO() {
	_logger->info("[GPIOExpander] publish GPIO");
	
	// Apro il dispositivo
	if (!i2cOpen()) {
		return;
	}
	
	// Leggo i valori attuali
	if (!_readPinValues()) {
		i2cClose();
		return;
	}
	
	// Chiudo il dispositivo
	if (!i2cClose()) {
		return;
	}
	
	Message msg;
	
	// Analizzo ogni pin e se è configurato come input leggo e pubblico il suo valore
	for (unsigned int i = 0; i < GPIO_NUMBER; i++) {
		GPIOPinConfiguration &pc = _pin_configuration[i];
		if (pc.mode != GPIOPinMode::Input) continue;
		
		string str_value = string(pc.value ? "1" : "0");
		
		// Creo il messaggio e lo invio
		msg.clear();
		msg += (string("GPIOEXPANDER/") + pc.name.toUpper().toStdString()).c_str();
		msg += (string("VALUE = ") + str_value).c_str();
		//msg += _measure_timestamp.toString("dd/MM/yyyy HH:mm:ss.zzz").toStdString().c_str();
		emit requestPublishMessage(msg);
		
		// Verifico se è stato impostato il nome del file
		if (_gpio_filename.find(i) == _gpio_filename.end()) continue;
		
		// Verifico se esiste il QFile
		if (_gpio_files.find(i) == _gpio_files.end()) {
			_gpio_files[i] = new QFile();
		}
		
		QFile *file;
		file = _gpio_files[i];
		QString filename;
		filename = _gpio_filename[i];
		int &index = _gpio_file_index[i];
		
		if (!file->isOpen()) {
			// Verifico se è stato creato il file
			bool exists;
			if (!_getNextFreeIndex(_storage_dir, filename, _filesize_limit, index, exists)) {
				continue;
			}
			
			// Imposto il nome del file
			file->setFileName(_storage_dir.absoluteFilePath(filename + QString(".") + QString::number(index)));
			
			if (exists) {
				// Se esiste lo apro in modalità Append
				file->open(QIODevice::Append);
			}
			else {
				file->open(QIODevice::WriteOnly);
			}
		}
		
		// Salvo il dato su file
		QString record;
		
		record += QString("[") + QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss.zzz") + QString("] ");
		record += QString::fromStdString(str_value) + QString("\n");
		file->write(record.toStdString().c_str());
		
		// Se il file supera la dimensione massima consentita lo chiudo e apro un nuovo file
		if (file->size() >= _filesize_limit) {
			file->close();
			index++;
			
			// Imposto il nome del file
			file->setFileName(_storage_dir.absoluteFilePath(filename + QString(".") + QString::number(index)));
			file->open(QIODevice::WriteOnly);
		}
	}
}

// Metodi per il salvataggio dei dati
bool GPIOExpander::_getNextFreeIndex(QDir &folder, QString &filename, qint64 &size_limit, int &index, bool &file_exists) {
	QStringList filters;
	filters += filename + QString(".*");
	
	QFileInfoList files;
	files = folder.entryInfoList(filters);
	
	// Non esistono file a cui collegarsi
	if (files.count() == 0) {
		index = 0;
		file_exists = false;
		return true;
	}
	
	// Trovo l'indice massimo
	QRegularExpression regex;
	QRegularExpressionMatch match;
	int max_index = 0;
	QFileInfo max_fi;
	regex.setPattern(QString("^") + filename + QString("\\.(?<index>\\d+)$"));
	for (int i = 0; i < files.count(); i++) {
		match = regex.match(files[i].fileName());
		if (!match.hasMatch()) continue;
		int temp;
		temp = match.captured("index").toInt();
		if (max_index < temp) {
			max_fi = files[i];
			max_index = temp;
		}
	}
	
	if (max_fi.size() >= size_limit) {
		index = max_index + 1;
		file_exists = false;
	}
	else {
		index = max_index;
		file_exists = true;
	}
	
	return true;
}
