# Qt
QT += core
QT -= gui

# Config
CONFIG -= app_bundle
CONFIG += c++11
CONFIG += console
CONFIG += thread

# Target
TARGET = Drex_SensorAcquisition

# Template
TEMPLATE = app

# Sources
SOURCES += \
	src/main.cpp \
	src/Process/sap_command_selector.cpp \
	src/Process/sap_constants.cpp \
	src/Process/sap_constructor.cpp \
	src/Process/sap_destructor.cpp \
	src/Process/sap_load_configuration.cpp \
	src/Process/sap_notify.cpp \
	src/Process/sap_publish_message.cpp \
	src/Process/sap_socket_management.cpp \
	src/Process/sap_sys_signals.cpp \
	src/Process/sap_verify_command_syntax.cpp \
	src/Process/Commands/sap_cmd_get_state.cpp \
	src/Process/Commands/sap_cmd_kill.cpp \
	src/Process/Commands/sap_cmd_set_state.cpp \
	src/Process/Features/sap_set_state.cpp \
	src/Process/Features/sap_kill_process.cpp \
	src/Process/Features/sap_get_state.cpp \
	src/Process/Inits/sap_init.cpp \
	src/Process/Inits/sap_init_file_logger.cpp \
	src/Process/Inits/sap_init_networking.cpp \
	src/Process/Inits/sap_init_sensors.cpp \
	src/Process/Inits/sap_init_states_map.cpp \
	src/Process/States/sap_run.cpp \
	src/Process/States/sap_idle.cpp \
	src/Device/sap_i2c_device.cpp \
	src/Device/sap_spi_device.cpp \
	src/Sensor/sap_barometer.cpp \
	src/Sensor/sap_imu.cpp \
	src/Sensor/sap_magnetometer.cpp \
	src/Sensor/sap_thermometer.cpp \
	src/Sensor/sap_vacuometer.cpp \
    src/Actuator/sap_dac.cpp \
    src/Process/Inits/sap_init_actuators.cpp \
    src/Process/Features/sap_set_dac.cpp \
    src/Process/Commands/sap_cmd_set_dac.cpp \
    src/Bidirectional/sap_gpio_expander.cpp \
    src/Process/sap_publish_log.cpp \
    src/Process/Inits/sap_init_storage.cpp \
    ../Drex/src/PublisherSink/publisher_sink.cpp \
    src/Process/Inits/sap_init_bidirectionals.cpp \
    src/Process/Commands/sap_cmd_set_gpio_value.cpp \
    src/Process/Commands/sap_cmd_set_gpio_mode.cpp \
    src/Process/Inits/sap_init_publishing.cpp \
    src/Process/sap_publishing.cpp \
    src/Process/Commands/sap_cmd_set_gpio_pwm.cpp \
    src/Process/Commands/sap_cmd_set_pam.cpp \
    src/Process/Features/sap_set_pam.cpp \
    src/Process/Commands/sap_cmd_help.cpp

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000	# disables all the APIs deprecated before Qt 6.0.0

# Include path
INCLUDEPATH += \
	include \
	../ \
	../nzmqt/include \
	../Drex/include

# Altri file
DISTFILES += \
	config/sap.yaml \
    init_scripts/sap.sh \
    ../Drex/config/common.yaml

# Pacchetti
CONFIG += link_pkgconfig
PKGCONFIG += libzmq

# Librerie
LIBS += /usr/local/lib/libyaml-cpp.a

# Header
HEADERS += \
	include/Actuator/sap_dac.h \
	include/Bidirectional/sap_gpio_expander.h \
	include/Process/sap.h \
	include/Sensor/sap_barometer.h \
	include/Sensor/sap_generic_sensor.h \
	include/Sensor/sap_magnetometer.h \
	include/Sensor/sap_vacuometer.h \
	include/Sensor/sap_imu.h \
	include/Sensor/sap_thermometer.h \
	include/Device/sap_i2c_device.h \
	include/Device/sap_spi_device.h \
	../nzmqt/include/nzmqt/nzmqt.hpp \
	../Drex/include/PublisherSink/publisher_sink.h \
	../Drex/include/State/state_object.h \
	../Drex/include/Utilities/vector3d.h \
	../Drex/include/Utilities/linear_conversion.h \
	../Drex/include/Utilities/linear_bounds.h \
	../Drex/include/Socket/socket_utils.h \
    ../Drex/include/Utilities/int_to_hex.h
