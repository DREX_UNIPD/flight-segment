//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		07/08/17
//	Descrizione:		Classe che rappresenta il DAC MCP4725
//****************************************************************************************************//

#ifndef SAP_DAC_H
#define SAP_DAC_H

// Header DREX
#include <Device/sap_i2c_device.h>

// Header Qt
#include <QtCore>

namespace drex {
namespace actuator {

class DAC : public QObject, public device::I2CDevice {
	Q_OBJECT
	
	using Message = QList<QByteArray>;
	
private:
	using Logger = spdlog::logger;
	
	// Variabili private
	
	// Logger
	std::shared_ptr<Logger> _logger;
	
	// Costanti
	static constexpr double MAX_VOLTAGE = 3.322;
	static constexpr double MIN_VOLTAGE = 0;
	static constexpr __u16 MAX_VALUE = 4095; // 2^12 - 1
	static constexpr __u16 MIN_VALUE = 0;
	
public:
	// Costruttore
	DAC(
		const std::string device,
		const unsigned char address,
		QObject *parent = 0
	);
	
	// Distruttore
	~DAC();
	
	// Settaggio dell'output
	bool setOutput(double voltage, bool persistent);
};

}
}

#endif // SAP_DAC_H
