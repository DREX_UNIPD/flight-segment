//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		25/04/17
//	Descrizione:		Classe per processo SAP
//****************************************************************************************************//

#ifndef SAP_PROCESS_H
#define SAP_PROCESS_H

// Header per logging
#include <spdlog/spdlog.h>

// Header nzmqt
#include <nzmqt/nzmqt.hpp>

// Header Qt
#include <QCoreApplication>
#include <QSettings>
#include <QMap>

// Header YAML
#include <yaml-cpp/yaml.h>

// Header DREX
#include <Sensor/sap_barometer.h>
#include <Sensor/sap_imu.h>
#include <Sensor/sap_magnetometer.h>
#include <Sensor/sap_vacuometer.h>
#include <Sensor/sap_thermometer.h>
#include <Actuator/sap_dac.h>
#include <Bidirectional/sap_gpio_expander.h>

#include <State/state_object.h>
#include <Socket/socket_utils.h>

//----------------------------------------------------//

namespace drex {
namespace processes {

// Classe del processo SAP
class SAProcess : public QCoreApplication {
	Q_OBJECT
	
	using super = QCoreApplication;
	using Message = QList<QByteArray>;
	using Logger = spdlog::logger;
	
private:
	//*****************//
	// Sezione privata
	//*****************//
	
	// // Variabili
	
	// Logging
	
	// Logger su console
	std::shared_ptr<Logger> _console;
	
	// Logger su file e console
	std::shared_ptr<Logger> _logger;
	
	// Configurazione
	
	// Configurazione del processo
	YAML::Node _configuration;
	
	// Storage
	
	QDir _storage_home;
	QDir _storage_ssd_dir;
	QDir _storage_sensor_dir;
	QDir _storage_log_dir;
	
	qint64 _storage_filesize_limit;
	
	// Socket
	
	// Contesto per i socket
	nzmqt::ZMQContext *_context;
	
	// Command (ROUTER)
	drex::socket::SocketInfo _command_socket;
	// Logger (PUBLISHER)
	drex::socket::SocketInfo _logger_socket;
	// Data (PUBLISHER)
	drex::socket::SocketInfo _data_socket;
	// Reply (SUBSCRIBER)
	drex::socket::SocketInfo _reply_socket;
	
	// Gestione degli stati
	
	// Stato
	std::string _state;
	
	// Timer per la pubblicazione dello stato
	bool _publishing_enabled;
	
	// Pubblicazione dello stato
	bool _publishing_state_enabled;
	int _publishing_state_period;
	QTimer _publishing_state_timer;
	
	// Comandi
	QMap<std::string, drex::state::StateObject *> _state_objects;
	
	// Sensoristica
	
	// Sensori
	bool _barometer_enabled;
	drex::sensor::Barometer *_barometer;
	
	bool _imu_enabled;
	drex::sensor::Imu *_imu;
	
	bool _magnetometer_enabled;
	drex::sensor::Magnetometer *_magnetometer;
	
	bool _vacuometer_enabled;
	drex::sensor::Vacuometer *_vacuometer;
	
	bool _thermometer_enabled;
	QList<__u8> _thermometer_channel_enabled_list;
	drex::sensor::Thermometer *_thermometer;
	
//	bool _microswitch_enabled;
//	drex::sensor::Microswitch *_microswitch;
	
	// Attuatori
	
	bool _dac_led_1_enabled;
	drex::actuator::DAC *_dac_led_1;
	
	bool _dac_led_2_enabled;
	drex::actuator::DAC *_dac_led_2;
	
	bool _dac_heater_enabled;
	drex::actuator::DAC *_dac_heater;
	
	bool _dac_thermalcutter_enabled;
	drex::actuator::DAC *_dac_thermalcutter;
	
	// Conversione corrente -> tensione dac
	double _pam_voltage_min;
	double _pam_voltage_max;
	double _pam_current_min;
	double _pam_current_max;
	
	// Bidirezionali
	
	bool _gpio_expander_enabled;
	drex::bidirectional::GPIOExpander *_gpio_expander;
	
	// // Metodi
	
	// Caricamento della configurazione
	bool loadConfiguration(QStringList &paths, YAML::Node &destination);
	
	// Variabili per i segnali di sistema
	QSocketNotifier *_sn_hup;
	QSocketNotifier *_sn_int;
	QSocketNotifier *_sn_term;
	int *_sig_hup_fd;
	int *_sig_int_fd;
	int *_sig_term_fd;
	
	//**********//
	// Features
	//**********//
	
	// Metodo per la terminazione del processo
	void killProcess();
	
	// Metodi per la gestione dello stato
	std::string getState();
	bool setState(std::string newstate);
	
	// Metodi per la gestione dei DAC
	bool setDAC(drex::actuator::DAC *dac, double voltage, bool persistent);
	bool setPAM(drex::actuator::DAC *dac, double current, bool persistent);
	
	//******************//
	// Metodi ausiliari
	//******************//
	
	// Verifica della sintassi del comando ed eventuale risposta
	bool verifyCommandSyntax(const Message &msg, const std::vector<const char *> &syntax, QList<QRegularExpressionMatch> &match);
	
	// Inizializzazioni
	
	// Metodo principale
	void init();
	
	// Inizializzazione dello storage
	bool initStorage();
	
	// Inizializzazione del logger su file
	bool initFileLogger();
	
	// Inizializzazione dei sensori
	bool initSensors();
	bool initIMU(const YAML::Node &imu);
	bool initBarometer(const YAML::Node &barometer);
	bool initMagnetometer(const YAML::Node &magnetometer);
	bool initVacuometer(const YAML::Node &vacuometer);
	bool initThermometer(const YAML::Node &thermometer);
//	bool initMicroswitch(const YAML::Node &microswitch);
	
	// Inizializzazione degli attuatori
	bool initActuators();
	bool initDAC(const YAML::Node &dac_config, drex::actuator::DAC *&dac);
	
	// Inizializzazione dei dispositivi bidirezionali
	bool initBidirectionals();
	bool initGPIOExpander(const YAML::Node &gpio_expander);
	
	// Inizializzazione dell'interfaccia di rete
	bool initNetworking();
	
	// Inizializzazione degli stati
	bool initStatesMap();
	
	// Inizializzazione della pubblicazione
	bool initPublishing();
	
	// Inizializzazione dei socket
	bool initSocket(drex::socket::SocketInfo &si, const YAML::Node &node, std::string name, int linger);
	bool startSocket(drex::socket::SocketInfo &si);
	
	//**********************//
	// Metodi per gli stati
	//**********************//
	
	// IDLE
	bool stateIdleInit();
	bool stateIdleCleanup();
	
	// RUN
	bool stateRunInit();
	bool stateRunCleanup();
	
private slots:
	//**********************//
	// Selettore di comandi
	//**********************//
	
	// Seleziona ed esegue il comando appropriato
	void commandSelector(const Message &msg);
	
	//******************//
	// Comandi standard
	//******************//
	
	// Aiuto
	bool commandHelp(const Message &msg, std::vector<const char *> &parameters);
	
	// Chiusura del processo
	bool commandKill(const Message &msg, std::vector<const char *> &parameters);
	
	// Lettura stato del processo
	bool commandGetState(const Message &msg, std::vector<const char *> &parameters);
	
	// Impostazione stato del processo
	bool commandSetState(const Message &msg, std::vector<const char *> &parameters);
	
	//*******************//
	// Comandi specifici
	//*******************//
	
	// Impostazione dell'uscita del DAC
	bool commandSetDAC(const Message &msg, std::vector<const char *> &parameters);
	
	// Impostazione dell'uscita di un GPIO
	bool commandSetGPIOValue(const Message &msg, std::vector<const char *> &parameters);
	
	// Impostazione del PWM di un GPIO
	bool commandSetGPIOPWM(const Message &msg, std::vector<const char *> &parameters);
	
	// Impostazione della modalità di un GPIO
	bool commandSetGPIOMode(const Message &msg, std::vector<const char *> &parameters);
	
	// Impostazione dell'uscita del PAM
	bool commandSetPAM(const Message &msg, std::vector<const char *> &parameters);
	
	//***************//
	// Pubblicazione
	//***************//
	
	// Pubblicazione dello stato
	void publishingState();
	
public slots:
	// Gestori dei segnali di sistema di Qt
	void handleSigHup();
	void handleSigInt();
	void handleSigTerm();
	
	// Pubblicazione di un messaggio tramite socket definito in questa classe
	void publishMessage(const Message &msg);
	
	// Pubblicazione di un messaggio di Log
	void publishLog(const Message &msg);
	
signals:
	//*********//
	// Segnali
	//*********//
	
	void requestSetState(const Message &msg);
	
public:
	//******************//
	// Sezione pubblica
	//******************//
	
	// // Costanti
	
	// Nome del processo
	static const std::string SHORT_NAME;
	static const std::string FULL_NAME;
	
	// Comandi
	static const std::string CMD_HELP;
	static const std::string CMD_KILL;
	static const std::string CMD_GET_STATE;
	static const std::string CMD_SET_STATE;
	
	static const std::string CMD_SET_GPIO_VALUE;
	static const std::string CMD_SET_GPIO_PWM;
	static const std::string CMD_SET_GPIO_MODE;
	static const std::string CMD_SET_DAC;
	static const std::string CMD_SET_PAM;
	
	// Stati
	static const std::string ST_DEFAULT;
	static const std::string ST_IDLE;
	static const std::string ST_RUN;
	
	// // Metodi
	
	// Costruttore
	explicit SAProcess(int &argc, char **argv);
	
	// Distruttore
	~SAProcess() override;
	
	// Notifica evento
	bool notify(QObject *obj, QEvent *event) override;
	
	// Installazione signal handlers
	void setupSignalHandlers(int *p_hup, int *p_int, int *p_term);
};

}
}

#endif // SAP_PROCESS_H

