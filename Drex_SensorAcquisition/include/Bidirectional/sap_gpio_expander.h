//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		12/09/17
//	Descrizione:		Classe che rappresenta il GPIO Expander SX1509
//****************************************************************************************************//

#ifndef SAP_GPIO_EXPANDER_H
#define SAP_GPIO_EXPANDER_H

// Header DREX
#include <Device/sap_i2c_device.h>
#include <Sensor/sap_generic_sensor.h>

// Header Qt
#include <QtCore>

namespace drex {
namespace bidirectional {

enum class GPIOPinMode : unsigned char {
	Disabled = 0,
	Input = 1,
	Output = 2,
	PWM = 3,
	OpenDrain = 4
};

struct GPIOPinConfiguration {
	// // Nome del pin
	QString name;
	// // Modalità del pin
	// Disabled, Input, Output, PWM
	GPIOPinMode mode;
	// // Valore del pin (input / output)
	bool value;
	// // Valore del pwm (input / output)
	// PWM: 0.0 = 0%, 1.0 = 100%
	double pwm_value;
	
	static std::string mode_to_string(GPIOPinMode mode) {
		switch(mode) {
		case GPIOPinMode::Disabled:
			return std::string("Disabled");
		case GPIOPinMode::Input:
			return std::string("Input");
		case GPIOPinMode::Output:
			return std::string("Output");
		case GPIOPinMode::PWM:
			return std::string("PWM");
		case GPIOPinMode::OpenDrain:
			return std::string("OpenDrain");
		default:
			return std::string("?");
		}
	}
};

class GPIOExpander : public QObject, public device::I2CDevice, public sensor::GenericSensor {
	Q_OBJECT
	
	using Message = QList<QByteArray>;
	
private:
	using Logger = spdlog::logger;
	
	// Variabili private
	
	// Logger
	std::shared_ptr<Logger> _logger;
	
	// Configurazione dei pin
	std::vector<GPIOPinConfiguration> _pin_configuration;
	
	// Parametri di salvataggio
	QDir _storage_dir;
	qint64 _filesize_limit;
	
	QMap<int, QString> _gpio_filename;
	QMap<int, QFile *> _gpio_files;
	QMap<int, int> _gpio_file_index;
	
	// Parametri di pubblicazione
	double _frequency;
	bool _publishing;
	
	// Timer per la pubblicazione dei GPIO
	QTimer _timer;
	
public:
	// Costanti
	static constexpr unsigned int GPIO_NUMBER = 16;
	static constexpr uint8_t RESET_MAGIC1 = 0x12;			// Reset magic number #1
	static constexpr uint8_t RESET_MAGIC2 = 0x34;			// Reset magic number #2
	static constexpr uint8_t MASK_CLOCK = 0x60;
	static constexpr uint8_t CLOCK_OFF = 0x00;
	static constexpr uint8_t CLOCK_EXTERNAL = 0x20;
	static constexpr uint8_t CLOCK_INTERNAL = 0x40;
	static constexpr uint8_t MASK_DIVIDER = 0x70;
	static constexpr int OFFSET_DIVIDER = 4;
	
	// Registri
	
	// Device and IO Banks
	static constexpr uint8_t RegInputDisableB = 0x00;		// Default: 0x00
	static constexpr uint8_t RegInputDisableA = 0x01;		// Default: 0x00
	static constexpr uint8_t RegLongSlewB = 0x02;			// Default: 0x00
	static constexpr uint8_t RegLongSlewA = 0x03;			// Default: 0x00
	static constexpr uint8_t RegLowDriveB = 0x04;			// Default: 0x00
	static constexpr uint8_t RegLowDriveA = 0x05;			// Default: 0x00
	static constexpr uint8_t RegPullUpB = 0x06;				// Default: 0x00
	static constexpr uint8_t RegPullUpA = 0x07;				// Default: 0x00
	static constexpr uint8_t RegPullDownB = 0x08;			// Default: 0x00
	static constexpr uint8_t RegPullDownA = 0x09;			// Default: 0x00
	static constexpr uint8_t RegOpenDrainB = 0x0A;			// Default: 0x00
	static constexpr uint8_t RegOpenDrainA = 0x0B;			// Default: 0x00
	static constexpr uint8_t RegPolarityB = 0x0C;			// Default: 0x00
	static constexpr uint8_t RegPolarityA = 0x0D;			// Default: 0x00
	static constexpr uint8_t RegDirB = 0x0E;				// Default: 0xFF
	static constexpr uint8_t RegDirA = 0x0F;				// Default: 0xFF
	static constexpr uint8_t RegDataB = 0x10;				// Default: 0xFF
	static constexpr uint8_t RegDataA = 0x11;				// Default: 0xFF
	static constexpr uint8_t RegInterruptMaskB = 0x12;		// Default: 0xFF
	static constexpr uint8_t RegInterruptMaskA = 0x13;		// Default: 0xFF
	static constexpr uint8_t RegSenseHighB = 0x14;			// Default: 0x00
	static constexpr uint8_t RegSenseLowB = 0x15;			// Default: 0x00
	static constexpr uint8_t RegSenseHighA = 0x16;			// Default: 0x00
	static constexpr uint8_t RegSenseLowA = 0x17;			// Default: 0x00
	static constexpr uint8_t RegInterruptSourceB = 0x18;	// Default: 0x00
	static constexpr uint8_t RegInterruptSourceA = 0x19;	// Default: 0x00
	static constexpr uint8_t RegEventStatusB = 0x1A;		// Default: 0x00
	static constexpr uint8_t RegEventStatusA = 0x1B;		// Default: 0x00
	static constexpr uint8_t RegLevelShifter1 = 0x1C;		// Default: 0x00
	static constexpr uint8_t RegLevelShifter2 = 0x1D;		// Default: 0x00
	static constexpr uint8_t RegClock = 0x1E;				// Default: 0x00
	static constexpr uint8_t RegMisc = 0x1F;				// Default: 0x00
	static constexpr uint8_t RegLEDDriverEnableB = 0x20;	// Default: 0x00
	static constexpr uint8_t RegLEDDriverEnableA = 0x21;	// Default: 0x00
	
	// Debounce and Keypad Engine
	static constexpr uint8_t RegDebounceConfig = 0x22;		// Default: 0x00
	static constexpr uint8_t RegDebounceEnableB = 0x23;		// Default: 0x00
	static constexpr uint8_t RegDebounceEnableA = 0x24;		// Default: 0x00
	static constexpr uint8_t RegKeyConfig1 = 0x25;			// Default: 0x00
	static constexpr uint8_t RegKeyConfig2 = 0x26;			// Default: 0x00
	static constexpr uint8_t RegKeyData1 = 0x27;			// Default: 0xFF
	static constexpr uint8_t RegKeyData2 = 0x28;			// Default: 0xFF
	
	// LED Driver (PWM, blinking, breathing)
	static constexpr uint8_t RegTOn0 = 0x29;				// Default: 0x00
	static constexpr uint8_t RegIOn0 = 0x2A;				// Default: 0xFF
	static constexpr uint8_t RegOff0 = 0x2B;				// Default: 0x00
	
	static constexpr uint8_t RegTOn1 = 0x2C;				// Default: 0x00
	static constexpr uint8_t RegIOn1 = 0x2D;				// Default: 0xFF
	static constexpr uint8_t RegOff1 = 0x2E;				// Default: 0x00
	
	static constexpr uint8_t RegTOn2 = 0x2F;				// Default: 0x00
	static constexpr uint8_t RegIOn2 = 0x30;				// Default: 0xFF
	static constexpr uint8_t RegOff2 = 0x31;				// Default: 0x00
	
	static constexpr uint8_t RegTOn3 = 0x32;				// Default: 0x00
	static constexpr uint8_t RegIOn3 = 0x33;				// Default: 0xFF
	static constexpr uint8_t RegOff3 = 0x34;				// Default: 0x00
	
	static constexpr uint8_t RegTOn4 = 0x35;				// Default: 0x00
	static constexpr uint8_t RegIOn4 = 0x36;				// Default: 0xFF
	static constexpr uint8_t RegOff4 = 0x37;				// Default: 0x00
	static constexpr uint8_t RegTRise4 = 0x38;				// Default: 0x00
	static constexpr uint8_t RegTFall4 = 0x39;				// Default: 0x00
	
	static constexpr uint8_t RegTOn5 = 0x3A;				// Default: 0x00
	static constexpr uint8_t RegIOn5 = 0x3B;				// Default: 0xFF
	static constexpr uint8_t RegOff5 = 0x3C;				// Default: 0x00
	static constexpr uint8_t RegTRise5 = 0x3D;				// Default: 0x00
	static constexpr uint8_t RegTFall5 = 0x3E;				// Default: 0x00
	
	static constexpr uint8_t RegTOn6 = 0x3F;				// Default: 0x00
	static constexpr uint8_t RegIOn6 = 0x40;				// Default: 0xFF
	static constexpr uint8_t RegOff6 = 0x41;				// Default: 0x00
	static constexpr uint8_t RegTRise6 = 0x42;				// Default: 0x00
	static constexpr uint8_t RegTFall6 = 0x43;				// Default: 0x00
	
	static constexpr uint8_t RegTOn7 = 0x44;				// Default: 0x00
	static constexpr uint8_t RegIOn7 = 0x45;				// Default: 0xFF
	static constexpr uint8_t RegOff7 = 0x46;				// Default: 0x00
	static constexpr uint8_t RegTRise7 = 0x47;				// Default: 0x00
	static constexpr uint8_t RegTFall7 = 0x48;				// Default: 0x00
	
	static constexpr uint8_t RegTOn8 = 0x49;				// Default: 0x00
	static constexpr uint8_t RegIOn8 = 0x4A;				// Default: 0xFF
	static constexpr uint8_t RegOff8 = 0x4B;				// Default: 0x00
	
	static constexpr uint8_t RegTOn9 = 0x4C;				// Default: 0x00
	static constexpr uint8_t RegIOn9 = 0x4D;				// Default: 0xFF
	static constexpr uint8_t RegOff9 = 0x4E;				// Default: 0x00
	
	static constexpr uint8_t RegTOn10 = 0x4F;				// Default: 0x00
	static constexpr uint8_t RegIOn10 = 0x50;				// Default: 0xFF
	static constexpr uint8_t RegOff10 = 0x51;				// Default: 0x00
	
	static constexpr uint8_t RegTOn11 = 0x52;				// Default: 0x00
	static constexpr uint8_t RegIOn11 = 0x53;				// Default: 0xFF
	static constexpr uint8_t RegOff11 = 0x54;				// Default: 0x00
	
	static constexpr uint8_t RegTOn12 = 0x55;				// Default: 0x00
	static constexpr uint8_t RegIOn12 = 0x56;				// Default: 0xFF
	static constexpr uint8_t RegOff12 = 0x57;				// Default: 0x00
	static constexpr uint8_t RegTRise12 = 0x58;				// Default: 0x00
	static constexpr uint8_t RegTFall12 = 0x59;				// Default: 0x00
	
	static constexpr uint8_t RegTOn13 = 0x5A;				// Default: 0x00
	static constexpr uint8_t RegIOn13 = 0x5B;				// Default: 0xFF
	static constexpr uint8_t RegOff13 = 0x5C;				// Default: 0x00
	static constexpr uint8_t RegTRise13 = 0x5D;				// Default: 0x00
	static constexpr uint8_t RegTFall13 = 0x5E;				// Default: 0x00
	
	static constexpr uint8_t RegTOn14 = 0x5F;				// Default: 0x00
	static constexpr uint8_t RegIOn14 = 0x60;				// Default: 0xFF
	static constexpr uint8_t RegOff14 = 0x61;				// Default: 0x00
	static constexpr uint8_t RegTRise14 = 0x62;				// Default: 0x00
	static constexpr uint8_t RegTFall14 = 0x63;				// Default: 0x00
	
	static constexpr uint8_t RegTOn15 = 0x64;				// Default: 0x00
	static constexpr uint8_t RegIOn15 = 0x65;				// Default: 0xFF
	static constexpr uint8_t RegOff15 = 0x66;				// Default: 0x00
	static constexpr uint8_t RegTRise15 = 0x67;				// Default: 0x00
	static constexpr uint8_t RegTFall15 = 0x68;				// Default: 0x00
	
	const uint8_t _REG_ION[GPIO_NUMBER] = {
		RegIOn0, RegIOn1, RegIOn2, RegIOn3,
		RegIOn4, RegIOn5, RegIOn6, RegIOn7,
		RegIOn8, RegIOn9, RegIOn10, RegIOn11,
		RegIOn12, RegIOn13, RegIOn14, RegIOn15
	};
	
	// Miscellaneous
	static constexpr uint8_t RegHighInputB = 0x69;			// Default: 0x00
	static constexpr uint8_t RegHighInputA = 0x6A;			// Default: 0x00
	
	// Software Reset
	static constexpr uint8_t RegReset = 0x7D;				// Default: 0x00
	
	// Test (not to be written)
	static constexpr uint8_t RegTest1 = 0x7E;				// Default: 0x00
	static constexpr uint8_t RegTest2 = 0x7F;				// Default: 0x00
	
	// Metodi interni
	bool _get(uint8_t address_lower, uint8_t address_higher, uint16_t &data);
	bool _set(uint8_t address_lower, uint8_t address_higher, uint16_t data);
	bool _update(uint8_t address_lower, uint8_t address_higher, uint16_t data, uint16_t mask);
	
	bool _getInputBufferDisable(uint16_t &data);
	bool _setInputBufferDisable(uint16_t data);
	bool _updateInputBufferDisable(uint16_t data, uint16_t mask);
	
	bool _getLongSlew(uint16_t &data);
	bool _setLongSlew(uint16_t data);
	bool _updateLongSlew(uint16_t data, uint16_t mask);
	
	bool _getLowDrive(uint16_t &data);
	bool _setLowDrive(uint16_t data);
	bool _updateLowDrive(uint16_t data, uint16_t mask);
	
	bool _getPullUp(uint16_t &data);
	bool _setPullUp(uint16_t data);
	bool _updatePullUp(uint16_t data, uint16_t mask);
	
	bool _getPullDown(uint16_t &data);
	bool _setPullDown(uint16_t data);
	bool _updatePullDown(uint16_t data, uint16_t mask);
	
	bool _getOpenDrain(uint16_t &data);
	bool _setOpenDrain(uint16_t data);
	bool _updateOpenDrain(uint16_t data, uint16_t mask);
	
	bool _getPolarityInversion(uint16_t &data);
	bool _setPolarityInversion(uint16_t data);
	bool _updatePolarityInversion(uint16_t data, uint16_t mask);
	
	bool _getDirection(uint16_t &data);
	bool _setDirection(uint16_t data);
	bool _updateDirection(uint16_t data, uint16_t mask);
	
	uint16_t _out_data;
	bool _getOutData(uint16_t &data, uint16_t &mask);
	bool _getInData(uint16_t &data, uint16_t &mask);
	bool _setData(uint16_t data);
	bool _updateData(uint16_t data, uint16_t mask);
	
	bool _getInterruptMask(uint16_t &data);
	bool _setInterruptMask(uint16_t data);
	bool _updateInterruptMask(uint16_t data, uint16_t mask);
	
	bool _getEdgeSensitivity(uint16_t &rising, uint16_t &falling);
	bool _setEdgeSensitivity(uint16_t rising, uint16_t falling);
	bool _updateEdgeSensitivity(uint16_t rising, uint16_t falling, uint16_t mask);
	
	bool _getInterruptSource(uint16_t &data);
	bool _clearInterruptSource(uint16_t mask);
	
	bool _getEventStatus(uint16_t data);
	bool _clearEventStatus(uint16_t mask);
	
	bool _getLevelShifter(uint8_t &a_to_b, uint8_t &b_to_a);
	bool _setLevelShifter(uint8_t a_to_b, uint8_t b_to_a);
	bool _updateLevelShifter(uint8_t a_to_b, uint8_t b_to_a, uint8_t mask);
	
	bool _getOscillatorSource(uint8_t &source);
	bool _setOscillatorSource(uint8_t source);
	bool _getOSCIODirection(bool output);
	bool _setOSCIODirection(bool output);
	bool _getOSCOUTFrequency(bool &always_off, bool &always_on, uint8_t &frequency);
	bool _setOSCOUTFrequency(bool always_off, bool always_on, uint8_t frequency);
	
	bool _getLEDDriverMode(bool bank_a, bool &logarithmic);
	bool _setLEDDriverMode(bool bank_a, bool logarithmic);
	bool _getLEDDriverDivider(uint8_t &divider);
	bool _setLEDDriverDivider(uint8_t divider);
	bool _getNRESETFunction(bool &POR);
	bool _setNRESETFunction(bool POR);
	bool _getAddressRegisterAutoincrement(bool &autoincrement);
	bool _setAddressRegisterAutoincrement(bool autoincrement);
	bool _getAutoclearNINT(bool &autoclear);
	bool _setAutoclearNINT(bool autoclear);
	
	bool _getLEDDriverEnable(uint16_t &data);
	bool _setLEDDriverEnable(uint16_t data);
	bool _updateLEDDriverEnable(uint16_t data, uint16_t mask);
	
	bool _getDebounceTime(uint8_t &data);
	bool _setDebounceTime(uint8_t data);
	bool _updateDebounceTime(uint8_t data, uint8_t mask);
	
	bool _getDebounceEnable(uint16_t &data);
	bool _setDebounceEnable(uint16_t data);
	bool _updateDebounceEnable(uint16_t data, uint16_t mask);
	
	bool _getAutoSleepTime(uint8_t &data);
	bool _setAutoSleepTime(uint8_t data);
	
	bool _getScanTime(uint8_t &data);
	bool _setScanTime(uint8_t data);
	
	bool _getRowNumber(uint8_t &data);
	bool _setRowNumber(uint8_t data);
	
	bool _getColNumber(uint8_t &data);
	bool _setColNumber(uint8_t data);
	
	bool _getNINTColumn(uint8_t &column);
	bool _getNINTRow(uint8_t &row);
	
	bool _getTOn(unsigned int number, uint8_t &data);
	bool _setTOn(unsigned int number, uint8_t data);
	
	bool _getIOn(unsigned int number, uint8_t &data);
	bool _setIOn(unsigned int number, uint8_t data);
	
	bool _getOff(unsigned int number, uint8_t &data);
	bool _setOff(unsigned int number, uint8_t data);
	
	bool _getTRise(unsigned int number, uint8_t &data);
	bool _setTRise(unsigned int number, uint8_t data);
	
	bool _getTFall(unsigned int number, uint8_t &data);
	bool _setTFall(unsigned int number, uint8_t data);
	
	bool _getHighInput(uint16_t &data);
	bool _setHighInput(uint16_t data);
	bool _updateHighInput(uint16_t data, uint16_t mask);
	
	bool _softwareReset();
	
	bool _readPinValues();
	
	// Metodi per il salvataggio
	bool _getNextFreeIndex(QDir &folder, QString &filename, qint64 &size_limit, int &index, bool &file_exists);
	
public:
	// Costruttore
	GPIOExpander(
		const std::string device,
		const unsigned char address,
//		const std::vector<GPIOPinConfiguration> pin_configuration,
		QDir storage_dir,
		qint64 filesize_limit,
		const double frequency = 0.0,
		QObject *parent = 0
	);
	
	// Distruttore
	~GPIOExpander();
	
	// Inizializzazione
	bool initDevice() override;
	
	// Inizio lettura
	bool startReading() override;
	
	// Fine lettura
	bool stopReading() override;
	
	// Impostazione parametri del pin
	bool setPinName(unsigned int number, QString name);
	bool setPinMode(unsigned int number, GPIOPinMode mode);
	bool setPinValue(unsigned int number, bool value);
	bool setPinPWM(unsigned int number, double pwm);
	
	bool setFileName(unsigned int number, std::string filename);
	
	bool getPinName(unsigned int number, QString &name);
	bool getPinMode(unsigned int number, GPIOPinMode &mode);
	bool getPinValue(unsigned int number, bool &value);
	bool getPinPWM(unsigned int number, double &pwm);
	
	// Reset del dispositivo
	bool softwareReset();
	
public slots:
	// Segnali di pubblicazione
	void publishGPIO();
	
signals:
	// Invio la richiesta di pubblicazione al processo
	void requestPublishMessage(const Message &msg);
};

}
}

#endif // SAP_GPIO_EXPANDER_H
