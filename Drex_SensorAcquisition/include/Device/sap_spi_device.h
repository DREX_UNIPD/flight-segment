//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		16/07/17
//	Descrizione:		Classe che rappresenta un generico dispositivo SPI
//****************************************************************************************************//

#ifndef SAP_SPI_DEVICE_H
#define SAP_SPI_DEVICE_H

// Header per logging
#include <spdlog/spdlog.h>

// Header SPI
#include <linux/spi/spidev.h>

// Header STL
#include <string>

namespace drex {
namespace device {

class SPIDevice {
private:
	using Logger = spdlog::logger;
	
	// Logger
	std::shared_ptr<Logger> _logger;
	
protected:
	// Variabili
	std::string _device;
	__u32 _mode;
	__u8 _bits_per_word;
	__u8 _cs_change;
	__u16 _delay_usec;
	__u16 _pad;
	__u32 _speed_hz;
	
	// File
	int _file;
	
	// Metodi
	bool spiOpen();
	bool spiInit();
	bool spiClose();
	bool spiTransfer(__u32 len, __u8 *tx_buf, __u8 *rx_buf);
	
	// Costruttore
	SPIDevice(
		const std::string device,
		const __u32 mode = SPI_MODE_0,
		const __u32 speed_hz = 500000,
		const __u8 bits_per_word = 8,
		const __u8 cs_change = 0,
		const __u16 delay_usec = 0,
		const __u16 pad = 0
	);
};

}
}

#endif // SAP_SPI_DEVICE_H
