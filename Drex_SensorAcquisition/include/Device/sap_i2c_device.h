//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		29/04/17
//	Descrizione:		Classe che rappresenta un generico dispositivo I2C
//****************************************************************************************************//

#ifndef SAP_I2C_DEVICE_H
#define SAP_I2C_DEVICE_H

// Header per logging
#include <spdlog/spdlog.h>

// Header I2C
#include <linux/i2c-dev.h>

// Header STL
#include <string>

namespace drex {
namespace device {

class I2CDevice {
private:
	using Logger = spdlog::logger;
	
	// Logger
	std::shared_ptr<Logger> _logger;
	
	// File
	int _file;
	
protected:
	// Variabili
	std::string _device;
	unsigned char _address;
	
	// Metodi
	bool i2cOpen();
	bool i2cClose();
	bool i2cGetRegister(__u8 address, __u8 &value);
	bool i2cSetRegister(__u8 address, __u8 value);
	bool i2cGetWord(__u8 address, int16_t &value);
	bool i2cGetRaw(__u8 *value, size_t size);
	bool i2cSetRaw(__u8 *value, size_t size);
	
	// Costruttore
	I2CDevice(const std::string device, const unsigned char address);
};

}
}

#endif // SAP_I2C_DEVICE_H
