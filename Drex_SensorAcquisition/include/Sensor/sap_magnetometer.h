//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		30/04/17
//	Descrizione:		Classe che rappresenta il magnetometro LIS3MDL
//****************************************************************************************************//

#ifndef SAP_MAGNETOMETER_H
#define SAP_MAGNETOMETER_H

// Header DREX
#include <Utilities/linear_bounds.h>
#include <Utilities/linear_conversion.h>
#include <Utilities/vector3d.h>
#include <Sensor/sap_generic_sensor.h>
#include <Device/sap_i2c_device.h>

// Header nzmqt
#include <nzmqt/nzmqt.hpp>

// Header Qt
#include <QMap>

namespace drex {
namespace sensor {

// Enumerazioni per l'estensione di scala
enum class MagnetometerScale : unsigned char;

class Magnetometer : public QObject, public GenericSensor, public device::I2CDevice {
	Q_OBJECT
	
	using Message = QList<QByteArray>;
	
private:
	using Logger = spdlog::logger;
	
	// Variabili private
	
	// Logger
	std::shared_ptr<Logger> _logger;
	
	// Abilitazione
	bool _magnetic_field_enabled;
	bool _temperature_enabled;
	
	// Parametri di pubblicazione
	bool _publishing;
	double _thermometer_frequency;
	double _magnetometer_frequency;
	
	// Timer per la pubblicazione
	QTimer _thermometer_timer;
	QTimer _magnetometer_timer;
	
	// Autoscale
	bool _magnetometer_autoscale;
	
public:
	// Registri
	
	// Device identification register (always 0x3d)
	static constexpr unsigned char WHO_AM_I = 0x0f;
	static constexpr unsigned char WHO_AM_I__DEFAULT = 0x0f;
	
	// Control registers
	static constexpr unsigned char CTRL_REG1 = 0x20;
	static constexpr int ST = 0;
	static constexpr int FAST_ODR = 1;
	static constexpr int DO0 = 2;
	static constexpr int DO1 = 3;
	static constexpr int DO2 = 4;
	static constexpr int OM0 = 5;
	static constexpr int OM1 = 6;
	static constexpr int TEMP_EN = 7;
	static constexpr unsigned char CTRL_REG1__DEFAULT = (1 << TEMP_EN) | (1 << FAST_ODR);
	static constexpr unsigned char CTRL_REG2 = 0x21;
	static constexpr unsigned char CTRL_REG2__DEFAULT = 0x00;
	static constexpr int SOFT_RST = 2;
	static constexpr int REBOOT = 3;
	static constexpr int FS0 = 5;
	static constexpr int FS1 = 6;
	static constexpr unsigned char CTRL_REG3 = 0x22;
	static constexpr unsigned char CTRL_REG3__DEFAULT = 0x03;
	static constexpr int MD0 = 0;
	static constexpr int MD1 = 1;
	static constexpr int SIM = 2;
	static constexpr unsigned char CTRL_REG4 = 0x23;
	static constexpr unsigned char CTRL_REG4__DEFAULT = 0x00;
	static constexpr int BLE = 1;
	static constexpr int OMZ0 = 2;
	static constexpr int OMZ1 = 3;
	static constexpr unsigned char CTRL_REG5 = 0x24;
	static constexpr unsigned char CTRL_REG5__DEFAULT = 0x00;
	static constexpr int BDU = 6;
	static constexpr int FAST_READ = 7;
	
	// Status register
	static constexpr unsigned char STATUS_REG = 0x27;
	static constexpr int XDA = 0;
	static constexpr int YDA = 1;
	static constexpr int ZDA = 2;
	static constexpr int ZYXDA = 3;
	static constexpr int XOR = 4;
	static constexpr int YOR = 5;
	static constexpr int ZOR = 6;
	static constexpr int ZYXOR = 7;
	
	// Output registers
	static constexpr unsigned char OUT_X_L = 0x28;
	static constexpr unsigned char OUT_X_H = 0x29;
	static constexpr unsigned char OUT_Y_L = 0x2a;
	static constexpr unsigned char OUT_Y_H = 0x2b;
	static constexpr unsigned char OUT_Z_L = 0x2c;
	static constexpr unsigned char OUT_Z_H = 0x2d;
	static constexpr unsigned char TEMP_OUT_L = 0x2e;
	static constexpr unsigned char TEMP_OUT_H = 0x2f;
	
	// Interrupt configuration registers
	static constexpr unsigned char INT_CFG = 0x30;
	static constexpr unsigned char INT_CFG__DEFAULT = 0x00;
	static constexpr int IEN = 0;
	static constexpr int LIR = 1;
	static constexpr int IEA = 2;
	static constexpr int ZIEN = 5;
	static constexpr int YIEN = 6;
	static constexpr int XIEN = 7;
	static constexpr unsigned char INT_SRC = 0x31;
	static constexpr unsigned char INT_SRC__DEFAULT = 0x00;
	static constexpr int INT = 0;
	static constexpr int MROI = 1;
	static constexpr int NTH_Z = 2;
	static constexpr int NTH_Y = 3;
	static constexpr int NTH_X = 4;
	static constexpr int PTH_Z = 5;
	static constexpr int PTH_Y = 6;
	static constexpr int PTH_X = 7;
	static constexpr unsigned char INT_THS_L = 0x32;
	static constexpr unsigned char INT_THS_L__DEFAULT = 0x00;
	static constexpr int THS0 = 0;
	static constexpr int THS1 = 1;
	static constexpr int THS2 = 2;
	static constexpr int THS3 = 3;
	static constexpr int THS4 = 4;
	static constexpr int THS5 = 5;
	static constexpr int THS6 = 6;
	static constexpr int THS7 = 7;
	static constexpr unsigned char INT_THS_H = 0x33;
	static constexpr unsigned char INT_THS_H__DEFAULT = 0x00;
	static constexpr int THS8 = 0;
	static constexpr int THS9 = 1;
	static constexpr int THS10 = 2;
	static constexpr int THS11 = 3;
	static constexpr int THS12 = 4;
	static constexpr int THS13 = 5;
	static constexpr int THS14 = 6;
	
	// Maschere di bit
	static constexpr unsigned char MAGNETOMETER_SCALE_MASK = 0x60;
	
	// Fattori di conversione della temperatura
	static constexpr double TEMPERATURE_OFFSET = 25.0;
	static constexpr double TEMPERATURE_SCALE = 1.0 / 8;
	
public:
	
	// Costruttore
//	Magnetometer(Magnetometer *other);
	Magnetometer(const std::string device,
		const unsigned char address,
		bool magnetic_field_enabled,
		bool temperature_enabled,
		const double thermometer_frequency = 0.0,
		const double magnetometer_frequency = 0.0,
		QObject *parent = 0
	);
	
	// Distruttore
	~Magnetometer();
	
	// Inizializzazione
	bool initDevice() override;
	
	// Inizio lettura
	bool startReading() override;
	
	// Fine lettura
	bool stopReading() override;
	
	// Configurazione dei parametri del magnetometro
	bool getMagnetometerScale(MagnetometerScale &scale);
	bool setMagnetometerScale(MagnetometerScale scale);
	
	// Lettura della temperatura
	bool getTemperature(double &value);
	bool getRawTemperature(int16_t &value);
	
	// Lettura del magnetometro
	bool getMagneticField(Vector3D<double> &value);
	bool getRawMagneticField(Vector3D<int16_t> &value);
	
private:
	// Conversioni lineari
	MagnetometerScale _magnetometer_scale;
	QMap<MagnetometerScale, LinearConversion> _magnetometer_conversion;
	QMap<MagnetometerScale, LinearBounds> _magnetometer_scale_bounds;
	QMap<MagnetometerScale, MagnetometerScale> _magnetometer_scale_previous;
	QMap<MagnetometerScale, MagnetometerScale> _magnetometer_scale_next;
	
private slots:
	// Segnali di pubblicazione
	void publishTemperature();
	void publishMagneticField();
	
signals:
	// Invio la richiesta di pubblicazione al processo
	void requestPublishMessage(const Message &msg);
};

// Enumerazioni
enum class MagnetometerScale : unsigned char {
	GAUSS_4 = 0x00,
	GAUSS_8 = (1 << Magnetometer::FS0),
	GAUSS_12 = (1 << Magnetometer::FS1),
	GAUSS_16 = (1 << Magnetometer::FS1) | (1 << Magnetometer::FS0)
};

}
}

#endif // SAP_MAGNETOMETER_H
