//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		16/07/17
//	Descrizione:		Classe che rappresenta il termometro Linear LTC2983
//****************************************************************************************************//

#ifndef SAP_THERMOMETER_H
#define SAP_THERMOMETER_H

// Header DREX
#include <Sensor/sap_generic_sensor.h>
#include <Device/sap_spi_device.h>

// Header Qt
#include <QtCore>

// Macro per il calcolo della dimensione di un array
#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))

namespace drex {
namespace sensor {

// Configurazione di un canale
struct ChannelConfiguration {
	bool temperature;
	__u8 channel;
};

// Enumerazione per la segnalazione dello stato
enum class Converting : unsigned char;

// Enumerazioni per la configurazione
//enum class ThermocoupleType : unsigned char;
enum class RTDType : unsigned char;
//enum class ThermistorType : unsigned char;

//enum class ColdJunctionChannelAssignment : unsigned char;
enum class EndingMode : unsigned char;
//enum class OCCheck : bool;
enum class RSenseChannelAssignment : unsigned char;
enum class Wires : unsigned char;
enum class ExcitationMode : unsigned char;
enum class ExcitationCurrent : unsigned char;
enum class Curve : unsigned char;
//enum class OCCurrent : unsigned char;
//enum class CustomAddress : unsigned char;
//enum class CustomLength : unsigned char;

class Thermometer : public QObject, public GenericSensor, public device::SPIDevice {
	Q_OBJECT
	
	using Message = QList<QByteArray>;
	
private:
	using Logger = spdlog::logger;
	
	// Variabili private
	
	// Logger
	std::shared_ptr<Logger> _logger;
	
	// Configurazione
	RSenseChannelAssignment _sense_resistor_channel;
	double _sense_resistor_value;
	
	// Parametri di salvataggio
	QDir _storage_dir;
	qint64 _filesize_limit;
	
	QString _temperature_filename;
	QMap<int, QFile *> _temperature_files;
	QMap<int, int> _temperature_file_index;
	
	QString _voltage_filename;
	QMap<int, QFile *> _voltage_files;
	QMap<int, int> _voltage_file_index;
	
	// Timer per la pubblicazione
	QTimer _timer;
	
	// Flag per la conversione
	Converting _converting_flag;
	QList<ChannelConfiguration> _converting_channels;
	QList<ChannelConfiguration> _converting_registered_channels;
	QTimer _converting_timer;
	int _converting_attempts_counter;
	
	// Conversione della tensione di batteria
	bool _battery_voltage_enabled;
	__u8 _battery_voltage_channel;
	double _battery_voltage_ideal_ratio;
	double _battery_voltage_correction_factor;
	
	// Parametri di pubblicazione
	bool _publishing;
	double _frequency;
	
public:
	// Intervallo di attesa per la conversione dei dati [ms]
	static constexpr int CONVERTING_PERIOD = 100;
	// Numero di tentativi di lettura dell'avvenuta conversione
	static constexpr int CONVERTING_ATTEMPTS = 20;
	
	// Istruzioni di lettura e scrittura
	static constexpr __u8 WRITE_INSTR = 0x02;
	static constexpr __u8 READ_INSTR = 0x03;
	
	// Registri di configurazione degli ingressi
	static constexpr int16_t COMMAND_REGISTER = 0x000;
	
	static constexpr int START_BIT = 7;
	static constexpr int DONE_BIT = 6;
	
	static constexpr int16_t MULTICONVERT_MASK_REGISTER = 0x0F4;
	
	static constexpr int16_t GLOBAL_CONFIGURATION_REGISTER = 0x0F0;
	
	static constexpr __u8 TEMP_UNIT_CELSIUS = 0x00;
	static constexpr __u8 REJECTION_50_60_HZ = 0x00;
	
	static constexpr int16_t MUX_CONFIGURATION_DELAY_REGISTER = 0x0FF;
	
	static constexpr int16_t CONFIG_BASE_ADDR = 0x200;
	static constexpr int16_t CONFIG_CH1 = 0x200;
	static constexpr int16_t CONFIG_CH2 = 0x204;
	static constexpr int16_t CONFIG_CH3 = 0x208;
	static constexpr int16_t CONFIG_CH4 = 0x20C;
	static constexpr int16_t CONFIG_CH5 = 0x210;
	static constexpr int16_t CONFIG_CH6 = 0x214;
	static constexpr int16_t CONFIG_CH7 = 0x218;
	static constexpr int16_t CONFIG_CH8 = 0x21C;
	static constexpr int16_t CONFIG_CH9 = 0x220;
	static constexpr int16_t CONFIG_CH10 = 0x224;
	static constexpr int16_t CONFIG_CH11 = 0x228;
	static constexpr int16_t CONFIG_CH12 = 0x22C;
	static constexpr int16_t CONFIG_CH13 = 0x230;
	static constexpr int16_t CONFIG_CH14 = 0x234;
	static constexpr int16_t CONFIG_CH15 = 0x238;
	static constexpr int16_t CONFIG_CH16 = 0x23C;
	static constexpr int16_t CONFIG_CH17 = 0x240;
	static constexpr int16_t CONFIG_CH18 = 0x244;
	static constexpr int16_t CONFIG_CH19 = 0x248;
	static constexpr int16_t CONFIG_CH20 = 0x24C;
	
	// Fixed sensor types
	static constexpr int SENSOR_TYPE_UNASSIGNED = 0;
	
	static constexpr int SENSOR_TYPE_SENSE_RESISTOR = 29;
	static constexpr int SENSOR_TYPE_DIRECT_ADC = 30;
	
	// Offsets
	static constexpr int SENSOR_TYPE_OFFSET = 27;
	
//	static constexpr int THERMOCOUPLE_CJCA_OFFSET = 22;
//	static constexpr int THERMOCOUPLE_ENDING_MODE_OFFSET = 21;
//	static constexpr int THERMOCOUPLE_OC_CHECK_OFFSET = 20;
//	static constexpr int THERMOCOUPLE_OC_CURRENT_OFFSET = 18;
//	static constexpr int THERMOCOUPLE_CUSTOM_ADDRESS_OFFSET = 6;
//	static constexpr int THERMOCOUPLE_CUSTOM_LENGTH_OFFSET = 0;
	
	static constexpr int RTD_RSENSE_CHANNEL_ASSINGMENT_OFFSET = 22;
	static constexpr int RTD_WIRES_OFFSET = 20;
	static constexpr int RTD_EXCITATION_MODE_OFFSET = 18;
	static constexpr int RTD_EXCITATION_CURRENT_OFFSET = 14;
	static constexpr int RTD_CURVE_OFFSET = 12;
//	static constexpr int RTD_CUSTOM_ADDRESS_OFFSET = 6;
//	static constexpr int RTD_CUSTOM_LENGTH_OFFSET = 0;
	
	static constexpr int DIRECT_ADC_ENDING_MODE_OFFSET = 26;
	
	// Registri dei risultati delle acquisizioni
	static constexpr int16_t RESULT_BASE_ADDR = 0x010;
	static constexpr int16_t RESULT_CH1 = 0x010;
	static constexpr int16_t RESULT_CH2 = 0x014;
	static constexpr int16_t RESULT_CH3 = 0x018;
	static constexpr int16_t RESULT_CH4 = 0x01C;
	static constexpr int16_t RESULT_CH5 = 0x020;
	static constexpr int16_t RESULT_CH6 = 0x024;
	static constexpr int16_t RESULT_CH7 = 0x028;
	static constexpr int16_t RESULT_CH8 = 0x02C;
	static constexpr int16_t RESULT_CH9 = 0x030;
	static constexpr int16_t RESULT_CH10 = 0x034;
	static constexpr int16_t RESULT_CH11 = 0x038;
	static constexpr int16_t RESULT_CH12 = 0x03C;
	static constexpr int16_t RESULT_CH13 = 0x040;
	static constexpr int16_t RESULT_CH14 = 0x044;
	static constexpr int16_t RESULT_CH15 = 0x048;
	static constexpr int16_t RESULT_CH16 = 0x04C;
	static constexpr int16_t RESULT_CH17 = 0x050;
	static constexpr int16_t RESULT_CH18 = 0x054;
	static constexpr int16_t RESULT_CH19 = 0x058;
	static constexpr int16_t RESULT_CH20 = 0x05C;
	
	// Bit per l'interpretazione dell'errore
	static constexpr int SENSOR_HARD_FAILURE_BIT = 7 + 24;
	static constexpr int ADC_HARD_FAILURE_BIT = 6 + 24;
	static constexpr int CJ_HARD_FAILURE_BIT = 5 + 24;
	static constexpr int CJ_SOFT_FAILURE_BIT = 4 + 24;
	static constexpr int SENSOR_ABOVE_BIT = 3 + 24;
	static constexpr int SENSOR_BELOW_BIT = 2 + 24;
	static constexpr int ADC_RANGE_ERROR_BIT = 1 + 24;
	static constexpr int VALID_BIT = 0 + 24;
	
private:
	// Primitive
	bool _configChannel(__u8 channel, __u32 configuration);
	bool _convertChannel(__u8 channel, bool temperature);
	bool _addChannel(__u8 channel, bool temperature);
	bool _removeChannel(__u8 channel);
	bool _convertMultipleChannels();
	bool _convertDone(bool &result);
	void _convertReset();
	std::string _strFaultData(__u8 fault);
	bool _read(uint16_t address, __u8 &value);
	bool _read(uint16_t address, __u32 &value);
	bool _write(uint16_t address, __u8 value);
	bool _write(uint16_t address, __u32 value);
	bool _getRawResult(__u8 channel, __u32 &value);
	bool _getSignedFromRaw(__u32 raw, int32_t &value);
	bool _getTemperature(__u8 channel, double &value);
	bool _getVoltage(__u8 channel, double &value, bool &sensor_hard_fault, bool &range_hard_fault, bool &soft_above, bool &soft_below, bool &soft_range);
	
	// Configurazione ingressi
	bool _setChannelUnassigned(__u8 channel);
	//bool _setChannelThermocouple(__u8 channel, ColdJunctionChannelAssignment cjca, EndingMode ending_node, OCCheck oc_check, OCCurrent oc_current, CustomAddress custom_address, CustomLength custom_length);
	bool _setChannelRTD(__u8 channel, RTDType type, RSenseChannelAssignment rsca, Wires wires, ExcitationMode excitation_mode, ExcitationCurrent excitation_current, Curve curve);
	//bool _setChannelThermistor(__u8 channel);
	//bool _setChannelDiode(__u8 channel);
	bool _setChannelSenseResistor(RSenseChannelAssignment rsca, double sense_resistor_value);
	bool _setChannelDirectADC(__u8 channel, EndingMode ending_mode);
	
	// Timeout attesa conversione
	void _tryPublish();
	void _checkConversionDone();
	
	// Metodi per il salvataggio
	bool _getNextFreeIndex(QDir &folder, QString &filename, qint64 &size_limit, int &index, bool &file_exists);
	
public:
	// Costruttore
	Thermometer(
		const std::string device,
		const RSenseChannelAssignment sense_resistor_channel,
		const double sense_resistor_value,
		const QList<ChannelConfiguration> registered_channels,
		QDir storage_dir,
		qint64 filesize_limit,
		std::string temperature_filename,
		std::string voltage_filename,
		const bool battery_voltage_enabled,
		const __u8 battery_voltage_channel,
		const double battery_voltage_ideal_ratio,
		const double battery_voltage_correction_factor,
		const double frequency = 0.0,
		QObject *parent = 0
	);
	
	// Distruttore
	~Thermometer();
	
	// Inizializzazione
	bool initDevice() override;
	
	// Inizio lettura
	bool startReading() override;
	
	// Fine lettura
	bool stopReading() override;
	
private slots:
	// Segnali di pubblicazione
	void publishMultiple();
	
signals:
	// Invio la richiesta di pubblicazione al processo
	void requestPublishMessage(const Message &msg);
};

enum class Converting : unsigned char {
	FREE = 0,
	SINGLE = 1,
	MULTIPLE = 2
};

//enum class ThermocoupleType : unsigned char {
//	J = 1,
//	K = 2,
//	E = 3,
//	N = 4,
//	R = 5,
//	S = 6,
//	T = 7,
//	B = 8,
//	Custom = 9
//};

enum class RTDType : unsigned char {
	PT10 = 10,
	PT50 = 11,
	PT100 = 12,
	PT200 = 13,
	PT500 = 14,
	PT1000 = 15,
	PT1000_3750 = 16,
	Custom = 17
};

//enum class ThermistorType : unsigned char;

//enum class ColdJunctionChannelAssignment : unsigned char;

enum class EndingMode : unsigned char {
	DIFFERENTIAL = 0,
	SINGLE = 1
};

//enum class OCCheck : bool;

enum class RSenseChannelAssignment : unsigned char {
	CH_1_2 = 2,
	CH_2_3 = 3,
	CH_3_4 = 4,
	CH_4_5 = 5,
	CH_5_6 = 6,
	CH_6_7 = 7,
	CH_7_8 = 8,
	CH_8_9 = 9,
	CH_9_10 = 10,
	CH_10_11 = 11,
	CH_11_12 = 12,
	CH_12_13 = 13,
	CH_13_14 = 14,
	CH_14_15 = 15,
	CH_15_16 = 16,
	CH_16_17 = 17,
	CH_17_18 = 18,
	CH_18_19 = 19,
	CH_19_20 = 20
};

enum class Wires : unsigned char {
	TWO = 0,
	THREE = 1,
	FOUR = 2,
	FOUR_KELVIN = 3
};

enum class ExcitationMode : unsigned char {
	RSENSE_NOT_SHARED = 0,
	RSENSE_SHARED = 1
};

enum class ExcitationCurrent : unsigned char {
	I_5_uA = 1,
	I_10_uA = 2,
	I_25_uA = 3,
	I_50_uA = 4,
	I_100_uA = 5,
	I_250_uA = 6,
	I_500_uA = 7,
	I_1_mA = 8
};

enum class Curve : unsigned char {
	EUROPEAN = 0,
	AMERICAN = 1,
	JAPANESE = 2,
	ITS_90 = 3,
	RTD1000_3750 = 4
};

//enum class OCCurrent : unsigned char;

//enum class CustomAddress : unsigned char;

//enum class CustomLength : unsigned char;

}
}

#endif // SAP_THERMOMETER_H
