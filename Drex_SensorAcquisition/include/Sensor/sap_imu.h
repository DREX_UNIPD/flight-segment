//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		29/04/17
//	Descrizione:		Classe che rappresenta l'IMU (accelerometro e giroscopio) LSM6DS33
//****************************************************************************************************//

#ifndef SAP_IMU_H
#define SAP_IMU_H

// Header STL
#include <string>
#include <vector>

// Header DREX
#include <Utilities/linear_bounds.h>
#include <Utilities/linear_conversion.h>
#include <Utilities/vector3d.h>
#include <Sensor/sap_generic_sensor.h>
#include <Device/sap_i2c_device.h>

// Header nzmqt
#include <nzmqt/nzmqt.hpp>

// Header Qt
#include <QtCore>
#include <QMap>

namespace drex {
namespace sensor {

// Enumerazioni per l'estensione di scala
enum class GyroscopeScale : unsigned char;
enum class GyroscopeSpeed : unsigned char;
enum class AccelerometerScale : unsigned char;
enum class AccelerometerSpeed : unsigned char;

class Imu : public QObject, public GenericSensor, public device::I2CDevice {
	Q_OBJECT
	
	using Message = QList<QByteArray>;
	
private:
	using Logger = spdlog::logger;
	
	// Variabili private
	
	// Logger
	std::shared_ptr<Logger> _logger;
	
	// Socket per pubblicare i dati
	nzmqt::ZMQSocket *_publisher;
	
	// Abilitazioni
	bool _acceleration_enabled;
	bool _freefall_enabled;
	bool _angular_rate_enabled;
	bool _temperature_enabled;
	
	// Parametri di salvataggio
	QDir _storage_dir;
	qint64 _filesize_limit;
	
	QString _temperature_filename;
	QFile _temperature_file;
	int _temperature_file_index;
	
	QString _acceleration_filename;
	QFile _acceleration_file;
	int _acceleration_file_index;
	
	QString _freefall_filename;
	QFile _freefall_file;
	int _freefall_file_index;
	
	QString _angular_rate_filename;
	QFile _angular_rate_file;
	int _angular_rate_file_index;
	
	// Parametri di pubblicazione
	bool _publishing;
	double _thermometer_frequency;
	double _gyroscope_frequency;
	double _accelerometer_frequency;
	double _freefall_acceleration_limit;
	int _freefall_counter_limit;
	int _freefall_counter;
	
	// Timer per la pubblicazione
	QTimer _thermometer_timer;
	QTimer _gyroscope_timer;
	QTimer _accelerometer_timer;
	
	// Autoscale
	bool _gyroscope_autoscale;
	bool _accelerometer_autoscale;
	
public:
	// Registri
	
	// Embedded functions configuration register
	static constexpr unsigned char FUNC_CFG_ACCESS = 0x01;
	static constexpr unsigned char FUNC_CFG_ACCESS__DEFAULT = 0x00;
	static constexpr int FUNC_CFG_EN = 7;
	
	// FIFO configuration registers
	static constexpr unsigned char FIFO_CTRL1 = 0x06;
	static constexpr unsigned char FIFO_CTRL1__DEFAULT = 0x00;
	static constexpr int FTH_0 = 0;
	static constexpr int FTH_1 = 1;
	static constexpr int FTH_2 = 2;
	static constexpr int FTH_3 = 3;
	static constexpr int FTH_4 = 4;
	static constexpr int FTH_5 = 5;
	static constexpr int FTH_6 = 6;
	static constexpr int FTH_7 = 7;
	static constexpr unsigned char FIFO_CTRL2 = 0x07;
	static constexpr unsigned char FIFO_CTRL2__DEFAULT = 0x00;
	static constexpr int FTH_8 = 0;
	static constexpr int FTH_9 = 1;
	static constexpr int FTH_10 = 2;
	static constexpr int FTH_11 = 3;
	static constexpr int TIMER_PEDO_FIFO_DRDY = 6;
	static constexpr int TIMER_PEDO_FIFO_EN = 7;
	static constexpr unsigned char FIFO_CTRL3 = 0x08;
	static constexpr unsigned char FIFO_CTRL3__DEFAULT = 0x00;
	static constexpr int DEC_FIFO_XL0 = 0;
	static constexpr int DEC_FIFO_XL1 = 1;
	static constexpr int DEC_FIFO_XL2 = 2;
	static constexpr int DEC_FIFO_GYRO0 = 3;
	static constexpr int DEC_FIFO_GYRO1 = 4;
	static constexpr int DEC_FIFO_GYRO2 = 5;
	static constexpr unsigned char FIFO_CTRL4 = 0x09;
	static constexpr unsigned char FIFO_CTRL4__DEFAULT = 0x00;
	static constexpr int TIMER_PEDO_DEC_FIFO0 = 3;
	static constexpr int TIMER_PEDO_DEC_FIFO1 = 4;
	static constexpr int TIMER_PEDO_DEC_FIFO2 = 5;
	static constexpr int ONLY_HIGH_DATA = 6;
	static constexpr unsigned char FIFO_CTRL5 = 0x0a;
	static constexpr unsigned char FIFO_CTRL5__DEFAULT = 0x00;
	static constexpr int FIFO_MODE_0 = 0;
	static constexpr int FIFO_MODE_1 = 1;
	static constexpr int FIFO_MODE_2 = 2;
	static constexpr int ODR_FIFO_0 = 3;
	static constexpr int ODR_FIFO_1 = 4;
	static constexpr int ODR_FIFO_2 = 5;
	static constexpr int ODR_FIFO_3 = 6;
	static constexpr unsigned char ORIENT_CFG_G = 0x0b;
	static constexpr unsigned char ORIENT_CFG_G__DEFAULT = 0x00;
	static constexpr int ORIENT_0 = 0;
	static constexpr int ORIENT_1 = 1;
	static constexpr int ORIENT_2 = 2;
	static constexpr int SIGNZ_G = 3;
	static constexpr int SIGNY_G = 4;
	static constexpr int SIGNX_G = 5;
	
	// INT1 pin control
	static constexpr unsigned char INT1_CTRL = 0x0d;
	static constexpr unsigned char INT1_CTRL__DEFAULT = 0x00;
	static constexpr int INT1_DRDY_XL = 0;
	static constexpr int INT1_DRDY_G = 1;
	static constexpr int INT1_BOOT = 2;
	static constexpr int INT1_FTH = 3;
	static constexpr int INT1_FIFO_OVR = 4;
	static constexpr int INT1_FULL_FLAG = 5;
	static constexpr int INT1_SIGN_MOT = 6;
	static constexpr int INT1_STEP_DETECTOR = 7;
	// INT2 pin control
	static constexpr unsigned char INT2_CTRL = 0x0e;
	static constexpr unsigned char INT2_CTRL__DEFAULT = 0x00;
	static constexpr int INT2_DRDY_XL = 0;
	static constexpr int INT2_DRDY_G = 1;
	static constexpr int INT2_DRDY_TEMP = 2;
	static constexpr int INT2_FTH = 3;
	static constexpr int INT2_FIFO_OVR = 4;
	static constexpr int INT2_FULL_FLAG = 5;
	static constexpr int INT2_STEP_COUNT_OV = 6;
	static constexpr int INT2_STEP_DELTA = 7;
	// Who I am ID (0x69)
	static constexpr unsigned char WHO_AM_I = 0x0f;
	static constexpr unsigned char WHO_AM_I__DEFAULT = 0x69;
	// Accelerometer and Gyroscope control registers
	static constexpr unsigned char CTRL1_XL = 0x10;
	static constexpr int BW_XL0 = 0;
	static constexpr int BW_XL1 = 1;
	static constexpr int FS_XL0 = 2;
	static constexpr int FS_XL1 = 3;
	static constexpr int ODR_XL0 = 4;
	static constexpr int ODR_XL1 = 5;
	static constexpr int ODR_XL2 = 6;
	static constexpr int ODR_XL3 = 7;
	static constexpr unsigned char CTRL1_XL__DEFAULT = (1 << ODR_XL3);
	static constexpr unsigned char CTRL2_G = 0x11;
	static constexpr int FS_125 = 1;
	static constexpr int FS_G0 = 2;
	static constexpr int FS_G1 = 3;
	static constexpr int ODR_G0 = 4;
	static constexpr int ODR_G1 = 5;
	static constexpr int ODR_G2 = 6;
	static constexpr int ODR_G3 = 7;
	static constexpr unsigned char CTRL2_G__DEFAULT = (1 << ODR_G3) | (1 << FS_125);
	static constexpr unsigned char CTRL3_C = 0x12;
	static constexpr unsigned char CTRL3_C__DEFAULT = 0x04;
	static constexpr int SW_RESET = 0;
	static constexpr int BLE = 1;
	static constexpr int IF_INC = 2;
	static constexpr int SIM = 3;
	static constexpr int PP_OD = 4;
	static constexpr int H_LACTIVE = 5;
	static constexpr int BDU = 6;
	static constexpr int BOOT = 7;
	static constexpr unsigned char CTRL4_C = 0x13;
	static constexpr unsigned char CTRL4_C__DEFAULT = 0x00;
	static constexpr int STOP_ON_FTH = 0;
	static constexpr int I2C_DISABLE = 2;
	static constexpr int DRDY_MASK = 3;
	static constexpr int FIFO_TEMP_EN = 4;
	static constexpr int INT2_ON_INT1 = 5;
	static constexpr int SLEEP_G = 6;
	static constexpr int XL_BW_SCAL_ODR = 7;
	static constexpr unsigned char CTRL5_C = 0x14;
	static constexpr unsigned char CTRL5_C__DEFAULT = 0x00;
	static constexpr int ST0_XL = 0;
	static constexpr int ST1_XL = 1;
	static constexpr int ST0_G = 2;
	static constexpr int ST1_G = 3;
	static constexpr int ROUNDING0 = 5;
	static constexpr int ROUNDING1 = 6;
	static constexpr int ROUNDING2 = 7;
	static constexpr unsigned char CTRL6_C = 0x15;
	static constexpr unsigned char CTRL6_C__DEFAULT = 0x00;
	static constexpr int XL_MH_MODE = 4;
	static constexpr int LVL2_EN = 5;
	static constexpr int LVLEN = 6;
	static constexpr int TRIG_EN = 7;
	static constexpr unsigned char CTRL7_G = 0x16;
	static constexpr unsigned char CTRL7_G__DEFAULT = 0x00;
	static constexpr int ROUNDING_STATUS = 2;
	static constexpr int HP_G_R_ST = 3;
	static constexpr int HPCF_G0 = 4;
	static constexpr int HPCF_G1 = 5;
	static constexpr int HP_G_EN = 6;
	static constexpr int G_HM_MODE = 7;
	static constexpr unsigned char CTRL8_XL = 0x17;
	static constexpr unsigned char CTRL8_XL__DEFAULT = 0x00;
	static constexpr int LOW_PASS_ON_6D = 0;
	static constexpr int HP_SLOPE_XL_EN = 2;
	static constexpr int HPCF_XL0 = 5;
	static constexpr int HPCF_XL1 = 6;
	static constexpr int LPF2_XL_EN = 7;
	static constexpr unsigned char CTRL9_XL = 0x18;
	static constexpr unsigned char CTRL9_XL__DEFAULT = 0x38;
	static constexpr int XEN_XL = 3;
	static constexpr int YEN_XL = 4;
	static constexpr int ZEN_XL = 5;
	static constexpr unsigned char CTRL10_C = 0x19;
	static constexpr unsigned char CTRL10_C__DEFAULT = 0x38;
	static constexpr int SIGN_MOTION_EN = 0;
	static constexpr int PEDO_RST_STEP = 1;
	static constexpr int FUNC_EN = 2;
	static constexpr int XEN_G = 3;
	static constexpr int YEN_G = 4;
	static constexpr int ZEN_G = 5;
	
	// Interrupt registers
	static constexpr unsigned char WAKE_UP_SRC = 0x1b;
	static constexpr int Z_WU = 0;
	static constexpr int Y_WU = 1;
	static constexpr int X_WU = 2;
	static constexpr int WU_IA = 3;
	static constexpr int SLEEP_STATE_IA = 4;
	static constexpr int FF_IA = 5;
	static constexpr unsigned char TAP_SRC = 0x1c;
	static constexpr int Z_TAP = 0;
	static constexpr int Y_TAP = 1;
	static constexpr int X_TAP = 2;
	static constexpr int TAP_SIGN = 3;
	static constexpr int DOUBLE_TAP = 4;
	static constexpr int SINGLE_TAP = 5;
	static constexpr int TAP_IA = 6;
	static constexpr unsigned char D6D_SRC = 0x1d;
	static constexpr int XL = 0;
	static constexpr int XH = 1;
	static constexpr int YL = 2;
	static constexpr int YH = 3;
	static constexpr int ZL = 4;
	static constexpr int ZH = 5;
	static constexpr int D6D_IA = 6;
	// Status data register
	static constexpr unsigned char STATUS_REG = 0x1e;
	static constexpr int XLDA = 0;
	static constexpr int GDA = 1;
	static constexpr int TDA = 2;
	static constexpr int EV_BOOT = 3;
	
	// Temperature output data register
	static constexpr unsigned char OUT_TEMP_L = 0x20;
	static constexpr unsigned char OUT_TEMP_H = 0x21;
	// Gyroscope output register
	static constexpr unsigned char OUTX_L_G = 0x22;
	static constexpr unsigned char OUTX_H_G = 0x23;
	static constexpr unsigned char OUTY_L_G = 0x24;
	static constexpr unsigned char OUTY_H_G = 0x25;
	static constexpr unsigned char OUTZ_L_G = 0x26;
	static constexpr unsigned char OUTZ_H_G = 0x27;
	// Accelerometer output register
	static constexpr unsigned char OUTX_L_XL = 0x28;
	static constexpr unsigned char OUTX_H_XL = 0x29;
	static constexpr unsigned char OUTY_L_XL = 0x2a;
	static constexpr unsigned char OUTY_H_XL = 0x2b;
	static constexpr unsigned char OUTZ_L_XL = 0x2c;
	static constexpr unsigned char OUTZ_H_XL = 0x2d;
	
	// FIFO status registers
	static constexpr unsigned char FIFO_STATUS1 = 0x3a;
	static constexpr int DIFF_FIFO_0 = 0;
	static constexpr int DIFF_FIFO_1 = 1;
	static constexpr int DIFF_FIFO_2 = 2;
	static constexpr int DIFF_FIFO_3 = 3;
	static constexpr int DIFF_FIFO_4 = 4;
	static constexpr int DIFF_FIFO_5 = 5;
	static constexpr int DIFF_FIFO_6 = 6;
	static constexpr int DIFF_FIFO_7 = 7;
	static constexpr unsigned char FIFO_STATUS2 = 0x3b;
	static constexpr int DIFF_FIFO_8 = 0;
	static constexpr int DIFF_FIFO_9 = 1;
	static constexpr int DIFF_FIFO_10 = 2;
	static constexpr int DIFF_FIFO_11 = 3;
	static constexpr int FIFO_EMPTY = 4;
	static constexpr int FIFO_FULL = 5;
	static constexpr int FIFO_OVER_RUN = 6;
	static constexpr int FTH = 7;
	static constexpr unsigned char FIFO_STATUS3 = 0x3c;
	static constexpr unsigned char FIFO_STATUS4 = 0x3d;
	// FIFO data output registers
	static constexpr unsigned char FIFO_DATA_OUT_L = 0x3e;
	static constexpr unsigned char FIFO_DATA_OUT_H = 0x3f;
	// Timestamp output registers
	static constexpr unsigned char TIMESTAMP0_REG = 0x40;
	static constexpr unsigned char TIMESTAMP1_REG = 0x41;
	static constexpr unsigned char TIMESTAMP2_REG = 0x42;
	
	// Step counter timestamp registers
	static constexpr unsigned char STEP_TIMESTAMP_L = 0x49;
	static constexpr unsigned char STEP_TIMESTAMP_H = 0x4a;
	// Step counter output registers
	static constexpr unsigned char STEP_COUNTER_L = 0x4b;
	static constexpr unsigned char STEP_COUNTER_H = 0x4c;
	
	// Interrupt register
	static constexpr unsigned char FUNC_SRC = 0x53;
	static constexpr int STEP_OVERFLOW = 3;
	static constexpr int STEP_DETECTED = 4;
	static constexpr int TILT_IA = 5;
	static constexpr int SIGN_MOTION_IA = 6;
	static constexpr int STEP_COUNT_DELTA_IA = 7;
	
	// Interrupt registers
	static constexpr unsigned char TAP_CFG = 0x58;
	static constexpr unsigned char TAP_CFG__DEFAULT = 0x00;
	static constexpr int LIR = 0;
	static constexpr int TAP_Z_EN = 1;
	static constexpr int TAP_Y_EN = 2;
	static constexpr int TAP_X_EN = 3;
	static constexpr int SLOPE_FDS = 4;
	static constexpr int TILT_EN = 5;
	static constexpr int PEDO_EN = 6;
	static constexpr int TIMER_EN = 7;
	static constexpr unsigned char TAP_THS_6D = 0x59;
	static constexpr unsigned char TAP_THS_6D__DEFAULT = 0x00;
	static constexpr int TAP_THS0 = 0;
	static constexpr int TAP_THS1 = 1;
	static constexpr int TAP_THS2 = 2;
	static constexpr int TAP_THS3 = 3;
	static constexpr int TAP_THS4 = 4;
	static constexpr int SIXD_THS0 = 5;
	static constexpr int SIXD_THS1 = 6;
	static constexpr int D4D_EN = 7;
	static constexpr unsigned char INT_DUR2 = 0x5a;
	static constexpr unsigned char INT_DUR2__DEFAULT = 0x00;
	static constexpr int SHOCK0 = 0;
	static constexpr int SHOCK1 = 1;
	static constexpr int QUIET0 = 2;
	static constexpr int QUIET1 = 3;
	static constexpr int DUR0 = 4;
	static constexpr int DUR1 = 5;
	static constexpr int DUR2 = 6;
	static constexpr int DUR3 = 7;
	static constexpr unsigned char WAKE_UP_THS = 0x5b;
	static constexpr unsigned char WAKE_UP_THS__DEFAULT = 0x00;
	static constexpr int WK_THS0 = 0;
	static constexpr int WK_THS1 = 1;
	static constexpr int WK_THS2 = 2;
	static constexpr int WK_THS3 = 3;
	static constexpr int WK_THS4 = 4;
	static constexpr int WK_THS5 = 5;
	static constexpr int INACTIVITY = 6;
	static constexpr int SINGLE_DOUBLE_TAP = 7;
	static constexpr unsigned char WAKE_UP_DUR = 0x5c;
	static constexpr unsigned char WAKE_UP_DUR__DEFAULT = 0x00;
	static constexpr int SLEEP_DUR0 = 0;
	static constexpr int SLEEP_DUR1 = 1;
	static constexpr int SLEEP_DUR2 = 2;
	static constexpr int SLEEP_DUR3 = 3;
	static constexpr int TIMER_HR = 4;
	static constexpr int WAKE_DUR0 = 5;
	static constexpr int WAKE_DUR1 = 6;
	static constexpr int FF_DUR5 = 7;
	static constexpr unsigned char FREE_FALL = 0x5d;
	static constexpr unsigned char FREE_FALL__DEFAULT = 0x00;
	static constexpr int FF_THS0 = 0;
	static constexpr int FF_THS1 = 1;
	static constexpr int FF_THS2 = 2;
	static constexpr int FF_DUR0 = 3;
	static constexpr int FF_DUR1 = 4;
	static constexpr int FF_DUR2 = 5;
	static constexpr int FF_DUR3 = 6;
	static constexpr int FF_DUR4 = 7;
	static constexpr unsigned char MD1_CFG = 0x5e;
	static constexpr unsigned char MD1_CFG__DEFAULT = 0x00;
	static constexpr int INT1_TIMER = 0;
	static constexpr int INT1_TILT = 1;
	static constexpr int INT1_6D = 2;
	static constexpr int INT1_DOUBLE_TAP = 3;
	static constexpr int INT1_FF = 4;
	static constexpr int INT1_WU = 5;
	static constexpr int INT1_SINGLE_TAP = 6;
	static constexpr int INT1_INACT_STATE = 7;
	static constexpr unsigned char MD2_CFG = 0x5f;
	static constexpr unsigned char MD2_CFG__DEFAULT = 0x00;
	static constexpr int INT2_TILT = 1;
	static constexpr int INT2_6D = 2;
	static constexpr int INT2_DOUBLE_TAP = 3;
	static constexpr int INT2_FF = 4;
	static constexpr int INT2_WU = 5;
	static constexpr int INT2_SINGLE_TAP = 6;
	static constexpr int INT2_INACT_STATE = 7;
	
	// Embedded functions register mapping
	static constexpr unsigned char PEDO_THS_REG = 0x0f;
	static constexpr unsigned char PEDO_THS_REG__DEFAULT = 0x00;
	static constexpr int THS_MIN0 = 0;
	static constexpr int THS_MIN1 = 1;
	static constexpr int THS_MIN2 = 2;
	static constexpr int THS_MIN3 = 3;
	static constexpr int THS_MIN4 = 4;
	static constexpr int PEDO_4G = 7;
	static constexpr unsigned char SM_THS = 0x13;
	static constexpr unsigned char SM_THS__DEFAULT = 0x06;
	static constexpr unsigned char PEDO_DEB_REG = 0x14;
	static constexpr unsigned char PEDO_DEB_REG__DEFAULT = 0x00;
	static constexpr unsigned char STEP_COUNT_DELTA = 0x15;
	static constexpr unsigned char STEP_COUNT_DELTA__DEFAULT = 0x00;
	
	// Maschere di bit
	static constexpr unsigned char GYROSCOPE_SCALE_MASK = 0x0e;
	static constexpr unsigned char GYROSCOPE_SPEED_MASK = 0xf0;
	
	static constexpr unsigned char ACCELEROMETER_SCALE_MASK = 0x0c;
	static constexpr unsigned char ACCELEROMETER_SPEED_MASK = 0xf0;
	
	// Fattori di conversione della temperatura
	static constexpr double TEMPERATURE_OFFSET = 25.0;
	static constexpr double TEMPERATURE_SCALE = 1.0 / 16;
	
private:
	// Configurazione dei parametri del giroscopio
	bool _getGyroscopeScale(GyroscopeScale &scale);
	bool _setGyroscopeScale(GyroscopeScale scale);
	bool _getGyroscopeSpeed(GyroscopeSpeed &speed);
	bool _setGyroscopeSpeed(GyroscopeSpeed speed);
	bool _getGyroscopeScaleAndSpeed(GyroscopeScale &scale, GyroscopeSpeed &speed);
	bool _setGyroscopeScaleAndSpeed(GyroscopeScale scale, GyroscopeSpeed speed);
	
	// Configurazione dei parametri dell'accelerometro
	bool _getAccelerometerScale(AccelerometerScale &scale);
	bool _setAccelerometerScale(AccelerometerScale scale);
	bool _getAccelerometerSpeed(AccelerometerSpeed &speed);
	bool _setAccelerometerSpeed(AccelerometerSpeed speed);
	bool _getAccelerometerScaleAndSpeed(AccelerometerScale &scale, AccelerometerSpeed &speed);
	bool _setAccelerometerScaleAndSpeed(AccelerometerScale scale, AccelerometerSpeed speed);
	
	// Lettura della temperatura
	bool _getTemperature(double &value);
	bool _getRawTemperature(int16_t &value);
	
	// Lettura del giroscopio
	bool _getAngularRate(Vector3D<double> &value);
	bool _getRawAngularRate(Vector3D<int16_t> &value);
	
	// Lettura dell'accelerometro
	bool _getAcceleration(Vector3D<double> &value);
	bool _getRawAcceleration(Vector3D<int16_t> &value);
	
	// Metodi per il salvataggio
	bool _getNextFreeIndex(QDir &folder, QString &filename, qint64 &size_limit, int &index, bool &file_exists);
	
public:
	// Costruttore
//	Imu(Imu *other);
	Imu(
		const std::string device,
		const unsigned char address,
		bool acceleration_enabled,
		bool freefall_enabled,
		bool angular_rate_enabled,
		bool temperature_enabled,
		QDir storage_dir,
		qint64 filesize_limit,
		std::string temperature_filename,
		std::string acceleration_filename,
		std::string freefall_filename,
		std::string angularrate_filename,
		const double freefall_acceleration_limit,
		const int freefall_counter_limit,
		const double thermometer_frequency = 0.0,
		const double gyroscope_frequency = 0.0,
		const double accelerometer_frequency = 0.0,
		QObject *parent = 0
	);
	
	// Distruttore
	~Imu();
	
	// Inizializzazione
	bool initDevice() override;
	
	// Inizio lettura
	bool startReading() override;
	
	// Fine lettura
	bool stopReading() override;
	
	// Configurazione dei parametri del giroscopio
	bool getGyroscopeScale(GyroscopeScale &scale);
	bool setGyroscopeScale(GyroscopeScale scale);
	
	// Configurazione dei parametri dell'accelerometro
	bool getAccelerometerScale(AccelerometerScale &scale);
	bool setAccelerometerScale(AccelerometerScale scale);
	
	// Lettura della temperatura
	bool getTemperature(double &value);
	bool getRawTemperature(int16_t &value);
	
	// Lettura del giroscopio
	bool getAngularRate(Vector3D<double> &value);
	bool getRawAngularRate(Vector3D<int16_t> &value);
	
	// Lettura dell'accelerometro
	bool getAcceleration(Vector3D<double> &value);
	bool getRawAcceleration(Vector3D<int16_t> &value);
	
private:
	// Mappe
	GyroscopeScale _gyroscope_scale;
	GyroscopeSpeed _gyroscope_speed;
	QMap<GyroscopeScale, LinearConversion> _gyroscope_conversion;
	QMap<GyroscopeScale, LinearBounds> _gyroscope_scale_bounds;
	QMap<GyroscopeScale, GyroscopeScale> _gyroscope_scale_previous;
	QMap<GyroscopeScale, GyroscopeScale> _gyroscope_scale_next;
	
	AccelerometerScale _accelerometer_scale;
	AccelerometerSpeed _accelerometer_speed;
	QMap<AccelerometerScale, LinearConversion> _accelerometer_conversion;
	QMap<AccelerometerScale, LinearBounds> _accelerometer_scale_bounds;
	QMap<AccelerometerScale, AccelerometerScale> _accelerometer_scale_previous;
	QMap<AccelerometerScale, AccelerometerScale> _accelerometer_scale_next;
	
private slots:
	// Segnali di pubblicazione
	void publishTemperature();
	void publishAngularRate();
	void publishAcceleration();
	
signals:
	// Invio la richiesta di pubblicazione al processo
	void requestPublishMessage(const Message &msg);
};

// Enumerazioni
enum class GyroscopeScale : unsigned char {
	DPS_125 = (1 << Imu::FS_125),
	DPS_245 = 0x00,
	DPS_500 = (1 << Imu::FS_G0),
	DPS_1000 = (1 << Imu::FS_G1),
	DPS_2000 = (1 << Imu::FS_G1) || (1 << Imu::FS_G0)
};

enum class GyroscopeSpeed : unsigned char {
	POWER_DOWN = 0x00,
	SPEED_13HZ = (1 << Imu::ODR_G0),
	SPEED_26HZ = (1 << Imu::ODR_G1),
	SPEED_52HZ = (1 << Imu::ODR_G1) | (1 << Imu::ODR_G0),
	SPEED_104HZ = (1 << Imu::ODR_G2),
	SPEED_208HZ = (1 << Imu::ODR_G2) | (1 << Imu::ODR_G0),
	SPEED_416HZ = (1 << Imu::ODR_G2) | (1 << Imu::ODR_G1),
	SPEED_833HZ = (1 << Imu::ODR_G2) | (1 << Imu::ODR_G1) | (1 << Imu::ODR_G0),
	SPEED_1_66KHZ = (1 << Imu::ODR_G3)
};

enum class AccelerometerScale : unsigned char {
	G_2 = 0x00,
	G_4 = (1 << Imu::FS_XL1),
	G_8 = (1 << Imu::FS_XL1) | (1 << Imu::FS_XL0),
	G_16 = (1 << Imu::FS_XL0)
};

enum class AccelerometerSpeed : unsigned char {
	POWER_DOWN = 0x00,
	SPEED_13HZ = (1 << Imu::ODR_XL0),
	SPEED_26HZ = (1 << Imu::ODR_XL1),
	SPEED_52HZ = (1 << Imu::ODR_XL1) | (1 << Imu::ODR_XL0),
	SPEED_104HZ = (1 << Imu::ODR_XL2),
	SPEED_208HZ = (1 << Imu::ODR_XL2) | (1 << Imu::ODR_XL0),
	SPEED_416HZ = (1 << Imu::ODR_XL2) | (1 << Imu::ODR_XL1),
	SPEED_833HZ = (1 << Imu::ODR_XL2) | (1 << Imu::ODR_XL1) | (1 << Imu::ODR_XL0),
	SPEED_1_66KHZ = (1 << Imu::ODR_XL3),
	SPEED_3_33KHZ = (1 << Imu::ODR_XL3) | (1 << Imu::ODR_XL0),
	SPEED_6_66KHZ = (1 << Imu::ODR_XL3) | (1 << Imu::ODR_XL1)
};


}
}

#endif // SAP_IMU_H
