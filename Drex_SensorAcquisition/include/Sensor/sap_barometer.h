//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		26/04/17
//	Descrizione:		Classe che rappresenta il barometro LPS25H
//****************************************************************************************************//

#ifndef SAP_BAROMETER_H
#define SAP_BAROMETER_H

// Header STL
#include <string>
#include <vector>

// Header DREX
#include <Sensor/sap_generic_sensor.h>
#include <Device/sap_i2c_device.h>

// Header nzmqt
#include <nzmqt/nzmqt.hpp>

// Header Qt
#include <QtCore>

namespace drex {
namespace sensor {

class Barometer : public QObject, public GenericSensor, public device::I2CDevice {
	Q_OBJECT
	
	using Message = QList<QByteArray>;
	
private:
	using Logger = spdlog::logger;
	
	// Variabili private
	
	// Logger
	std::shared_ptr<Logger> _logger;
	
	// Abilitazioni
	bool _pressure_enabled;
	bool _altitude_enabled;
	bool _temperature_enabled;
	
	// Parametri di salvataggio
	QDir _storage_dir;
	qint64 _filesize_limit;
	
	QString _pressure_filename;
	QFile _pressure_file;
	int _pressure_file_index;
	
	QString _altitude_filename;
	QFile _altitude_file;
	int _altitude_file_index;
	
	QString _temperature_filename;
	QFile _temperature_file;
	int _temperature_file_index;
	
	// Parametri di pubblicazione
	double _thermometer_frequency;
	double _barometer_frequency;
	bool _publishing;
	
	// Timer per la pubblicazione
	QTimer _thermometer_timer;
	QTimer _barometer_timer;
	
public:
	// Registri
	
	// Reference pressure (LSB data)
	static constexpr unsigned char REF_P_XL = 0x08;
	static constexpr unsigned char REF_P_XL__DEFAULT = 0x00;
	// Reference pressure (middle part)
	static constexpr unsigned char REF_P_L = 0x09;
	static constexpr unsigned char REF_P_L__DEFAULT = 0x00;
	// Reference pressure (MSB data)
	static constexpr unsigned char REF_P_H = 0x0a;
	static constexpr unsigned char REF_P_H__DEFAULT = 0x00;
	
	// Device identification (always 0xBD)
	static constexpr unsigned char WHO_AM_I = 0x0f;
	static constexpr unsigned char WHO_AM_I__DEFAULT = 0xbd;
	
	// Pressure and temperature resolution mode
	static constexpr unsigned char RES_CONF = 0x10;
	static constexpr unsigned char RES_CONF__DEFAULT = 0x05;
	static constexpr int RES_CONF_AVGP0 = 0;
	static constexpr int RES_CONF_AVGP1 = 1;
	static constexpr int RES_CONF_AVGT0 = 2;
	static constexpr int RES_CONF_AVGT1 = 3;
	
	// Control register 1
	static constexpr unsigned char CTRL_REG1 = 0x20;
	static constexpr int CTRL_REG1_SIM = 0;
	static constexpr int CTRL_REG1_RESET_AZ = 1;
	static constexpr int CTRL_REG1_BDU = 2;
	static constexpr int CTRL_REG1_DIFF_EN = 3;
	static constexpr int CTRL_REG1_ODR0 = 4;
	static constexpr int CTRL_REG1_ODR1 = 5;
	static constexpr int CTRL_REG1_ODR2 = 6;
	static constexpr int CTRL_REG1_PD = 7;
	static constexpr unsigned char CTRL_REG1__DEFAULT = 0x00 | (1 << CTRL_REG1_PD);
	// Control register 2
	static constexpr unsigned char CTRL_REG2 = 0x21;
	static constexpr unsigned char CTRL_REG2__DEFAULT = 0x00;
	static constexpr int CTRL_REG2_ONE_SHOT = 0;
	static constexpr int CTRL_REG2_AUTO_ZERO = 1;
	static constexpr int CTRL_REG2_SWRESET = 2;
	static constexpr int CTRL_REG2_FIFO_MEAN_DEC = 4;
	static constexpr int CTRL_REG2_WTM_EN = 5;
	static constexpr int CTRL_REG2_FIFO_EN = 6;
	static constexpr int CTRL_REG2_BOOT = 7;
	// Interrupt control
	static constexpr unsigned char CTRL_REG3 = 0x22;
	static constexpr unsigned char CTRL_REG3__DEFAULT = 0x00;
	static constexpr int CTRL_REG3_INT1_S1 = 0;
	static constexpr int CTRL_REG3_INT1_S2 = 1;
	static constexpr int CTRL_REG3_PP_OD = 6;
	static constexpr int CTRL_REG3_INT_H_L = 7;
	// Interrupt configuration
	static constexpr unsigned char CTRL_REG4 = 0x23;
	static constexpr unsigned char CTRL_REG4__DEFAULT = 0x00;
	static constexpr int CTRL_REG4_P1_DRDY = 0;
	static constexpr int CTRL_REG4_P1_OVERRUN = 1;
	static constexpr int CTRL_REG4_P1_WTM = 2;
	static constexpr int CTRL_REG4_P1_EMPTY = 3;
	
	// Interrupt configuration
	static constexpr unsigned char INTERRUPT_CFG = 0x24;
	static constexpr unsigned char INTERRUPT_CFG__DEFAULT = 0x00;
	static constexpr int INTERRUPT_CFG_PH_E = 0;
	static constexpr int INTERRUPT_CFG_PL_E = 1;
	static constexpr int INTERRUPT_CFG_LIR = 2;
	// Interrupt source
	static constexpr unsigned char INT_SOURCE = 0x25;
	static constexpr unsigned char INT_SOURCE__DEFAULT = 0x00;
	static constexpr int INT_SOURCE_PH = 0;
	static constexpr int INT_SOURCE_PL = 1;
	static constexpr int INT_SOURCE_IA = 2;
	
	// Status register
	static constexpr unsigned char STATUS_REG = 0x27;
	static constexpr unsigned char STATUS_REG__DEFAULT = 0x00;
	static constexpr int STATUS_REG_T_DA = 0;
	static constexpr int STATUS_REG_P_DA = 1;
	static constexpr int STATUS_REG_T_OR = 2;
	static constexpr int STATUS_REG_P_OR = 3;
	// Pressure data (LSB)
	static constexpr unsigned char PRESS_OUT_XL = 0x28;
	// Pressure data (middle part)
	static constexpr unsigned char PRESS_OUT_L = 0x29;
	// Pressure data (MSB)
	static constexpr unsigned char PRESS_OUT_H = 0x2a;
	// Temperature data (LSB)
	static constexpr unsigned char TEMP_OUT_L = 0x2b;
	// Temperature data (MSB)
	static constexpr unsigned char TEMP_OUT_H = 0x2c;
	
	// FIFO control
	static constexpr unsigned char FIFO_CTRL = 0x2E;
	static constexpr unsigned char FIFO_CTRL__DEFAULT = 0x00;
	static constexpr int FIFO_CTRL_WTM_POINT0 = 0;
	static constexpr int FIFO_CTRL_WTM_POINT1 = 1;
	static constexpr int FIFO_CTRL_WTM_POINT2 = 2;
	static constexpr int FIFO_CTRL_WTM_POINT3 = 3;
	static constexpr int FIFO_CTRL_WTM_POINT4 = 4;
	static constexpr int FIFO_CTRL_F_MODE0 = 5;
	static constexpr int FIFO_CTRL_F_MODE1 = 6;
	static constexpr int FIFO_CTRL_F_MODE2 = 7;
	// FIFO status
	static constexpr unsigned char FIFO_STATUS = 0x2f;
	static constexpr unsigned char FIFO_STATUS__DEFAULT = 0x00;
	static constexpr int FIFO_STATUS_DIFF_POINT0 = 0;
	static constexpr int FIFO_STATUS_DIFF_POINT1 = 1;
	static constexpr int FIFO_STATUS_DIFF_POINT2 = 2;
	static constexpr int FIFO_STATUS_DIFF_POINT3 = 3;
	static constexpr int FIFO_STATUS_DIFF_POINT4 = 4;
	static constexpr int FIFO_STATUS_EMPTY_FIFO = 5;
	static constexpr int FIFO_STATUS_FULL_FIFO = 6;
	static constexpr int FIFO_STATUS_WTM_FIFO = 7;
	// Threshold pressure (LSB)
	static constexpr unsigned char THS_P_L = 0x30;
	static constexpr unsigned char THS_P_L__DEFAULT = 0x00;
	// Threshold pressure (MSB)
	static constexpr unsigned char THS_P_H = 0x31;
	static constexpr unsigned char THS_P_H__DEFAULT = 0x00;
	
	// Pressure offset (LSB)
	static constexpr unsigned char RPDS_L = 0x39;
	static constexpr unsigned char RPDS_L__DEFAULT = 0x38;
	// Pressure offset (MSB)
	static constexpr unsigned char RPDS_H = 0x3a;
	static constexpr unsigned char RPDS_H__DEFAULT = 0x00;
	
	// Costanti
	static constexpr double FT_TO_M_RATIO = 0.3048;
	static constexpr double SEA_LEVEL_PRESSURE = 1013.25;	// in mBar
	static constexpr double PRESSURE_EXP = 0.190284;
	static constexpr double PRESSURE_FACTOR = 145366.45;
	
private:
	// Timestamp per le misure
	QDateTime _measure_timestamp;
	
	// Metodi privati
	bool _doMeasure();
	bool _isTemperatureReady(bool &value);
	bool _isPressureReady(bool &value);
	bool _waitForTemperatureReady();
	bool _waitForPressureReady();
	bool _getRawTemperature(int16_t &value);
	bool _getRawPressure(int32_t &value);
	bool _getTemperature(double &value);
	bool _getPressure(double &value);
	
	// Metodi per il salvataggio
	bool _getNextFreeIndex(QDir &folder, QString &filename, qint64 &size_limit, int &index, bool &file_exists);
	
public:
	// Costanti pubbliche
	static constexpr double TEMPERATURE_OFFSET = 42.5;
	static constexpr double TEMPERATURE_SCALE = 1.0 / 480;
	
	static constexpr double PRESSURE_OFFSET = 0.0;
	static constexpr double PRESSURE_SCALE = 1.0 / 4096;
	
	// Costruttori
//	Barometer(Barometer *other);
	Barometer(const std::string device,
		const unsigned char address,
		bool pressure_enabled,
		bool altitude_enabled,
		bool temperature_enabled,
		QDir storage_dir,
		qint64 filesize_limit,
		std::string pressure_filename,
		std::string altitude_filename,
		std::string temperature_filename,
		const double thermometer_frequency = 0.0,
		const double barometer_frequency = 0.0,
		QObject *parent = 0
	);
	
	// Distruttore
	~Barometer();
	
	// Inizializzazione
	bool initDevice() override;
	
	// Inizio lettura
	bool startReading() override;
	
	// Fine lettura
	bool stopReading() override;
	
	// Lettura della temperatura
	bool getTemperature(double &value);
	bool getRawTemperature(int16_t &value);
	
	// Lettura della pressione
	bool getPressure(double &value);
	bool getRawPressure(int32_t &value);
	bool getAltitudeFromPressure(double &pressure, double &altitude);
	
public slots:
	// Segnali di pubblicazione
	void publishTemperature();
	void publishPressureAndAltitude();
	
signals:
	// Invio la richiesta di pubblicazione al processo
	void requestPublishMessage(const Message &msg);
};

}
}

#endif // SAP_BAROMETER_H
