//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		22/07/17
//	Descrizione:		Classe che rappresenta il vacuometro 86BSD
//****************************************************************************************************//

#ifndef SAP_VACUOMETER_H
#define SAP_VACUOMETER_H

// Header DREX
#include <Sensor/sap_generic_sensor.h>
#include <Device/sap_i2c_device.h>

// Header Qt
#include <QtCore>

namespace drex {
namespace sensor {

#define ARRAY_SIZE(a) (sizeof(a) / sizeof((a)[0]))

class Vacuometer : public QObject, public GenericSensor, public device::I2CDevice {
	Q_OBJECT
	
	using Message = QList<QByteArray>;
	
private:
	using Logger = spdlog::logger;
	
	// Variabili private
	
	// Logger
	std::shared_ptr<Logger> _logger;
	
	// Abilitazioni
	bool _pressure_enabled;
	bool _altitude_enabled;
	bool _temperature_enabled;
	
	// Parametri di salvataggio
	QDir _storage_dir;
	qint64 _filesize_limit;
	
	QString _pressure_filename;
	QFile _pressure_file;
	int _pressure_file_index;
	
	QString _altitude_filename;
	QFile _altitude_file;
	int _altitude_file_index;
	
	QString _temperature_filename;
	QFile _temperature_file;
	int _temperature_file_index;
	
	// Parametri di pubblicazione
	bool _publishing;
	double _frequency;
	
	// Parametri di conversione
	double _psi_min;
	double _psi_max;
	uint16_t _psi_raw_min;
	uint16_t _psi_raw_max;
	
	double _temperature_min;
	double _temperature_max;
	uint16_t _temperature_raw_min;
	uint16_t _temperature_raw_max;
	
	double _pressure_offset;
	
	// Timer per la pubblicazione
	QTimer _timer;
	
	// Costanti
	static constexpr double PSI_TO_MBAR_RATIO = 68.9476;
	static constexpr double FT_TO_M_RATIO = 0.3048;
	static constexpr double SEA_LEVEL_PRESSURE = 1013.25;	// in mBar
	static constexpr double PRESSURE_EXP = 0.190284;
	static constexpr double PRESSURE_FACTOR = 145366.45;
	
	// Metodi per il salvataggio
	bool _getNextFreeIndex(QDir &folder, QString &filename, qint64 &size_limit, int &index, bool &file_exists);
	
public:
	// Costruttore
	Vacuometer(
		const std::string device,
		const unsigned char address,
		bool pressure_enabled,
		bool altitude_enabled,
		bool temperature_enabled,
		QDir storage_dir,
		qint64 filesize_limit,
		std::string pressure_filename,
		std::string altitude_filename,
		std::string temperature_filename,
		double psi_min,
		double psi_max,
		uint16_t psi_raw_min,
		uint16_t psi_raw_max,
		double temperature_min,
		double temperature_max,
		uint16_t temperature_raw_min,
		uint16_t temperature_raw_max,
		double pressure_offset,
		const double frequency = 0.0,
		QObject *parent = 0
	);
	
	// Distruttore
	~Vacuometer();
	
	// Inizializzazione
	bool initDevice() override;
	
	// Inizio lettura
	bool startReading() override;
	
	// Fine lettura
	bool stopReading() override;
	
	// Lettura della pressione
	bool getAltitudeFromPressure(double &pressure, double &altitude, double offset);
	bool getPressureAndTemperature(double &pressure, double &temperature);
	bool getRawPressureAndTemperature(uint16_t &pressure, uint16_t &temperature);
	
private slots:
	// Segnali di pubblicazione
	void publishPressureAndTemperature();
	
signals:
	// Invio la richiesta di pubblicazione al processo
	void requestPublishMessage(const Message &msg);
};

}
}

#endif // SAP_VACUOMETER_H
