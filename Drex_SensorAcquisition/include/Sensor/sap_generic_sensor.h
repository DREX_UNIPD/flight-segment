//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		26/04/17
//	Descrizione:		Classe che rappresenta un generico sensore
//****************************************************************************************************//

#ifndef SAP_GENERIC_SENSOR_H
#define SAP_GENERIC_SENSOR_H

// Header per logging
#include <spdlog/spdlog.h>

// Header Qt
#include <QCoreApplication>

namespace drex {
namespace sensor {

class GenericSensor {
public:
	// Inizializzazione del dispositivo
	virtual bool initDevice() = 0;
	
	// Inizio lettura
	virtual bool startReading() = 0;
	
	// Fine lettura
	virtual bool stopReading() = 0;
};

}
}

#endif // SAP_GENERIC_SENSOR_H
