/*
 * Copyright 2016 The Imaging Source Europe GmbH
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/* This example will show you how to receive data from gstreamer in your application
   and how to get the actual iamge data */

#include <stdio.h>
#include <string.h>
#include <gst/gst.h>
#include <unistd.h>
#include <tcamprop.h>

// Cattura Ctrl+C o SIGTERM
#include <signal.h>
#include <stdlib.h>

// Verifica esistenza file
#include <sys/stat.h>

#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui.hpp>

#include <iostream>

#include <QtCore>

using namespace cv;
using namespace std;

GMainLoop *mainloop = NULL;

struct CameraUserData {
	QString camera_name;
	QList<QDir> trigger_folders;
	QList<QDir> image_folders;
	QList<int> trigger_index;
	QList<int> image_numbers;
	QList<bool> continuous;
	QList<bool> take_image;
	QList<int> counter;
	int divider;
	QString extension;
	int width;
	int height;
};

void term_handler(int){
	cout << "Caught signal" << endl;
	g_main_loop_quit(mainloop);
}

inline bool exists(const char *name) {
	struct stat buffer;
	return (stat (name, &buffer) == 0); 
}

/*
  This function will be called in a separate thread when our appsink
  says there is data for us. user_data has to be defined when calling g_signal_connect.
 */
GstFlowReturn callback(GstElement *sink, void *user_data) {
	GstSample *sample = NULL;
	CameraUserData *ptr_user_data = (CameraUserData *)user_data;
	
	static guint framecount = -1;
	framecount++;
	cout << "Frame(" << framecount << ")" << endl;
	
	// Flag che indica se devo catturare l'immagine corrente
	bool capture = false;
	int divider = ptr_user_data->divider;
	
	// Formato del comando
	QRegularExpression regex("(?i)^\\d+_(?<cmd>[a-z0-9]+)?$");
	
	// Verifico se è presente un nuovo comando
	for (int i = 0; i < ptr_user_data->trigger_folders.count(); i++) {
		QDir &dir = ptr_user_data->trigger_folders[i];
		int index = ptr_user_data->trigger_index[i];
		bool &continuous = ptr_user_data->continuous[i];
		bool &take_image = ptr_user_data->take_image[i];
		
		// Non aggiorno il comando fino all'esecuzione di quello corrente
		if (take_image) {
			capture = true;
			continue;
		}
		
		// Se sono in continuous mode acquisisco le immagini ogni divider
		int &counter = ptr_user_data->counter[i];
		counter++;
		
		// Verifico se è il frame da acquisire per l'acquisizione continua
		if (counter == divider) {
			// Resetto il contatore per la riduzione dei frame
			counter = 0;
			
			// Se continuous è true allora capture = true
			capture |= continuous;
		}
		
		// Trovo i file che corrispondono al pattern prestabilito
		QStringList filters;
		filters.clear();
		filters.append(QString::number(index) + "_*");
		QStringList triggers = dir.entryList(filters);
		
		// Aggiorno il contatore
		if (triggers.size() > 0) {
			// Acquisisco il prossimo trigger
			QString &trigger = triggers.first();
			
			cout << "Trigger found: " << trigger.toStdString() << endl;
			
			// Passo al prossimo trigger
			ptr_user_data->trigger_index[i]++;
			
			// Ho trovato un comando, lo interpreto
			QRegularExpressionMatch match = regex.match(trigger);
			if (match.hasMatch()) {
				// Case insensitive
				QString command = match.captured("cmd").toUpper();
				
				// Capture single image
				if (command == "IMAGE") {
					capture = true;
					take_image = true;
					continue;
				}
				
				// Set continuous capturing
				if (command == "START") {
					continuous = true;
					continue;
				}
				
				// Reset continuous capturing
				if (command == "STOP") {
					continuous = false;
					continue;
				}
				
				// Uscita
				if (command == "EXIT") {
					g_main_loop_quit(mainloop);
					return GST_FLOW_OK;
				}
			}
		}
	}
	
	if (!capture) return GST_FLOW_OK;
	
	cout << "Capturing enabled" << endl;
	
	/* Retrieve the buffer */
	g_signal_emit_by_name (sink, "pull-sample", &sample, NULL);
	
	// Se l'immagine è stata acquisita correttamente
	if (sample) {
		cout << "Sample ok" << endl;
		
		GstBuffer* buffer = gst_sample_get_buffer(sample);
		GstMapInfo info; // contains the actual image
		
		if (gst_buffer_map(buffer, &info, GST_MAP_READ)) {
			
			cout << "Buffer mapped" << endl;
			
			/* do things here */
			
			// Converto l'immagine in cv::Mat
			Mat image(ptr_user_data->height, ptr_user_data->width, CV_8U, info.data);
			
			// Per ogni cartella di trigger verifico se acquisire l'immagine
			for (int i = 0; i < ptr_user_data->trigger_folders.count(); i++) {
				int &img_index = ptr_user_data->image_numbers[i];
				QDir &img_dir = ptr_user_data->image_folders[i];
				bool &take_image = ptr_user_data->take_image[i];
				bool &continuous = ptr_user_data->continuous[i];
				
				if (!take_image && !continuous) continue;
				
				QString filename, filepath;
				filename = QString::number(img_index) + QString("_") + ptr_user_data->camera_name + QString(".") + ptr_user_data->extension;
				filepath = img_dir.absoluteFilePath(filename);
				
				cout << "Filename: " << filename.toStdString() << endl;
				
				QString tempfilepath;
				tempfilepath = img_dir.absoluteFilePath(QString("temp_") + ptr_user_data->camera_name + QString(".") + ptr_user_data->extension);
				imwrite(tempfilepath.toStdString(), image);
				QFile::rename(tempfilepath, filepath);
				
				// Elimino il segnale di cattura dell'immagine
				take_image = false;
				img_index++;
			}
			
			// delete our reference so that gstreamer can handle the sample
			gst_buffer_unmap(buffer, &info);
		}
		
		gst_sample_unref (sample);
	}
	
	return GST_FLOW_OK;
}

// Trovo il prossimo slot libero
bool getNextFreeSlot(QDir &dir, int &index) {
	index = -1;
	
	QStringList filters;
	QStringList files;
	
	do {
		index++;
		filters.clear();
		filters += QString::number(index) + QString("_*");
		files = dir.entryList(filters);
	}
	while (files.count() > 0);
	
	return true;
}

int main(int argc, char *argv[]) {
	// Init gstreamer
	gst_init(&argc, &argv);
	
	// Verifico che ci siano almeno due argomenti
	if (argc < 7) {
		cout << "Usage: Drex_Stereovision_Capture <camera_name> <camera_serial_number> <extension> <camera_mode> <divider> [<trigger_folder>, <trigger_initial_number>, <image_folder>, <image_initial_number>]" << endl;
		return 0;
	}
	
	// Configuro i dati utente
	CameraUserData user_data;
	user_data.camera_name = argv[1];
	char *serial = argv[2]; // The serial number of the camera we want to use
	user_data.extension = argv[3];
	int camera_mode = atoi(argv[4]);
	user_data.divider = atoi(argv[5]);
	for (int i = 6; i + 4 <= argc; i += 4) {
		// Per ogni directory di trigger configurata aggiungo un elemento alle tre liste
		QDir trigger_dir;
		QString str_trigger_index;
		int trigger_index;
		QDir images_dir;
		QString str_images_index;
		int images_index;
		
		trigger_dir.setPath(argv[i]);
		str_trigger_index = argv[i + 1];
		if (str_trigger_index.toLower() == "auto") {
			getNextFreeSlot(trigger_dir, trigger_index);
		}
		else {
			trigger_index = str_trigger_index.toInt();
		}
		
		images_dir.setPath(argv[i + 2]);
		str_images_index = argv[i + 3];
		if (str_images_index.toLower() == "auto") {
			getNextFreeSlot(images_dir, images_index);
		}
		else {
			images_index = str_images_index.toInt();
		}
		
		if (!trigger_dir.exists()) QDir::root().mkpath(trigger_dir.absolutePath());
		if (!images_dir.exists()) QDir::root().mkpath(images_dir.absolutePath());
		
		user_data.trigger_folders.append(trigger_dir);
		user_data.trigger_index.append(trigger_index);
		user_data.image_folders.append(images_dir);
		user_data.image_numbers.append(images_index);
		user_data.continuous.append(false);
		user_data.take_image.append(false);
		user_data.counter.append(0);
	}
	
	if (user_data.trigger_folders.count() == 0) {
		cout << "No folders configured" << endl;
		return -1;
	}
	
	// Creo il main loop
	mainloop = g_main_loop_new(NULL, FALSE);
	
	// Creo l'elemento sorgente della camera
	GstElement* source = gst_element_factory_make("tcambin", "source");
	
	// Imposto il valore del parametro dell'oggetto seriale
	GValue val = {};
	g_value_init(&val, G_TYPE_STRING);
	g_value_set_static_string(&val, serial);
	g_object_set_property(G_OBJECT(source), "serial", &val);
	
	// Get the caps from the source and use the first one in the list
	// for the capsfilter.
	// This sets the first video format supported by the device as
	// the target format for the appsink
	gst_element_set_state(source, GST_STATE_READY);
	GstPad* pad = gst_element_get_static_pad(source, "src");
	GstCaps* caps = gst_pad_query_caps(pad, NULL);
	if (caps == NULL) cout << "Failed to query caps from device!" << endl;
	
	// Seleziono la modalità della camera (0 = 640x480, 1 = 1920x1080, 2 = 10M)
	GstStructure *structure = gst_caps_get_structure(caps, camera_mode);
	gint width, height;
	gst_structure_get_int(structure, "width", &width);
	gst_structure_get_int(structure, "height", &height);
	GstCaps* formatcaps = gst_caps_new_simple("video/x-raw",
						"format", G_TYPE_STRING, gst_structure_get_string(structure, "format"),
						"width", G_TYPE_INT, width,
						"height", G_TYPE_INT, height,
						NULL);
	cout << "Using video format '" << gst_caps_to_string(formatcaps) << "'' for appsink." << endl;
	
	gst_element_set_state(source, GST_STATE_NULL);
	
	GstElement* pipeline = gst_pipeline_new("pipeline");
	GstElement* capsfilter = gst_element_factory_make("capsfilter", "caps");
	GstElement* sink = gst_element_factory_make("appsink", "sink");
	
	g_object_set(G_OBJECT(capsfilter), "caps", formatcaps, NULL);
	
	// Tell appsink to notify us when it receives an image
	g_object_set(G_OBJECT(sink), "emit-signals", TRUE, NULL);
	
	user_data.width = width;
	user_data.height = height;
	g_signal_connect(sink, "new-sample", G_CALLBACK(callback), (void *)&user_data);
	
	gst_bin_add(GST_BIN(pipeline), source);
	gst_bin_add(GST_BIN(pipeline), capsfilter);
	gst_bin_add(GST_BIN(pipeline), sink);
	
	gst_element_link_many(source, capsfilter, sink, NULL);
	
	gst_element_set_state(pipeline, GST_STATE_PLAYING);
	
	// Catturo l'evento SIGTERM
	struct sigaction sigIntHandler;
	
	sigIntHandler.sa_handler = term_handler;
	sigemptyset(&sigIntHandler.sa_mask);
	sigIntHandler.sa_flags = 0;
	
	sigaction(SIGINT, &sigIntHandler, NULL);
	sigaction(SIGTERM, &sigIntHandler, NULL);
	
	// Avvio il main loop
	g_main_loop_run(mainloop);
	
	// this stops the pipeline and frees all resources
	gst_element_set_state(pipeline, GST_STATE_NULL);
	
	/* the pipeline automatically handles all elements that have been added to it.
	   thus they do not have to be cleaned up manually */
	gst_object_unref(pipeline);
	
	cout << "Program exit" << endl;
	
	return 0;
}
