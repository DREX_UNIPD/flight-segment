# Qt
QT += core
QT -= gui

# Config
CONFIG -= app_bundle
CONFIG += c++11
CONFIG += console
CONFIG += thread

# Target
TARGET = Drex_Master

# Template
TEMPLATE = app

# Sources
SOURCES += \
	src/main.cpp \
	src/Process/mp_constructor.cpp \
	src/Process/mp_destructor.cpp \
	src/Process/mp_sys_signals.cpp \
	src/Process/mp_command_selector.cpp \
	src/Process/mp_constants.cpp \
	src/Process/mp_load_configuration.cpp \
	src/Process/mp_notify.cpp \
	src/Process/mp_verify_command_syntax.cpp \
	src/Process/mp_socket_management.cpp \
	src/Process/mp_watchdog.cpp \
	src/Process/mp_check_message_syntax.cpp \
	src/Process/mp_publish_message.cpp \
	src/Process/mp_state_management.cpp \
	src/Process/Inits/mp_init.cpp \
	src/Process/Inits/mp_init_states_map.cpp \
	src/Process/Inits/mp_init_watchdog.cpp \
	src/Process/Inits/mp_init_file_logger.cpp \
	src/Process/Inits/mp_init_networking.cpp \
	src/Process/Features/mp_kill_process.cpp \
	src/Process/Features/mp_get_state.cpp \
	src/Process/Features/mp_set_state.cpp \
	src/Process/Features/mp_thermalcutter.cpp \
	src/Process/Commands/mp_cmd_kill.cpp \
	src/Process/Commands/mp_cmd_set_state.cpp \
	src/Process/Commands/mp_cmd_get_state.cpp \
	src/Process/Commands/mp_cmd_deploy.cpp \
	src/Process/Commands/mp_cmd_restore.cpp \
	src/Process/Commands/mp_cmd_get_system_state.cpp \
	src/Process/States/mp_pre.cpp \
	src/Process/States/mp_cof.cpp \
	src/Process/States/mp_sch.cpp \
	src/Process/States/mp_dep.cpp \
	src/Process/States/mp_des.cpp \
	src/Process/States/mp_pos.cpp \
	src/Process/States/mp_fst.cpp \
	src/Process/States/mp_gnd.cpp \
	src/Process/States/mp_idle.cpp \
	src/Process/States/mp_shd.cpp \
	../Drex/src/State/state_object.cpp \
	../Drex/src/PublisherSink/publisher_sink.cpp \
    src/Process/mp_publish_log.cpp \
    src/Process/Inits/mp_init_storage.cpp \
    src/Process/States/mp_boot.cpp \
    src/Process/mp_ignore_answer.cpp \
    src/Process/mp_publishing.cpp \
    src/Process/Inits/mp_init_publishing.cpp \
    src/Process/mp_date_update.cpp \
    src/Process/Inits/mp_init_date_update.cpp \
    src/Process/Commands/mp_cmd_help.cpp \
    src/Process/mp_message_display.cpp \
    src/Process/Commands/mp_cmd_shutdown.cpp \
    src/Process/Commands/mp_cmd_set_date.cpp

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# Include path
INCLUDEPATH += \
	include \
	../ \
	../nzmqt/include \
	../Drex/include

# Altri file
DISTFILES += \
	config/mp.yaml \
    init_scripts/mp.sh \
    ../Drex/config/common.yaml

# Pacchetti
CONFIG += link_pkgconfig
PKGCONFIG += libzmq

# Librerie
LIBS += /usr/local/lib/libyaml-cpp.a

# Header
HEADERS += \
	include/Process/mp.h \
	../Drex/include/State/state_object.h \
	../nzmqt/include/nzmqt/nzmqt.hpp \
	../Drex/include/Socket/socket_utils.h \
	../Drex/include/Utilities/vector3d.h \
	../Drex/include/PublisherSink/publisher_sink.h
