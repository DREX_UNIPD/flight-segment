//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		05/08/2017
//	Descrizione:		Verifica della sintassi del messaggio
//****************************************************************************************************//

// Header del processo
#include <Process/mp.h>

// Header Qt
#include <QRegularExpression>

using namespace std;
using namespace drex::processes;

bool MProcess::checkMessageSyntax(const Message &msg, const QStringList &syntax, QList<QRegularExpressionMatch> &matches) {
	// Verifico che il numero di frame sia corretto
	if (msg.length() < syntax.length()) return false;
	
	// Per ogni frame verifico il matching e lo salvo nella lista dei match
	for (int i = 0; i < syntax.length(); i++) {
		// Interpreto i parametri del comando
		QString parameters = msg[i];
		QRegularExpression rx(syntax[i]);
		QRegularExpressionMatch match = rx.match(parameters);
		matches.append(match);
		
		// Verifico la sintassi del comando
		if (!match.hasMatch()) return false;
	}
	
	return true;
}
