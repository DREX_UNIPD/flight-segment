//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		23/07/2017
//	Descrizione:		Ground, verifica della condizione del passaggio di stato
//****************************************************************************************************//

// Header del processo
#include <Process/mp.h>

using namespace std;
using namespace nzmqt;
using namespace drex::processes;

// Inizializzazione dello stato
bool MProcess::stateGndInit() {
	_logger->info("GND: Init");
	
	// Connetto la funzione di ricezione degli eventi
	QObject::connect(_sap_data_socket.socket, &ZMQSocket::messageReceived, this, &MProcess::stateGndReceiveSensorsData, Qt::QueuedConnection);
	
	return true;
}

// Ricezione dati da parte del sensor acquisition process
void MProcess::stateGndReceiveSensorsData(const Message &msg) {
	// Sintassi del messaggio
	QStringList syntax;
	if (_barometer_altitude_enable) {
		if (_vacuometer_altitude_enable) {
			syntax.append("(?i)^ *(?:VACUOMETER|BAROMETER)/ALTITUDE *$");
		}
		else {
			syntax.append("(?i)^ *BAROMETER/ALTITUDE *$");
		}
	}
	else {
		if (_vacuometer_altitude_enable) {
			syntax.append("(?i)^ *VACUOMETER/ALTITUDE *$");
		}
		else {
			return;
		}
	}
	syntax.append("(?i)^ *ALTITUDE *= *(?<altitude>-?\\d+\\.?\\d*) *$");
	
	QList<QRegularExpressionMatch> matches;
	if (!checkMessageSyntax(msg, syntax, matches)) return;
	
	// Ricavo l'altitudine dal messaggio
	QString str_altitude;
	double altitude;
	str_altitude = matches[1].captured("altitude");
	altitude = str_altitude.toDouble();
	
	_logger->info(string("GND: Altitude received ") + str_altitude.toStdString());
	
	// Verifico se l'altiudine ha superato la soglia
	if (altitude >= _ground_altitude_limit) {
		setState(ST_PRE);
	}
}

// Pulizia dello stato
bool MProcess::stateGndCleanup() {
	_logger->info("GND: Cleanup");
	
	// Disonnetto la funzione di ricezione degli eventi
	QObject::disconnect(_sap_data_socket.socket, &ZMQSocket::messageReceived, this, &MProcess::stateGndReceiveSensorsData);
	
	return true;
}
