//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		19/09/2017
//	Descrizione:		Stato Boot: verifico lo stato del pin RESTORE signal per decidere il passaggio
//						allo stato successivo
//****************************************************************************************************//

// Header del processo
#include <Process/mp.h>

using namespace std;
using namespace drex::processes;
using namespace nzmqt;

// Inizializzazione dello stato
bool MProcess::stateBootInit() {
	_logger->info("BOOT: Init");
	
	// Connetto la funzione di ricezione degli eventi
	QObject::connect(_sap_data_socket.socket, &ZMQSocket::messageReceived, this, &MProcess::stateBootReceiveGPIO, Qt::QueuedConnection);
	
	Message msg;
	
	// Abilito il sensor
	msg.clear();
	msg += "SET STATE";
	msg += "RUN";
	_sap_command_socket.socket->sendMessage(msg);
	
	// Abilito lo stereovision
	if (!_manual_mode_stereovision) {
		msg.clear();
		msg += "SET STATE";
		msg += "RUN";
		_svp_command_socket.socket->sendMessage(msg);
	}
	
//	QTimer::singleShot(_boot_inspection_start_delay, [this] () {
//		// Abilito l'inspection
//		Message msg;
//		msg += "SET STATE";
//		msg += "RUN";
//		_inp_command_socket.socket->sendMessage(msg);
//	});
	
	return true;
}

void MProcess::stateBootReceiveGPIO(const Message &msg) {
	// Sintassi del messaggio
	QStringList syntax;
	
	syntax.append("(?i)^ *GPIOEXPANDER */ *RESTORE *$");
	syntax.append("(?i)^ *VALUE *= *(?<value>0|1) *$");
	
	QList<QRegularExpressionMatch> matches;
	if (!checkMessageSyntax(msg, syntax, matches)) return;
	
	// Ricavo lo stato del segnale di restore dal messaggio
	bool restore;
	restore = (matches[1].captured("value").toInt() != 0);
	
	_logger->info(string("BOOT: Restore = ") + string(restore ? "true" : "false"));
	
	// Verifico lo stato del segnale restore
	if (restore) {
		// Chiedo lo stato alla board remota e lo ripristino
		Message msg;
		msg += CMD_RESTORE.c_str();
		_mp_command_socket.socket->sendMessage(msg);
		setState(ST_IDLE);
	}
	else {
		if (_main_board) {
			// Gestisco il process flow dall'inizio
			setState(ST_FST);
		}
		else {
			// Vado in idle e verifico che la main board sia attiva
			setState(ST_IDLE);
		}
	}
}

bool MProcess::stateBootCleanup() {
	_logger->info("BOOT: Cleanup");
	
	// Connetto la funzione di ricezione degli eventi
	QObject::disconnect(_sap_data_socket.socket, &ZMQSocket::messageReceived, this, &MProcess::stateBootReceiveGPIO);
	
	return true;
}
