//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		22/05/2017
//	Descrizione:		Inizializzazione dello stato
//****************************************************************************************************//

// Header del processo
#include <Process/mp.h>

using namespace std;
using namespace drex::processes;
using namespace nzmqt;

// Inizializzazione dello stato
bool MProcess::stateDepInit() {
	_logger->info("DEP: Init");
	
	// Imposto il timer per il thermalcutter main
	QObject::connect(&_deployment_main_timer, &QTimer::timeout, this, &MProcess::stateDepMainTimeout, Qt::QueuedConnection);
	_deployment_main_timer.setSingleShot(true);
	_deployment_main_timer.start(_deployment_main_timeout);
	
	// Imposto il timer per il thermalcutter backup
	QObject::connect(&_deployment_backup_timer, &QTimer::timeout, this, &MProcess::stateDepBackupTimeout, Qt::QueuedConnection);
	_deployment_backup_timer.setSingleShot(true);
	
	// Imposto la funzione di ricezione della risposta del sensor
	QObject::disconnect(_sap_command_socket.socket, &ZMQSocket::messageReceived, this, &MProcess::ignoreAnswer);
	QObject::connect(_sap_command_socket.socket, &ZMQSocket::messageReceived, this, &MProcess::stateDepReceiveMainCommandAnswer, Qt::QueuedConnection);
	
	if (!setThermalCutter(_deployment_main_thermalcutter_period)) {
		_logger->error("DEP: Failed to activate main thermal cutter");
		if (!setState(ST_IDLE)) {
			return false;
		}
		return false;
	}
	
	// Connetto la funzione di ricezione degli eventi
	QObject::connect(_sap_data_socket.socket, &ZMQSocket::messageReceived, this, &MProcess::stateDepReceiveMicroswitch, Qt::QueuedConnection);
	
	Message msg;
	
	if (!_manual_mode_stereovision) {
		// Avvio la registrazione video
		msg.clear();
		msg += "START CAPTURING";
		msg += "CAMERA = LEFT";
		_svp_command_socket.socket->sendMessage(msg);
		
		// Avvio la registrazione video
		msg.clear();
		msg += "START CAPTURING";
		msg += "CAMERA = RIGHT";
		_svp_command_socket.socket->sendMessage(msg);
	}
	
	return true;
}

// Ricevo lo stato del microswitch
void MProcess::stateDepReceiveMicroswitch(const Message &msg) {
	// Sintassi del messaggio
	QStringList syntax;
	syntax.append("(?i)^ *GPIOEXPANDER */ *MICROSWITCH *$");
	syntax.append("(?i)^ *VALUE *= *(?<value>0|1) *$");
	
	QList<QRegularExpressionMatch> matches;
	if (!checkMessageSyntax(msg, syntax, matches)) return;
	
	// Ricavo l'altitudine dal messaggio
	bool value;
	value = (matches[1].captured("value").toInt() != 0);
	
	_logger->info(string("DEP: Microswitch state received ") + string(value ? "pushed" : "not pushed"));
	
	if (value) {
		// Deployment avvenuto con successo!
		if (_deployment_main_timer.isActive()) {
			_logger->info("DEP: Successful deployment with primary thermal cutter");
		}
		if (_deployment_backup_timer.isActive()) {
			_logger->info("DEP: Successful deployment with secondary thermal cutter");
		}
		setState(ST_POS);
		return;
	}
}

// Ricezione risposta al comando di deployment
void MProcess::stateDepReceiveMainCommandAnswer(const Message &msg) {
	// Sintassi del messaggio
	QStringList syntax;
	syntax.append("(?i)^ *(?<answer>OK|ERROR) *$");
	
	QList<QRegularExpressionMatch> matches;
	if (!checkMessageSyntax(msg, syntax, matches)) return;
	
	// Verifico che l'operazione sia andata a buon fine
	QString answer;
	answer = matches[0].captured("answer");
	
	if (answer != "OK") {
		string errmsg;
		for (int i = 1; i < msg.size(); i++) {
			errmsg += msg[i].toStdString();
			errmsg += "\n";
		}
		_logger->error(string("DEP: Failed to activate primary thermal cutter. Errmsg = " + errmsg).c_str());
	}
}

// Ricezione risposta al comando di deployment
void MProcess::stateDepReceiveBackupCommandAnswer(const Message &msg) {
	// Sintassi del messaggio
	QStringList syntax;
	syntax.append("(?i)^ *(?<answer>OK|ERROR) *$");
	
	QList<QRegularExpressionMatch> matches;
	if (!checkMessageSyntax(msg, syntax, matches)) return;
	
	// Verifico che l'operazione sia andata a buon fine
	QString answer;
	answer = matches[0].captured("answer");
	
	if (answer != "OK") {
		string errmsg;
		for (int i = 1; i < msg.size(); i++) {
			errmsg += msg[i].toStdString();
			errmsg += "\n";
		}
		_logger->error(string("DEP: Failed to activate secondary thermal cutter. Errmsg = " + errmsg).c_str());
	}
}

// Main timer timeout
void MProcess::stateDepMainTimeout() {
	// Sostituisco la funzione di ricezione degli eventi
	QObject::disconnect(_sap_command_socket.socket, &ZMQSocket::messageReceived, this, &MProcess::stateDepReceiveMainCommandAnswer);
	
	// Se è abilitato solo il primo thermalcutter ed è fallito vado in IDLE
	if (_deployment_only_main_thermalcutter) {
		_logger->error("DEP: Primary deployment failed, switching to IDLE state");
		setState(ST_IDLE);
		return;
	}
	
	// Deployment fallito, riproviamo con il backup thermalcutter...
	_logger->warn("DEP: Primary deployment failed, starting secondary deployment");
	
	QObject::connect(_sap_command_socket.socket, &ZMQSocket::messageReceived, this, &MProcess::stateDepReceiveBackupCommandAnswer, Qt::QueuedConnection);
	
	// Invio il comando alla board remota
	Message msg;
	msg += CMD_DEPLOY.c_str();
	msg += (string("TIME = ") + to_string(_deployment_backup_thermalcutter_period)).c_str();
	_mp_command_socket.socket->sendMessage(msg);
	
	// Imposto il timer di sicurezza
	_deployment_backup_timer.start(_deployment_backup_timeout);
}

// Backup timer timeout
void MProcess::stateDepBackupTimeout() {
	// Deployment fallito con entrambi i thermalcutter
	_logger->error("DEP: Secondary deployment failed, switching to IDLE state");
	setState(ST_IDLE);
}

// Pulizia prima del passaggio di stato
bool MProcess::stateDepCleanup() {
	_logger->info("DEP: Cleanup");
	
	// Resetto il thermal cutter
	resetThermalCutter();
	
	// Fermo e scollego i timer
	if (_deployment_main_timer.isActive()) _deployment_main_timer.stop();
	if (_deployment_backup_timer.isActive()) _deployment_backup_timer.stop();
	QObject::disconnect(&_deployment_main_timer, &QTimer::timeout, this, &MProcess::stateDepMainTimeout);
	QObject::disconnect(&_deployment_backup_timer, &QTimer::timeout, this, &MProcess::stateDepBackupTimeout);
	
	// Disconnetto la funzione di ricezione degli eventi
	QObject::disconnect(_sap_command_socket.socket, &ZMQSocket::messageReceived, this, &MProcess::stateDepReceiveMainCommandAnswer);
	QObject::disconnect(_sap_command_socket.socket, &ZMQSocket::messageReceived, this, &MProcess::stateDepReceiveBackupCommandAnswer);
	QObject::connect(_sap_command_socket.socket, &ZMQSocket::messageReceived, this, &MProcess::ignoreAnswer, Qt::QueuedConnection);
	QObject::disconnect(_sap_data_socket.socket, &ZMQSocket::messageReceived, this, &MProcess::stateDepReceiveMicroswitch);
	
	Message msg;
	
	if (!_manual_mode_stereovision) {
		// Fermo la registrazione video
		msg.clear();
		msg += "STOP CAPTURING";
		msg += "CAMERA = LEFT";
		_svp_command_socket.socket->sendMessage(msg);
		
		// Fermo la registrazione video
		msg.clear();
		msg += "STOP CAPTURING";
		msg += "CAMERA = RIGHT";
		_svp_command_socket.socket->sendMessage(msg);
		
		// Avvio la stereovisione
		msg.clear();
		msg += "START STEREOVISION";
		_svp_command_socket.socket->sendMessage(msg);
	}
	
	return true;
}
