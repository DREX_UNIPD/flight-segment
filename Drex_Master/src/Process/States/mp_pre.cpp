//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		29/07/2017
//	Descrizione:		Pre-deployment, attesa raggiungimento altitudine ottimale per il deployment
//****************************************************************************************************//

// Header del processo
#include <Process/mp.h>

using namespace std;
using namespace drex::processes;
using namespace nzmqt;

// Inizializzazione dello stato
bool MProcess::statePreInit() {
	_logger->info("PRE: Init");
	
	// Connetto la funzione di ricezione degli eventi
	QObject::connect(_sap_data_socket.socket, &ZMQSocket::messageReceived, this, &MProcess::statePreReceiveSensorsData, Qt::QueuedConnection);
	
	// Imposto il timer di sicurezza
	QObject::connect(&_predeployment_timer, &QTimer::timeout, this, &MProcess::statePreTimeout, Qt::QueuedConnection);
	_predeployment_timer.setSingleShot(true);
	_predeployment_timer.start(_predeployment_timeout);
	
	return true;
}

// Ricezione dati da parte del sensor acquisition process
void MProcess::statePreReceiveSensorsData(const Message &msg) {
	// Sintassi del messaggio
	QStringList syntax;
	if (_barometer_altitude_enable) {
		if (_vacuometer_altitude_enable) {
			syntax.append("(?i)^ *(?:VACUOMETER|BAROMETER)/ALTITUDE *$");
		}
		else {
			syntax.append("(?i)^ *BAROMETER/ALTITUDE *$");
		}
	}
	else {
		if (_vacuometer_altitude_enable) {
			syntax.append("(?i)^ *VACUOMETER/ALTITUDE *$");
		}
		else {
			return;
		}
	}
	syntax.append("(?i)^ *ALTITUDE *= *(?<altitude>-?\\d+\\.?\\d*) *$");
	
	QList<QRegularExpressionMatch> matches;
	if (!checkMessageSyntax(msg, syntax, matches)) return;
	
	// Ricavo l'altitudine dal messaggio
	QString str_altitude;
	double altitude;
	str_altitude = matches[1].captured("altitude");
	altitude = str_altitude.toDouble();
	
	_logger->info(string("PRE: Altitude received ") + str_altitude.toStdString());
	
	// Verifico se l'altiudine ha superato la soglia
	if (altitude >= _predeployment_altitude_limit) {
		setState(ST_SCH);
	}
}

// Timeout del timer di sicurezza
void MProcess::statePreTimeout() {
	_logger->warn("PRE: Safety timeout");
	setState(ST_SCH);
}

// Pulizia dello stato
bool MProcess::statePreCleanup() {
	_logger->info("PRE: Cleanup");
	
	// Fermo e scollego il timer
	if (_predeployment_timer.isActive()) _predeployment_timer.stop();
	QObject::disconnect(&_predeployment_timer, &QTimer::timeout, this, &MProcess::statePreTimeout);
	
	// Disconnetto la ricezione dei sensori
	QObject::disconnect(_sap_data_socket.socket, &ZMQSocket::messageReceived, this, &MProcess::statePreReceiveSensorsData);
	
	return true;
}
