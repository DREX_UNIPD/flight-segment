//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		30/07/2017
//	Descrizione:		Shutdown, stato ausiliario per la gestione dello spegnimento del'esperimento
//****************************************************************************************************//

// Header del processo
#include <Process/mp.h>

using namespace std;
using namespace drex::processes;
using namespace nzmqt;

// Inizializzazione dello stato
bool MProcess::stateShdInit() {
	_logger->info("SHD: Init");
	
	Message msg;
	
	// Disabilito l'autostart
	msg.clear();
	msg += "SET GPIO VALUE";
	msg += "NAME = AUTOSTART DISABLE";
	msg += "VALUE = 1";
	_sap_command_socket.socket->sendMessage(msg);
	
	// Collego le funzioni di ricezione dei messaggi
	QObject::disconnect(_sap_command_socket.socket, &ZMQSocket::messageReceived, this, &MProcess::ignoreAnswer);
	QObject::connect(_sap_command_socket.socket, &ZMQSocket::messageReceived, this, &MProcess::stateShdReceiveSAPAnswer, Qt::QueuedConnection);
//	QObject::disconnect(_inp_command_socket.socket, &ZMQSocket::messageReceived, this, &MProcess::ignoreAnswer);
//	QObject::connect(_inp_command_socket.socket, &ZMQSocket::messageReceived, this, &MProcess::stateShdReceiveINPAnswer, Qt::QueuedConnection);
	QObject::disconnect(_svp_command_socket.socket, &ZMQSocket::messageReceived, this, &MProcess::ignoreAnswer);
	QObject::connect(_svp_command_socket.socket, &ZMQSocket::messageReceived, this, &MProcess::stateShdReceiveSVPAnswer, Qt::QueuedConnection);
	
	// Imposto il timer per il timeout
	QObject::connect(&_shutdown_timer, &QTimer::timeout, this, &MProcess::stateShdTerminate, Qt::QueuedConnection);
	_shutdown_timer.setSingleShot(true);
	_shutdown_timer.start(_shutdown_timeout);
	
	// Inizializzo i flag
	_shutdown_sap_closed = false;
	_shutdown_svp_closed = false;
//	_shutdown_inp_closed = false;
	
	_shutdown_inp_closed = true;
	
	// Chiudo la board remota
	if (!_shutdown_from_remote) {
		msg.clear();
		msg += "SHUTDOWN";
		_mp_command_socket.socket->sendMessage(msg);
	}
	
	// Chiudo tutti i processi locali
	msg.clear();
	msg += "KILL";
	_sap_command_socket.socket->sendMessage(msg);
	
	// Creo il file di chiusura per l'inspection
	for (QDir dir : _shutdown_inp_trigger_dirs) {
		if (!dir.exists()) {
			_logger->info((QString("Folder not found: ") + dir.absolutePath()).toStdString());
			continue;
		}
		
		int index;
		stateShdGetNextFreeSlot(dir, index);
		stateShdTrigger(dir, index, "EXIT");
	}
	
//	msg.clear();
//	msg += "KILL";
//	_inp_command_socket.socket->sendMessage(msg);
	
	msg.clear();
	msg += "KILL";
	_svp_command_socket.socket->sendMessage(msg);
	
	return true;
}

// Trovo il prossimo slot libero
bool MProcess::stateShdGetNextFreeSlot(QDir &dir, int &index) {
	index = -1;
	
	QStringList filters;
	QStringList files;
	
	do {
		index++;
		filters.clear();
		filters += QString::number(index) + QString("_*");
		files = dir.entryList(filters);
	}
	while (files.count() > 0);
	
	return true;
}

// Creo un trigger
bool MProcess::stateShdTrigger(QDir &dir, int index, QString command) {
	_logger->info(
		string("Triggering: index = ") + to_string(index) + 
		string(", dir = ") + dir.absolutePath().toStdString() + 
		string(", command = ") + command.toStdString()
	);
	
	// Creo il file di trigger
	QString filename = dir.absoluteFilePath(QString::number(index) + QString("_") + command.toUpper());
	QFile file;
	
	// Creo il file per la left camera
	file.setFileName(filename);
	if (!file.open(QIODevice::WriteOnly)) {
		_logger->error("Failed to create trigger file");
		return false;
	}
	file.close();
	
	return true;
}

// Ricezione risposta del processo SAP
void MProcess::stateShdReceiveSAPAnswer(const Message &msg) {
	// Stampo il messaggio
	printMessage(msg);
	
	// Sintassi del messaggio
	QStringList syntax;
	syntax.append("(?i)^ *(?<answer>OK|ERROR) *$");
	
	QList<QRegularExpressionMatch> matches;
	if (!checkMessageSyntax(msg, syntax, matches)) return;
	
	// Verifico che l'operazione sia andata a buon fine
	QString answer;
	answer = matches[0].captured("answer");
	
	if (answer != "OK") {
		string errmsg;
		for (int i = 1; i < msg.size(); i++) {
			errmsg += msg[i].toStdString();
			errmsg += "\n";
		}
		_logger->error(string("SHD: Failed to shutdown SAP. Errmsg = " + errmsg).c_str());
		return;
	}
	
	_shutdown_sap_closed = true;
	_logger->info("SAP correctly shutdown");
	
	if (_shutdown_sap_closed && _shutdown_inp_closed && _shutdown_svp_closed) {
		QTimer::singleShot(1000, [this] () { stateShdTerminate(); });
	}
}

// Ricezione risposta del processo INP
void MProcess::stateShdReceiveINPAnswer(const Message &msg) {
	// Stampo il messaggio
	printMessage(msg);
	
	// Sintassi del messaggio
	QStringList syntax;
	syntax.append("(?i)^ *(?<answer>OK|ERROR) *$");
	
	QList<QRegularExpressionMatch> matches;
	if (!checkMessageSyntax(msg, syntax, matches)) return;
	
	// Verifico che l'operazione sia andata a buon fine
	QString answer;
	answer = matches[0].captured("answer");
	
	if (answer != "OK") {
		string errmsg;
		for (int i = 1; i < msg.size(); i++) {
			errmsg += msg[i].toStdString();
			errmsg += "\n";
		}
		_logger->error(string("SHD: Failed to shutdown INP. Errmsg = " + errmsg).c_str());
		return;
	}
	
	_shutdown_inp_closed = true;
	_logger->info("INP correctly shutdown");
	
	if (_shutdown_sap_closed && _shutdown_inp_closed && _shutdown_svp_closed) {
		QTimer::singleShot(1000, [this] () { stateShdTerminate(); });
	}
}

// Ricezione risposta del processo SVP
void MProcess::stateShdReceiveSVPAnswer(const Message &msg) {
	// Stampo il messaggio
	printMessage(msg);
	
	// Sintassi del messaggio
	QStringList syntax;
	syntax.append("(?i)^ *(?<answer>OK|ERROR) *$");
	
	QList<QRegularExpressionMatch> matches;
	if (!checkMessageSyntax(msg, syntax, matches)) return;
	
	// Verifico che l'operazione sia andata a buon fine
	QString answer;
	answer = matches[0].captured("answer");
	
	if (answer != "OK") {
		string errmsg;
		for (int i = 1; i < msg.size(); i++) {
			errmsg += msg[i].toStdString();
			errmsg += "\n";
		}
		_logger->error(string("SHD: Failed to shutdown SVP. Errmsg = " + errmsg).c_str());
		return;
	}
	
	_shutdown_svp_closed = true;
	_logger->info("SVP correctly shutdown");
	
	if (_shutdown_sap_closed && _shutdown_inp_closed && _shutdown_svp_closed) {
		QTimer::singleShot(1000, [this] () { stateShdTerminate(); });
	}
}

// Chiudo applicazione e sistema operativo
void MProcess::stateShdTerminate() {
	_logger->info("SHD: Shutdown");
	
	// Chiudo il sistema
	QProcess process;
	process.startDetached("shutdown -P now");
}

// ogni board si gestisce i suoi processi, così basta cambiare lo stato di entrambe dalla main
// e il gioco è fatto

// Pulizia dello stato
bool MProcess::stateShdCleanup() {
	_logger->info("SHD: Cleanup");
	
	return true;
}

