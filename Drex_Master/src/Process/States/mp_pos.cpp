//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		30/07/2017
//	Descrizione:		Inizializzazione dello stato
//****************************************************************************************************//

// Header del processo
#include <Process/mp.h>

// Header DREX
#include <Utilities/vector3d.h>

// Header STL
#include <cmath>

using namespace std;
using namespace nzmqt;
using namespace drex::processes;

// Inizializzazione dello stato
bool MProcess::statePosInit() {
	_logger->info("POS: Init");
	
	// Imposto il timer di sicurezza
	
	// Timer assoluto
	QObject::connect(&_postdeployment_safety_timer, &QTimer::timeout, this, &MProcess::statePosTimeout, Qt::QueuedConnection);
	_postdeployment_countdown = _postdeployment_safety_timeout;
	
	if (_postdeployment_safety_timeout <= _postdeployment_signaling_interval) {
		_postdeployment_safety_timer.start(_postdeployment_safety_timeout);
	}
	else {
		_postdeployment_safety_timer.start(_postdeployment_signaling_interval);
	}
	
	// Connetto la funzione di ricezione degli eventi
	QObject::connect(_sap_data_socket.socket, &ZMQSocket::messageReceived, this, &MProcess::statePosReceiveSensorsData, Qt::QueuedConnection);
	
	return true;
}

// Ricezione dati da parte del sensor acquisition process
void MProcess::statePosReceiveSensorsData(const Message &msg) {
	// Sintassi del messaggio
	QStringList syntax;
	syntax.append("(?i)^ *IMU */ *FREEFALL *$");
	syntax.append("(?i)^ *FREEFALL *= *(?<freefall>1|0) *$");
	
	QList<QRegularExpressionMatch> matches;
	if (!checkMessageSyntax(msg, syntax, matches)) return;
	
	// Ricavo il freefall dal messaggio
	int freefall = matches[1].captured("freefall").toInt();
	
	_logger->info(string("POS: Freefall received ") + to_string(freefall));
	
	// Verifico se è stato identificato un freefall
	if (freefall != 0) {
		// Cutoff individuato
		_logger->info("POS: Cutoff detected");
		setState(ST_COF);
		return;
	}
}

// Timeout del safety timer
void MProcess::statePosTimeout() {
	_postdeployment_countdown -= _postdeployment_signaling_interval;
	
	if (_postdeployment_countdown <= 0) {
		_postdeployment_safety_timer.stop();
		_logger->warn("POS: Safety timeout, shutting down the system");
		setState(ST_SHD);
		return;
	}
	else {
		string message;
		message = "POS: Post deployment time left ";
		message += to_string(_postdeployment_countdown / 1000);
		message += " seconds...";
		_logger->warn(message.c_str());
	}
}

// Pulizia prima del passaggio di stato
bool MProcess::statePosCleanup() {
	_logger->info("POS: Cleanup");
	
	// Fermo e scollego il timer
	if (_postdeployment_safety_timer.isActive()) _postdeployment_safety_timer.stop();
	QObject::disconnect(&_postdeployment_safety_timer, &QTimer::timeout, this, &MProcess::statePosTimeout);
	
	// Disonnetto la funzione di ricezione degli eventi
	QObject::disconnect(_sap_data_socket.socket, &ZMQSocket::messageReceived, this, &MProcess::statePosReceiveSensorsData);
	
	return true;
}
