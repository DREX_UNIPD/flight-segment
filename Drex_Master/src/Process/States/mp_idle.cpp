//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		30/07/2017
//	Descrizione:		Stato Idle: il processo è inattivo
//****************************************************************************************************//

// Header del processo
#include <Process/mp.h>

using namespace std;
using namespace drex::processes;

// Inizializzazione dello stato
bool MProcess::stateIdleInit() {
	_logger->info("IDLE: Init");
	
	return true;
}

// Pulizia dello stato
bool MProcess::stateIdleCleanup() {
	_logger->info("IDLE: Cleanup");
	
	return true;
}
