//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		23/07/2017
//	Descrizione:		First, verifica della correttezza della prima acquisizione
//****************************************************************************************************//

// Header del processo
#include <Process/mp.h>

using namespace std;
using namespace drex::processes;
using namespace nzmqt;

// Inizializzazione dello stato
bool MProcess::stateFstInit() {
	_logger->info("FST: Init");
	
	// Connetto la funzione di ricezione degli eventi
	QObject::connect(_sap_data_socket.socket, &ZMQSocket::messageReceived, this, &MProcess::stateFstReceiveSensorsData, Qt::QueuedConnection);
	
	return true;
}

// Ricezione dati da parte del sensor acquisition process
void MProcess::stateFstReceiveSensorsData(const Message &msg) {
	// Sintassi del messaggio
	QStringList syntax;
	if (_barometer_altitude_enable) {
		if (_vacuometer_altitude_enable) {
			syntax.append("(?i)^ *(?:VACUOMETER|BAROMETER)/ALTITUDE *$");
		}
		else {
			syntax.append("(?i)^ *BAROMETER/ALTITUDE *$");
		}
	}
	else {
		if (_vacuometer_altitude_enable) {
			syntax.append("(?i)^ *VACUOMETER/ALTITUDE *$");
		}
		else {
			return;
		}
	}
	syntax.append("(?i)^ *ALTITUDE *= *(?<altitude>-?\\d+\\.?\\d*) *$");
	
	QList<QRegularExpressionMatch> matches;
	if (!checkMessageSyntax(msg, syntax, matches)) return;
	
	// Ricavo l'altitudine dal messaggio
	QString str_altitude;
	double altitude;
	str_altitude = matches[1].captured("altitude");
	altitude = str_altitude.toDouble();
	
	_logger->info(string("FST: Altitude received ") + str_altitude.toStdString());
	
	// Verifico se la prima acquisizione è già oltre al limite
	if (altitude > _first_altitude_limit) {
		_logger->error("FST: First altitude received too high for nominal operations, switching to IDLE state");
		setState(ST_IDLE);
	}
	else {
		setState(ST_GND);
	}
}

// Pulizia dello stato
bool MProcess::stateFstCleanup() {
	_logger->info("FST: Cleanup");
	
	// Connetto la funzione di ricezione degli eventi
	QObject::disconnect(_sap_data_socket.socket, &ZMQSocket::messageReceived, this, &MProcess::stateFstReceiveSensorsData);
	
	return true;
}
