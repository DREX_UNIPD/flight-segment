//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		30/07/2017
//	Descrizione:		Status check, verifica delle condizioni per eventuale intervento manuale
//****************************************************************************************************//

// Header del processo
#include <Process/mp.h>

using namespace std;
using namespace drex::processes;

// Inizializzazione dello stato
bool MProcess::stateSchInit() {
	_logger->info("SCH: Init");
	
	// Imposto il timer
	QObject::connect(&_statuscheck_timer, &QTimer::timeout, this, &MProcess::stateSchTimeout, Qt::QueuedConnection);
	_statuscheck_countdown = _statuscheck_period;
	
	if (_statuscheck_period <= _statuscheck_signaling_interval) {
		_statuscheck_timer.start(_statuscheck_period);
	}
	else {
		_statuscheck_timer.start(_statuscheck_signaling_interval);
	}
	
	return true;
}

// Timeout del timer
void MProcess::stateSchTimeout() {
	_statuscheck_countdown -= _statuscheck_signaling_interval;
	
	if (_statuscheck_countdown <= 0) {
		_statuscheck_timer.stop();
		_logger->warn("SCH: Status check interval terminated, deploying");
		setState(ST_DEP);
	}
	else {
		string message;
		message = "SCH: Status check time left ";
		message += to_string(_statuscheck_countdown / 1000);
		message += " seconds...";
		_logger->warn(message.c_str());
	}
}

// Pulizia prima del passaggio di stato
bool MProcess::stateSchCleanup() {
	_logger->info("SCH: Cleanup");
	
	// Fermo e scollego il timer
	if (_statuscheck_timer.isActive()) _statuscheck_timer.stop();
	QObject::disconnect(&_statuscheck_timer, &QTimer::timeout, this, &MProcess::stateSchTimeout);
	
	return true;
}
