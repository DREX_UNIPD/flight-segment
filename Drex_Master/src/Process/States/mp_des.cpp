//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		22/05/2017
//	Descrizione:		Inizializzazione dello stato
//****************************************************************************************************//

// Header del processo
#include <Process/mp.h>

using namespace std;
using namespace nzmqt;
using namespace drex::processes;

// Inizializzazione dello stato
bool MProcess::stateDesInit() {
	_logger->info("DES: Init");
	
	// Imposto il timer
	QObject::connect(&_descending_timer, &QTimer::timeout, this, &MProcess::stateDesTimeout, Qt::QueuedConnection);
	
	_descending_countdown = _descending_safety_timeout;
	_descending_safety_flag = true;
	
	if (_descending_safety_timeout <= _descending_signaling_interval) {
		_descending_timer.start(_descending_safety_timeout);
	}
	else {
		_descending_timer.start(_descending_signaling_interval);
	}
	
	// Connetto la funzione di ricezione degli eventi
	QObject::connect(_sap_data_socket.socket, &ZMQSocket::messageReceived, this, &MProcess::stateDesReceiveSensorsData, Qt::QueuedConnection);
	
	return true;
}

// Ricezione dati da parte del sensor acquisition process
void MProcess::stateDesReceiveSensorsData(const Message &msg) {
	// Sintassi del messaggio
	QStringList syntax;
	if (_barometer_altitude_enable) {
		if (_vacuometer_altitude_enable) {
			syntax.append("(?i)^ *(?:VACUOMETER|BAROMETER)/ALTITUDE *$");
		}
		else {
			syntax.append("(?i)^ *BAROMETER/ALTITUDE *$");
		}
	}
	else {
		if (_vacuometer_altitude_enable) {
			syntax.append("(?i)^ *VACUOMETER/ALTITUDE *$");
		}
		else {
			return;
		}
	}
	syntax.append("(?i)^ *ALTITUDE *= *(?<altitude>-?\\d+\\.?\\d*) *$");
	
	QList<QRegularExpressionMatch> matches;
	if (!checkMessageSyntax(msg, syntax, matches)) return;
	
	// Ricavo l'altitudine dal messaggio
	QString str_altitude;
	double altitude;
	str_altitude = matches[1].captured("altitude");
	altitude = str_altitude.toDouble();
	
	_logger->info(string("DES: Altitude received ") + str_altitude.toStdString());
	
	// Verifico se l'altiudine ha superato la soglia
	if (altitude <= _descending_altitude_limit && _descending_safety_flag) {
		_logger->info("DES: Altitude under descending limit, setting timer for shutdown");
		_descending_safety_flag = false;
		
		// Faccio ripartire il timer di autospegnimento
		if (_descending_timer.isActive()) {
			_descending_timer.stop();
			
			_descending_countdown = _descending_nominal_timeout;
			
			if (_descending_nominal_timeout <= _descending_signaling_interval) {
				_descending_timer.start(_descending_nominal_timeout);
			}
			else {
				_descending_timer.start(_descending_signaling_interval);
			}
		}
	}
}

// Timeout del timer
void MProcess::stateDesTimeout() {
	_descending_countdown -= _descending_signaling_interval;
	
	if (_descending_countdown <= 0) {
		_descending_timer.stop();
		_logger->warn("DES: Safety timeout, shutting down the system...");
		setState(ST_SHD);
	}
	else {
		string message;
		message = "DES: Descending time left ";
		message += to_string(_descending_countdown / 1000);
		message += " seconds...";
		_logger->warn(message.c_str());
	}
}

// Pulizia prima del passaggio di stato
bool MProcess::stateDesCleanup() {
	_logger->info("DES: Cleanup");
	
	// Fermo e scollego il timer
	if (_descending_timer.isActive()) _descending_timer.stop();
	QObject::disconnect(&_descending_timer, &QTimer::timeout, this, &MProcess::stateDesTimeout);
	
	// Disonnetto la funzione di ricezione degli eventi
	QObject::disconnect(_sap_data_socket.socket, &ZMQSocket::messageReceived, this, &MProcess::stateDesReceiveSensorsData);
	
	return true;
}
