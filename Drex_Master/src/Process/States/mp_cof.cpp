//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		22/05/2017
//	Descrizione:		Stato CutOff: è avvenuto il cut-off del pallone e viene atteso un periodo
//						di tempo prestabilito prima di acquisire le immagini per la verifica dello
//						stato dell'esperimento
//****************************************************************************************************//

// Header del processo
#include <Process/mp.h>

using namespace std;
using namespace drex::processes;

// Inizializzazione dello stato
bool MProcess::stateCofInit() {
	_logger->info("COF: Init");
	
	// Imposto il timer
	QObject::connect(&_cutoff_timer, &QTimer::timeout, this, &MProcess::stateCofTimeout, Qt::QueuedConnection);
	
	_cutoff_countdown = _cutoff_period;
	
	if (_cutoff_period <= _cutoff_signaling_interval) {
		_cutoff_timer.start(_cutoff_period);
	}
	else {
		_cutoff_timer.start(_cutoff_signaling_interval);
	}
	
	return true;
}

// Timeout del timer
void MProcess::stateCofTimeout() {
	_cutoff_countdown -= _cutoff_signaling_interval;
	
	if (_cutoff_countdown <= 0) {
		_cutoff_timer.stop();
		_logger->warn("COF: Cutoff interval terminated, starting decending phase...");
		setState(ST_DES);
	}
	else {
		string message;
		message = "COF: Cutoff time left ";
		message += to_string(_cutoff_countdown / 1000);
		message += " seconds...";
		_logger->warn(message.c_str());
	}
}

// Pulizia prima del passaggio di stato
bool MProcess::stateCofCleanup() {
	_logger->info("COF: Cleanup");
	
	// Fermo e scollego il timer
	if (_cutoff_timer.isActive()) _cutoff_timer.stop();
	QObject::disconnect(&_cutoff_timer, &QTimer::timeout, this, &MProcess::stateCofTimeout);
	
	return true;
}
