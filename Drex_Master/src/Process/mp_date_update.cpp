//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		26/09/2017
//	Descrizione:		Richiesta della data alla ground station
//****************************************************************************************************//

// Header del processo
#include <Process/mp.h>

using namespace std;
using namespace drex::processes;
using namespace nzmqt;

void MProcess::requestDateUpdate() {
	Message msg;
	
	msg += "GET DATE";
	
	// Invio la richiesta di aggiornamento della data finchè non la ottengo
	_gss_command_socket.socket->sendMessage(msg);
}

void MProcess::receiveDateUpdate(const Message &msg) {
	// Sintassi del messaggio
	QStringList syntax;
	syntax.append("(?i)^ *(?<answer>\\w+) *$");
	syntax.append("(?i)^ *(?<date>\\d{4}-\\d{2}-\\d{2} \\d{2}:\\d{2}:\\d{2}) *$");
	
	QList<QRegularExpressionMatch> matches;
	if (!checkMessageSyntax(msg, syntax, matches)) return;
	
	QString ans = matches[0].captured("answer");
	
	if (ans.toUpper() != "OK") return;
	
	QString date = matches[1].captured("date");
	
	system((QString("date -s \"") + date + QString("\"")).toStdString().c_str());
	
	_date_update_timer.stop();
}
