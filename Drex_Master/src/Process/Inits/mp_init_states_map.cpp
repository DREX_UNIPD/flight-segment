//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		09/07/2017
//	Descrizione:		Inizializzazione degli stati
//****************************************************************************************************//

// Header del processo
#include <Process/mp.h>

using namespace std;
using namespace drex::processes;
using namespace drex::state;

bool MProcess::initStatesMap() {
	_logger->info("Init states map");
	
	// Carico le impostazioni dal file di configurazione
	const YAML::Node &process_flow = _configuration["ProcessFlow"];
	
	// Variabili globali per gli stati
	_barometer_altitude_enable = process_flow["BarometerAltitude"].as<bool>();
	_vacuometer_altitude_enable = process_flow["VacuometerAltitude"].as<bool>();
	
	// Popolo la mappa stato -> StateObject
	StateObject *so;
	
	// Elimino l'eventuale contenuto della mappa
	_state_objects.clear();
	
	// DEFAULT
	so = new StateObject();
	// Inizializzazione
	so->init = [this] () { return true; };
	
	// Help
	so->command.insert(CMD_HELP, [this] (const Message &msg, vector<const char *> &parameters) { return this->commandHelp(msg, parameters); });
	so->parameters.insert(CMD_HELP, vector<const char *>());
	
	// Kill
	so->command.insert(CMD_KILL, [this] (const Message &msg, vector<const char *> &parameters) { return this->commandKill(msg, parameters); });
	so->parameters.insert(CMD_KILL, vector<const char *>());
	
	// Get state
	so->command.insert(CMD_GET_STATE, [this] (const Message &msg, vector<const char *> &parameters) { return this->commandGetState(msg, parameters); });
	so->parameters.insert(CMD_GET_STATE, vector<const char *>());
	
	// Set state
	so->command.insert(CMD_SET_STATE, [this] (const Message &msg, vector<const char *> &parameters) { return this->commandSetState(msg, parameters); });
	so->parameters.insert(CMD_SET_STATE, vector<const char *>({
		"(?i)^ *(?<newstate>IDLE|BOOT|FST|GND|PRE|SCH|DEP|POS|COF|DES|SHD) *$"
	}));
	
	// Set date
	so->command.insert(CMD_SET_DATE, [this] (const Message &msg, vector<const char *> &parameters) { return this->commandSetDate(msg, parameters); });
	so->parameters.insert(CMD_SET_DATE, vector<const char *>({
		"(?i)^ *(?<date>\\d\\d\\d\\d-\\d\\d-\\d\\d \\d\\d:\\d\\d:\\d\\d) *$"
	}));
	
	// Deploy
	so->command.insert(CMD_DEPLOY, [this] (const Message &msg, vector<const char *> &parameters) { return this->commandDeploy(msg, parameters); });
	so->parameters.insert(CMD_DEPLOY, vector<const char *>({
		"(?i)^ *(?:TIME) *= *(?<time>\\d+) *$"
	}));
	
	// Restore
	so->command.insert(CMD_RESTORE, [this] (const Message &msg, vector<const char *> &parameters) { return this->commandRestore(msg, parameters); });
	so->parameters.insert(CMD_RESTORE, vector<const char *>());
	
	// Shutdown
	so->command.insert(CMD_SHUTDOWN, [this] (const Message &msg, vector<const char *> &parameters) { return this->commandShutdown(msg, parameters); });
	so->parameters.insert(CMD_SHUTDOWN, vector<const char *>());
	
	// Get system state
	so->command.insert(CMD_GET_SYSTEM_STATE, [this] (const Message &msg, vector<const char *> &parameters) { return this->commandGetSystemState(msg, parameters); });
	so->parameters.insert(CMD_GET_SYSTEM_STATE, vector<const char *>());
	
	// Cleanup
	so->cleanup = [this] () { return true; };
	_state_objects.insert(ST_DEFAULT, so);
	
	// IDLE
	so = new StateObject();
	so->init = [this] () { return this->stateIdleInit(); };
	so->cleanup = [this] () { return this->stateIdleCleanup(); };
	_state_objects.insert(ST_IDLE, so);
	
	// BOOT
	so = new StateObject();
	so->init = [this] () { return this->stateBootInit(); };
	so->cleanup = [this] () { return this->stateBootCleanup(); };
	_state_objects.insert(ST_BOOT, so);
	
	_boot_inspection_start_delay = process_flow["BootInspectionStartDelay"].as<int>() * 1000;
	
	// FST
	so = new StateObject();
	so->init = [this] () { return this->stateFstInit(); };
	so->cleanup = [this] () { return this->stateFstCleanup(); };
	_state_objects.insert(ST_FST, so);
	
	_first_altitude_limit = process_flow["FirstAltitudeLimit"].as<double>();
	
	// GND
	so = new StateObject();
	so->init = [this] () { return this->stateGndInit(); };
	so->cleanup = [this] () { return this->stateGndCleanup(); };
	_state_objects.insert(ST_GND, so);
	
	_ground_altitude_limit = process_flow["GroundAltitudeLimit"].as<double>();
	
	// PRE
	so = new StateObject();
	so->init = [this] () { return this->statePreInit(); };
	so->cleanup = [this] () { return this->statePreCleanup(); };
	_state_objects.insert(ST_PRE, so);
	
	_predeployment_altitude_limit = process_flow["PredeploymentAltitudeLimit"].as<double>();
	_predeployment_timeout = process_flow["PredeploymentTimeout"].as<int>() * 1000;
	
	// SCH
	so = new StateObject();
	so->init = [this] () { return this->stateSchInit(); };
	so->cleanup = [this] () { return this->stateSchCleanup(); };
	_state_objects.insert(ST_SCH, so);
	
	_statuscheck_period = process_flow["StatusCheckPeriod"].as<int>() * 1000;
	_statuscheck_signaling_interval = process_flow["StatusCheckSignalingInterval"].as<int>();
	
	// DEP
	so = new StateObject();
	so->init = [this] () { return this->stateDepInit(); };
	so->cleanup = [this] () { return this->stateDepCleanup(); };
	_state_objects.insert(ST_DEP, so);
	
	_deployment_only_main_thermalcutter = process_flow["OnlyMainThermalcutter"].as<bool>();
	_deployment_main_thermalcutter_period = process_flow["MainThermalCutterPeriod"].as<int>();
	_deployment_backup_thermalcutter_period = process_flow["BackupThermalCutterPeriod"].as<int>();
	_deployment_main_timeout = process_flow["MainThermalCutterTimeout"].as<int>() * 1000;
	_deployment_backup_timeout = process_flow["BackupThermalCutterTimeout"].as<int>() * 1000;
	_deployment_main_dac = process_flow["MainThermalCutterDAC"].as<string>();
	_deployment_main_current = process_flow["MainThermalCutterCurrent"].as<double>();
	
	// POS
	so = new StateObject();
	so->init = [this] () { return this->statePosInit(); };
	so->cleanup = [this] () { return this->statePosCleanup(); };
	_state_objects.insert(ST_POS, so);
	
	_postdeployment_safety_timeout = process_flow["PostdeploymentSafetyTimeout"].as<int>() * 1000;
	_postdeployment_signaling_interval = process_flow["PostdeploymentSignalingInterval"].as<int>();
	
	// COF
	so = new StateObject();
	so->init = [this] () { return this->stateCofInit(); };
	so->cleanup = [this] () { return this->stateCofCleanup(); };
	_state_objects.insert(ST_COF, so);
	
	_cutoff_period = process_flow["CutoffPeriod"].as<int>() * 1000;
	_cutoff_signaling_interval = process_flow["CutoffSignalingInterval"].as<int>();
	
	// DES
	so = new StateObject();
	so->init = [this] () { return this->stateDesInit(); };
	so->cleanup = [this] () { return this->stateDesCleanup(); };
	_state_objects.insert(ST_DES, so);
	
	_descending_altitude_limit = process_flow["DescendingAltitudeLimit"].as<double>();
	_descending_safety_timeout = process_flow["DescendingSafetyTimeout"].as<int>() * 1000;
	_descending_nominal_timeout = process_flow["DescendingNominalTimeout"].as<int>() * 1000;
	_descending_signaling_interval = process_flow["DescendingSignalingInterval"].as<int>();
	
	// SHD
	so = new StateObject();
	so->init = [this] () { return this->stateShdInit(); };
	so->cleanup = [this] () { return this->stateShdCleanup(); };
	_state_objects.insert(ST_SHD, so);
	
	_shutdown_timeout = process_flow["ShutdownTimeout"].as<int>() * 1000;
	
	QDir ssd;
	ssd = _storage_home;
	ssd.cd(_configuration["Storage"]["Common"]["SSD"].as<string>().c_str());
	vector<string> inp_dirs;
	inp_dirs = process_flow["ShutdownInspectionDirs"].as<vector<string>>();
	for (size_t i = 0; i < inp_dirs.size(); i++) {
		QDir dir;
		dir.setPath(ssd.absoluteFilePath(inp_dirs[i].c_str()));
		_shutdown_inp_trigger_dirs.append(dir);
	}
	_shutdown_from_remote = false;
	
	return true;
}
