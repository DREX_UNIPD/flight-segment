//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		29/07/2017
//	Descrizione:		Inizializzazione dell'aggiornamento della data
//****************************************************************************************************//

// Header del processo
#include <Process/mp.h>

using namespace drex::processes;

bool MProcess::initDateUpdate() {
	// Leggo i parametri dell'aggiornamento della data
	const YAML::Node &config_date_update = _configuration["DateUpdate"];
	
	_date_updated = false;
	
	_date_update_enabled = config_date_update["Enabled"].as<bool>();
	
	if (_date_update_enabled) {
		_date_update_period = config_date_update["Period"].as<int>() * 1000;
		QObject::connect(&_date_update_timer, &QTimer::timeout, this, &MProcess::requestDateUpdate, Qt::QueuedConnection);
		_date_update_timer.start(_date_update_period);
	}
	
	return true;
}
