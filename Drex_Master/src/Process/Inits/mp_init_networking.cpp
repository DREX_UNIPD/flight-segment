//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		29/07/2017
//	Descrizione:		Inizializzazione dell'interfaccia di rete
//****************************************************************************************************//

// Header del processo
#include <Process/mp.h>

using namespace std;
using namespace nzmqt;
using namespace drex::processes;

bool MProcess::initNetworking() {
	// Inizializzazione dell'interfaccia di rete
	
	// Leggo i parametri dei socket
	const YAML::Node &config_sockets = _configuration["Sockets"];
	
	int default_linger = config_sockets["DefaultLinger"].as<int>();
	
	// Creo i socket
	_context = createDefaultContext();
	_context->start();
	
	// Command socket
	if (!initSocket(_command_socket, config_sockets["Command"], "Command", default_linger)) return false;
	QObject::connect(_command_socket.socket, &ZMQSocket::messageReceived, this, &MProcess::commandSelector, Qt::QueuedConnection);
	if (!startSocket(_command_socket)) return false;
	
	// Logger socket
	if (!initSocket(_logger_socket, config_sockets["Logger"], "Logger", default_linger)) return false;
	if (!startSocket(_logger_socket)) return false;
	
	// Data socket
	if (!initSocket(_data_socket, config_sockets["Data"], "Data", default_linger)) return false;
	if (!startSocket(_data_socket)) return false;
	
	// SAP Command socket
	if (!initSocket(_sap_command_socket, config_sockets["SAPCommand"], "SAPCommand", default_linger)) return false;
	QObject::connect(_sap_command_socket.socket, &ZMQSocket::messageReceived, this, &MProcess::ignoreAnswer, Qt::QueuedConnection);
	if (!startSocket(_sap_command_socket)) return false;
	
	// SAP Data socket
	if (!initSocket(_sap_data_socket, config_sockets["SAPData"], "SAPData", default_linger)) return false;
	if (!startSocket(_sap_data_socket)) return false;
	
	// INP Command socket
	if (!initSocket(_inp_command_socket, config_sockets["INPCommand"], "INPCommand", default_linger)) return false;
	QObject::connect(_inp_command_socket.socket, &ZMQSocket::messageReceived, this, &MProcess::ignoreAnswer, Qt::QueuedConnection);
	if (!startSocket(_inp_command_socket)) return false;
	
	// SVP Command socket
	if (!initSocket(_svp_command_socket, config_sockets["SVPCommand"], "SVPCommand", default_linger)) return false;
	QObject::connect(_svp_command_socket.socket, &ZMQSocket::messageReceived, this, &MProcess::ignoreAnswer, Qt::QueuedConnection);
	if (!startSocket(_svp_command_socket)) return false;
	
	// MP Command socket
	if (!initSocket(_mp_command_socket, config_sockets["MPCommand"], "MPCommand", default_linger)) return false;
	QObject::connect(_mp_command_socket.socket, &ZMQSocket::messageReceived, this, &MProcess::ignoreAnswer, Qt::QueuedConnection);
	if (!startSocket(_mp_command_socket)) return false;
	
	// MP Command Self socket
	if (!initSocket(_mp_command_self_socket, config_sockets["MPCommandSelf"], "MPCommandSelf", default_linger)) return false;
	QObject::connect(_mp_command_self_socket.socket, &ZMQSocket::messageReceived, this, &MProcess::ignoreAnswer, Qt::QueuedConnection);
	if (!startSocket(_mp_command_self_socket)) return false;
	
	// Watchdog sockets
	_watchdog_enabled = _configuration["Watchdog"]["Enabled"].as<bool>();
	if (_watchdog_enabled) {
		// Master process (DEALER)
		if (!initSocket(_watchdog_mp_command_socket, config_sockets["WDMP"], "WDMP", default_linger)) return false;
		QObject::connect(_watchdog_mp_command_socket.socket, &ZMQSocket::messageReceived, this, &MProcess::wdMPReceiveAnswer, Qt::QueuedConnection);
		if (!startSocket(_watchdog_mp_command_socket)) return false;
		
		// Sensor acquisition process (DEALER)
		if (!initSocket(_watchdog_sap_command_socket, config_sockets["WDSAP"], "WDSAP", default_linger)) return false;
		QObject::connect(_watchdog_sap_command_socket.socket, &ZMQSocket::messageReceived, this, &MProcess::ignoreAnswer, Qt::QueuedConnection);
		if (!startSocket(_watchdog_sap_command_socket)) return false;
	}
	
	// Sensor acquisition process (SUBSCRIBER)
	if (!initSocket(_watchdog_sap_data_socket, config_sockets["WDSAP"], "WDSAP", default_linger)) return false;
	_watchdog_sap_data_socket.socket->setOption(ZMQSocket::OPT_SUBSCRIBE, "PUBLISHING/STATE");
	QObject::connect(_watchdog_sap_data_socket.socket, &ZMQSocket::messageReceived, this, &MProcess::wdSAPReceiveState, Qt::QueuedConnection);
	if (!startSocket(_watchdog_sap_data_socket)) return false;
	
	// Inspection process (SUBSCRIBER)
	if (!initSocket(_watchdog_inp_signaling_socket, config_sockets["WDINP"], "WDINP", default_linger)) return false;
	_watchdog_inp_signaling_socket.socket->setOption(ZMQSocket::OPT_SUBSCRIBE, "PUBLISHING/STATE");
	QObject::connect(_watchdog_inp_signaling_socket.socket, &ZMQSocket::messageReceived, this, &MProcess::wdINPReceiveState, Qt::QueuedConnection);
	if (!startSocket(_watchdog_inp_signaling_socket)) return false;
	
	// Stereovision process (SUBSCRIBER)
	if (!initSocket(_watchdog_svp_data_socket, config_sockets["WDSVP"], "WDSVP", default_linger)) return false;
	_watchdog_svp_data_socket.socket->setOption(ZMQSocket::OPT_SUBSCRIBE, "PUBLISHING/STATE");
	QObject::connect(_watchdog_svp_data_socket.socket, &ZMQSocket::messageReceived, this, &MProcess::wdSVPReceiveState, Qt::QueuedConnection);
	if (!startSocket(_watchdog_svp_data_socket)) return false;
	
	// GSS Command socket
	if (!initSocket(_gss_command_socket, config_sockets["GSS"], "GSS", default_linger)) return false;
	QObject::connect(_gss_command_socket.socket, &ZMQSocket::messageReceived, this, &MProcess::receiveDateUpdate, Qt::QueuedConnection);
	if (!startSocket(_gss_command_socket)) return false;
	
	return true;
}
