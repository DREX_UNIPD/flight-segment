//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		17/05/2017
//	Descrizione:		Inizializzazione del processo
//****************************************************************************************************//

// Header del processo
#include <Process/mp.h>

// Header Qt
#include <QDir>

using namespace std;
using namespace nzmqt;
using namespace drex::processes;
using namespace drex::state;
using namespace drex::socket;

// Inizializzazione del processo
void MProcess::init() {
	// Argomenti del processo
	QStringList args = arguments();
	
	// Verifico che sia stato specificato il file di configurazione
	QString path_config_file;
	if (args.size() < 2) {
		path_config_file = "config/mp.yaml";
		_console->warn("Config filepath not specified, assuming \"" + path_config_file.toStdString() + "\"");
	}
	else {
		// Considero il primo parametro come il percorso del file di configurazione
		path_config_file = args[1];
		
		// Se ci sono altri parametri avverto che non li userò
		if (args.size() > 2) {
			_console->warn("Too many arguments, only the first will be take in account");
		}
	}
	
	// Carico i file di configurazione
	QStringList config_files;
	config_files.append("config/common.yaml");
	config_files.append(path_config_file);
	if (!loadConfiguration(config_files, _configuration)) {
		quit();
		return;
	}
	
	// Inizializzo lo storage
	if (!initStorage()) {
		quit();
		return;
	}
	
	// Inizializzo il logger su file
	if (!initFileLogger()) {
		quit();
		return;
	}
	
	// Inizializzo gli stati
	if (!initStatesMap()) {
		quit();
		return;
	}
	
	// Inizializzo l'interfaccia di rete
	if (!initNetworking()) {
		quit();
		return;
	}
	
	// Inizializzo il watchdog
	if (!initWatchdog()) {
		quit();
		return;
	}
	
	// Inizializzo la pubblicazione
	if (!initPublishing()) {
		quit();
		return;
	}
	
	// Inizializzazione dell'aggiornamento della data
	if (!initDateUpdate()) {
		quit();
		return;
	}
	
	// Leggo le impostazioni della scheda
	_main_board = _configuration["MainBoard"].as<bool>();
	_manual_mode = _configuration["ManualMode"].as<bool>();
	_manual_mode_stereovision = _configuration["StereovisionManualMode"].as<bool>();
	
	// In modalità manuale vado sempre in IDLE
	if (_manual_mode) {
		_state = ST_IDLE;
	}
	else {
		_state = ST_BOOT;
	}
	
	// Se è abilitato il watchdog aggiorno lo stato del master
	if (_watchdog_enabled) {
		_gso_local.masterProcessState = QString::fromStdString(_state);
	}
	
	// Inizializzo lo stato
	_state_objects[_state]->init();
	
	// Messaggio informativo
	_logger->info("Init completed");
}
