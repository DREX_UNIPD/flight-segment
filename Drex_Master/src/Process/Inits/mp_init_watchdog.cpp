//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		29/07/2017
//	Descrizione:		Inizializzazione del watchdog
//****************************************************************************************************//

// Header del processo
#include <Process/mp.h>

using namespace std;
using namespace nzmqt;
using namespace drex::processes;

bool MProcess::initWatchdog() {
	_logger->info("Init watchdog");
	
	const YAML::Node &config_watchdog = _configuration["Watchdog"];
	
	_watchdog_enabled = config_watchdog["Enabled"].as<bool>();
	
	if (!_watchdog_enabled) return true;
	
	// Connetto i timer
	QObject::connect(&_watchdog_keepalive_timer, &QTimer::timeout, this, &MProcess::wdRequestState, Qt::QueuedConnection);
	QObject::connect(&_watchdog_answer_timer, &QTimer::timeout, this, &MProcess::wdReboot, Qt::QueuedConnection);
	QObject::connect(&_watchdog_power_down_timer, &QTimer::timeout, this, &MProcess::wdRestorePower, Qt::QueuedConnection);
	QObject::connect(&_watchdog_boot_delay_timer, &QTimer::timeout, this, &MProcess::wdAfterBootDelay, Qt::QueuedConnection);
	QObject::connect(&_watchdog_switch_roles_timer, &QTimer::timeout, this, &MProcess::wdSwitchRoles, Qt::QueuedConnection);
	
	// Leggo le impostazioni dalla configurazione
	_watchdog_keepalive_period = config_watchdog["KeepalivePeriod"].as<int>() * 1000;
	_watchdog_answer_timeout = config_watchdog["AnswerTimeout"].as<int>() * 1000;
	_watchdog_power_down_period = config_watchdog["PowerDownPeriod"].as<int>() * 1000;
	_watchdog_boot_delay_period = config_watchdog["BootDelayPeriod"].as<int>() * 1000;
	_watchdog_switch_roles_timeout = config_watchdog["SwitchRolesTimeout"].as<int>() * 1000;
	
	// Imposto single shot su tutti i timer tranne keepalive
	_watchdog_answer_timer.setSingleShot(true);
	_watchdog_power_down_timer.setSingleShot(true);
	_watchdog_boot_delay_timer.setSingleShot(true);
	_watchdog_switch_roles_timer.setSingleShot(true);
	
	// Faccio partire il controllo
	_watchdog_keepalive_timer.start(_watchdog_keepalive_period);
	
	return true;
}
