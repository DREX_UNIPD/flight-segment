//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		17/05/2017
//	Descrizione:		Costanti
//****************************************************************************************************//

// Header del processo
#include <Process/mp.h>

using namespace std;
using namespace drex::processes;

// // Costanti

// Nome del processo
const string MProcess::SHORT_NAME = "MP";
const string MProcess::FULL_NAME = "Master process";

// Comandi
const string MProcess::CMD_HELP = "HELP";
const string MProcess::CMD_KILL = "KILL";
const string MProcess::CMD_GET_STATE = "GET STATE";
const string MProcess::CMD_SET_STATE = "SET STATE";
const string MProcess::CMD_SET_DATE = "SET DATE";

const string MProcess::CMD_DEPLOY = "DEPLOY";

const string MProcess::CMD_RESTORE = "RESTORE";
const string MProcess::CMD_GET_SYSTEM_STATE = "GET SYSTEM STATE";

const string MProcess::CMD_SHUTDOWN = "SHUTDOWN";

// Stati
const string MProcess::ST_DEFAULT = "DEFAULT";
const string MProcess::ST_IDLE = "IDLE";
const string MProcess::ST_BOOT = "BOOT";
const string MProcess::ST_FST = "FST";
const string MProcess::ST_GND = "GND";
const string MProcess::ST_PRE = "PRE";
const string MProcess::ST_SCH = "SCH";
const string MProcess::ST_DEP = "DEP";
const string MProcess::ST_POS = "POS";
const string MProcess::ST_COF = "COF";
const string MProcess::ST_DES = "DES";
const string MProcess::ST_SHD = "SHD";
