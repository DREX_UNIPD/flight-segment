//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		19/09/2017
//	Descrizione:		Ignoro la risposta del comando
//****************************************************************************************************//

// Header del processo
#include <Process/mp.h>

using namespace drex::processes;

// Ricezione dummy delle risposte
void MProcess::ignoreAnswer(const Message &) {
//	_logger->info("Ignore answer message frames: " + std::to_string(msg.length()));
	
//	// Stampo tutti i campi del messaggio
//	for (int i = 0; i < msg.length(); i++) {
//		QByteArray a;
//		a = msg[i];
//		_logger->info("Frame #" + std::to_string(i) + ": " + a.toStdString());
//	}
}
