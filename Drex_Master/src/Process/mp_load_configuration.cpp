//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		17/05/2017
//	Descrizione:		Caricamento della configurazione da file YAML
//****************************************************************************************************//

// Header del processo
#include <Process/mp.h>

// Header Qt
#include <QFileInfo>
#include <QTextStream>

// Header YAML
#include <yaml-cpp/yaml.h>

using namespace std;
using namespace drex::processes;

// Carico la configurazione in base ai parametri forniti
bool MProcess::loadConfiguration(QStringList &paths, YAML::Node &destination) {
	QString file_content;
	
	for (int i = 0; i < paths.count(); i++) {
		QString path = paths[i];
		
		QFileInfo check_file(path);
		
		// Verifico che il file esista
		if (!check_file.exists()) {
			_console->error("Config file \"" + path.toStdString() + "\" don't exists!");
			return false;
		}
		
		if (!check_file.isFile()) {
			_console->error("Config file \"" + path.toStdString() + "\" is not a file!");
			return false;
		}
		
		// Sostituisco le tabulazioni con spazi altrimenti la libreria
		// yaml-cpp crasha
		QFile input_file(path);
		if (!input_file.open(QIODevice::ReadOnly | QIODevice::Text)) {
			_console->error("Configuration file opening failed!");
			return false;
		}
		file_content += QTextStream(&input_file).readAll();
	}
	
	file_content = file_content.replace(QRegularExpression("\\t"), QString("    "));
	
	// Carico la configurazione
	try {
		destination = YAML::Load(file_content.toStdString());
	}
	catch (exception e) {
		_console->error(e.what());
		return false;
	}
	
	_console->info("Configuration loaded");
	
	return true;
}
