//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		05/08/2017
//	Descrizione:		Pubblicazione dei messaggi
//****************************************************************************************************//

// Header del processo
#include <Process/mp.h>

using namespace std;
using namespace nzmqt;
using namespace drex::processes;

void MProcess::publishMessage(const Message &msg) {
	_logger_socket.socket->sendMessage(msg);
}
