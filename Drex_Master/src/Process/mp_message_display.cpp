//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		11/10/2017
//	Descrizione:		Stampa di un messaggio
//****************************************************************************************************//

// Header del processo
#include <Process/mp.h>

using namespace std;
using namespace drex::processes;

void MProcess::printMessage(const Message &msg, bool sender) {
	// Stampo tutti i campi del messaggio
	QByteArray a;
	int start;
	
	// Stampo il mittente
	if (sender) {
		a = msg[0];
		_logger->info((QString("Sender address: 0x") + a.toHex()).toStdString());
		start = 1;
	}
	else start = 0;
	
	for (int i = start; i < msg.length(); i++) {
		a = msg[i];
		_logger->info("Frame #" + std::to_string(i) + ": " + a.toStdString());
	}
}
