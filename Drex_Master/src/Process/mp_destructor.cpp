//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		16/05/2017
//	Descrizione:		Distruttore
//****************************************************************************************************//

// Header del processo
#include <Process/mp.h>

using namespace drex::processes;

// Distruttore
MProcess::~MProcess() {
	// Messaggio informativo
	if (_logger != nullptr) _logger->info("Stopping (" + SHORT_NAME + ") " + FULL_NAME);
	else _console->info("Stopping (" + SHORT_NAME + ") " + FULL_NAME);
}
