//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		03/08/2017
//	Descrizione:		Comando per richiedere lo stato completo della board locale
//****************************************************************************************************//

// Header del processo
#include <Process/mp.h>

using namespace std;
using namespace drex::processes;

bool MProcess::commandGetSystemState(const Message &msg, vector<const char *> &) {
	_logger->info("Command: Get System State");
	
	_gso_local.masterProcessState = QString::fromStdString(_state);
	
	// Invio la risposta
	Message ans;
	ans += msg[0];
	if (_gso_local.isEmpty()) {
		ans += "ERROR";
		ans += "Local global state object empty";
		_command_socket.socket->sendMessage(ans);
		return false;
	}
	ans += "OK";
	ans += _gso_local.serialize().toStdString().c_str();
	_command_socket.socket->sendMessage(ans);
	
	return true;
}
