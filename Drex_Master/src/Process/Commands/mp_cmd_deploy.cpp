//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		23/07/2017
//	Descrizione:		Comando di deployment
//****************************************************************************************************//

// Header del processo
#include <Process/mp.h>

using namespace std;
using namespace drex::processes;

bool MProcess::commandDeploy(const Message &msg, vector<const char *> &parameters) {
	_logger->info("Command: Deploy");
	
	// Verifica della sintassi
	QList<QRegularExpressionMatch> matches;
	if (!verifyCommandSyntax(msg, parameters, matches)) {
		return false;
	}
	
	// Invio la risposta
	Message ans;
	ans += msg[0];
	
	unsigned long duration;
	duration = matches[0].captured("time").toULong();
	if (!setThermalCutter(duration)) {
		ans += "ERROR";
		ans += "Failed to activate thermal cutter";
		_command_socket.socket->sendMessage(ans);
		return false;
	}
	
	ans += "OK";
	_command_socket.socket->sendMessage(ans);
	
	return false;
}

















