//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		11/10/2017
//	Descrizione:		Spegnimento irreversibile della scheda
//****************************************************************************************************//

// Header del processo
#include <Process/mp.h>

using namespace std;
using namespace drex::processes;

bool MProcess::commandShutdown(const Message &msg, std::vector<const char *> &) {
	_logger->info("Command: Shutdown");
	
	// Invio la risposta
	Message ans;
	ans += msg[0];
	ans += "OK";
	_command_socket.socket->sendMessage(ans);
	
	_shutdown_from_remote = true;
	setState(ST_SHD);
	
	return true;
}
