//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		14/10/2017
//	Descrizione:		Comando per il settaggio della data
//****************************************************************************************************//

// Header del processo
#include <Process/mp.h>

// Header Qt
#include <QRegularExpression>

using namespace std;
using namespace drex::processes;

bool MProcess::commandSetDate(const Message &msg, std::vector<const char *> &parameters) {
	// Verifica della sintassi
	QList<QRegularExpressionMatch> matches;
	if (!verifyCommandSyntax(msg, parameters, matches)) {
		return false;
	}
	
	// Prendo la data
	QString date;
	date = matches[0].captured("date");
	
	system((QString("date -s \"") + date + QString("\"")).toStdString().c_str());
	
	// Invio la risposta
	Message ans;
	ans += msg[0];
	ans += "OK";
	_command_socket.socket->sendMessage(ans);
	
	return true;
}
