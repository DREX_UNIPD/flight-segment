//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		23/07/2017
//	Descrizione:		Comando di terminazione del processo
//****************************************************************************************************//

// Header del processo
#include <Process/mp.h>

using namespace std;
using namespace drex::processes;

// Chiusura del processo
bool MProcess::commandKill(const Message &msg, vector<const char *> &) {
	_logger->info("Command: Kill");
	
	// Termino l'applicazione
	killProcess();
	
	// Invio la risposta
	Message ans;
	ans += msg[0];
	ans += "OK";
	_command_socket.socket->sendMessage(ans);
	
	return true;
}
