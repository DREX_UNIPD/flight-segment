//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		17/05/2017
//	Descrizione:		Comando per l'interrogazione dello stato del processo
//****************************************************************************************************//

// Header del processo
#include <Process/mp.h>

using namespace std;
using namespace drex::processes;

// Lettura stato del processo
bool MProcess::commandGetState(const Message &msg, vector<const char *> &) {
	_logger->info("Command: Get State");
	
	// Restituisco lo stato
	string state;
	state = getState();
	
	// Invio la risposta
	Message ans;
	ans += msg[0];
	ans += "OK";
	ans += state.c_str();
	_command_socket.socket->sendMessage(ans);
	
	return true;
}
