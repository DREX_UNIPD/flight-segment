//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		03/08/2017
//	Descrizione:		Comando per richiedere lo stato remoto memorizzato per il restore della board
//****************************************************************************************************//

// Header del processo
#include <Process/mp.h>

using namespace std;
using namespace drex::processes;

// La board remota mi chiede il suo ultimo stato valido
bool MProcess::commandRestore(const Message &msg, vector<const char *> &) {
	_logger->info("Command: Restore");
	
	Message ans;
	ans += msg[0];
	
	// Verifico di avere uno stato remoto completo
	if (_gso_remote.isEmpty()) {
		ans += "ERROR";
		ans += "Empty remote global state object";
		_command_socket.socket->sendMessage(msg);
		return false;
	}
	
	// Invio la risposta
	ans += "OK";
	ans += _gso_remote.serialize().toStdString().c_str();
	_command_socket.socket->sendMessage(ans);
	
	// Resetto il segnale di RESTORE
	Message cmd;
	
	cmd.clear();
	cmd += "SET GPIO VALUE";
	cmd += "NAME = RESTORE";
	cmd += "VALUE = 0";
	_sap_command_socket.socket->sendMessage(cmd);
	
	cmd.clear();
	cmd += "SET GPIO MODE";
	cmd += "NAME = RESTORE";
	cmd += "MODE = INPUT";
	_sap_command_socket.socket->sendMessage(cmd);
	
	return true;
}
