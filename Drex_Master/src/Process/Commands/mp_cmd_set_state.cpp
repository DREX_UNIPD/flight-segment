//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		17/05/2017
//	Descrizione:		Comando per il settaggio dello stato del processo
//****************************************************************************************************//

// Header del processo
#include <Process/mp.h>

// Header Qt
#include <QRegularExpression>

using namespace std;
using namespace drex::processes;
using namespace drex::state;

// Impostazione stato del processo
bool MProcess::commandSetState(const Message &msg, vector<const char *> &parameters) {
	// Cambio lo stato del processo
	_logger->info("Command: Set State");
	
	// Verifica della sintassi
	QList<QRegularExpressionMatch> matches;
	if (!verifyCommandSyntax(msg, parameters, matches)) {
		return false;
	}
	
	// Estraggo il nuovo stato
	string newstate = matches[0].captured("newstate").toUpper().toStdString();
	
	// Messaggio di risposta
	Message ans;
	ans += msg[0];
	
	// Cambio lo stato
	if (!setState(newstate)) {
		ans += "ERROR";
		ans += "On change state";
		_command_socket.socket->sendMessage(ans);
		return false;
	}
	
	// Invio la risposta
	ans += "OK";
	_command_socket.socket->sendMessage(ans);
	
	return true;
}
