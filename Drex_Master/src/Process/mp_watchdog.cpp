//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		29/07/2017
//	Descrizione:		Watchdog, la board verifica il buono stato della board remota e in caso di
//						malfunzionamento interviene resettandola
//****************************************************************************************************//

// Header del processo
#include <Process/mp.h>

// Header DREX
#include <State/state_object.h>

using namespace std;
using namespace drex::processes;
using namespace drex::state;
using namespace nzmqt;

// Remote master process (DEALER)
void MProcess::wdMPReceiveAnswer(const Message &msg) {
	// Sintassi del messaggio
	QStringList syntax;
	
	syntax.append("(?i)^ *(?<answer>\\w+) *$");
	syntax.append("(?i)^ *(?<global_state>\\.+) *$");
	
	QList<QRegularExpressionMatch> matches;
	if (!checkMessageSyntax(msg, syntax, matches)) return;
	
	// Estraggo il nuovo stato
	QString global_state = matches[1].captured("global_state").toUpper();
	
	if (!_gso_remote.deserialize(global_state)) {
		_logger->error("Failed to deserialize global state object");
		return;
	}
	
	// Fermo il timer di timeout
	_logger->info("[Watchdog] Global state received, stopping watchdog timer");
	
	// Se era in attesa di una normale risposta fermo il timer
	if (_watchdog_answer_timer.isActive()) _watchdog_answer_timer.stop();
	
	// Se la board era stata resettata faccio ripartire il timer keepalive che era stato fermato
	if (_watchdog_switch_roles_timer.isActive()) {
		wdRestartKeepalive();
	}
}

// Local sensor acquisition process (SUBSCRIBER)
void MProcess::wdSAPReceiveState(const Message &msg) {
	// Sintassi del messaggio
	QStringList syntax;
	
	syntax.append("(?i)^ *PUBLISHING */ *STATE *$");
	syntax.append("(?i)^ *(?<state>\\w+) *$");
	
	QList<QRegularExpressionMatch> matches;
	if (!checkMessageSyntax(msg, syntax, matches)) return;
	
	// Estraggo lo stato
	QString state = matches[1].captured("state").toUpper();
	_gso_local.sensorAcquisitionProcessState = state;
}
// Local inspection process (SUBSCRIBER)
void MProcess::wdINPReceiveState(const Message &msg) {
	// Sintassi del messaggio
	QStringList syntax;
	
	syntax.append("(?i)^ *PUBLISHING */ *STATE *$");
	syntax.append("(?i)^ *(?<state>\\w+) *$");
	
	QList<QRegularExpressionMatch> matches;
	if (!checkMessageSyntax(msg, syntax, matches)) return;
	
	// Estraggo lo stato
	QString state = matches[1].captured("state").toUpper();
	_gso_local.inspectionProcessState = state;
}
// Local stereovision process (SUBSCRIBER)
void MProcess::wdSVPReceiveState(const Message &msg) {
	// Sintassi del messaggio
	QStringList syntax;
	
	syntax.append("(?i)^ *PUBLISHING */ *STATE *$");
	syntax.append("(?i)^ *(?<state>\\w+) *$");
	
	QList<QRegularExpressionMatch> matches;
	if (!checkMessageSyntax(msg, syntax, matches)) return;
	
	// Estraggo lo stato
	QString state = matches[1].captured("state").toUpper();
	_gso_local.stereoVisionProcessState = state;
}

// Invio messaggio di richiesta stato
void MProcess::wdRequestState() {
	// Invio il messaggio
	Message req;
	req += CMD_GET_SYSTEM_STATE.c_str();
	_watchdog_mp_command_socket.socket->sendMessage(req);
	
	// Faccio partire il timer di sicurezza
	_watchdog_answer_timer.start(_watchdog_answer_timeout);
}

// Timeout del timer di sicurezza
void MProcess::wdReboot() {
	_logger->info("[Watchdog] Reboot");
	
	// Verifico se sono in possesso di uno stato globale dell'altra board
	if (_gso_remote.isEmpty()) {
		_logger->warn("[Watchdog] Failed to reboot: remote global state object empty");
		return;
	}
	
	// Il timer di sicurezza è terminato senza ricevere la risposta, fermo il timer keepalive
	_watchdog_keepalive_timer.stop();
	
	Message msg;
	
	// Imposto il restore signal a 0
	msg.clear();
	msg += "SET GPIO VALUE";
	msg += "NAME = RESTORE";
	msg += "VALUE = 1";
	_watchdog_sap_command_socket.socket->sendMessage(msg);
	
	// Imposto il restore signal in output
	msg.clear();
	msg += "SET GPIO MODE";
	msg += "NAME = RESTORE";
	msg += "MODE = OUTPUT";
	_watchdog_sap_command_socket.socket->sendMessage(msg);
	
	// Tolgo l'alimentazione all'altra scheda e faccio partire il timer
	msg.clear();
	msg += "SET GPIO VALUE";
	msg += "NAME = POWER DISABLE JETSON";
	msg += "VALUE = 1";
	_watchdog_sap_command_socket.socket->sendMessage(msg);
	
	// Faccio partire il timer per il ripristino dell'alimentazione
	_watchdog_power_down_timer.start(_watchdog_power_down_period);
}

void MProcess::wdRestorePower() {
	_logger->info("[Watchdog] Restore power");
	
	Message msg;
	
	// E' terminato il periodo di power down, ripristino la tensione sulla board
	msg.clear();
	msg += "SET GPIO VALUE";
	msg += "NAME = POWER DISABLE JETSON";
	msg += "VALUE = 0";
	_watchdog_sap_command_socket.socket->sendMessage(msg);
	
	// Aspetto il riavvio della scheda
	_watchdog_boot_delay_timer.start(_watchdog_boot_delay_period);
}

void MProcess::wdAfterBootDelay() {
	_logger->info("[Watchdog] After boot delay");
	
	Message msg;
	
	// Invio un'ulteriore richiesta di stato al master remoto
	msg.clear();
	msg += CMD_GET_SYSTEM_STATE.c_str();
	_watchdog_mp_command_socket.socket->sendMessage(msg);
	
	// Faccio partire il timer di sicurezza entro il quale la board remota deve farsi viva
	_watchdog_switch_roles_timer.start(_watchdog_switch_roles_timeout);
}

// Metodo che cambia i ruoli tra backup e main board
void MProcess::wdSwitchRoles() {
	_logger->info("[Watchdog] Switch roles");
	
	Message msg;
	
	// Ripristino il segnale di restore
	msg.clear();
	msg += "SET GPIO VALUE";
	msg += "NAME = RESTORE";
	msg += "VALUE = 0";
	_watchdog_sap_command_socket.socket->sendMessage(msg);
	
	msg.clear();
	msg += "SET GPIO MODE";
	msg += "NAME = RESTORE";
	msg += "MODE = INPUT";
	_watchdog_sap_command_socket.socket->sendMessage(msg);
	
	// Scambio i ruoli delle board se questa è la backup
	if (!_main_board) {
		_logger->warn("[Watchdog] Switched roles between backup and main board");
		
		msg.clear();
		msg += "SET STATE";
		msg += _gso_remote.sensorAcquisitionProcessState.toStdString().c_str();
		_sap_command_socket.socket->sendMessage(msg);
		
		msg.clear();
		msg += "SET STATE";
		msg += _gso_remote.inspectionProcessState.toStdString().c_str();
		_inp_command_socket.socket->sendMessage(msg);
		
		msg.clear();
		msg += "SET STATE";
		msg += _gso_remote.stereoVisionProcessState.toStdString().c_str();
		_svp_command_socket.socket->sendMessage(msg);
		
		msg.clear();
		msg += "SET STATE";
		msg += _gso_remote.masterProcessState.toStdString().c_str();
		_mp_command_self_socket.socket->sendMessage(msg);
	}
	else {
		_logger->error("[Watchdog] Backup board lost");
	}
}

// Metodo che va chiamato quando viene richiesto un ripristino dello stato
void MProcess::wdRestartKeepalive() {
	Message msg;
	
	// Ripristino il segnale di restore
	msg.clear();
	msg += "SET GPIO VALUE";
	msg += "NAME = RESTORE";
	msg += "VALUE = 0";
	_watchdog_sap_command_socket.socket->sendMessage(msg);
	
	msg.clear();
	msg += "SET GPIO MODE";
	msg += "NAME = RESTORE";
	msg += "MODE = INPUT";
	_watchdog_sap_command_socket.socket->sendMessage(msg);
	
	// Fermo il timer di scambio ruoli
	_watchdog_switch_roles_timer.stop();
	
	// Faccio ripartire il timer di interrogazione dello stato
	_watchdog_keepalive_timer.start(_watchdog_keepalive_period);
}
