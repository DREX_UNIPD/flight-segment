//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		17/05/2017
//	Descrizione:		Verifica della sintassi del comando
//****************************************************************************************************//

// Header del processo
#include <Process/mp.h>

// Header Qt
#include <QRegularExpression>

using namespace std;
using namespace drex::processes;

bool MProcess::verifyCommandSyntax(const Message &msg, const vector<const char *> &syntax, QList<QRegularExpressionMatch> &matches) {
	// Messaggio di risposta
	Message ans;
	ans += msg[0];
	
	// Verifico che il numero di frame sia corretto
	int syntax_size = (int) syntax.size();
	if (msg.length() < syntax_size + 2) {
		ans += "ERROR";
		
		string str_error;
		str_error += "Invalid syntax: expected ";
		str_error += to_string(syntax.size() + 1).c_str();
		str_error += " frames, got ";
		str_error += to_string(msg.length()).c_str();
		ans += str_error.c_str();
		
		_logger->error(str_error);
		
		_command_socket.socket->sendMessage(ans);
		return false;
	}
	
	// Per ogni frame verifico il matching e lo salvo nella lista dei match
	for (int i = 0; i < syntax_size; i++) {
		// Interpreto i parametri del comando
		QString parameters = msg[i + 2];
		QRegularExpression rx_command(syntax[i]);
		QRegularExpressionMatch match = rx_command.match(parameters);
		matches.append(match);
	
		// Verifico la sintassi del comando
		if (!match.hasMatch()) {
			ans += "ERROR";
			
			string str_error;
			str_error += "Invalid syntax at frame(";
			str_error += to_string(i + 1).c_str();
			str_error += "): expression must match \"";
			str_error += syntax[i];
			str_error += "\"";
			ans += str_error.c_str();
			
			_logger->error(str_error);
			
			_command_socket.socket->sendMessage(ans);
			return false;
		}
	}
	
	return true;
}
