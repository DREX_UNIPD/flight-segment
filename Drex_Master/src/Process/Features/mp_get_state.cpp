//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		22/07/2017
//	Descrizione:		Richiesta dello stato attuale
//****************************************************************************************************//

// Header del processo
#include <Process/mp.h>

using namespace std;
using namespace drex::processes;
using namespace drex::state;

string MProcess::getState() {
	_logger->info("Get state");
	
	return _state;
}
