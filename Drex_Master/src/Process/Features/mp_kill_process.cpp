//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		23/07/2017
//	Descrizione:		Terminazione del processo
//****************************************************************************************************//

// Header del processo
#include <Process/mp.h>

using namespace drex::processes;

// Metodo per la terminazione del processo
bool MProcess::killProcess() {
	_logger->info("Kill process");
	
	// Fermo tutti i timer del watchdog
	_watchdog_power_down_timer.stop();
	_watchdog_answer_timer.stop();
	_watchdog_switch_roles_timer.stop();
	_watchdog_keepalive_timer.stop();
	
	// Mando il segnale di chiusura del processo
	QTimer::singleShot(0, this, &MProcess::quit);
	
	return true;
}
