//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		23/07/2017
//	Descrizione:		Gestione del thermal cutter
//****************************************************************************************************//

// Header del processo
#include <Process/mp.h>

using namespace std;
using namespace drex::processes;
 
bool MProcess::setThermalCutter(unsigned long duration = 0) {
	_logger->info("Set thermal cutter");
	
	// Attivo il thermal cutter
	Message cmd;
	cmd += "SET PAM";
	cmd += (string("DAC = ") + _deployment_main_dac).c_str();
	cmd += (string("CURRENT = ") + to_string(_deployment_main_current)).c_str();
	cmd += "PERSISTENT = FALSE";
	_sap_command_socket.socket->sendMessage(cmd);
	
	if (duration > 0) {
		QTimer::singleShot(duration, this, &MProcess::resetThermalCutter);
	}
	
	return true;
}

bool MProcess::resetThermalCutter() {
	_logger->info("Reset thermal cutter");
	
	Message cmd;
	cmd += "SET PAM";
	cmd += (string("DAC = ") + _deployment_main_dac).c_str();
	cmd += "CURRENT = 0.0";
	cmd += "PERSISTENT = FALSE";
	_sap_command_socket.socket->sendMessage(cmd);
	
	return true;
}

//bool MProcess::getThermalCutter(bool &value) {
//	_logger->info("Get thermal cutter");
	
//	GPIOValue pinvalue;
	
//	if (!GPIOHelper::read(GPIOPin::ThermalCutter, pinvalue)) {
//		return false;
//	}
	
//	value = (pinvalue == GPIOValue::HIGH);
	
//	return true;
//}
