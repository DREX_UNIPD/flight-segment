//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		16/05/17
//	Descrizione:		Classe per processo MP
//****************************************************************************************************//

#ifndef MP_PROCESS_H
#define MP_PROCESS_H

// Header per logging
#include <spdlog/spdlog.h>

// Header nzmqt
#include <nzmqt/nzmqt.hpp>

// Header Qt
#include <QCoreApplication>
#include <QSettings>
#include <QMap>

// Header YAML
#include <yaml-cpp/yaml.h>

// Header DREX
#include <State/state_object.h>
#include <Socket/socket_utils.h>

//----------------------------------------------------//

namespace drex {
namespace processes {

// Classe del processo MP
class MProcess : public QCoreApplication {
	Q_OBJECT
	
	using super = QCoreApplication;
	using Message = QList<QByteArray>;
	using Logger = spdlog::logger;
	
private:
	//*****************//
	// Sezione privata
	//*****************//
	
	// // Variabili
	
	// Logging
	
	// Logger su console
	std::shared_ptr<Logger> _console;
	
	// Logger su file e console
	std::shared_ptr<Logger> _logger;
	
	// Configurazione
	
	// Configurazione del processo
	YAML::Node _configuration;
	
	// Storage
	
	// Percorso base
	QDir _storage_home;
	QDir _storage_ssd_dir;
	QDir _storage_log_dir;
	
	// Socket
	
	// Contesto per i socket
	nzmqt::ZMQContext *_context;
	
	// Command (ROUTER)
	drex::socket::SocketInfo _command_socket;
	// Logger (PUBLISHER)
	drex::socket::SocketInfo _logger_socket;
	// Data (PUBLISHER)
	drex::socket::SocketInfo _data_socket;
	
	// // SAP
	// Command (DEALER)
	drex::socket::SocketInfo _sap_command_socket;
	// Data (SUBSCRIBER)
	drex::socket::SocketInfo _sap_data_socket;
	
	// // INP
	// Command (DEALER)
	drex::socket::SocketInfo _inp_command_socket;
	
	// // SVP
	// Command (DEALER)
	drex::socket::SocketInfo _svp_command_socket;
	
	// // MP
	// Command (REQUEST)
	drex::socket::SocketInfo _mp_command_socket;
	
	// // MP Self
	// Command (REQUEST)
	drex::socket::SocketInfo _mp_command_self_socket;
	
	// // GSS Command
	// Command (DEALER)
	drex::socket::SocketInfo _gss_command_socket;
	
	// Gestione degli stati
	
	// Stato
	std::string _state;
	
	// Timer per la pubblicazione dello stato
	bool _publishing_enabled;
	
	// Pubblicazione dello stato
	bool _publishing_state_enabled;
	int _publishing_state_period;
	QTimer _publishing_state_timer;
	
	// Comandi
	QMap<std::string, drex::state::StateObject *> _state_objects;
	
	// Identificatore main board
	bool _main_board;
	
	// Modalità manuale
	bool _manual_mode;
	
	// Modalità manuale per la stereo
	bool _manual_mode_stereovision;
	
	// // Metodi
	
	// Caricamento della configurazione
	bool loadConfiguration(QStringList &paths, YAML::Node &destination);
	
	// Variabili per i segnali di sistema
	QSocketNotifier *_sn_hup;
	QSocketNotifier *_sn_int;
	QSocketNotifier *_sn_term;
	int *_sig_hup_fd;
	int *_sig_int_fd;
	int *_sig_term_fd;
	
	//**********//
	// Features
	//**********//
	
	// Metodo per la terminazione del processo
	bool killProcess();
	
	// Metodi per la gestione dello stato
	std::string getState();
	bool setState(std::string newstate);
	
	// Metodi per la gestione del thermal cutter
bool setThermalCutter(unsigned long duration);
private slots:
	bool resetThermalCutter();
private:
//	bool getThermalCutter(bool &value);
	
	// Metodi per l'impostazione e il ritrovamento dello stato globale del sistema
	state::GlobalStateObject _gso_local, _gso_remote;
	
	// Aggiornamento della data
	bool _date_updated;
	QTimer _date_update_timer;
	int _date_update_period;
	bool _date_update_enabled;
	
	// Invio della richiesta di aggiornamento della data
	void requestDateUpdate();
	
	// Ricezione dei messaggi da ground station
	void receiveDateUpdate(const Message &msg);
	
	//******************//
	// Metodi ausiliari
	//******************//
	
	// Verifica della sintassi del messaggio
	bool checkMessageSyntax(const Message &msg, const QStringList &syntax, QList<QRegularExpressionMatch> &matches);
	
	// Verifica della sintassi del comando ed eventuale risposta
	bool verifyCommandSyntax(const Message &msg, const std::vector<const char *> &syntax, QList<QRegularExpressionMatch> &matches);
	
	// Stampa di un messaggio
	void printMessage(const Message &msg, bool sender = false);
	
	// Inizializzazioni
	
	// Metodo principale
	void init();
	
	// Inizializzazione dello storage
	bool initStorage();
	
	// Inizializzazione del logger su file
	bool initFileLogger();
	
	// Inizializzazione Watchdog
	bool initWatchdog();
	
	// Inizializzazione dell'interfaccia di rete
	bool initNetworking();
	
	// Inizializzazione degli stati
	bool initStatesMap();
	
	// Inizializzazione della pubblicazione
	bool initPublishing();
	
	// Inizializzazione dell'aggiornamento della data
	bool initDateUpdate();
	
	// Inizializzazione dei socket
	bool initSocket(drex::socket::SocketInfo &si, const YAML::Node &node, std::string name, int linger);
	bool startSocket(drex::socket::SocketInfo &si);
	
	//**********************//
	// Metodi per gli stati
	//**********************//
	
	// Variabili globali per gli stati
	
	// Abilitazioni fonte altitudine
	bool _barometer_altitude_enable;
	bool _vacuometer_altitude_enable;
	
	// Metodi e variabili degli stati
	
	// IDLE
	bool stateIdleInit();
	bool stateIdleCleanup();
	
	// BOOT
	int _boot_inspection_start_delay;
	bool stateBootInit();
	void stateBootReceiveGPIO(const Message &msg);
	bool stateBootCleanup();
	
	// FST
	double _first_altitude_limit;
	bool stateFstInit();
	void stateFstReceiveSensorsData(const Message &msg);
	bool stateFstCleanup();

	// GND
	double _ground_altitude_limit;
	bool stateGndInit();
	void stateGndReceiveSensorsData(const Message &msg);
	bool stateGndCleanup();
	
	// PRE
	double _predeployment_altitude_limit;
	double _predeployment_timeout;
	QTimer _predeployment_timer;
	bool statePreInit();
	void statePreReceiveSensorsData(const Message &msg);
	void statePreTimeout();
	bool statePreCleanup();
	
	// SCH
	int _statuscheck_period;
	QTimer _statuscheck_timer;
	int _statuscheck_signaling_interval;
	int _statuscheck_countdown;
	bool stateSchInit();
	void stateSchTimeout();
	bool stateSchCleanup();
	
	// DEP
	bool _deployment_only_main_thermalcutter;
	int _deployment_main_thermalcutter_period;
	int _deployment_main_timeout;
	QTimer _deployment_main_timer;
	int _deployment_backup_thermalcutter_period;
	int _deployment_backup_timeout;
	QTimer _deployment_backup_timer;
	std::string _deployment_main_dac;
	double _deployment_main_current;
	bool stateDepInit();
	void stateDepReceiveMicroswitch(const Message &msg);
	void stateDepReceiveMainCommandAnswer(const Message &msg);
	void stateDepReceiveBackupCommandAnswer(const Message &msg);
	void stateDepMainTimeout();
	void stateDepBackupTimeout();
	bool stateDepCleanup();
	
	// POS
	int _postdeployment_safety_timeout;
	QTimer _postdeployment_safety_timer;
	int _postdeployment_signaling_interval;
	int _postdeployment_countdown;
	bool statePosInit();
	void statePosReceiveSensorsData(const Message &msg);
	void statePosTimeout();
	bool statePosCleanup();
	
	// COF
	int _cutoff_period;
	QTimer _cutoff_timer;
	int _cutoff_signaling_interval;
	int _cutoff_countdown;
	bool stateCofInit();
	void stateCofTimeout();
	bool stateCofCleanup();
	
	// DES
	int _descending_safety_timeout;
	int _descending_nominal_timeout;
	QTimer _descending_timer;
	double _descending_altitude_limit;
	int _descending_signaling_interval;
	int _descending_countdown;
	bool _descending_safety_flag;
	bool stateDesInit();
	void stateDesReceiveSensorsData(const Message &msg);
	void stateDesTimeout();
	bool stateDesCleanup();
	
	// SHD
	bool _shutdown_from_remote;
	QTimer _shutdown_timer;
	int _shutdown_timeout;
	bool _shutdown_sap_closed;
	bool _shutdown_inp_closed;
	QList<QDir> _shutdown_inp_trigger_dirs;
	bool _shutdown_svp_closed;
	bool stateShdInit();
	void stateShdReceiveSAPAnswer(const Message &msg);
	void stateShdReceiveINPAnswer(const Message &msg);
	bool stateShdGetNextFreeSlot(QDir &dir, int &index);
	bool stateShdTrigger(QDir &dir, int index, QString command);
	void stateShdReceiveSVPAnswer(const Message &msg);
	void stateShdTerminate();
	bool stateShdCleanup();
	
private slots:
	//**********************//
	// Selettore di comandi
	//**********************//
	
	// Seleziona ed esegue il comando appropriato
	void commandSelector(const Message &msg);
	
	//******************//
	// Comandi standard
	//******************//
	
	// Aiuto
	bool commandHelp(const Message &msg, std::vector<const char *> &parameters);
	
	// Chiusura del processo
	bool commandKill(const Message &msg, std::vector<const char *> &parameters);
	
	// Lettura stato del processo
	bool commandGetState(const Message &msg, std::vector<const char *> &parameters);
	
	// Impostazione stato del processo
	bool commandSetState(const Message &msg, std::vector<const char *> &parameters);
	
	// Settaggio della data
	bool commandSetDate(const Message &msg, std::vector<const char *> &parameters);
	
	//*******************//
	// Comandi specifici
	//*******************//
	
	// Comando per l'attivazione del thermal cutter
	bool commandDeploy(const Message &msg, std::vector<const char *> &parameters);
	
	// Comandi per il watchdog
	bool commandRestore(const Message &msg, std::vector<const char *> &parameters);
	bool commandGetSystemState(const Message &msg, std::vector<const char *> &parameters);
	
	bool commandShutdown(const Message &msg, std::vector<const char *> &parameters);
	
	//***************//
	// Pubblicazione
	//***************//
	
	// Pubblicazione dello stato
	void publishingState();
	
	//**********//
	// Watchdog
	//**********//
	
	// Flag per l'abilitazione del watchdog
private:
	bool _watchdog_enabled;
	
	// Socket
	drex::socket::SocketInfo _watchdog_mp_command_socket;
	drex::socket::SocketInfo _watchdog_sap_command_socket;
	drex::socket::SocketInfo _watchdog_sap_data_socket;
	drex::socket::SocketInfo _watchdog_inp_signaling_socket;
	drex::socket::SocketInfo _watchdog_svp_data_socket;
	
private slots:
	// Callback dei socket
	void wdMPReceiveAnswer(const Message &msg);
	void wdSAPReceiveState(const Message &msg);
	void wdINPReceiveState(const Message &msg);
	void wdSVPReceiveState(const Message &msg);
	
	// Callbacks dei timer
	void wdRequestState();
	void wdReboot();
	void wdRestorePower();
	void wdAfterBootDelay();
	void wdSwitchRoles();
	void wdRestartKeepalive();
	
private:
	// Parametri dei timer
	int _watchdog_answer_timeout;
	int _watchdog_keepalive_period;
	int _watchdog_power_down_period;
	int _watchdog_boot_delay_period;
	int _watchdog_switch_roles_timeout;
	
	// Oggetti timer
	QTimer _watchdog_answer_timer;
	QTimer _watchdog_keepalive_timer;
	QTimer _watchdog_power_down_timer;
	QTimer _watchdog_boot_delay_timer;
	QTimer _watchdog_switch_roles_timer;
	
public slots:
	// Gestori dei segnali di sistema di Qt
	void handleSigHup();
	void handleSigInt();
	void handleSigTerm();
	
	// Pubblicazione di un messaggio tramite socket definito in questa classe
	void publishMessage(const Message &msg);
	
	// Pubblicazione di un messaggio di Log
	void publishLog(const Message &msg);
	
	// Ricezione dummy delle risposte
	void ignoreAnswer(const Message &msg);
	
signals:
	//*********//
	// Segnali
	//*********//
	
	// Richiesta di cambio stato
	void requestSetState(const Message &msg);
	
public:
	//******************//
	// Sezione pubblica
	//******************//
	
	// // Costanti
	
	// Nome del processo
	static const std::string SHORT_NAME;
	static const std::string FULL_NAME;
	
	// Comandi
	static const std::string CMD_HELP;
	static const std::string CMD_KILL;
	static const std::string CMD_GET_STATE;
	static const std::string CMD_SET_STATE;
	static const std::string CMD_SET_DATE;
	
	static const std::string CMD_SET_T0;
	static const std::string CMD_GET_T0;
	
	static const std::string CMD_DEPLOY;
	
	static const std::string CMD_RESTORE;
	static const std::string CMD_GET_SYSTEM_STATE;
	
	static const std::string CMD_SHUTDOWN;
	
	// Stati
	static const std::string ST_DEFAULT;
	static const std::string ST_IDLE;
	static const std::string ST_BOOT;
	static const std::string ST_FST;
	static const std::string ST_GND;
	static const std::string ST_PRE;
	static const std::string ST_SCH;
	static const std::string ST_DEP;
	static const std::string ST_POS;
	static const std::string ST_COF;
	static const std::string ST_DES;
	static const std::string ST_SHD;
	
	// // Metodi
	
	// Costruttore
	explicit MProcess(int &argc, char **argv);
	
	// Distruttore
	~MProcess() override;
	
	// Notifica evento
	bool notify(QObject *obj, QEvent *event) override;
	
	// Installazione signal handlers
	void setupSignalHandlers(int *p_hup, int *p_int, int *p_term);
};

}
}

#endif // MP_PROCESS_H

