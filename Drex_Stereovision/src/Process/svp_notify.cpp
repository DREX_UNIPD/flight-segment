//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		06/08/2017
//	Descrizione:		Funzione di notifica di QObject
//****************************************************************************************************//

// Header del processo
#include <Process/svp.h>

using namespace drex::processes;

// Notifica evento
bool SVProcess::notify(QObject *obj, QEvent *event) {
	try {
		return super::notify(obj, event);
	}
	catch (std::exception &ex) {
		_logger->error(ex.what());
		return false;
	}
}
