//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		06/08/2017
//	Descrizione:		Costanti
//****************************************************************************************************//

// Header del processo
#include <Process/svp.h>

using namespace std;
using namespace drex::processes;

// // Costanti

// Nome del processo
const string SVProcess::SHORT_NAME = "SVP";
const string SVProcess::FULL_NAME = "Stereovision process";

// Comandi
const string SVProcess::CMD_HELP = "HELP";
const string SVProcess::CMD_KILL = "KILL";
const string SVProcess::CMD_GET_STATE = "GET STATE";
const string SVProcess::CMD_SET_STATE = "SET STATE";

const string SVProcess::CMD_SAVE_IMAGE = "SAVE IMAGE";
const string SVProcess::CMD_START_CAPTURING = "START CAPTURING";
const string SVProcess::CMD_STOP_CAPTURING = "STOP CAPTURING";
const string SVProcess::CMD_START_STEREOVISION = "START STEREOVISION";
const string SVProcess::CMD_STOP_STEREOVISION = "STOP STEREOVISION";
const string SVProcess::CMD_STEREOMATCH = "STEREOMATCH";

// Stati
const string SVProcess::ST_DEFAULT = "DEFAULT";
const string SVProcess::ST_IDLE = "IDLE";
const string SVProcess::ST_RUN = "RUN";

// Comandi per il processo di stereovisione
const string SVProcess::SV_IMAGE = "IMAGE";
const string SVProcess::SV_START = "START";
const string SVProcess::SV_STOP = "STOP";
