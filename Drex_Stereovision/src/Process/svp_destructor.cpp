//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		06/08/2017
//	Descrizione:		Distruttore
//****************************************************************************************************//

// Header del processo
#include <Process/svp.h>

using namespace drex::processes;

// Distruttore
SVProcess::~SVProcess() {
	// Messaggio informativo
	if (_logger != nullptr) _logger->info("Stopping (" + SHORT_NAME + ") " + FULL_NAME);
	else _console->info("Stopping (" + SHORT_NAME + ") " + FULL_NAME);
}
