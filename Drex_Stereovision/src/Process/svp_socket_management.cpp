//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		06/08/2017
//	Descrizione:		Configurazione della struttura socket info
//****************************************************************************************************//

// Header del processo
#include <Process/svp.h>

using namespace std;
using namespace drex::processes;
using namespace drex::socket;
using namespace nzmqt;

// Inizializzo il socket usando le informazioni del file di configurazione
bool SVProcess::initSocket(SocketInfo &si, const YAML::Node &node, string name, int linger) {
	ZMQSocket::Type type;
	type = (ZMQSocket::Type) node["Type"].as<int>();
	
	si.socket = _context->createSocket(type, this);
	
	si.ip = node["IP"].as<string>();
	si.port = node["Port"].as<int>();
	si.connect = node["Connect"].as<bool>();
	si.socket->setLinger(linger);
	si.socket->setObjectName(QString::fromStdString(name));
	
	if (type == ZMQSocket::TYP_SUB) {
		// Inserisco un filtro vuoto per ricevere tutti i messaggi
		si.socket->setOption(ZMQSocket::OPT_SUBSCRIBE, "");
	}
	
	_logger->info("Socket " + name + " created");
	
	return true;
}

bool SVProcess::startSocket(SocketInfo &si) {
	string endpoint = string("tcp://") + si.ip + string(":") + std::to_string(si.port);
	
	if (si.connect) {
		// Connessione
		si.socket->connectTo(endpoint.c_str());
		_logger->info("Socket connected to " + endpoint);
	}
	else {
		// Binding
		si.socket->bindTo(endpoint.c_str());
		_logger->info("Socket bound to " + endpoint);
	}
	
	return true;
}

