//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		23/09/2017
//	Descrizione:		Inizializzazione della pubblicazione dei dati del processo
//****************************************************************************************************//

// Header del processo
#include <Process/svp.h>

using namespace std;
using namespace drex::processes;

// Inizializzazione della pubblicazione
bool SVProcess::initPublishing() {
	_logger->info("Init publishing");
	
	const YAML::Node &config_publishing = _configuration["Publishing"];
	_publishing_enabled = config_publishing["Enabled"].as<bool>();
	
	if (!_publishing_enabled) return true;
	
	const YAML::Node &config_state = config_publishing["State"];
	_publishing_state_enabled = config_state["Enabled"].as<bool>();
	_publishing_state_period = config_state["Interval"].as<int>() * 1000;
	
	if (_publishing_state_enabled) {
		QObject::connect(&_publishing_state_timer, &QTimer::timeout, this, &SVProcess::publishingState, Qt::QueuedConnection);
		_publishing_state_timer.start(_publishing_state_period);
	}
	
	return true;
}
