//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		15/09/2017
//	Descrizione:		Inizializzazione dello storage
//****************************************************************************************************//

// Header del processo
#include <Process/svp.h>

using namespace std;
using namespace drex::processes;

// Inizializzo lo storage
bool SVProcess::initStorage() {
	// Leggo i parametri dello storage
	const YAML::Node &config_storage = _configuration["Storage"];
	
	// Carico le cartelle dello storage e le creo se non esistono
	auto create_dir = [this] (QDir &base, string path, string errmsg) {
		QString p = QString::fromStdString(path);
		if (!base.cd(p)) {
			if (!base.mkpath(p)) {
				_console->error(errmsg);
				return false;
			}
			base.cd(p);
		}
		return true;
	};
	
	// Dimensione massima dei file di storage della depth map
	_storage_filesize_limit = config_storage["FilesizeLimit"].as<qint64>();
	
	_storage_home = QDir(config_storage["Common"]["Home"].as<string>().c_str());
	_storage_ssd_dir = _storage_home;
//	_storage_ram_dir = _storage_home;
	
	// Verifico che esista la cartella SSD
	if (!_storage_ssd_dir.cd(QString::fromStdString(config_storage["Common"]["SSD"].as<string>()))) {
		_console->error("SSD folder doesn't exists");
		return false;
	}
	
//	// Verifico che esista la cartella RAM
//	if (!_storage_ram_dir.cd(QString::fromStdString(config_storage["Common"]["RAM"].as<string>()))) {
//		_console->error("RAM folder doesn't exists");
//		return false;
//	}
	
	if (!create_dir(_storage_ssd_dir, config_storage["Process"].as<string>(), "Failed to create SSD process folder")) return false;
//	if (!create_dir(_storage_ram_dir, config_storage["Process"].as<string>(), "Failed to create RAM process folder")) return false;
	
//	_storage_image_dir = _storage_ssd_dir;
	_storage_depth_map_dir = _storage_ssd_dir;
	_storage_log_dir = _storage_ssd_dir;
	
//	if (!create_dir(_storage_image_dir, config_storage["Image"].as<string>(), "Failed to create Image folder")) return false;
	if (!create_dir(_storage_depth_map_dir, config_storage["DepthMap"].as<string>(), "Failed to create Depth Map folder")) return false;
	if (!create_dir(_storage_log_dir, config_storage["Log"].as<string>(), "Failed to create Log folder")) return false;
	
	_storage_left_triggers_dir = _storage_ssd_dir;
	_storage_left_images_dir = _storage_ssd_dir;
	_storage_right_triggers_dir = _storage_ssd_dir;
	_storage_right_images_dir = _storage_ssd_dir;
	_storage_both_triggers_dir = _storage_ssd_dir;
	_storage_both_images_dir = _storage_ssd_dir;
	
	// Cartelle per il salvataggio delle immagini
	const YAML::Node &left_camera = _configuration["Stereovision"]["Left"];
	const YAML::Node &right_camera = _configuration["Stereovision"]["Right"];
	const YAML::Node &both_camera = _configuration["Stereovision"]["Both"];
	
	// Left
	if (!create_dir(
		_storage_left_triggers_dir,
		left_camera["Triggers"].as<string>(),
		"Failed to create left camera triggers folder"
	)) return false;
	if (!create_dir(
		_storage_left_images_dir,
		left_camera["Images"].as<string>(),
		"Failed to create left camera images folder"
	)) return false;
	
	// Right
	if (!create_dir(
		_storage_right_triggers_dir,
		right_camera["Triggers"].as<string>(),
		"Failed to create right camera triggers folder"
	)) return false;
	if (!create_dir(
		_storage_right_images_dir,
		right_camera["Images"].as<string>(),
		"Failed to create right camera images folder"
	)) return false;
	
	// Both
	if (!create_dir(
		_storage_both_triggers_dir,
		both_camera["Triggers"].as<string>(),
		"Failed to create both cameras triggers folder"
	)) return false;
	if (!create_dir(
		_storage_both_images_dir,
		both_camera["Images"].as<string>(),
		"Failed to create both cameras images folder"
	)) return false;
	
	return true;
}
