//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		06/08/2017
//	Descrizione:		Inizializzazione degli stati
//****************************************************************************************************//

// Header del processo
#include <Process/svp.h>

using namespace std;
using namespace drex::processes;
using namespace drex::state;

bool SVProcess::initStatesMap() {
	_logger->info("Init states map");
	
	// Elimino l'eventuale contenuto della mappa
	_state_objects.clear();
	
	// Popolo la mappa stato -> StateObject
	StateObject *so;
	
	// DEFAULT
	so = new StateObject();
	so->init = [this] () { return true; };
	
	// Help
	so->command.insert(CMD_HELP, [this] (const Message &msg, vector<const char *> &parameters) { return this->commandHelp(msg, parameters); });
	so->parameters.insert(CMD_HELP, vector<const char *>());
	
	// Kill
	so->command.insert(CMD_KILL, [this] (const Message &msg, vector<const char *> &parameters) { return this->commandKill(msg, parameters); });
	so->parameters.insert(CMD_KILL, vector<const char *>());
	
	// Get state
	so->command.insert(CMD_GET_STATE, [this] (const Message &msg, vector<const char *> &parameters) { return this->commandGetState(msg, parameters); });
	so->parameters.insert(CMD_GET_STATE, vector<const char *>());
	
	// Set state
	so->command.insert(CMD_SET_STATE, [this] (const Message &msg, vector<const char *> &parameters) { return this->commandSetState(msg, parameters); });
	so->parameters.insert(CMD_SET_STATE, vector<const char *>({
		"(?i)^ *(?<newstate>IDLE|RUN) *$"
	}));
	
	// Start stereovision
	so->command.insert(CMD_START_STEREOVISION, [this] (const Message &msg, vector<const char *> &parameters) { return this->commandStartStereovision(msg, parameters); });
	so->parameters.insert(CMD_START_STEREOVISION, vector<const char *>());
	
	// Stop stereovision
	so->command.insert(CMD_STOP_STEREOVISION, [this] (const Message &msg, vector<const char *> &parameters) { return this->commandStopStereovision(msg, parameters); });
	so->parameters.insert(CMD_STOP_STEREOVISION, vector<const char *>());
	
	// Stereomatch
	so->command.insert(CMD_STEREOMATCH, [this] (const Message &msg, vector<const char *> &parameters) { return this->commandStereoMatch(msg, parameters); });
	so->parameters.insert(CMD_STEREOMATCH, vector<const char *>({
		"(?i)^ *LEFT *= *(?<file>(?:/(?:\\w+))+\\.\\w+) *$",
		"(?i)^ *RIGHT *= *(?<file>(?:/(?:\\w+))+\\.\\w+) *$"
	}));
	
	so->cleanup = [this] () { return true; };
	_state_objects.insert(SVProcess::ST_DEFAULT, so);
	
	// IDLE
	so = new StateObject();
	so->init = [this] () { return this->stateIdleInit(); };
	so->cleanup = [this] () { return this->stateIdleCleanup(); };
	_state_objects.insert(SVProcess::ST_IDLE, so);
	
	// RUN
	so = new StateObject();
	so->init = [this] () { return this->stateRunInit(); };
	
	// Save image
	so->command.insert(CMD_SAVE_IMAGE, [this] (const Message &msg, vector<const char *> &parameters) { return this->commandSaveImage(msg, parameters); });
	so->parameters.insert(CMD_SAVE_IMAGE, vector<const char *>({
		"(?i)^ *CAMERA *= *(?<camera>LEFT|RIGHT) *$"
	}));
	
	// Start capturing
	so->command.insert(CMD_START_CAPTURING, [this] (const Message &msg, vector<const char *> &parameters) { return this->commandStartCapturing(msg, parameters); });
	so->parameters.insert(CMD_START_CAPTURING, vector<const char *>({
		"(?i)^ *CAMERA *= *(?<camera>LEFT|RIGHT) *$"
	}));
	
	// Stop capturing
	so->command.insert(CMD_STOP_CAPTURING, [this] (const Message &msg, vector<const char *> &parameters) { return this->commandStopCapturing(msg, parameters); });
	so->parameters.insert(CMD_STOP_CAPTURING, vector<const char *>({
		"(?i)^ *CAMERA *= *(?<camera>LEFT|RIGHT) *$"
	}));
	
	so->cleanup = [this] () { return this->stateRunCleanup(); };
	_state_objects.insert(SVProcess::ST_RUN, so);
	
	return true;
}



