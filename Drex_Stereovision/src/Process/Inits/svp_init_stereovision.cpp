//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		23/09/2017
//	Descrizione:		Inizializzazione della stereovisione
//****************************************************************************************************//

// Header del processo
#include <Process/svp.h>

using namespace std;
using namespace drex::processes;

// Inizializzazione della streovisione
bool SVProcess::initStereovision() {
	_logger->info("Init stereovision");
	
	_stereovision_running = false;
	
	const YAML::Node &config_stereovision = _configuration["Stereovision"];
	_stereovision_enabled = config_stereovision["Enabled"].as<bool>();
	_stereovision_filename = QString::fromStdString(config_stereovision["DepthMapFilename"].as<string>());
	if (!_stereovision_enabled) return true;
	_stereovision_divider = config_stereovision["Divider"].as<int>();
	_stereovision_delay = config_stereovision["Delay"].as<int>();
	_stereovision_capture_process = QString::fromStdString(config_stereovision["CaptureProcess"].as<string>());
	_stereovision_image_check_period = config_stereovision["ImageCheckPeriod"].as<int>();
	
	// Parametri ottici
	const YAML::Node &camera_matrix = config_stereovision["CameraMatrix"];
	_stereovision_camera_matrix_fx = camera_matrix["fx"].as<double>();
	_stereovision_camera_matrix_fy = camera_matrix["fy"].as<double>();
	_stereovision_camera_matrix_cx = camera_matrix["cx"].as<double>();
	_stereovision_camera_matrix_cy = camera_matrix["cy"].as<double>();
	
	// Aruco
	_stereovision_aruco_dictionary = config_stereovision["ArucoDictionary"].as<string>();
	_stereovision_marker_size = config_stereovision["MarkerSize"].as<float>();
	
	// Coefficienti di distorsione
	_stereovision_distortion_coefficients = config_stereovision["DistortionCoefficients"].as<vector<double>>();
	
	// Dimensioni dell'immagine
	_stereovision_image_width = config_stereovision["Width"].as<int>();
	_stereovision_image_height = config_stereovision["Height"].as<int>();
	
	// Dimensioni della scalebar
	const YAML::Node &scalebar = config_stereovision["Scalebar"];
	_stereovision_scalebar_height = scalebar["Width"].as<float>();
	_stereovision_scalebar_width = scalebar["Height"].as<float>();
	
	// Left camera
	const YAML::Node &config_left = config_stereovision["Left"];
	_stereovision_left_camera_name = QString::fromStdString(config_left["Name"].as<string>());
	_stereovision_left_camera_serial = QString::fromStdString(config_left["Serial"].as<string>());
	_stereovision_left_camera_mode = config_left["Mode"].as<int>();
	_stereovision_left_extension = QString::fromStdString(config_left["Extension"].as<string>());
	
	// Left scalebar ROI
	const YAML::Node &left_scalebar_roi = config_left["ScalebarROI"];
	_stereovision_left_scalebar_roi_x = (int) ((left_scalebar_roi["x"].as<double>() / 100.0) * _stereovision_image_width);
	_stereovision_left_scalebar_roi_y = (int) ((left_scalebar_roi["y"].as<double>() / 100.0) * _stereovision_image_height);
	_stereovision_left_scalebar_roi_width = (int) ((left_scalebar_roi["width"].as<double>() / 100.0) * _stereovision_image_width);
	_stereovision_left_scalebar_roi_height = (int) ((left_scalebar_roi["height"].as<double>() / 100.0) * _stereovision_image_height);
	
	// Right camera
	const YAML::Node &config_right = config_stereovision["Right"];
	_stereovision_right_camera_name = QString::fromStdString(config_right["Name"].as<string>());
	_stereovision_right_camera_serial = QString::fromStdString(config_right["Serial"].as<string>());
	_stereovision_right_camera_mode = config_right["Mode"].as<int>();
	_stereovision_right_extension = QString::fromStdString(config_right["Extension"].as<string>());
	
	// Right scalebar ROI
	const YAML::Node &right_scalebar_roi = config_right["ScalebarROI"];
	_stereovision_right_scalebar_roi_x = (int) ((right_scalebar_roi["x"].as<double>() / 100.0) * _stereovision_image_width);
	_stereovision_right_scalebar_roi_y = (int) ((right_scalebar_roi["y"].as<double>() / 100.0) * _stereovision_image_height);
	_stereovision_right_scalebar_roi_width = (int) ((right_scalebar_roi["width"].as<double>() / 100.0) * _stereovision_image_width);
	_stereovision_right_scalebar_roi_height = (int) ((right_scalebar_roi["height"].as<double>() / 100.0) * _stereovision_image_height);
	
	const YAML::Node &config_led = config_stereovision["LED"];
	_stereovision_led_enabled = config_led["Enabled"].as<bool>();
	if (!_stereovision_led_enabled) return true;
	
	// TODO: inizializzazione dei LED
	
	return true;
}
