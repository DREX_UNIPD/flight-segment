//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		02/10/2017
//	Descrizione:		Comando per l'inizio della stereovisione
//****************************************************************************************************//

// Header del processo
#include <Process/svp.h>

using namespace std;
using namespace drex::processes;

// Inizio della stereovisione
bool SVProcess::commandStartStereovision(const Message &msg, vector<const char *> &) {
	_logger->info("Command: Start Stereovision");
	
	// Invio la risposta
	Message ans;
	ans += msg[0];
	
	if (_stereovision_running || _stereovision_trigger_timer.isActive() || _stereovision_image_check_timer.isActive()) {
		ans += "ERROR";
		ans += "Stereovision already running";
	}
	else {
		_stereovision_running = true;
		if (_state == "RUN") {
			_stereovision_trigger_timer.start(0);
		}
		
		ans += "OK";
	}
	
	_command_socket.socket->sendMessage(ans);
	
	return true;
}
