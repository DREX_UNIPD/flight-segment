//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		09/08/2017
//	Descrizione:		Comando per il salvataggio dell'immagine corrente
//****************************************************************************************************//

// Header del processo
#include <Process/svp.h>

using namespace std;
using namespace drex::processes;

// Salvataggio dell'ultima immagine acquisita
bool SVProcess::commandSaveImage(const Message &msg, vector<const char *> &parameters) {
	_logger->info("Command: Save Image");
	
	// Verifica della sintassi
	QList<QRegularExpressionMatch> matches;
	if (!verifyCommandSyntax(msg, parameters, matches)) {
		return false;
	}
	
	// Estraggo il nome della camera
	QString camera = matches[0].captured("camera");
	
	// Salvo l'immagine
	if (!saveImage(camera)) {
		_logger->error("Failed to save image");
		return false;
	}
	
	// Invio la risposta
	Message ans;
	ans += msg[0];
	ans += "OK";
	_command_socket.socket->sendMessage(ans);
	
	return true;
}
