//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		06/08/2017
//	Descrizione:		Comando di terminazione del processo
//****************************************************************************************************//

// Header del processo
#include <Process/svp.h>

using namespace std;
using namespace drex::processes;

// Chiusura del processo
bool SVProcess::commandKill(const Message &msg, vector<const char *> &) {
	_logger->info("Command: Kill");
	
	// Imposto lo stato su IDLE per terminare correttamente i processi di acquisizione
	setState(ST_IDLE);
	
	// Termino l'applicazione
	killProcess();
	
	// Invio la risposta
	Message ans;
	ans += msg[0];
	ans += "OK";
	_command_socket.socket->sendMessage(ans);
	
	return true;
}
