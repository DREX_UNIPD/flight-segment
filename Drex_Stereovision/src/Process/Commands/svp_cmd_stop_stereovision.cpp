//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		02/10/2017
//	Descrizione:		Comando per la fine della stereovisione
//****************************************************************************************************//

// Header del processo
#include <Process/svp.h>

using namespace std;
using namespace drex::processes;

// Fine della stereovisione
bool SVProcess::commandStopStereovision(const Message &msg, vector<const char *> &) {
	_logger->info("Command: Stop Stereovision");
	
	// Invio la risposta
	Message ans;
	ans += msg[0];
	
	if (!_stereovision_running) {
		ans += "ERROR";
		ans += "Stereovision not running";
	}
	else {
		_stereovision_running = false;
//		_stereovision_trigger_timer.stop();
//		_stereovision_image_check_timer.stop();
		
		ans += "OK";
	}
	
	_command_socket.socket->sendMessage(ans);
	
	return true;
}
