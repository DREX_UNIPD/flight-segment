//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		04/10/2017
//	Descrizione:		Comando per l'avvio della stereovisione su due immagini salvate su file
//****************************************************************************************************//

// Header del processo
#include <Process/svp.h>

using namespace cv;
using namespace std;
using namespace drex::processes;

// Avvio manuale della stereovisione su due immagini salvate su file
bool SVProcess::commandStereoMatch(const Message &msg, vector<const char *> &parameters) {
	_logger->info("Command: Stereo match");
	
	// Verifica della sintassi
	QList<QRegularExpressionMatch> matches;
	if (!verifyCommandSyntax(msg, parameters, matches)) {
		return false;
	}
	
	// Verifico che i file indicati esistano
	QString left_file, right_file;
	QFileInfo left_fi, right_fi;
	
	left_file = matches[0].captured("file");
	right_file = matches[1].captured("file");
	
	left_fi.setFile(left_file);
	right_fi.setFile(right_file);
	
	// Messaggio di risposta
	Message ans;
	ans += msg[0];
	
	if (!left_fi.exists()) {
		ans += "ERROR";
		ans += "Left file not found";
		_command_socket.socket->sendMessage(ans);
		return true;
	}
	if (!right_fi.exists()) {
		ans += "ERROR";
		ans += "Right file not found";
		_command_socket.socket->sendMessage(ans);
		return true;
	}
	
	Mat left_img, right_img;
	
	try {
		left_img = imread(left_file.toStdString(), CV_LOAD_IMAGE_GRAYSCALE);
		right_img = imread(right_file.toStdString(), CV_LOAD_IMAGE_GRAYSCALE);
		
		Mat points3d;
		if (svComputeDepthMap(left_img, right_img, points3d)) {
			QString record;
			
			_logger->info("Depth map successfully computed!");
			
			ans += "OK";
			
			record = QString("[") + QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss.zzz") + QString("] STEREOMATCH\n");
			
			// Estraggo i punti in 3D
			for (int i = 0; i < points3d.cols; i++) {
				QString point;
				QString original;
				
				point = QString("POINT(") + QString::number(i) + QString("): ");
				original = QString("ORIGINAL(") + QString::number(i) + QString("): ");
				
				double x, y, z, w;
				Mat col = points3d.col(i);
				w = col.at<double>(3, 0);
				
				x = col.at<double>(0, 0);
				y = col.at<double>(1, 0);
				z = col.at<double>(2, 0);
				
				original += QString("x = ") + QString::number(x);
				original += QString(", y = ") + QString::number(y);
				original += QString(", z = ") + QString::number(z);
				original += QString(", w = ") + QString::number(w);
				
				x /= w;
				y /= w;
				z /= w;
				
				point += QString("x = ") + QString::number(x);
				point += QString(", y = ") + QString::number(y);
				point += QString(", z = ") + QString::number(z);
				
				ans += point.toStdString().c_str();
				ans += original.toStdString().c_str();
				
				_logger->info(point.toStdString());
				_logger->info(original.toStdString());
				
				record += point;
				record += "\n";
				record += original;
				record += "\n";
			}
			
			// Salvataggio su file
			if (!_stereovision_file.isOpen()) {
				// Verifico se è stato creato il file
				bool exists;
				if (!svGetNextFreeIndex(_storage_depth_map_dir, _stereovision_filename, _storage_filesize_limit, _stereovision_file_index, exists)) {
					_logger->error("Failed to get next free index");
					ans += "ERROR";
					ans += "Failed to get next free index";
					_command_socket.socket->sendMessage(ans);
					return false;
				}
				
				// Imposto il nome del file
				_stereovision_file.setFileName(_storage_depth_map_dir.absoluteFilePath(_stereovision_filename + QString(".") + QString::number(_stereovision_file_index)));
				
				if (exists) {
					// Se esiste lo apro in modalità Append
					_stereovision_file.open(QIODevice::Append);
				}
				else {
					_stereovision_file.open(QIODevice::WriteOnly);
				}
			}
			
			_stereovision_file.write(record.toStdString().c_str());
			
			// Se il file supera la dimensione massima consentita lo chiudo e apro un nuovo file
			if (_stereovision_file.size() >= _storage_filesize_limit) {
				_stereovision_file.close();
				_stereovision_file_index++;
				
				// Imposto il nome del file
				_stereovision_file.setFileName(_storage_depth_map_dir.absoluteFilePath(_stereovision_filename + QString(".") + QString::number(_stereovision_file_index)));
				_stereovision_file.open(QIODevice::WriteOnly);
			}
		}
		else {
			_logger->error("Failed to compute depth map");
			ans += "ERROR";
			ans += "Failed to compute depth map";
		}
	}
	catch (Exception e) {
		_logger->error(e.what());
		ans += "ERROR";
		ans += e.what();
	}
	_command_socket.socket->sendMessage(ans);
	
	return true;
}
