//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		02/10/2017
//	Descrizione:		Comando per l'inizio della cattura continua
//****************************************************************************************************//

// Header del processo
#include <Process/svp.h>

using namespace std;
using namespace drex::processes;

// Salvataggio dell'ultima immagine acquisita
bool SVProcess::commandStartCapturing(const Message &msg, vector<const char *> &parameters) {
	_logger->info("Command: Start Capturing");
	
	// Verifica della sintassi
	QList<QRegularExpressionMatch> matches;
	if (!verifyCommandSyntax(msg, parameters, matches)) {
		return false;
	}
	
	// Estraggo il nome della camera
	QString camera = matches[0].captured("camera");
	
	// Inizio la cattura continua
	if (!startCapturing(camera)) {
		_logger->error("Failed to start capturing");
		return false;
	}
	
	// Invio la risposta
	Message ans;
	ans += msg[0];
	ans += "OK";
	_command_socket.socket->sendMessage(ans);
	
	return true;
}
