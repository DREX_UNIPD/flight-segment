//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		23/09/2017
//	Descrizione:		Pubblicazione informazioni del processo
//****************************************************************************************************//

// Header del processo
#include <Process/svp.h>

using namespace std;
using namespace drex::processes;

void SVProcess::publishingState() {
	Message msg;
	msg += "PUBLISHING/STATE";
	msg += _state.c_str();
	_data_socket.socket->sendMessage(msg);
}
