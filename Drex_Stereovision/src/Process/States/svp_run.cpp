//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		06/08/2017
//	Descrizione:		Stato Run: il processo regola la luminosità dei led, acquisisce le immagini
//						dalle stereo camere e calcola la depth map, pubblicandola
//****************************************************************************************************//

// Header del processo
#include <Process/svp.h>

using namespace std;
using namespace drex::processes;

// Inizializzazione dello stato
bool SVProcess::stateRunInit() {
	_logger->info("RUN: Init");
	
	// Individuo i prossimi slot disponibili per i trigger
	svGetNextFreeSlot(_storage_left_triggers_dir, _stereovision_left_next_trigger);
	svGetNextFreeSlot(_storage_right_triggers_dir, _stereovision_right_next_trigger);
	svGetNextFreeSlot(_storage_both_triggers_dir, _stereovision_both_next_trigger);
	
	// Individuo i prossimi slot disponibili per le immagini
	svGetNextFreeSlot(_storage_left_images_dir, _stereovision_left_next_image);
	svGetNextFreeSlot(_storage_right_images_dir, _stereovision_right_next_image);
	svGetNextFreeSlot(_storage_both_images_dir, _stereovision_both_next_image);
	
	// Faccio partire i processi
	svStartCameraAcquisition(
		// Device parameters
		_stereovision_left_camera_name,
		_stereovision_left_camera_serial,
		_stereovision_left_extension,
		_stereovision_left_camera_mode,
		_stereovision_divider,
		// Left
		_storage_left_triggers_dir,
		_stereovision_left_next_trigger,
		_storage_left_images_dir,
		_stereovision_left_next_image,
		// Both
		_storage_both_triggers_dir,
		_stereovision_both_next_trigger,
		_storage_both_images_dir,
		_stereovision_both_next_image,
		// Process
		_stereovision_left_process
	);
	
	svStartCameraAcquisition(
		// Device parameters
		_stereovision_right_camera_name,
		_stereovision_right_camera_serial,
		_stereovision_right_extension,
		_stereovision_right_camera_mode,
		_stereovision_divider,
		// Left
		_storage_right_triggers_dir,
		_stereovision_right_next_trigger,
		_storage_right_images_dir,
		_stereovision_right_next_image,
		// Both
		_storage_both_triggers_dir,
		_stereovision_both_next_trigger,
		_storage_both_images_dir,
		_stereovision_both_next_image,
		// Process
		_stereovision_right_process
	);
	
	// Collego i timer
	QObject::connect(&_stereovision_trigger_timer, &QTimer::timeout, this, &SVProcess::svStereoTriggerBoth, Qt::QueuedConnection);
	QObject::connect(&_stereovision_image_check_timer, &QTimer::timeout, this, &SVProcess::svStereoCheckBoth, Qt::QueuedConnection);
	
	QObject::connect(&_stereovision_left_publishing_timer, &QTimer::timeout, this, &SVProcess::svCheckLeftImage, Qt::QueuedConnection);
	QObject::connect(&_stereovision_right_publishing_timer, &QTimer::timeout, this, &SVProcess::svCheckRightImage, Qt::QueuedConnection);
	
	_stereovision_trigger_timer.setSingleShot(true);
	
	// Verifico se la stereovisione è abilitata
	if (_stereovision_running) {
		// Inizio l'acquisizione
		_logger->info(string("Starting trigger with delay ") + to_string(_stereovision_delay) + string(" ms"));
		_stereovision_trigger_timer.start(_stereovision_delay);
	}
	
	_stereovision_left_publishing_timer.start(_stereovision_image_check_period);
	_stereovision_right_publishing_timer.start(_stereovision_image_check_period);
	
	return true;
}

// Pulizia dello stato
bool SVProcess::stateRunCleanup() {
	_logger->info("RUN: Cleanup");
	
	// Fermo i timer se sono attivi
	if (_stereovision_trigger_timer.isActive()) _stereovision_trigger_timer.stop();
	if (_stereovision_image_check_timer.isActive()) _stereovision_image_check_timer.stop();
	
	if (_stereovision_left_publishing_timer.isActive()) _stereovision_left_publishing_timer.stop();
	if (_stereovision_right_publishing_timer.isActive()) _stereovision_right_publishing_timer.stop();
	
	// Disconnetto i timer
	QObject::disconnect(&_stereovision_trigger_timer, &QTimer::timeout, this, &SVProcess::svStereoTriggerBoth);
	QObject::disconnect(&_stereovision_image_check_timer, &QTimer::timeout, this, &SVProcess::svStereoCheckBoth);
	
	QObject::disconnect(&_stereovision_left_publishing_timer, &QTimer::timeout, this, &SVProcess::svCheckLeftImage);
	QObject::disconnect(&_stereovision_right_publishing_timer, &QTimer::timeout, this, &SVProcess::svCheckRightImage);
	
	// Termino l'acquisizione
	svTrigger(_storage_both_triggers_dir, _stereovision_both_next_trigger, "EXIT");
//	_stereovision_left_process.terminate();
//	_stereovision_right_process.terminate();
	
	return true;
}
