//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		24/09/2017
//	Descrizione:		Stereovisione
//****************************************************************************************************//

// Header del processo
#include <Process/svp.h>

// Header Aruco
#include <aruco/aruco.h>
#include <aruco/cvdrawingutils.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/calib3d/calib3d.hpp>

using namespace aruco;
using namespace cv;
using namespace std;
using namespace drex::processes;

// Trovo il prossimo trigger libero
bool SVProcess::svGetNextFreeSlot(QDir &slot_dir, int &slot_index) {
	slot_index = -1;
	
	QStringList filters;
	QStringList files;
	
	do {
		slot_index++;
		filters.clear();
		filters += QString::number(slot_index) + QString("_*");
		files = slot_dir.entryList(filters);
	}
	while (files.count() > 0);
	
	return true;
}

// Elimino tutti i file trigger specificati
bool SVProcess::svDeleteTriggers(QDir &trigger_dir) {
	_logger->info("Delete triggers");
	
	// Lista dei file
	QFileInfoList trigger_files;
	trigger_files = trigger_dir.entryInfoList();
	
	// Espressione regolare per il riconoscimento dei trigger
	QRegularExpression regex;
	regex.setPattern("(?i)^\\d+_(?:image|start|stop)$");
	
	// Individuo i file che corrispondono a trigger e li elimino
	for (int i = 0; i < trigger_files.count(); i++) {
		QFileInfo &current_trigger = trigger_files[i];
		QString filename = current_trigger.fileName();
		
		// Verifico se il file corrisponde ad un file di trigger
		QRegularExpressionMatch match;
		match = regex.match(filename);
		
		// Se non è un file di trigger non lo tocco
		if (!match.hasMatch()) continue;
		
		// Elimino il file
		QFile::remove(current_trigger.absoluteFilePath());
	}
	
	return true;
}

// Inizio l'acquisizione della camera
bool SVProcess::svStartCameraAcquisition(
		// Device parameters
		QString &name,
		QString &serial,
		QString &extension,
		int &mode,
		int &divider,
		// Left
		QDir &camera_trigger_dir,
		int &camera_trigger_index,
		QDir &camera_images_dir,
		int &camera_image_index,
		// Both
		QDir &both_trigger_dir,
		int &both_trigger_index,
		QDir &both_images_dir,
		int &both_image_index,
		// Process
		QProcess &process
	) {
	
	QString capture_process_path;
	QStringList arguments;
	
	capture_process_path = _storage_home.absoluteFilePath(_stereovision_capture_process);
	
	// Device parameters
	arguments += name;
	arguments += serial;
	arguments += extension;
	arguments += QString::number(mode);
	arguments += QString::number(divider);
	
	// Left
	arguments += camera_trigger_dir.absolutePath();
	arguments += QString::number(camera_trigger_index);
	arguments += camera_images_dir.absolutePath();
	arguments += QString::number(camera_image_index);
	
	// Both
	arguments += both_trigger_dir.absolutePath();
	arguments += QString::number(both_trigger_index);
	arguments += both_images_dir.absolutePath();
	arguments += QString::number(both_image_index);
	
	_logger->info((
		QString("Starting camera ") + name +
		QString(", serial = ") + serial +
		QString(", extension = ") + extension +
		QString(", mode = ") + QString::number(mode) +
		QString(", divider = ") + QString::number(divider) +
		QString(", trigger_index = ") + QString::number(camera_trigger_index) +
		QString(", image_index = ") + QString::number(camera_image_index)
	).toStdString());
	
	process.start(capture_process_path, arguments);
	
	return true;
}

// Creo il prossimo file trigger
bool SVProcess::svTrigger(QDir &trigger_dir, int trigger_index, QString trigger_command) {
	_logger->info(
		string("Triggering: index = ") + to_string(trigger_index) + 
		string(", dir = ") + trigger_dir.absolutePath().toStdString() + 
		string(", command = ") + trigger_command.toStdString()
	);
	
	// Creo il file di trigger
	QString filename = trigger_dir.absoluteFilePath(QString::number(trigger_index) + QString("_") + trigger_command.toUpper());
	QFile file;
	
	// Creo il file per la left camera
	file.setFileName(filename);
	if (!file.open(QIODevice::WriteOnly)) {
		_logger->error("Failed to create trigger file");
		return false;
	}
	file.close();
	
	return true;
}

void SVProcess::svStereoTriggerBoth() {
	_logger->info("[Streovision] Trigger both");
	
	if (!_stereovision_running) {
		_logger->warn("[Stereovision] Stereovision not running");
		return;
	}
	
	if (!svTrigger(_storage_both_triggers_dir, _stereovision_both_next_trigger, QString::fromStdString(SV_IMAGE))) {
		_logger->error("Failed to trigger images for stereovision");
		return;
	}
	
	_stereovision_image_check_timer.start(_stereovision_image_check_period);
}

void SVProcess::svStereoCheckBoth() {
	// Verifico l'esistenza di entrambe le immagini
	if (!svImagesExist(_storage_both_images_dir, _stereovision_left_camera_name, _stereovision_both_next_image, _stereovision_left_extension)) return;
	if (!svImagesExist(_storage_both_images_dir, _stereovision_right_camera_name, _stereovision_both_next_image, _stereovision_right_extension)) return;
	
	_logger->info("[Stereovision] Both images found");
	
	// Fermo il timer
	_stereovision_image_check_timer.stop();
	
	// Elaboro le due immagini
	QString left_image_path, right_image_path;
	left_image_path = svImagePath(_storage_both_images_dir, _stereovision_left_camera_name, _stereovision_both_next_image, _stereovision_left_extension);
	right_image_path = svImagePath(_storage_both_images_dir, _stereovision_right_camera_name, _stereovision_both_next_image, _stereovision_right_extension);
	
	Mat left_img, right_img;
	
	try {
		left_img = imread(left_image_path.toStdString(), CV_LOAD_IMAGE_GRAYSCALE);
		right_img = imread(right_image_path.toStdString(), CV_LOAD_IMAGE_GRAYSCALE);
		
		Mat points3d;
		if (svComputeDepthMap(left_img, right_img, points3d)) {
			QString record;
			
			_logger->info("Depth map successfully computed!");
			
			record = QString("[") + QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss.zzz") + QString("] STEREOMATCH\n");
			
			// Estraggo i punti in 3D
			for (int i = 0; i < points3d.cols; i++) {
				QString point;
				point = QString("POINT(") + QString::number(i) + QString("): ");
				
				double x, y, z, w;
				Mat col = points3d.col(i);
				w = col.at<double>(3, 0);
				
				x = col.at<double>(0, 0) / w;
				y = col.at<double>(1, 0) / w;
				z = col.at<double>(2, 0) / w;
				
				point += QString("x = ") + QString::number(x);
				point += QString(", y = ") + QString::number(y);
				point += QString(", z = ") + QString::number(z);
				
				_logger->info(point.toStdString());
				
				record += point;
				record += "\n";
			}
			
			// Salvataggio su file
			if (!_stereovision_file.isOpen()) {
				// Verifico se è stato creato il file
				bool exists;
				if (!svGetNextFreeIndex(_storage_depth_map_dir, _stereovision_filename, _storage_filesize_limit, _stereovision_file_index, exists)) {
					_logger->error("Failed to get next free index");
					_stereovision_both_next_trigger++;
					_stereovision_trigger_timer.start(0);
					return;
				}
				
				// Imposto il nome del file
				_stereovision_file.setFileName(_storage_depth_map_dir.absoluteFilePath(_stereovision_filename + QString(".") + QString::number(_stereovision_file_index)));
				
				if (exists) {
					// Se esiste lo apro in modalità Append
					_stereovision_file.open(QIODevice::Append);
				}
				else {
					_stereovision_file.open(QIODevice::WriteOnly);
				}
			}
			
			_stereovision_file.write(record.toStdString().c_str());
			
			// Se il file supera la dimensione massima consentita lo chiudo e apro un nuovo file
			if (_stereovision_file.size() >= _storage_filesize_limit) {
				_stereovision_file.close();
				_stereovision_file_index++;
				
				// Imposto il nome del file
				_stereovision_file.setFileName(_storage_depth_map_dir.absoluteFilePath(_stereovision_filename + QString(".") + QString::number(_stereovision_file_index)));
				_stereovision_file.open(QIODevice::WriteOnly);
			}
		}
		else {
			_logger->error("Failed to compute depth map");
		}
	}
	catch (Exception e) {
		_logger->error(e.what());
	}
	
	// Aggiorno l'indice
	_stereovision_both_next_trigger++;
	
	// Faccio partire la nuova acquisizione
	_stereovision_trigger_timer.start(0);
}

// Verifico se esistono le immagini per la pubblicazione
void SVProcess::svCheckLeftImage() {
	if (!svImagesExist(_storage_left_images_dir, _stereovision_left_camera_name, _stereovision_left_next_image, _stereovision_left_extension)) return;
	
	string filename = svImagePath(_storage_left_images_dir, _stereovision_left_camera_name, _stereovision_left_next_image, _stereovision_left_extension).toStdString();
	
	_logger->info((string("Left image found: ") + filename).c_str());
	
	Message msg;
	msg += "NEW IMAGE";
	msg += "CAMERA = LEFT";
	msg += (string("PATH = ") + filename).c_str();
	_data_socket.socket->sendMessage(msg);
	
	_stereovision_left_next_image++;
}
void SVProcess::svCheckRightImage() {
	if (!svImagesExist(_storage_right_images_dir, _stereovision_right_camera_name, _stereovision_right_next_image, _stereovision_right_extension)) return;
	
	string filename = svImagePath(_storage_right_images_dir, _stereovision_right_camera_name, _stereovision_right_next_image, _stereovision_right_extension).toStdString();
	
	_logger->info((string("Right image found: ") + filename).c_str());
	
	Message msg;
	msg += "NEW IMAGE";
	msg += "CAMERA = RIGHT";
	msg += (string("PATH = ") + filename).c_str();
	_data_socket.socket->sendMessage(msg);
	
	_stereovision_right_next_image++;
}

// Verifico se esistono i file specificati
bool SVProcess::svImagesExist(QDir &images_dir, QString name, int number, QString extension) {
	QFileInfo image_file;
	QString image_path;
	image_path = svImagePath(images_dir, name, number, extension);
	image_file.setFile(image_path);
	return image_file.exists();
}

// Costruisco il path in funzione del nome della camera, del numero dell'immagine e dell'estensione del file
QString SVProcess::svImagePath(QDir &folder, QString name, int number, QString extension) {
	QString filename;
	filename = QString::number(number) + QString("_") + name + QString(".") + extension;
	return folder.absoluteFilePath(filename);
}

// Calcolo la depth map
bool SVProcess::svComputeDepthMap(Mat &left, Mat &right, Mat &points3d) {
	_logger->info("Compute depth map");
	
	Mat P_matrix_1, P_matrix_2;
	Mat AR_1, AR_2;
	
	if (!svRototraslation(left, right, P_matrix_1, P_matrix_2)) return false;
	if (!svStereoMatch(left, right, P_matrix_1, P_matrix_2, AR_1, AR_2, points3d)) return false;
	
	return true;
}

// Calcolo la rototraslazione
bool SVProcess::svRototraslation(const Mat &Image_1, const Mat &Image_2, Mat &P_matrix_1, Mat &P_matrix_2) {
	_logger->info("Rototraslation");
	
	// Check if the images are loaded
	if (Image_1.empty()) {
		_logger->error("Image 1 empty");
		return false;
	}
	
	if (Image_2.empty()) {
		_logger->error("Image 2 empty");
		return false;
	}
	
	// Threshold
	Mat ThreshImage_1, ThreshImage_2;
	
	// Definitions of contours of the scalebar
	vector<vector<Point>> contours_1, contours_2;
	
	// Compute mask (you could use a simple threshold if the image is always as good as the one you provided)
	Mat mask_1, mask_2;
	adaptiveThreshold(Image_1, ThreshImage_1, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, 105, 1);
	erode(ThreshImage_1, mask_1, Mat(), Point(-1,-1), 3, BORDER_CONSTANT, 1);
	adaptiveThreshold(Image_2, ThreshImage_2, 255, ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, 105, 1);
	erode(ThreshImage_2, mask_2, Mat(), Point(-1,-1), 3, BORDER_CONSTANT, 1);
	
	// Regions of Interest containing the scalebars
	Mat roi_1 = mask_1(Rect(
			_stereovision_left_scalebar_roi_x,
			_stereovision_left_scalebar_roi_y,
			_stereovision_left_scalebar_roi_width,
			_stereovision_left_scalebar_roi_height
	));
	Mat roi_2 = mask_2(Rect(
			_stereovision_right_scalebar_roi_x,
			_stereovision_right_scalebar_roi_y,
			_stereovision_right_scalebar_roi_width,
			_stereovision_right_scalebar_roi_height
	));
	
	// Find contours (if always so easy to segment as your image,
	// you could just add the black/rect pixels to a vector)
	Point Offset_1 = Point(_stereovision_left_scalebar_roi_x, _stereovision_left_scalebar_roi_y);
	findContours(roi_1, contours_1, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Offset_1);
	Point Offset_2 = Point(_stereovision_right_scalebar_roi_x, _stereovision_right_scalebar_roi_y);
	findContours(roi_2, contours_2, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_SIMPLE, Offset_2);
	
	// Draws the contours and detects the largest one (if there are multiple contours in the image,
	// the largest one is taken as the wanted one). Drawing only for demonstration.
	int biggestContourIdx_1 = -1, biggestContourIdx_2 = -1;
	float biggestContourArea_1 = 0, biggestContourArea_2 = 0;
	
	// Draws contours of the scalebars.
	for(unsigned int i = 0; i < contours_1.size(); i++ ) {
		double ctArea_1 = contourArea(contours_1[i]);
		if(ctArea_1 > biggestContourArea_1) {
			biggestContourArea_1 = ctArea_1;
			biggestContourIdx_1 = i;
		}
	}
	
	for(unsigned int i = 0; i < contours_2.size(); i++ ){
		double ctArea_2 = contourArea(contours_2[i]);
		if(ctArea_2 > biggestContourArea_2){
			biggestContourArea_2 = ctArea_2;
			biggestContourIdx_2 = i;
		}
	}
	
	// Check if no contour is detected.
	if(biggestContourIdx_1 < 0 || biggestContourIdx_2 < 0) {
		_logger->warn("No scalebar contour detected, aborting");
		return false;
	}
	
	// Calculate the rotated bounding rectangle of the biggest contours.
	RotatedRect boundingBox_1 = minAreaRect(contours_1[biggestContourIdx_1]);
	RotatedRect boundingBox_2 = minAreaRect(contours_2[biggestContourIdx_2]);
	
	// NB: Calculate the EXTERNAL boundary box, in case erode/dilate can be used to define better images
	
	// Draws the rotated rectangle.
	Point2f corners_1[4];
	boundingBox_1.points(corners_1);
	
	Point2f corners_2[4];
	boundingBox_2.points(corners_2);
	
	// Coordinates in pixel of the corners of the scalebars, the origin is on the bottom left corner.
	int nimages = 2;
	
	vector<vector<Point3f>> vector_points3d;
	vector_points3d.resize(nimages);
	for(int i = 0; i < nimages; i++) {
		vector_points3d[i].push_back(Point3f(0.0f, _stereovision_scalebar_height, 0));
		vector_points3d[i].push_back(Point3f(0.0f, 0.0f, 0));
		vector_points3d[i].push_back(Point3f(_stereovision_scalebar_width, 0.0f, 0));
		vector_points3d[i].push_back(Point3f(_stereovision_scalebar_width, _stereovision_scalebar_height, 0));
	}
	
	// Coordinates in pixel of the detected 2D points of the scalebars.
	vector<vector<Point2f>> vector_points2d_1;
	vector_points2d_1.resize(nimages);
	for(int i = 0; i < nimages; i++){
		vector_points2d_1[i].push_back(corners_1[0]);
		vector_points2d_1[i].push_back(corners_1[1]);
		vector_points2d_1[i].push_back(corners_1[2]);
		vector_points2d_1[i].push_back(corners_1[3]);
	}
	
	vector<vector<Point2f>> vector_points2d_2;
	vector_points2d_2.resize(nimages);
	for(int i = 0; i < nimages; i++){
		vector_points2d_2[i].push_back(corners_2[0]);
		vector_points2d_2[i].push_back(corners_2[1]);
		vector_points2d_2[i].push_back(corners_2[2]);
		vector_points2d_2[i].push_back(corners_2[3]);
	}
	
	// Camera internals
	Mat K_matrix_1 = (
		Mat_<double>(3,3) <<
			_stereovision_camera_matrix_fx, 0., _stereovision_camera_matrix_cx,
			0., _stereovision_camera_matrix_fy, _stereovision_camera_matrix_cy,
			0., 0., 1.
	);
	Mat K_matrix_2 = (
		Mat_<double>(3,3) <<
			_stereovision_camera_matrix_fx, 0., _stereovision_camera_matrix_cx,
			0., _stereovision_camera_matrix_fy, _stereovision_camera_matrix_cy,
			0., 0., 1.
	);
	
	// Coefficienti di distorsione della lente
	Mat distCoeffs_1 = (
		Mat_<double>(5,1) <<
			_stereovision_distortion_coefficients[0],
			_stereovision_distortion_coefficients[1],
			_stereovision_distortion_coefficients[2],
			_stereovision_distortion_coefficients[3],
			_stereovision_distortion_coefficients[4]
	);
	Mat distCoeffs_2 = (
		Mat_<double>(5,1) <<
			_stereovision_distortion_coefficients[0],
			_stereovision_distortion_coefficients[1],
			_stereovision_distortion_coefficients[2],
			_stereovision_distortion_coefficients[3],
			_stereovision_distortion_coefficients[4]
	);
	
	Mat R, T, E, F;
	Mat R1, R2, Q;
	
	double rms =
		stereoCalibrate(
			vector_points3d,
			vector_points2d_1,
			vector_points2d_2,
			K_matrix_1,
			distCoeffs_1,
			K_matrix_2,
			distCoeffs_2,
			Size(_stereovision_image_width, _stereovision_image_height),
			R,
			T,
			E,
			F,
			CALIB_FIX_ASPECT_RATIO | CALIB_ZERO_TANGENT_DIST | CALIB_USE_INTRINSIC_GUESS | CALIB_FIX_INTRINSIC
	);
	_logger->info((string("RMS value = ") + to_string(rms)).c_str());
	
	stereoRectify(
		K_matrix_1,
		distCoeffs_1,
		K_matrix_2,
		distCoeffs_2,
		Size(_stereovision_image_width, _stereovision_image_height),
		R,
		T,
		R1,
		R2,
		P_matrix_1,
		P_matrix_2,
		Q,
		1
	);
	
	return true;
}

// Calcolo il match stereo degli aruco
bool SVProcess::svStereoMatch(Mat Image_1, Mat Image_2, Mat P_matrix_1, Mat P_matrix_2, Mat AR_1, Mat AR_2, Mat &points3d) {
	_logger->info("Stereo match");
	
	// Definition of Markers detection function
	MarkerDetector MDetector;
	
	// vector including information of Markers:
	// ID, coordinates in pixel of the corners and Tvec/Rvec
	vector<Marker> TheMarkers_1 ,TheMarkers_2;
	
	// input of parameters of cameras
	CameraParameters TheCameraParameters_1;
	CameraParameters TheCameraParameters_2;
	
	if (Image_1.empty()) {
		_logger->error("Image 1 empty");
		return false;
	}
	
	if (Image_2.empty()) {
		_logger->error("Image 2 empty");
		return false;
	}
	
	// Takes intrinsics of cameras
	TheCameraParameters_1.CameraMatrix = (
		Mat_<double>(3,3) <<
			_stereovision_camera_matrix_fx, 0., _stereovision_camera_matrix_cx,
			0., _stereovision_camera_matrix_fy, _stereovision_camera_matrix_cy,
			0., 0., 1.
	);
	TheCameraParameters_2.CameraMatrix = (
		Mat_<double>(3,3) <<
			_stereovision_camera_matrix_fx, 0., _stereovision_camera_matrix_cx,
			0., _stereovision_camera_matrix_fy, _stereovision_camera_matrix_cy,
			0., 0., 1.
	);
	
	if (TheCameraParameters_1.isValid()) TheCameraParameters_1.resize(Image_1.size());
	if (TheCameraParameters_2.isValid()) TheCameraParameters_2.resize(Image_1.size());
	
	// Markers dictionary
	MDetector.setDictionary(_stereovision_aruco_dictionary);
	
	// Thresholding parameters
	MDetector.setThresholdParams(7, 7);
	MDetector.setThresholdParamRange(2, 0);
	
	// Lenght of the side of the Markers (detection needs it, but its implications are not used).
	float TheMarkerSize = _stereovision_marker_size;
	
	// Detection of Markers in the images
	TheMarkers_1 = MDetector.detect(Image_1, TheCameraParameters_1, TheMarkerSize);
	TheMarkers_2 = MDetector.detect(Image_2, TheCameraParameters_2, TheMarkerSize);
	
	// Number of Markers detected in the first image
	_logger->info((string("Number of detected AR Markers in the first image = ") + to_string(TheMarkers_1.size())).c_str());
	
	// Number of Markers detected in the second image
	_logger->info((string("Number of detected AR Markers in the first image = ") + to_string(TheMarkers_2.size())).c_str());
	
	// Definition of the vectors including the centres of the Markers: they must have at the same index the Marker
	// with the same ID.
	// Goes along the first vector and for each Marker look for the one with the same ID in the second one,
	// writes in the i-th position of both vector coordinates of the centre of the Marker.
	vector <Point2f> ARcentres_1, ARcentres_2;
	for (unsigned int i = 0; i < TheMarkers_1.size(); i++) {
		for (unsigned int j = 0; j < TheMarkers_2.size(); j++) {
			if (TheMarkers_1[i].id == TheMarkers_2[j].id) {
				ARcentres_1.push_back(
					Point2f(
						(TheMarkers_1[j][0].x + TheMarkers_1[j][2].x) / 2,
						(TheMarkers_1[j][0].y + TheMarkers_1[j][2].y) / 2
					)
				);
			}
		}
	}
	for (unsigned int i = 0; i < TheMarkers_2.size(); i++) {
		for (unsigned int j = 0; j < TheMarkers_1.size(); j++) {
			if (TheMarkers_2[i].id == TheMarkers_1[j].id) {
				ARcentres_2.push_back(
					Point2f(
						(TheMarkers_2[j][0].x + TheMarkers_2[j][2].x) / 2,
						(TheMarkers_2[j][0].y + TheMarkers_2[j][2].y) / 2
					)
				);
			}
		}
	}
	
	AR_1 = Mat(ARcentres_1, CV_64FC2);
	AR_2 = Mat(ARcentres_2, CV_64FC2);
	
//	cout << "AR Markers in the first image =" << endl << AR_1 << endl
//	<< "AR Markers in the second image =" << endl << AR_2 << endl;
	
	// 3D points vector
	points3d = Mat(4, TheMarkers_1.size(), CV_64FC4);
	
	triangulatePoints(P_matrix_1, P_matrix_2, AR_1, AR_2, points3d);
	//cout << "3D points = " << endl << points3d << endl;
	
	return true;
}

// Metodi per il salvataggio dei dati
bool SVProcess::svGetNextFreeIndex(QDir &folder, QString &filename, qint64 &size_limit, int &index, bool &file_exists) {
	QStringList filters;
	filters += filename + QString(".*");
	
	QFileInfoList files;
	files = folder.entryInfoList(filters);
	
	// Non esistono file a cui collegarsi
	if (files.count() == 0) {
		index = 0;
		file_exists = false;
		return true;
	}
	
	// Trovo l'indice massimo
	QRegularExpression regex;
	QRegularExpressionMatch match;
	int max_index = 0;
	QFileInfo max_fi;
	regex.setPattern(QString("^") + filename + QString("\\.(?<index>\\d+)$"));
	for (int i = 0; i < files.count(); i++) {
		match = regex.match(files[i].fileName());
		if (!match.hasMatch()) continue;
		int temp;
		temp = match.captured("index").toInt();
		if (max_index < temp) {
			max_fi = files[i];
			max_index = temp;
		}
	}
	
	if (max_fi.size() >= size_limit) {
		index = max_index + 1;
		file_exists = false;
	}
	else {
		index = max_index;
		file_exists = true;
	}
	
	return true;
}
