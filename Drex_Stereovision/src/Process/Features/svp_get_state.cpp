//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		06/08/2017
//	Descrizione:		Richiesta dello stato attuale
//****************************************************************************************************//

// Header del processo
#include <Process/svp.h>

using namespace std;
using namespace drex::processes;
using namespace drex::state;

string SVProcess::getState() {
	_logger->info("Get state");
	
	return _state;
}
