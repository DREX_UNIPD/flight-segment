//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		09/08/2017
//	Descrizione:		Salvataggio dell'immagine corrente
//****************************************************************************************************//

// Header del processo
#include <Process/svp.h>

using namespace std;
using namespace drex::processes;

bool SVProcess::saveImage(QString camera) {
	if (camera.toUpper() == "LEFT") {
		if (!svTrigger(_storage_left_triggers_dir, _stereovision_left_next_trigger, QString::fromStdString(SV_IMAGE))) {
			_logger->error("Failed to save Left image");
			return false;
		}
		
		_stereovision_left_next_trigger++;
		
		return true;
	}
	
	if (camera.toUpper() == "RIGHT") {
		if (!svTrigger(_storage_right_triggers_dir, _stereovision_right_next_trigger, QString::fromStdString(SV_IMAGE))) {
			_logger->error("Failed to save Right image");
			return false;
		}
		
		_stereovision_right_next_trigger++;
		
		return true;
	}
	
	return false;
//	vector<int> compression_params;
//	compression_params.push_back(CV_IMWRITE_PNG_COMPRESSION);
//	compression_params.push_back(9);
	
//	// Salvo l'immagine sinistra
//	if (!_left_image.empty()) {
//		imwrite(path + string(".left.png"), _left_image, compression_params);
//	}
//	if (!_right_image.empty()) {
//		imwrite(path + string(".right.png"), _right_image, compression_params);
//	}
}
