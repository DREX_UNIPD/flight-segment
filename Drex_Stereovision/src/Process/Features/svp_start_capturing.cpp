//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		02/10/2017
//	Descrizione:		Inizio cattura continua delle immagini
//****************************************************************************************************//

// Header del processo
#include <Process/svp.h>

using namespace std;
using namespace drex::processes;

bool SVProcess::startCapturing(QString camera) {
	if (camera.toUpper() == "LEFT") {
		if (!svTrigger(_storage_left_triggers_dir, _stereovision_left_next_trigger, QString::fromStdString(SV_START))) {
			_logger->error("Failed to start Left camera capturing");
			return false;
		}
		
		_stereovision_left_next_trigger++;
		
		return true;
	}
	
	if (camera.toUpper() == "RIGHT") {
		if (!svTrigger(_storage_right_triggers_dir, _stereovision_right_next_trigger, QString::fromStdString(SV_START))) {
			_logger->error("Failed to start Right camera capturing");
			return false;
		}
		
		_stereovision_right_next_trigger++;
		
		return true;
	}
	
	return false;
}
