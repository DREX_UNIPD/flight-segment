//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		06/08/2017
//	Descrizione:		Impostazione dello stato
//****************************************************************************************************//

// Header del processo
#include <Process/svp.h>

using namespace std;
using namespace drex::processes;

bool SVProcess::setState(string newstate) {
	_logger->info("Set state");
	
	string state;
	
	// Salvo lo stato in caso di errori
	state = getState();
	
	try {
		// Richiamo la funzione di clean-up dello stato corrente
		if (!_state.empty()) _state_objects[_state]->cleanup();
	}
	catch (...) {
		// Ripristino lo stato salvato
		return false;
	}
	
	_state = newstate;
	
	// Richiamo la funzione di inizializzazione del nuovo stato
	try {
		_state_objects[_state]->init();
	}
	catch (...) {
		// Cerco di ripristinare lo stato precedente
		_state = state;
		_state_objects[_state]->init();
		return false;
	}
	
	return true;
}
