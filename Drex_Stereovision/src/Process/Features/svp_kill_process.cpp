//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		06/08/2017
//	Descrizione:		Terminazione del processo
//****************************************************************************************************//

// Header del processo
#include <Process/svp.h>

using namespace drex::processes;

// Metodo per la terminazione del processo
void SVProcess::killProcess() {
	_logger->info("Kill process");
	
	// Chiudo i processi di acquisizione se sono aperti
	if (_stereovision_left_process.state() != QProcess::ProcessState::NotRunning) _stereovision_left_process.terminate();
	if (_stereovision_right_process.state() != QProcess::ProcessState::NotRunning) _stereovision_right_process.terminate();
	
	// Mando il segnale di chiusura del processo
	QTimer::singleShot(0, this, &SVProcess::quit);
}
