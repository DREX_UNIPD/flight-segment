//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		06/08/2017
//	Descrizione:		Main - (SVP) Stereovision Process
//****************************************************************************************************//

// Header DREX
#include <Process/svp.h>

// Header per i segnali di sistema
#include <signal.h>
#include <sys/socket.h>

// Namespaces
using namespace std;
using namespace drex::processes;

//**************************************************//

// Logger

shared_ptr<spdlog::logger> console;

// Variabili per la gestione dei segnali di sistema

int sig_hup_fd[2];
int sig_int_fd[2];
int sig_term_fd[2];

// Gestori dei segnali di sistema

void hupSignalHandler(int unused);
void intSignalHandler(int unused);
void termSignalHandler(int unused);

void signalHandlersInstall() {
	struct sigaction sa_hup, sa_term, sa_int;
	
	// HUP
	
	sa_hup.sa_handler = hupSignalHandler;
	sigemptyset(&sa_hup.sa_mask);
	sa_hup.sa_flags = 0;
	sa_hup.sa_flags |= SA_RESTART;
	
	if (sigaction(SIGHUP, &sa_hup, 0)) {
		console->error("Cannot install signal handler for HUP");
		exit(EXIT_FAILURE);
	}
	
	// INT
	
	sa_int.sa_handler = intSignalHandler;
	sigemptyset(&sa_int.sa_mask);
	sa_int.sa_flags = 0;
	sa_int.sa_flags |= SA_RESTART;
	
	if (sigaction(SIGINT, &sa_int, 0)) {
		console->error("Cannot install signal handler for INT");
		exit(EXIT_FAILURE);
	}
	
	// TERM
	
	sa_term.sa_handler = termSignalHandler;
	sigemptyset(&sa_term.sa_mask);
	sa_term.sa_flags |= SA_RESTART;
	
	if (sigaction(SIGTERM, &sa_term, 0)) {
		console->error("Cannot install signal handler for TERM");
		exit(EXIT_FAILURE);
	}
}

void hupSignalHandler(int) {
	char a = 1;
	::write(sig_hup_fd[0], &a, sizeof(a));
}

void intSignalHandler(int) {
	char a = 1;
	::write(sig_int_fd[0], &a, sizeof(a));
}

void termSignalHandler(int) {
	char a = 1;
	::write(sig_term_fd[0], &a, sizeof(a));
}

// Punto di ingresso
int main(int argc, char **argv) {
	// Creo l'oggetto del processo
	SVProcess process(argc, argv);
	
	// Logger
	console = spdlog::get("console");
	
	// Installo i gestori dei segnali di sistema
	console->info("Installing signal handlers");
	
	process.setupSignalHandlers(sig_hup_fd, sig_int_fd, sig_term_fd);
	
	signalHandlersInstall();
	
	// Installo i gestori dei segnali di sistema
	console->info("Signal handlers installed");
	
	// Avvio il processo
	int retval = process.exec();
	
	// Flusho il logger
	console->flush();
	
	return retval;
}
