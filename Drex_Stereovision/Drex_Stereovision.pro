# Qt
QT += core
QT -= gui

# Config
CONFIG -= app_bundle
CONFIG += c++11
CONFIG += console
CONFIG += thread

# Target
TARGET = Drex_Stereovision

# Template
TEMPLATE = app

SOURCES += \
	src/main.cpp \
	src/Process/svp_command_selector.cpp \
	src/Process/svp_constants.cpp \
	src/Process/svp_constructor.cpp \
	src/Process/svp_destructor.cpp \
	src/Process/svp_load_configuration.cpp \
	src/Process/svp_notify.cpp \
	src/Process/svp_sys_signals.cpp \
	src/Process/svp_verify_command_syntax.cpp \
	src/Process/svp_socket_management.cpp \
	src/Process/svp_publish_log.cpp \
	src/Process/svp_publishing.cpp \
	src/Process/svp_stereovision.cpp \
	src/Process/Inits/svp_init.cpp \
	src/Process/Inits/svp_init_states_map.cpp \
	src/Process/Inits/svp_init_networking.cpp \
	src/Process/Inits/svp_init_file_logger.cpp \
	src/Process/Inits/svp_init_storage.cpp \
	src/Process/Inits/svp_init_publishing.cpp \
	src/Process/Inits/svp_init_stereovision.cpp \
	src/Process/Features/svp_kill_process.cpp \
	src/Process/Features/svp_get_state.cpp \
	src/Process/Features/svp_set_state.cpp \
	src/Process/Features/svp_save_image.cpp \
	src/Process/Features/svp_start_capturing.cpp \
	src/Process/Features/svp_stop_capturing.cpp \
	src/Process/States/svp_idle.cpp \
	src/Process/States/svp_run.cpp \
	../Drex/src/State/state_object.cpp \
	../Drex/src/PublisherSink/publisher_sink.cpp \
	src/Process/Commands/svp_cmd_kill.cpp \
	src/Process/Commands/svp_cmd_set_state.cpp \
	src/Process/Commands/svp_cmd_get_state.cpp \
	src/Process/Commands/svp_cmd_save_image.cpp \
	src/Process/Commands/svp_cmd_start_capturing.cpp \
	src/Process/Commands/svp_cmd_stop_capturing.cpp \
	src/Process/Commands/svp_cmd_start_stereovision.cpp \
	src/Process/Commands/svp_cmd_stop_stereovision.cpp \
	src/Process/Commands/svp_cmd_stereomatch.cpp \
    src/Process/Commands/svp_cmd_help.cpp

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# Include path
INCLUDEPATH += \
	include \
	../ \
	../nzmqt/include \
	../Drex/include

# Altri file
DISTFILES += \
	config/svp.yaml \
	init_scripts/svp.sh \
	../Drex/config/common.yaml

# Pacchetti
CONFIG += link_pkgconfig
PKGCONFIG += libzmq

# Librerie
LIBS += /usr/local/lib/libyaml-cpp.a
LIBS += /usr/local/lib/libaruco.so
LIBS += -L/usr/local/lib \
	-lopencv_core \
	-lopencv_highgui \
	-lopencv_imgcodecs \
	-lopencv_imgproc \
	-lopencv_calib3d

HEADERS += \
	include/Process/svp.h \
	../nzmqt/include/nzmqt/nzmqt.hpp \
	../Drex/include/State/state_object.h \
	../Drex/include/Socket/socket_utils.h \
	../Drex/include/PublisherSink/publisher_sink.h
