//****************************************************************************************************//
//	Team:				Drex
//	Autore:				Loris Bogo
//	Data creazione:		06/08/17
//	Descrizione:		Classe per processo SVP
//****************************************************************************************************//

#ifndef SVP_PROCESS_H
#define SVP_PROCESS_H

// Header per logging
#include <spdlog/spdlog.h>

// Header nzmqt
#include <nzmqt/nzmqt.hpp>

// Header Qt
#include <QCoreApplication>
#include <QMap>

// Header YAML
#include <yaml-cpp/yaml.h>

// Header DREX
#include <State/state_object.h>
#include <Socket/socket_utils.h>

// Header OpenCV
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>

//----------------------------------------------------//

namespace drex {
namespace processes {

// Classe del processo SVP
class SVProcess : public QCoreApplication {
	Q_OBJECT
	
	using super = QCoreApplication;
	using Message = QList<QByteArray>;
	using Logger = spdlog::logger;
	
private:
	//*****************//
	// Sezione privata
	//*****************//
	
	// // Variabili
	
	// Logging
	
	// Logger su console
	std::shared_ptr<Logger> _console;
	
	// Logger su file e console
	std::shared_ptr<Logger> _logger;
	
	// Configurazione
	
	// Configurazione del processo
	YAML::Node _configuration;
	
	// Storage
	
	QDir _storage_home;
	QDir _storage_ssd_dir;
//	QDir _storage_ram_dir;
	QDir _storage_left_triggers_dir;
	QDir _storage_left_images_dir;
	QDir _storage_right_triggers_dir;
	QDir _storage_right_images_dir;
	QDir _storage_both_triggers_dir;
	QDir _storage_both_images_dir;
	QDir _storage_depth_map_dir;
	QDir _storage_log_dir;
	
	qint64 _storage_filesize_limit;
	
	// Socket
	
	// Contesto per i socket
	nzmqt::ZMQContext *_context;
	
	// Command (ROUTER)
	drex::socket::SocketInfo _command_socket;
	// Logger (PUBLISHER)
	drex::socket::SocketInfo _logger_socket;
	// Data (PUBLISHER)
	drex::socket::SocketInfo _data_socket;
	
	// Gestione degli stati
	
	// Stato
	std::string _state;
	
	// Timer per la pubblicazione dello stato
	bool _publishing_enabled;
	
	// Pubblicazione dello stato
	bool _publishing_state_enabled;
	int _publishing_state_period;
	QTimer _publishing_state_timer;
	
	// Comandi
	QMap<std::string, drex::state::StateObject *> _state_objects;
	
	// Stereovisione
	bool _stereovision_enabled;
	int _stereovision_divider;
	bool _stereovision_running;
	QTimer _stereovision_trigger_timer;
	QTimer _stereovision_image_check_timer;
	int _stereovision_image_check_period;
//	int _stereovision_trigger_uptime;
//	int _stereovision_trigger_wait_before_acquisition;
	int _stereovision_delay;
	QString _stereovision_capture_process;
	
	int _stereovision_left_next_image;
	int _stereovision_left_next_trigger;
	QString _stereovision_left_camera_name;
	QString _stereovision_left_camera_serial;
	int _stereovision_left_camera_mode;
	QString _stereovision_left_signal;
	QString _stereovision_left_extension;
	QProcess _stereovision_left_process;
	
	int _stereovision_left_scalebar_roi_x;
	int _stereovision_left_scalebar_roi_y;
	int _stereovision_left_scalebar_roi_width;
	int _stereovision_left_scalebar_roi_height;
	
	QTimer _stereovision_left_publishing_timer;
	
	int _stereovision_right_next_image;
	int _stereovision_right_next_trigger;
	QString _stereovision_right_camera_name;
	QString _stereovision_right_camera_serial;
	int _stereovision_right_camera_mode;
	QString _stereovision_right_signal;
	QString _stereovision_right_extension;
	QProcess _stereovision_right_process;
	
	int _stereovision_right_scalebar_roi_x;
	int _stereovision_right_scalebar_roi_y;
	int _stereovision_right_scalebar_roi_width;
	int _stereovision_right_scalebar_roi_height;
	
	QTimer _stereovision_right_publishing_timer;
	
	int _stereovision_both_next_image;
	int _stereovision_both_next_trigger;
	
	QFile _stereovision_file;
	QString _stereovision_filename;
	int _stereovision_file_index;
	
	// Parametri ottici delle telecamere
	double _stereovision_camera_matrix_fx;
	double _stereovision_camera_matrix_fy;
	double _stereovision_camera_matrix_cx;
	double _stereovision_camera_matrix_cy;
	
	// Aruco
	std::string _stereovision_aruco_dictionary;
	float _stereovision_marker_size;
	
	// Coefficienti di distorsione
	std::vector<double> _stereovision_distortion_coefficients;
	
	// Dimensioni dell'immagine
	int _stereovision_image_width;
	int _stereovision_image_height;
	
	// Dimensioni della scalebar
	float _stereovision_scalebar_width;
	float _stereovision_scalebar_height;
	
	// Metodi per la stereovisione
	
	// Inizializzo una camera
	bool svStartCameraAcquisition(// Device parameters
		QString &name,
		QString &serial,
		QString &extension,
		int &mode,
		int &divider,
		// Left
		QDir &camera_trigger_dir,
		int &camera_trigger_index,
		QDir &camera_images_dir,
		int &camera_image_index,
		// Both
		QDir &both_trigger_dir,
		int &both_trigger_index,
		QDir &both_images_dir,
		int &both_image_index,
		// Process
		QProcess &process
	);
	
	// Individuo il prossimo slot libero
	bool svGetNextFreeSlot(QDir &slot_dir, int &slot_index);
	
	// Elimino i trigger esistenti
	bool svDeleteTriggers(QDir &trigger_dir);
	
	// Acquisizione della prossima immagine
	bool svTrigger(QDir &trigger_dir, int trigger_index, QString trigger_command);
	
	// Callback per i timer della stereo
	void svStereoTriggerBoth();
	void svStereoCheckBoth();
	
	// Verifica se le immagini sono state acquisite
	void svCheckRightImage();
	void svCheckLeftImage();
	
	// Verifica se le immagini esistono
	bool svImagesExist(QDir &images_dir, QString name, int number, QString extension);
	
	// Percorso completo dell'immagine
	QString svImagePath(QDir &img_dir, QString name, int number, QString extension);
	
	// Calcolo della depth map
	bool svComputeDepthMap(cv::Mat &left, cv::Mat &right, cv::Mat &points3d);
	bool svRototraslation(const cv::Mat &Image_1, const cv::Mat &Image_2, cv::Mat &P_matrix_1, cv::Mat &P_matrix_2);
	bool svStereoMatch(cv::Mat Image_1, cv::Mat Image_2, cv::Mat P_matrix_1, cv::Mat P_matrix_2, cv::Mat AR_1, cv::Mat AR_2, cv::Mat &points3d);
	
	// LED
	bool _stereovision_led_enabled;
	
	// Metodi per il salvataggio dei dati
	bool svGetNextFreeIndex(QDir &folder, QString &filename, qint64 &size_limit, int &index, bool &file_exists);
	
	// // Metodi
	
	// Caricamento della configurazione
	bool loadConfiguration(QStringList &paths, YAML::Node &destination);
	
	// Variabili per i segnali di sistema
	QSocketNotifier *_sn_hup;
	QSocketNotifier *_sn_int;
	QSocketNotifier *_sn_term;
	int *_sig_hup_fd;
	int *_sig_int_fd;
	int *_sig_term_fd;
	
	//**********//
	// Features
	//**********//
	
	// Metodo per la terminazione del processo
	void killProcess();
	
	// Metodi per la gestione dello stato
	std::string getState();
	bool setState(std::string newstate);
	
	// Metodo per il salvataggio dell'immagine corrente
	bool saveImage(QString camera);
	
	// Metodo per l'inizio della cattura continua
	bool startCapturing(QString camera);
	bool stopCapturing(QString camera);
	
	//******************//
	// Metodi ausiliari
	//******************//
	
	// Verifica della sintassi del messaggio
	bool checkMessageSyntax(const Message &msg, const QStringList &syntax, QList<QRegularExpressionMatch> &matches);
	
	// Verifica della sintassi del comando ed eventuale risposta
	bool verifyCommandSyntax(const Message &msg, const std::vector<const char *> &syntax, QList<QRegularExpressionMatch> &match);
	
	// Inizializzazioni
	
	// Metodo principale
	void init();
	
	// Inizializzazione dello storage
	bool initStorage();
	
	// Inizializzazione del logger su file
	bool initFileLogger();
	
	// Inizializzazione dell'interfaccia di rete
	bool initNetworking();
	
	// Inizializzazione degli stati
	bool initStatesMap();
	
	// Inizializzazione della streovisione
	bool initStereovision();
	
	// Inizializzazione della pubblicazione
	bool initPublishing();
	
	// Inizializzazione dei socket
	bool initSocket(drex::socket::SocketInfo &si, const YAML::Node &node, std::string name, int linger);
	bool startSocket(drex::socket::SocketInfo &si);
	
	//**********************//
	// Metodi per gli stati
	//**********************//
	
	// IDLE
	bool stateIdleInit();
	bool stateIdleCleanup();
	
	// RUN
	bool stateRunInit();
	bool stateRunCleanup();
	
private slots:
	//**********************//
	// Selettore di comandi
	//**********************//
	
	// Seleziona ed esegue il comando appropriato
	void commandSelector(const Message &msg);
	
	//******************//
	// Comandi standard
	//******************//
	
	// Aiuto
	bool commandHelp(const Message &msg, std::vector<const char *> &parameters);
	
	// Chiusura del processo
	bool commandKill(const Message &msg, std::vector<const char *> &parameters);
	
	// Lettura stato del processo
	bool commandGetState(const Message &msg, std::vector<const char *> &parameters);
	
	// Impostazione stato del processo
	bool commandSetState(const Message &msg, std::vector<const char *> &parameters);
	
	//*******************//
	// Comandi specifici
	//*******************//
	
	// Salvataggio dell'immagine corrente
	bool commandSaveImage(const Message &msg, std::vector<const char *> &parameters);
	
	// Inizio salvataggio continuo
	bool commandStartCapturing(const Message &msg, std::vector<const char *> &parameters);
	
	// Fine salvataggio continuo
	bool commandStopCapturing(const Message &msg, std::vector<const char *> &parameters);
	
	// Inizio stereovisione
	bool commandStartStereovision(const Message &msg, std::vector<const char *> &parameters);
	
	// Fine stereovisione
	bool commandStopStereovision(const Message &msg, std::vector<const char *> &parameters);
	
	// Stereovisione manuale
	bool commandStereoMatch(const Message &msg, std::vector<const char *> &parameters);
	
	//***************//
	// Pubblicazione
	//***************//
	
	// Pubblicazione dello stato
	void publishingState();
	
public slots:
	// Gestori dei segnali di sistema di Qt
	void handleSigHup();
	void handleSigInt();
	void handleSigTerm();
	
	// Pubblicazione di un messaggio di Log
	void publishLog(const Message &msg);
	
public:
	//******************//
	// Sezione pubblica
	//******************//
	
	// // Costanti
	
	// Nome del processo
	static const std::string SHORT_NAME;
	static const std::string FULL_NAME;
	
	// Comandi
	static const std::string CMD_HELP;
	static const std::string CMD_KILL;
	static const std::string CMD_GET_STATE;
	static const std::string CMD_SET_STATE;
	
	static const std::string CMD_SAVE_IMAGE;
	static const std::string CMD_START_CAPTURING;
	static const std::string CMD_STOP_CAPTURING;
	static const std::string CMD_START_STEREOVISION;
	static const std::string CMD_STOP_STEREOVISION;
	static const std::string CMD_STEREOMATCH;
	
	// Stati
	static const std::string ST_DEFAULT;
	static const std::string ST_IDLE;
	static const std::string ST_RUN;
	
	// Comandi per il processo di stereovisione
	static const std::string SV_IMAGE;
	static const std::string SV_START;
	static const std::string SV_STOP;
	
	// // Metodi
	
	// Costruttore
	explicit SVProcess(int &argc, char **argv);
	
	// Distruttore
	~SVProcess() override;
	
	// Notifica evento
	bool notify(QObject *obj, QEvent *event) override;
	
	// Installazione signal handlers
	void setupSignalHandlers(int *p_hup, int *p_int, int *p_term);
};

}
}

#endif // SVP_PROCESS_H

